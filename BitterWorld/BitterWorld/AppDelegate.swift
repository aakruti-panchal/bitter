		//
//  AppDelegate.swift
//  BitterWorld
//
//  Created by  " " on 10/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import TwitterKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import UserNotifications
import Google
import FirebaseCore

// test
@UIApplicationMain
@objcMembers
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    

    var window: UIWindow?
    @objc public static var shared : AppDelegate! {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        _ = AppUtility.shared

       // FIRApp.configure()
        
        // Google Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "1060231126498-srtvfcht35vugvd70cbpm5rg5ln6l3fc.apps.googleusercontent.com"

        GMSServices.provideAPIKey(K.GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(K.GOOGLE_API_KEY)
        
        //Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FBSDKSettings.setAppID(K.FACEBOOK_KEY)
        
        
         //Twitter
         TWTRTwitter.sharedInstance().start(withConsumerKey: K.TWITTER_CONSUMER_KEY, consumerSecret: K.TWITTER_CONSUMER_SECRET)
        
        
        
        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    //MARK: - Open URL
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if (url.absoluteString.hasPrefix("twitterkit")) {
            return TWTRTwitter.sharedInstance().application(application, open: url, options: [:])
        }
        else if (url.absoluteString.hasPrefix("fb")) {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
//        else if (url.absoluteString.hasPrefix("fb")){
//            return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
//        }
        return true
    }
    
    
    func application(_ application:UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        print("called")
        let directedByTWTR =  TWTRTwitter.sharedInstance().application(application, open: url, options: options)
        let directedByGGL =  GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        let directedByFB = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, options: options)

        return directedByTWTR || directedByGGL || directedByFB
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

   

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

