//
//  ChatMessageViewController.swift
//  BitterWorld
//
//  Created by  " " on 08/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper
import MobileCoreServices
import AVKit
import AVFoundation
import AlamofireImage

enum AnimationState {
    case close
    case semiOpen
    case open
    case changed
}

let leftPadding:CGFloat = 85.0
let rightPadding:CGFloat = 50.0
let topPadding:CGFloat = 20.0
let bottomPadding:CGFloat = 40.0
let horizontalPadding = leftPadding + rightPadding
let verticalPadding = topPadding + bottomPadding

enum Action : Int {
   case MUTE = 1
   case BLOCK = 2
    
}



class ChatMessageViewController:UIViewController,UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    //MARK: - IBOutlet
    @IBOutlet weak var viewContainer        :UIView!
    
    @IBOutlet weak var tblChatList              :UITableView!
    
    @IBOutlet weak var rightView                :UIView!
    @IBOutlet weak var btnAttachment            :UIButton!
    
    @IBOutlet weak var leftView                 :UIView!
    @IBOutlet weak var btnSend                  :UIButton!
    
    @IBOutlet weak var viewMediaOption          :UIView!
    @IBOutlet weak var viewMediaContainer       :UIView!
    @IBOutlet weak var imgCaptureIcon           :UIImageView!
    @IBOutlet weak var imgAudioIcon             :UIImageView!
    @IBOutlet weak var imgVideoIcon             :UIImageView!
    
    // Animation for Conversion Menu.
    @IBOutlet weak var viewConversionMenu       :UIView!
    @IBOutlet weak var viewTblConversions       :UIView!
    @IBOutlet weak var tblConversionList        :UITableView!
    
    @IBOutlet weak var viewMenuHandle           :UIView!
    @IBOutlet weak var viewMenuVerticalSeparator:UIView!
    @IBOutlet weak var imgMenuArrow            :UIImageView!
    
    @IBOutlet weak var imgCurrentChatIndicator  :UIImageView!
    
    @IBOutlet var barBtnDelete: UIButton!
    @IBOutlet var barBtnMenu: UIButton!
    
    @IBOutlet var viewInputContainer: UIView!
    @IBOutlet var growingTextView: RSKGrowingTextView!
    
    
    
    //MARK: - Class Variables
    let loginUser = User.loggedInUser()
    
    var isOpenFirstTime         : Bool! = true
    
    var selectedUserId           : Int! = 0
    var selectedUserName         : String! = ""
    
    var selectedConversionId     : Int = 0
    var chatConversionType       : String = "1"
    var isGroupMember            : Int = 0
    
    var selectedChatList         : ChatList?
    
    var isAttachmentOptionShow   : Bool = false
    var isAttachmentThere        : Bool = false
    var attachmentImage          : UIImage? = nil
    var attachmentVideoFilePath  : NSString! = ""
    var attachmentAudioFilePath  : NSString! = ""
    var attachmentType           : MessageType! = .image
    
    var isEnableMessageSelection : Bool = false
    var arrSelectedMessage       : [String]! = []
    
    var isCallingGetMessage      : Bool = false
    var arrChatMessages          : [ChatMessage]! = []
    
    var minChatId                : Int! = 0
    var maxChatId                : Int! = 0
    
    var getMessageCallingCounter : Int = 0
    var fetchMessageTask         : DispatchWorkItem?
    var viewbottomHeight         : CGFloat = 0
    
    var arrConversionList        : Array<ChatList>! = []
    var selectedConversionIndex  : Int! = -1
    var prevSelectedConversionIndex  : Int! = -1
    var lastYOffset              : CGFloat = 0.0
    
    var animationState           : AnimationState! = .semiOpen
    var avPlayerViewController   : AVPlayerViewController?
    
    private var isVisibleKeyboard = true
    var keyboardHeight: CGFloat = 0.0
    var isMuted                  : Int = 0
    var isReported               : Int = 0
    var isBlocked               : Int = 0
    
    var menuOptionImageNameArray : [String] = []
    var menuOptionNameArray : [String] = []
    
    
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add Input view
        configureInputBar()
        self.setupLayout()
        self.setupActions()
        self.viewContainer.isHidden = false
        // Get Chat Messages
        self.getChatMessages(0, isRecent: true, showLoading: true, resetMessageList: true)
        // call webservice when from Notification and Connections List and Profile
        if arrConversionList.count == 0 {
            self.getChatConversion()
        }
        self.barBtnDelete.isHidden = true
        //self.navigationItem.rightBarButtonItem = nil
        self.viewInputContainer.y = tblChatList.bottom - viewInputContainer.height - 20
        
        self.addLogo()
        
        AppUtility.shared.isMenuPadding = true
        
        let config = FTConfiguration.shared
        config.menuWidth = 200
        config.menuSeparatorColor = UIColor.clear
        config.textAlignment = .left
        //config.textFont = UIFont.systemFont(ofSize: 13)
        config.menuRowHeight = 40
        //config.cornerRadius = 6
        
        
     
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Perform Action According to State
        if arrConversionList.count > 0 {
            self.animationState = .semiOpen
            self.viewConversionMenu.isHidden = false
        }
        else {
            self.animationState = .close
            self.viewConversionMenu.isHidden = true
        }
        self.performActionAccording(self.animationState)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        fetchMessageTask?.cancel()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterForKeyboardNotifications()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Comment Input view
    func configureInputBar() {
        growingTextView.heightChangeUserActionsBlock = { (oldHeight, newHeight) in
            self.viewInputContainer.height = self.growingTextView.height + 14
            self.viewInputContainer.y = self.view.height -  self.viewInputContainer.height - self.keyboardHeight
        }
    }
    
    private func adjustContent(for keyboardRect: CGRect) {
        let keyboardYPosition = self.isVisibleKeyboard ? keyboardRect.height : 0.0;
        tblChatList.contentInset.bottom = keyboardYPosition
        self.viewInputContainer.y = self.view.height - self.viewInputContainer.height - keyboardYPosition
    }
    
    func registerForKeyboardNotifications() {
        self.rsk_subscribeKeyboardWith(beforeWillShowOrHideAnimation: nil,
                                       willShowOrHideAnimation: { [unowned self] (keyboardRectEnd, duration, isShowing) -> Void in
                                        self.isVisibleKeyboard = isShowing
                                        self.keyboardHeight = keyboardRectEnd.height
                                        self.adjustContent(for: keyboardRectEnd)
            }, onComplete: { (finished, isShown) -> Void in
                self.isVisibleKeyboard = isShown
        }
        )
        
        self.rsk_subscribeKeyboard(willChangeFrameAnimation: { [unowned self] (keyboardRectEnd, duration) -> Void in
            self.keyboardHeight = keyboardRectEnd.height
            self.adjustContent(for: keyboardRectEnd)
            }, onComplete: nil)
    }
    
    func unregisterForKeyboardNotifications() {
        self.rsk_unsubscribeKeyboard()
    }
    
    //MARK: - Setup Controller
    func setupLayout() -> Void {
        // Set Corner Radius
        self.viewContainer.isHidden = false
        self.viewContainer.setCornerRadius(radius: 5)
        
        self.viewMediaOption.isHidden = true
    }
    
    func setupActions() -> Void {
        
        // Setup Pangesture on View for Show/Hide Conversaion List.
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanOnView(_:)))
        panGesture.delegate = self
        self.view.addGestureRecognizer(panGesture)
        
        // Set Tap Gesture to Handler
        self.imgMenuArrow.addTapGesture { (gesture) in
            if self.animationState == .close {
                self.animationState = .semiOpen
            }
            else if self.animationState == .semiOpen {
                self.animationState = .open
            }
            else if self.animationState == .open {
                self.animationState = .close
            }
            self.performActionAccording(self.animationState)
        }
        
        
        // Add Tap Gesture for View Actions
        self.imgCaptureIcon.addTapGesture { (gesture) in
            self.isAttachmentOptionShow = !self.isAttachmentOptionShow
            self.viewMediaOption.isHidden = !self.isAttachmentOptionShow
            
            UIAlertController.showActionSheet(in: self, withTitle: nil, message: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: ["Capture Image", "Select From Gallery"], popoverPresentationControllerBlock: { (popover) in
                popover.sourceView = self.imgCaptureIcon
                popover.sourceRect = self.imgCaptureIcon.frame
            }, tap: { (alert, action, index) in
                if index == 2 {
                    self.showCameraForImage()
                }
                else if index == 3 {
                    self.openGalleryView()
                }
            })
        }
        
        self.imgAudioIcon.addTapGesture { (gesture) in
            self.isAttachmentOptionShow = !self.isAttachmentOptionShow
            self.viewMediaOption.isHidden = !self.isAttachmentOptionShow
            
            let editAudioViewController = AppUtility.GET_CONTROLLER(controllerName: "EditAudioViewController") as! EditAudioViewController
            editAudioViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            
            AppUtility.shared.navigationController?.presentPopupViewController(editAudioViewController, animationType: .slideBottomTop, backgroundTouch: false, dismissed: {
                
                if let trimmedUrl = editAudioViewController.strTrimmedAudioURL {
                    self.attachmentAudioFilePath = NSString.temporaryPath().appendingPathComponent("attachmentAudioFile.wav") as NSString
                    
                    // Remove Previously added file.
                    if FileManager.default.fileExists(atPath: self.attachmentAudioFilePath as String) {
                        try? FileManager.default.removeItem(atPath: self.attachmentAudioFilePath as String)
                    }
                    
                    try? FileManager.default.copyItem(atPath: trimmedUrl, toPath: self.attachmentAudioFilePath as String)
                    
                    self.attachmentType = .audio
                    self.isAttachmentThere = true
                    //TODO: Upload Media Service
                    self.uploadCommonFiles()
                }
            })
        }
        
        self.imgVideoIcon.addTapGesture { (gesture) in
            self.isAttachmentOptionShow = !self.isAttachmentOptionShow
            self.viewMediaOption.isHidden = !self.isAttachmentOptionShow
            
            UIAlertController.showActionSheet(in: self, withTitle: nil, message: nil, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: ["Capture Video", "Select From Gallery"], popoverPresentationControllerBlock: { (popover) in
                popover.sourceView = self.imgVideoIcon
                popover.sourceRect = self.imgVideoIcon.frame
            }, tap: { (alert, action, index) in
                if index == 2 {
                    self.showCameraForVideo()
                }
                else if index == 3 {
                    self.openGalleryViewForVideo()
                }
            })
        }
        
    }
    
    //MARK: - Utility Methods
    
    
     
    func scrollToBottom(animated: Bool) {
        if self.arrChatMessages.count > 0 {
            self.tblChatList.scrollToRow(at: IndexPath(row: self.arrChatMessages.count - 1, section: 0), at: .bottom, animated: animated)
        }
    }
    
    
    //MARK: - IBActions
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        
    }
    
    @IBAction func btnSendAttachmentAction(_ sender: Any) {
        self.view.endEditing(true)
        self.growingTextView.text = ""
        
        self.isAttachmentOptionShow = !self.isAttachmentOptionShow
        self.viewMediaOption.superview?.bringSubview(toFront: self.viewMediaOption)
        self.viewMediaOption.isHidden = !self.isAttachmentOptionShow
    }
    
    @IBAction func btnSendMessageAction(_ sender: Any) {
        let txtComment = self.growingTextView.text
        if (txtComment?.length)! > 0 {
            self.sendMessage(txtComment!)
        }
        else {
            ISMessages.show("Please enter message", type: .info)
        }
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        if self.arrSelectedMessage.count > 0 {
            self.deleteChatMesages()
        }
    }
    
    
    @IBAction func handleButtonTap(_ sender: UIButton) {

        self.menuOptionImageNameArray  = ["ic_bell","ic_block",self.isReported == 1 ?  "ic_flag_post" : "ic_flag_post_red"]
        
        self.menuOptionNameArray  = [self.isMuted == 1 ? "UnMute Notification":"Mute Notification",
                                     self.isBlocked == 1 ? "UnBlock" : "Block",
                                     "Report"]
        
        FTPopOverMenu.showForSender(sender: sender, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray as [String], done: { (selectedIndex) -> () in
            
            
            if selectedIndex == 0 {
                  self.action(actionsType:.MUTE)
            }
            else if selectedIndex == 1{
                self.action(actionsType:.BLOCK)
            }
            else{
                self.blockUser()
            }
            
            
        }) {
            
        }
    }
    //MARK:- REPORT USER/BLOCK USER /MUTE
    
    func action(actionsType: Action)
    {
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "to_user_id":selectedUserId,
                                                   "action":actionsType.rawValue]
        
        _ = WebClient.requestWithUrl(url: K.URL.USER_ACTION, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                if actionsType == .MUTE{
                    if self.isMuted == 1 {
                        self.isMuted = 0
                    }
                    else{
                        self.isMuted = 1
                    }
                    
                }
                else{
                    if self.isReported == 1 {
                        self.isReported = 0
                    }
                    else{
                        self.isReported = 1
                    }
                }
                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            
        })
        
    }
    
    
    func blockUser () {
        
        let req = ["user_id": User.loggedInUser()?.ID! ?? 0,
                   "blocked_user_id" :self.selectedUserId! ?? 0] as Dictionary<String,Any>
        
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.BLOCK_UNBLOCK_USER, parameters: req, completion: { (responseObject, error) in
            SVProgressHUD.dismiss()
            
            if(error == nil)
            {
                    if(self.isBlocked == 1)
                    {
                        self.isBlocked = 0
                    }else{
                        self.isBlocked = 1
                    }
                
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
        })
        
    }
    
    
    //MARK: - Select Attachment
    
    func openGalleryView() -> Void {
        let imagePicker = UIImagePickerController();
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showCameraForImage() -> Void {
        let imagePicker = UIImagePickerController();
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }
        else {
            imagePicker.sourceType = .photoLibrary
        }
        imagePicker.mediaTypes = [kUTTypeImage as String]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openGalleryViewForVideo() -> Void {
        let imagePicker = UIImagePickerController();
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeMovie as String]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showCameraForVideo() -> Void {
        let imagePicker = UIImagePickerController();
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }
        else {
            imagePicker.sourceType = .photoLibrary
        }
        imagePicker.mediaTypes = [kUTTypeMovie as String]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    // MARK: Image or Video Picker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true) {
            
            if info[UIImagePickerControllerMediaType] as! String == (kUTTypeImage as String) {
                let image: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                
                let selectedImageLocalURL = NSString.temporaryPath().appendingPathComponent("attachment.jpg")
                if let data = UIImageJPEGRepresentation(image, 0.5) {
                    (data as NSData).write(toFile: selectedImageLocalURL, atomically: true)
                    
                    self.attachmentImage = image;
                    self.attachmentType = .image;
                    self.isAttachmentThere = true;
                    //TODO:- Upload Media Service
                    self.uploadCommonFiles()
                }
            }
            else if info[UIImagePickerControllerMediaType] as! String == (kUTTypeMovie as String) {
                
                var mediaURL = info[UIImagePickerControllerMediaURL] as? URL
                if mediaURL == nil {
                    mediaURL = info[UIImagePickerControllerReferenceURL] as? URL
                }
                
                if mediaURL != nil {
                    // Check for Video Length.
                    var durationInSeconds: TimeInterval = 0.0
                    
                    let asset = AVURLAsset(url: mediaURL!, options: nil)
                    durationInSeconds = CMTimeGetSeconds(asset.duration);
                    
                    // If video length is less or equalto five second.
                    if durationInSeconds > 0 && durationInSeconds < 6 {
                        
                        self.attachmentVideoFilePath = NSString.temporaryPath().appendingPathComponent("attachmentVideo.mp4") as NSString
                        
                        let videoPathURL = URL(fileURLWithPath: self.attachmentVideoFilePath as String)
                        AppUtility.shared.convertVideoToMp4(mediaURL!, withFileUrl: videoPathURL, completion: { (success) in
                            if (success) {
                                self.attachmentType = .video
                                self.isAttachmentThere = true;
                                //TODO: Upload Media Service
                                self.uploadCommonFiles()
                            }
                        })
                    }
                        // If video length is more than five second.
                    else if durationInSeconds > 5 {
                        let editVideoViewController = AppUtility.GET_CONTROLLER(controllerName: "EditVideoViewController") as! EditVideoViewController
                        editVideoViewController.mediaURL = mediaURL;
                        editVideoViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                        AppUtility.shared.navigationController?.presentPopupViewController(editVideoViewController, animationType: .slideBottomTop, backgroundTouch: false, dismissed: {
                            
                            if let trimmedURL = editVideoViewController.trimmedVideoUrl {
                                self.attachmentVideoFilePath = NSString.temporaryPath().appendingPathComponent("attachmentVideo.mp4") as NSString
                                
                                // Remove Previously added file.
                                if(FileManager.default.fileExists(atPath: self.attachmentVideoFilePath as String)) {
                                    try? FileManager.default.removeItem(atPath: self.attachmentVideoFilePath as String)
                                }
                                
                                try? FileManager.default.copyItem(at: trimmedURL, to: URL(fileURLWithPath: self.attachmentVideoFilePath as String))
                                
                                self.attachmentType = .video;
                                self.isAttachmentThere = true;
                                //TODO: Upload Media Service
                                self.uploadCommonFiles()
                            }
                        })
                    }
                }
            }
        }
    }
    
    // MARK: Upload Files for Chat Media
    func uploadCommonFiles() -> Void {
        
        // Media should be image or URL.
        var media : Any? = nil
        var thumbImage : UIImage?
        if attachmentType == .image {
            media = attachmentImage!
            thumbImage = UIImage(data: UIImageJPEGRepresentation(attachmentImage!, 0.6)!)
        }
        else if attachmentType == .video {
            let videoPathURL = URL(fileURLWithPath: attachmentVideoFilePath as String)
            media = videoPathURL
            print(videoPathURL)
            if let image = AppUtility.shared.thumbFromVieoURL(videoURL: videoPathURL) {
                thumbImage = UIImage(data: UIImageJPEGRepresentation(image, 0.6)!)
            }
        }
        else if attachmentType == .audio {
            let audioPathURL = URL(fileURLWithPath: attachmentAudioFilePath as String)
            media = audioPathURL
        }
        
        if media != nil {
            SVProgressHUD.show()
            // Upload Original Media
            
            // Upload Original Media
            AppUtility.uploadMedia(media: media!, dirName: K.MediaDir.CHAT_PHOTO, model: K.MediaDir.CHAT_PHOTO, completion: { (response, file, error) in
                if error == nil {
                    var fileName = ""
                    var thumbName = ""
                    
                    // Set File Name
                    fileName = file!
                    
                    // Upload Media Thumb
                    if thumbImage != nil {
                        AppUtility.uploadMedia(media: thumbImage!, dirName:"chat", model:"chat", completion: { (response, thumbFile, error) in
                            if error == nil {
                                // Set Thumb File Name
                                thumbName = thumbFile!
                            }
                            self.sendAttachment(fileName, thumbName)
                        })
                    }
                    else {
                        // Call Service For Send Media Message
                        self.sendAttachment(fileName, thumbName)
                    }
                }
                else {
                    SVProgressHUD.dismiss()
                    ISMessages.show(error?.localizedDescription, type: .error)
                }
            })
            
          
        }
        else {
            ISMessages.show("Please select valid media file.", type: .info)
        }
    }
    
    //MARK: - Webservice Calls
    
    
    func sendAttachment(_ fileName: String, _ thumbName: String) -> Void {
        
        // If File is not uploaded.
        if fileName.isEmpty && thumbName.isEmpty {
            SVProgressHUD.dismiss()
            ISMessages.show("Error while uploaded media. Please try again.", type: .error)
            return
        }
        
        var dictParam = [String:Any]()
        dictParam["owner_id"] = "\((loginUser?.ID)!)"
        dictParam["receiver_id"] = "\(selectedUserId!)"
        dictParam["conversation_id"] = "\(selectedConversionId)"
        dictParam["message_type"] = attachmentType.rawValue
        dictParam["type"] = self.chatConversionType
        dictParam["message"] = ""
        dictParam["file"] = fileName
        dictParam["thumbnail"] = thumbName
        
        // Reset Text Field.
        self.growingTextView.text = ""
        
        //SVProgressHUD.show()
        let dataRequest = WebClient.requestWithUrl(url: K.URL.ADD_MESSAGE, parameters:dictParam) { (responseObject, error) in
            SVProgressHUD.dismiss()
            if (responseObject) != nil {
                
                if let dictResponse = responseObject as? [String: Any], let status = dictResponse["status"] as? Int, status == 1 {
                    
                    print(dictResponse["data"] ?? "")
                    let chatMessage = ChatMessage(dictionary: dictResponse as NSDictionary)
                    
                        if self.selectedConversionId == 0 {
                            self.selectedConversionId = chatMessage?.conversationId ?? 0
                        }

                    self.getChatMessages(0, isRecent: true, showLoading: true, resetMessageList: true)
                        self.growingTextView.text = ""
                   
                   }
                else {
                            ISMessages.show("Failed to sent message. Please try again", type: .error)
                            self.growingTextView.text = ""
                    }
                
 
            }
        }
        dataRequest?.resume()
    }
    
    
    
    // Message Order will be:  (1 : prev , 0 : next)
    func getChatMessages(_ messageOrder: Int, isRecent: Bool, showLoading: Bool, resetMessageList: Bool = false) -> Void {
        
        var dictParam = [String:Any]()
        dictParam["owner_id"] = User.loggedInUser()?.ID!
        dictParam["receiver_id"] = "\((selectedUserId)!)"
        dictParam["type"] = self.chatConversionType
        dictParam["page"] = "1"
        dictParam["conversation_id"] = selectedConversionId
        
        
        // Reset All Previous Messages.
        if resetMessageList == true {
            self.arrChatMessages.removeAll(keepingCapacity: false)
        }
        
        var chatId : Int? = 0
        
        if self.arrChatMessages.count > 0 {
            if messageOrder == 1 {
                chatId = self.arrChatMessages.first?.chatId
            }
            else {
                chatId = self.arrChatMessages.last?.chatId
            }
        }
        
        dictParam["message_order"] = "\(messageOrder)"
        dictParam["last_msg_id"] = "\(chatId!)"
        
        // Only Show Loading on First Time.
        if showLoading == true {
            SVProgressHUD.show()
        }
        
        let dataRequest = WebClient.requestWithUrl(url:K.URL.GET_CHAT_LIST, parameters: dictParam) { (response, error) in
            SVProgressHUD.dismiss()
            
            if error == nil {
                let dict = response as! Dictionary<String, Any>
                if dict["status"]as! Int == 1{
                    // Set Min And Max Chat Ids.
                    if let minId = (dict["chat_list"] as? [String:Any])?["min_chat_id"] as? String {
                        self.minChatId = minId.toInt()
                    }
                    if let maxId = (dict["chat_list"] as? [String:Any])?["max_chat_id"] as? String {
                        self.maxChatId = maxId.toInt()
                    }
                    
                    if let arrOfChats =  dict["data"] as? Array<[String : Any]>, arrOfChats.count > 0 {
                        
                        // Set Chat Message Objects
                        var arrChatObjects = ChatMessage.modelsFromDictionaryArray(array: arrOfChats as NSArray)
                        
                        // Remove Duplecate Objects.
                        arrChatObjects = self.removeDuplicateObjects(arrChatObjects)
                        
                        var chatMessage: ChatMessage?
                        if arrChatObjects.count > 0 {
                            chatMessage = self.arrChatMessages.first
                            if isRecent {
                                self.arrChatMessages.append(contentsOf: arrChatObjects)
                            }
                            else {
                                self.arrChatMessages.insert(contentsOf: arrChatObjects, at: 0)
                            }
                        }
                        
                        self.tblChatList.reloadData()
                        if self.arrChatMessages.count > 0 {
                            if isRecent {
                                self.scrollToBottom(animated: false)
                            }
                            else {
                                if chatMessage != nil {
                                    let row = self.arrChatMessages.index(where: { (message) -> Bool in
                                        return (message.chatId == chatMessage?.chatId)
                                    })
                                    let indexPath = IndexPath(row: row!, section: 0)
                                    self.tblChatList.scrollToRow(at: indexPath, at: .top, animated: false)
                                }
                            }
                        }
                        
                    }
                    
                }
                // Reset Flag
                self.isCallingGetMessage = false
                
                // Get New Messages
                self.fetchMessageTask?.cancel()
                if self.allowFetchChatMessages() {
                    self.fetchMessageTask = DispatchWorkItem {
                        self.getChatMessages(0, isRecent: true, showLoading: false)
                        
                        // Count Counter for Call Fetch Conversion
                        //self.getChatConversion()
                        self.getMessageCallingCounter += 1
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: self.fetchMessageTask!)
                }
                
            }
            else{
                ISMessages.show(error?.localizedDescription, type: .warning)
            }
            
        }
        
        dataRequest?.resume()
    }
    
    func allowFetchChatMessages() -> Bool {
        // Calculate Bottom Height or Hide / Show view
        return true
    }
    
    func  removeDuplicateObjects(_ arrChatMessages: [ChatMessage]) -> [ChatMessage] {
        let chatIds = self.arrChatMessages.map { (message) -> String in
            return "\(message.chatId!)"
        }
        let chatObjects = arrChatMessages.filter({ (message) -> Bool in
            return !chatIds.contains("\(message.chatId!)")
        })
        return chatObjects
    }
    
    func  containsChatMessage(_ chatId: Int) -> Bool {
        let chatObjects = self.arrChatMessages.filter({ (chatMessage) -> Bool in
            return (chatMessage.chatId == chatId)
        })
        
        return (chatObjects.count > 0)
    }
    
    
    
    func sendMessage(_ strMessage : String) -> Void {
        
        var dictParam = [String:Any]()
        dictParam["owner_id"] = User.loggedInUser()?.ID
        dictParam["receiver_id"] = selectedUserId
        dictParam["conversation_id"] = selectedConversionId
        dictParam["message_type"] = "0"
        dictParam["type"] = self.chatConversionType
        dictParam["message"] = strMessage
        
        // Reset Text Field.
        self.growingTextView.text = ""
        
        
        SVProgressHUD.show()
        let dataRequest = WebClient.requestWithUrl(url: K.URL.ADD_MESSAGE, parameters:dictParam) { (response, error) in
            SVProgressHUD.dismiss()
            if (response) != nil {
                
                let dict = response as! Dictionary<String, Any>
                if dict["status"]as! Int == 1{
                    
                    let dictData = response as! Dictionary<String, Any>
                    let dictResponse =  dictData["data"] as! Dictionary<String, Any>
                    // Set Chat Message Objects
                    
                    let chatMessage = ChatMessage(dictionary: dictResponse as NSDictionary)
                    
                    if self.selectedConversionId == 0 {
                        self.selectedConversionId = chatMessage?.conversationId ?? 0
                    }
                    
                    if self.containsChatMessage((chatMessage?.chatId)!) == false {
                        self.arrChatMessages.append(chatMessage!)
                        self.tblChatList.reloadData()
                        self.scrollToBottom(animated: true)
                    }
                    
                    self.growingTextView.text = ""
                    
                    
                    self.tblChatList.reloadData()
                    self.scrollToBottom(animated: true)
                    
                }
                    
                else {
                    ISMessages.show("Failed to sent message. Please try again", type: .error)
                    self.growingTextView.text = strMessage
                }
            }
        }
        dataRequest?.resume()
        
    }
    
    // Delete Chat Message
    func deleteChatMesages() -> Void {
        var dictParam = [String:Any]()
        dictParam["owner_id"] = "\((loginUser?.ID)!)"
        dictParam["chat_id"] = self.arrSelectedMessage.joined(separator: ",")
        dictParam["receiver_id"] = self.selectedUserId
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.DELETE_CHAT_MESSAGE, parameters:dictParam) { (responseObject, error) in
            SVProgressHUD.dismiss()
            if let dict = responseObject as? [String: Any], let status = dict["status"] as? Int, status == 1 {
                self.isEnableMessageSelection = false
                self.arrChatMessages = self.arrChatMessages.filter({ (chatMessage) -> Bool in
                    return !self.arrSelectedMessage.contains("\(chatMessage.chatId!)")
                })
                self.tblChatList.reloadData()
                
                // Remove Delete Button After Remove Message.
                self.arrSelectedMessage.removeAll(keepingCapacity: false)
                self.barBtnDelete.isHidden = self.isEnableMessageSelection ? false : true
               // self.navigationItem.rightBarButtonItem = self.isEnableMessageSelection ? self.barBtnDelete : nil
            }
            else {
                ISMessages.show(error?.localizedDescription, type: .error)
            }
        }
    }
    
    //MARK: - TableView Datasource Delegate Methods
    
    func getMessageTextSize(strMessage: String) -> CGSize {
        let maxWidth = tblChatList.width - 130//160
        let font = UIFont(name: kPTSansRegularFontName, size: 15)
        var messageSize = (strMessage).size(maxWidth, font: font!, lineBreakMode: .byWordWrapping)
        if messageSize.height <  30 {
            messageSize.height = 30
        }
        return messageSize
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblConversionList {
            if self.animationState == .semiOpen {
                return 80
            }
            else {
                return 60
            }
        }
        else {
            let chatMessage = self.arrChatMessages[indexPath.row]
            
            if chatMessage.messageType == .text {
                let strMessage = chatMessage.message!
                var cellSize = self.getMessageTextSize(strMessage: strMessage)
                // Added Static Height
                let staticHeight = CellChatMessage.getChatStaticHeight()
                cellSize.height += staticHeight
                return cellSize.height
            }
            
            else if chatMessage.messageType == .feed{
                 return ChatFeedCell.getChatStaticHeight()
            }
            else{
                return ChatMediaCell.getChatStaticHeight()
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblConversionList {
            return self.arrConversionList.count
        }
        else {
            return self.arrChatMessages.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tblConversionList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell", for: indexPath) as! ChatListCell
            
            let data = self.arrConversionList[indexPath.row]
            cell.lblName.text = data.receiverName
            if let userPic = data.receiverProfilePic{
                cell.imgProfile.setImageWithURL(userPic, placeHolderImage:#imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray)
            }
            
            cell.lblShortName.text = cell.lblName.text
            cell.lblLastMessage.text = data.message
            cell.lblDate.text = NSDate(timeIntervalSince1970: TimeInterval(data.createdDate!)).timeAgoForB()
            
            if let count = data.unreadMessageCount, count > 0 {
                cell.lblMsgCount.text = "\(count)"
                cell.lblMsgCount.isHidden = false
            }
            else {
                cell.lblMsgCount.isHidden = true
            }
            
            // Show Selected Conversion Indicator
            cell.selectedView.isHidden = (self.selectedConversionIndex != indexPath.row)
            
            // Set Cell Layout according to state
            if self.animationState == .semiOpen {
                cell.lblShortName.isHidden = false
                cell.lblMsgCount.x = 5
                cell.lblMsgCount.y = 4
                cell.lblMsgCount.width = 4
                cell.lblMsgCount.height = 4
                cell.lblMsgCount.roundView()
                cell.lblMsgCount.textColor = .clear
                cell.viewInfoContainer.isHidden = true
                
            }
            else {
                cell.lblShortName.isHidden = true
                cell.lblMsgCount.x = 24
                cell.lblMsgCount.y = 25
                cell.lblMsgCount.width = 16
                cell.lblMsgCount.height = 16
                cell.lblMsgCount.roundView()
                cell.lblMsgCount.textColor = .white
                cell.viewInfoContainer.isHidden = false
            }
            
            // Set Hidden if State is Closed.
            if self.animationState == .close {
                cell.viewInfoContainer.isHidden = true
                cell.selectedView.isHidden = true
            }
            
            // Set Container Layout
            cell.viewInfoContainer.x = 60
            cell.viewInfoContainer.width = self.tblConversionList.width - 75
            
            // Set Label Set.
            cell.lblDate.sizeToFit()
            cell.lblDate.height = 21
            cell.lblDate.right = 0
            
            // Hide If Animation State is changed
            cell.lblName.x = 0
            if self.animationState == .changed {
                cell.lblDate.isHidden = true
                
                // Set Label Width
                cell.lblName.width = cell.lblDate.right - (cell.lblName.x)
            }
            else {
                cell.lblDate.isHidden = false
                
                // Set Label Width
                cell.lblName.width = cell.lblDate.x - (cell.lblName.x + 8)
            }
            
            return cell
        }
        else {
            // Set Message Container Size.
            let textFont = UIFont(name: kPTSansRegularFontName, size: 15)
            let chatMessage = self.arrChatMessages[indexPath.row]
            
            if chatMessage.messageType == .text {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatMessage", for: indexPath) as! CellChatMessage
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                let strMessage = chatMessage.message!
                cell.lblMessage.text = chatMessage.message
                let textSize = chatMessage.message?.size(tblChatList.width - horizontalPadding, font: cell.lblMessage.font, lineBreakMode: cell.lblMessage.lineBreakMode)
                
                
                let bubbleMinWidth: CGFloat = 55.0
                let bubbleWidth = (textSize?.width)! + bubbleMinWidth - 10
                cell.viewBubbleContainer.width = bubbleWidth < bubbleMinWidth ? bubbleMinWidth : bubbleWidth
                
                if (self.arrSelectedMessage.contains("\(chatMessage.chatId!)")) {
                    cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                }
                else {
                    cell.backgroundColor = .clear
                }
                
                if loginUser?.ID == chatMessage.ownerId {
                    cell.imgViewBubble.image = #imageLiteral(resourceName: "ic_greychatbox")
                    cell.viewBubbleContainer.x = cell.width - cell.viewBubbleContainer.width - 10
                    cell.lblMessageTime.textAlignment = .right
                    cell.lblMessageTime.rightView = cell.viewBubbleContainer.rightView - 15
                    cell.lblMessage.x = 20
                    cell.lblMessage.textColor = K.Color.black
                    
                }
                else {
                    
                    cell.imgViewBubble.image = #imageLiteral(resourceName: "ic_redchatbox")
                    cell.viewBubbleContainer.x = cell.left + 10
                    cell.lblMessageTime.textAlignment = .left
                    cell.lblMessageTime.x = cell.viewBubbleContainer.x + 15
                    cell.lblMessage.x = 25
                    cell.lblMessage.textColor = UIColor.white
                }
                
                
                // Set Message Data
                cell.lblMessage?.font = textFont
                cell.lblMessage?.text = strMessage
                
                // Set Message Time
                let messageDate:NSDate = NSDate(timeIntervalSince1970: chatMessage.createdDate!)
                if (messageDate as Date).isInToday == true {
                    cell.lblMessageTime?.text = messageDate.timeAgoForB()
                }
                else {
                    cell.lblMessageTime?.text = (messageDate as Date).toString(format: K.DateFormat.feedCreate)
                }
                
                // Add Gesture for Delete Message
                cell.addLongPressGesture(action: { (gesture) in
                    if (self.isEnableMessageSelection == false) {
                        self.isEnableMessageSelection = true
                        
                        // If Message exist then remove.
                        if (self.arrSelectedMessage.contains("\(chatMessage.chatId!)")) {
                            self.arrSelectedMessage.remove(element: "\(chatMessage.chatId!)")
                            cell.backgroundColor = .clear
                        }
                            // If Message not exist then add.
                        else {
                            self.arrSelectedMessage.append("\(chatMessage.chatId!)")
                            cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                        }
                       // self.navigationItem.rightBarButtonItem = self.isEnableMessageSelection ? self.barBtnDelete : nil
                        self.barBtnDelete.isHidden = self.isEnableMessageSelection ? false : true
                    }
                })
                
                cell.addTapGesture(action: { (gesture) in
                    if (self.isEnableMessageSelection == true) {
                        
                        // If Message exist then remove.
                        if (self.arrSelectedMessage.contains("\(chatMessage.chatId!)")) {
                            self.arrSelectedMessage.remove(element: "\(chatMessage.chatId!)")
                            cell.backgroundColor = .clear
                        }
                            // If Message not exist then add.
                        else {
                            self.arrSelectedMessage.append("\(chatMessage.chatId!)")
                            cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                        }
                        
                        
                        // Disable Message Selection
                        self.isEnableMessageSelection = !(self.arrSelectedMessage.count <= 0);
                        //self.navigationItem.rightBarButtonItem = self.isEnableMessageSelection ? self.barBtnDelete : nil
                        
                        self.barBtnDelete.isHidden = self.isEnableMessageSelection ? false : true
                    }
                })
                return cell
                
            }
                
            else if chatMessage.messageType == .feed {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatFeedCell", for: indexPath) as! ChatFeedCell
                cell.selectionStyle = .none
                cell.displayCellForSender(isSender: chatMessage.isSender())
                
                let messageDate:NSDate = NSDate(timeIntervalSince1970: chatMessage.createdDate!)
                if (messageDate as Date).isInToday == true {
                    cell.lblTime?.text = messageDate.timeAgoForB()
                }
                else {
                    cell.lblTime?.text = (messageDate as Date).toString(format: K.DateFormat.feedCreate)
                }


                if chatMessage.feed_data.first?.pics.first?.file_type == 1
                {
                    cell.btnDownaload.isHidden = true
                    
                    if let imageUrl = URL(string: (chatMessage.feed_data.first?.pics.first?.feed_photo)!) {
                        cell.imgReceived.af_setImage(withURL: imageUrl)
                        
                    }
                    else {
                        cell.imgReceived.image = #imageLiteral(resourceName: "ic_image_placeholder")
                    }
                }
                else if chatMessage.feed_data.first?.pics.first?.file_type == 2 {
                    
                    cell.btnDownaload.isHidden = false
                    cell.btnDownaload.setImage(UIImage(named: "ic_play"), for: .normal)
                    if let imageUrl = URL(string: (chatMessage.feed_data.first?.pics.first?.thumb_photo)!) {
                        cell.imgReceived.af_setImage(withURL: imageUrl)
                    }
                    else {
                        cell.imgReceived.image = #imageLiteral(resourceName: "ic_image_placeholder")
                    }
                    
                    
                }
                else if chatMessage.feed_data.first?.pics.first?.file_type == 3 {
                    
                    cell.btnDownaload.isHidden = false
                    cell.btnDownaload.setImage(UIImage(named: "ic_play"), for: .normal)

                     cell.imgReceived.image = #imageLiteral(resourceName: "ic_sound_placeholder")
                    
                }
                
                cell.lblCaption.text = chatMessage.feed_data.first?.desc ?? ""
                
                return cell
            }
                
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMediaCell", for: indexPath) as! ChatMediaCell
                cell.selectionStyle = .none
                
                cell.displayCellForSender(isSender: chatMessage.isSender())
                
                // Set Message Time
                let messageDate:NSDate = NSDate(timeIntervalSince1970: chatMessage.createdDate!)
                if (messageDate as Date).isInToday == true {
                    cell.lblTime?.text = messageDate.timeAgoForB()
                }
                else {
                    cell.lblTime?.text = (messageDate as Date).toString(format: K.DateFormat.feedCreate)
                }
                
                // Set Message Data
                if let fileUrl = URL(string: chatMessage.file) {
                    let filePath = chatMessage.file!
                    var localFilePath = NSTemporaryDirectory() + fileUrl.lastPathComponent
                    
                    cell.imgReceived.image = nil
                    
                    // Set Media block
                    func setMediaData() -> Void {
                        cell.viewDarkLayer.isHidden = true
                        
                        if(chatMessage.messageType == .image) {
                            cell.btnDownaload.isHidden = true
                            cell.imgReceived.af_setImage(withURL: URL(fileURLWithPath: localFilePath))
                            
                            cell.viewMediaContainer.addTapGesture(action: { (gesture) in
                                if(localFilePath.isValid) {
                                    //TODO :- show full screen image
                                    ShowImageViewController.presentFromViewController(from: self, images: [localFilePath], selectedIndex: 0, withTitle: "")
                                    }
                            })
                        }
                        else if(chatMessage.messageType == .video) {
                            cell.btnDownaload.isHidden = false
                            cell.btnDownaload.setImage(UIImage(named: "ic_play"), for: .normal)
                            
                            if let thumbURL = URL(string: chatMessage.thumbnail) {
                                cell.imgReceived.af_setImage(withURL: thumbURL)
                            }
                            else {
                                cell.imgReceived.image = #imageLiteral(resourceName: "ic_image_placeholder")
                            }
                        }
                        else if(chatMessage.messageType == .audio) {
                            cell.btnDownaload.isHidden = false
                            cell.btnDownaload.setImage(UIImage(named: "ic_play"), for: .normal)
                            cell.imgReceived.image = #imageLiteral(resourceName: "audio_placeholder")
                        }
                    }
                    
                    // Add Download Tap Gesture
                    cell.btnDownaload.addTapGesture(action: { (gesture) in
                        // Delete message enable then not open media.
                        if (self.isEnableMessageSelection == true) {
                            return;
                        }
                        
                        if (self.isGroupMember == 1 || self.chatConversionType == "1")
                        {
                            if(!FileManager.default.fileExists(atPath: localFilePath))
                            {
                                cell.activityIndicator.isHidden = false
                                cell.activityIndicator.superview?.bringSubview(toFront: cell.activityIndicator)
                                cell.activityIndicator.startAnimating()
                                cell.btnDownaload.isHidden = true
                                
                                WebClientObjc.downloadFile(filePath, atPath: localFilePath, downloadProgress: nil, downloadFileCompletionBlock: { (url, data, error) in
                                    cell.activityIndicator.stopAnimating()
                                    
                                    
                                    if (error != nil) {
                                        do {
                                            try FileManager.default.removeItem(atPath: localFilePath)
                                        }
                                        catch let error as NSError {
                                            print("Unable to remove item \(error.localizedDescription)")
                                        }
                                        
                                        localFilePath = ""
                                        ISMessages.show(error?.localizedDescription, type: .error)
                                        cell.btnDownaload.isHidden = false
                                        cell.viewDarkLayer.isHidden = false
                                    }
                                    else {
                                        // Other wise call Set Media Block
                                        setMediaData()
                                    }
                                })
                            }
                            else {
                                if(chatMessage.messageType == .video) {
                                    // Play Video
                                    self.avPlayerViewController = AVPlayerViewController();
                                    self.avPlayerViewController?.player = AVPlayer.init(url: URL.init(fileURLWithPath: localFilePath))
                                    self.present(self.avPlayerViewController!, animated: true, completion: {
                                        self.avPlayerViewController?.player?.play()
                                    })
                                }
                                else if(chatMessage.messageType == .audio) {
                                    self.playSound(soundUrl: chatMessage.file)

                                    
//                                    let playAudioViewController = AppUtility.GET_CONTROLLER(controllerName: "SendAudioViewController") as! SendAudioViewController
//                                    playAudioViewController.enablePlayerMode = true
//                                    playAudioViewController.filePath = chatMessage.file
//                                    playAudioViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//
//                                    self.navigationController?.presentPopupViewController(playAudioViewController, animationType: .slideBottomTop)

                                    //AppUtility.shared.navigationController?.presentPopupViewController(playAudioViewController, animationType: .slideBottomTop, backgroundTouch: false, dismissed: nil)
                                }
                            }
                        }
                        else {
                            ISMessages.show("You are no longer member of this group.", type: .info)
                        }
                    })
                    
                    // Set Chat Media
                    if (!FileManager.default.fileExists(atPath: localFilePath)) {
                        if (chatMessage.messageType == .audio) {
                            cell.imgReceived.image = #imageLiteral(resourceName: "audio_placeholder")
                        }
                        else {
                            cell.imgReceived.setImageWithURL(chatMessage.thumbnail, placeHolderImage: nil)
                        }
                        
                        cell.viewDarkLayer.superview?.bringSubview(toFront: cell.viewDarkLayer)
                        cell.btnDownaload.isHidden = false
                        cell.viewDarkLayer.isHidden = false
                        cell.btnDownaload.superview?.bringSubview(toFront: cell.btnDownaload)
                    }
                    else {
                        // Set Media
                        setMediaData()
                    }
                    
                    
                    // Add Gesture for Delete Message
                    cell.addLongPressGesture(action: { (gesture) in
                        if (self.isEnableMessageSelection == false) {
                            self.isEnableMessageSelection = true
                            
                            // If Message exist then remove.
                            if (self.arrSelectedMessage.contains("\(chatMessage.chatId!)")) {
                                self.arrSelectedMessage.remove(element: "\(chatMessage.chatId!)")
                                cell.backgroundColor = .clear
                            }
                                // If Message not exist then add.
                            else {
                                self.arrSelectedMessage.append("\(chatMessage.chatId!)")
                                cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                            }
                            // self.navigationItem.rightBarButtonItem = self.isEnableMessageSelection ? self.barBtnDelete : nil
                            self.barBtnDelete.isHidden = self.isEnableMessageSelection ? false : true
                        }
                    })
                    
                    cell.addTapGesture(action: { (gesture) in
                        if (self.isEnableMessageSelection == true) {
                            
                            // If Message exist then remove.
                            if (self.arrSelectedMessage.contains("\(chatMessage.chatId!)")) {
                                self.arrSelectedMessage.remove(element: "\(chatMessage.chatId!)")
                                cell.backgroundColor = .clear
                            }
                                // If Message not exist then add.
                            else {
                                self.arrSelectedMessage.append("\(chatMessage.chatId!)")
                                cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                            }
                            
                            
                            // Disable Message Selection
                            self.isEnableMessageSelection = !(self.arrSelectedMessage.count <= 0);
                            //self.navigationItem.rightBarButtonItem = self.isEnableMessageSelection ? self.barBtnDelete : nil
                            
                            self.barBtnDelete.isHidden = self.isEnableMessageSelection ? false : true
                        }
                    })
                    
                }
                
                return cell
            }
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tblConversionList {
            let data = self.arrConversionList[indexPath.row]
            selectedUserId = data.receiverID
            self.selectedConversionIndex = indexPath.row
            self.reloadNewChatThread(data)
            
            self.isEnableMessageSelection = false
            // Remove Delete Button After Remove Message.
            self.arrSelectedMessage.removeAll(keepingCapacity: false)
           // self.navigationItem.rightBarButtonItem = self.isEnableMessageSelection ? self.barBtnDelete : nil
            self.barBtnDelete.isHidden = self.isEnableMessageSelection ? false : true
            
            self.tblConversionList.reloadData()
            
        }
        else{
            
            let chatMessage = self.arrChatMessages[indexPath.row]
            
            if chatMessage.messageType == .feed {
                let feedDetailVC  = self.storyboard?.instantiateViewController(withIdentifier: "FeedDetailViewController")  as! FeedDetailViewController
                feedDetailVC.feedID = chatMessage.feedId ?? 0
                self.navigationController?.pushViewController(feedDetailVC, animated: true)
                
            }
        }
    }
    
    
    //MARK:- Play Audio
    
    func playSound(soundUrl: String) {
        let sound = URL(fileURLWithPath: soundUrl)
        do {
            let player = AVPlayer(url: sound)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
            
        }catch let error {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    
    // MARK: - Scroll View Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is UITableView, scrollView == self.tblChatList {
            let currentOffset = scrollView.contentOffset.y
            if currentOffset <= -20.0 && arrChatMessages.count > 0 {
                let currentMinChatId = (arrChatMessages.first?.chatId)!
                if currentMinChatId > self.minChatId && self.isCallingGetMessage == false {
                    self.isCallingGetMessage = true
                    self.getChatMessages(1, isRecent: false, showLoading: false)
                }
            }
        }
        else if scrollView is UITableView, scrollView == self.tblConversionList {
            self.imgCurrentChatIndicator.centerY = self.lastYOffset - scrollView.contentOffsetY
        }
    }
    
    //MARK: Load New Chat Thread From Conversion List.
    
    func reloadNewChatThread(_ data: ChatList) -> Void {
        
        fetchMessageTask?.cancel()
        
        // Setup View Layout
        self.setupLayout()
        // Get Chat Messages
        self.getChatMessages(0, isRecent: true, showLoading: true, resetMessageList: true)
    }
    
    // MARK: Get Conversion List after 30 Secs.
    
    func getChatConversion() -> Void {
        
        if self.getMessageCallingCounter >= 6 {
            self.getMessageCallingCounter = 0
        }
        
        if self.getMessageCallingCounter == 0 {
            // Set Parameters.
            let regParameters : Dictionary<String, Any>! = ["owner_id":User.loggedInUser()?.ID ?? 0,
                                                            "type":self.chatConversionType,
                                                            "user_id":User.loggedInUser()?.ID ?? 0]
            
            
            //            SVProgressHUD.show()
            _ = WebClient.requestWithUrl(url: K.URL.GET_CONVERSATIONS, parameters: regParameters, completion: { (response, error) in
                SVProgressHUD.dismiss()
                if error == nil{
                    let dictData = response as! Dictionary<String, Any>
                    let data = dictData["data"] as! Array<Dictionary<String, Any>>
                    self.arrConversionList = Mapper<ChatList>().mapArray(JSONArray: data)
                    if self.arrConversionList.count > 0 {
                        self.animationState = .semiOpen
                        self.viewConversionMenu.isHidden = false
                    }
                    else {
                        self.animationState = .close
                        self.viewConversionMenu.isHidden = true
                    }
                    self.reloadConversionTable()
                    self.performActionAccording(self.animationState)
                }
                else  {
                    ISMessages.show(error?.localizedDescription, type: .error)
                }
            })
        }
    }
    
    //MARK:- Pangesture for Slide Menu
    
    @objc func handlePanOnView(_ sender: UIPanGestureRecognizer) -> Void {
        if self.arrConversionList.count == 0 {
            return
        }
        if(sender.state == .began) {
            
        }
        else if sender.state == .changed {
            let velocity = sender.velocity(in: self.view)
            if(velocity.x > 0 && self.viewConversionMenu.x == 26) {
                return
            }
            
            let smallWidth : CGFloat = 65 + 30
            if (self.viewConversionMenu.width < smallWidth) {
                if self.animationState != .close {
                    self.animationState = .close
                    self.reloadConversionTable()
                }
            }
            else if (self.viewConversionMenu.width > smallWidth + 20) {
                if self.animationState != .changed {
                    self.animationState = .changed
                    self.reloadConversionTable()
                }
            }
            
            let translation = sender.translation(in: self.view)
            var width = self.viewConversionMenu.width
            if width >= 26 {
                width += translation.x
                self.viewConversionMenu.width = width
                self.viewConversionMenu.backgroundColor = .clear
            }
            sender.setTranslation(CGPoint(x: 0, y:0), in: self.view)
            self.viewContainer.x = self.viewConversionMenu.width - 26
        }
        else if(sender.state == .ended ||
            sender.state == .cancelled){
            let smallWidth : CGFloat = 65 + 30
            if(self.viewConversionMenu.width < smallWidth) {
                self.animationState = .close
            }
            else if(self.viewConversionMenu.width <= self.view.width/2) {
                self.animationState = .semiOpen
            }
            else if(self.viewConversionMenu.width > self.view.width/2) {
                self.animationState = .open
            }
            else {
                self.animationState = .close
            }
            
            // Perform Action According to State
            self.performActionAccording(self.animationState)
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = (gestureRecognizer as? UIPanGestureRecognizer)?.velocity(in: gestureRecognizer.view)
        return fabs(velocity!.x) > fabs(velocity!.y)
    }
    
    func performActionAccording(_ animationState: AnimationState) -> Void {
        
        self.view.bringSubview(toFront: self.viewConversionMenu)
        
        if self.animationState == .open {
            self.viewConversionMenu.backgroundColor = .white
        }
        else {
            self.viewConversionMenu.backgroundColor = .clear
        }
        
        let animationTime = 0.2
        if self.animationState == .close {
            UIView.animate(withDuration: animationTime, animations: {
                self.viewConversionMenu.x = 0
                self.viewConversionMenu.width = 26
                self.imgMenuArrow.image = UIImage(named: "panel_left_icon")
                self.viewContainer.x = 0
                self.viewContainer.width = self.view.width
            }, completion: { (success) in
                self.view.bringSubview(toFront: self.viewInputContainer)
                self.tblChatList.reloadData()
            })
            
        }
        else if self.animationState == .semiOpen {
            UIView.animate(withDuration: animationTime, animations: {
                let smallWidth : CGFloat = 65 + 30
                self.viewConversionMenu.width = smallWidth
                self.viewContainer.x = self.viewConversionMenu.width - 26
                self.viewContainer.width = self.view.width - self.viewContainer.x
                self.imgMenuArrow.image = UIImage(named: "panel_left_icon")
            }, completion: { (success) in
                self.tblChatList.reloadData()
            })
        }
        else if self.animationState == .open {
            UIView.animate(withDuration: animationTime, animations: {
                self.viewConversionMenu.width = self.view.width
                self.viewContainer.x = self.viewConversionMenu.width - 26
                self.viewContainer.width = self.view.width
                self.imgMenuArrow.image = UIImage(named: "panel_right_icon")
            }, completion: { (success) in
                self.tblChatList.reloadData()
            })
        }
        else {
            UIView.animate(withDuration: animationTime, animations: {
                self.viewConversionMenu.width = 26
                self.viewContainer.x = 0
                self.viewContainer.width = self.view.width
                self.imgMenuArrow.image = UIImage(named: "panel_left_icon")
            }, completion: { (success) in
                self.view.bringSubview(toFront: self.viewInputContainer)
                self.tblChatList.reloadData()
            })
        }
        
        self.reloadConversionTable()
    }
    
    func reloadConversionTable() -> Void {
        // Set Selected Index. Call after Reload If implement as Origin Design.
        self.getSelectedConversionIndex()
        // Reload Data
        self.tblConversionList.reloadSections(IndexSet(integer: 0), with: .fade)
        
        // Reset Conversion Table Width And X Value.
        self.viewTblConversions.x = 0
        self.viewTblConversions.width = self.viewConversionMenu.width - 30
    }
    
    func getSelectedConversionIndex() -> Void {
        self.imgCurrentChatIndicator.isHidden = true
        
        self.arrConversionList.forEachEnumerated { (idx, chatList) in
            //            if chatList.conversationID == self.selectedConversionId {
            if chatList.receiverID == self.selectedUserId {
                self.selectedConversionIndex = idx
            }
        }
        return;
        
        
    }
    
    func resetCurrentChatIndicator() -> Void {
        if self.selectedConversionIndex != -1 {
            let indexPath = IndexPath(row: self.selectedConversionIndex, section: 0)
            if let cell = self.tblConversionList.cellForRow(at: indexPath) {
                UIView.animate(withDuration: 0.1, animations: {
                    let newFrame = self.view.convert(cell.frame, to: cell.superview)
                    let yOffset = newFrame.y + newFrame.height / 2 + self.viewConversionMenu.y
                    self.imgCurrentChatIndicator.centerY = yOffset
                    self.lastYOffset = yOffset
                })
            }
            
            if self.prevSelectedConversionIndex != self.selectedConversionIndex {
                self.tblConversionList.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            self.prevSelectedConversionIndex = self.selectedConversionIndex
        }
        else {
            self.imgCurrentChatIndicator.isHidden = true
        }
    }
    
    
    
    
}

