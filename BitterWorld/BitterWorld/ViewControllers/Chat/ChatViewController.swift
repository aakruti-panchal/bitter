//
//  ChatViewController.swift
//  BitterWorld
//
//  Created by  " " on 08/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper
import Floaty
import Alamofire

class ChatViewController: UIViewController,MenuProtocol,UISearchBarDelegate {
   
    

    @IBOutlet weak var tblChatist    : UITableView!
    
    @IBOutlet weak var btnSearch     : UIButton!
    @IBOutlet weak var btnCamera     : UIButton!
    
    @IBOutlet var viewRight          : UIView!
    @IBOutlet var viewLeft           : UIView!
    
    @IBOutlet var lblNoData          : UILabel!
    
    var arrChatList: Array<ChatList> = []
    var arrFilterFriend: Array<ChatList> = []
    var isSerching : Bool = false
    var dataRequest: DataRequest? = nil
    
    //search bar
    var searchBar: UISearchBar?
    
     //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.getSelectedTabData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let floaty = Floaty()
        AppUtility.shared.addFloatingButton(to: self.view, floaty: floaty)
        self.tblChatist.reloadData()
    }
    
    //MARK: - IBAction
    @IBAction func btnSearchTapped (_sender : UIButton){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar?.alpha = 0.0
        }) { (finished) in
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.titleView = self.searchBar
            self.searchBar?.alpha = 1.0
            self.viewRight.isHidden = true
            self.searchBar?.becomeFirstResponder()
        }
    }
    @IBAction func btnCameraTapped (_ sender : UIButton)
    {
        self.showCamera()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar?.alpha = 0.0
            self.viewRight.isHidden = false
            
            let RightNavBarButton = UIBarButtonItem(customView:self.viewRight)
            self.navigationItem.rightBarButtonItem = RightNavBarButton
            let LeftNavBarButton = UIBarButtonItem(customView:self.viewLeft)
            self.navigationItem.leftBarButtonItem = LeftNavBarButton
            self.addLogo()
            self.searchBar?.text = ""
            self.isSerching = false
            self.tblChatist.reloadData()
        }) { (finished) in
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text \(searchBar.text!)")
        if (searchBar.text?.isValid)!{
            isSerching = true
            self.arrFilterFriend = self.arrChatList.filter({ (chatList) -> Bool in
                return (chatList.displayName().localizedCaseInsensitiveContains(searchBar.text!))
            })
            self.tblChatist.reloadData()
        }
        else{
            isSerching = false
            self.tblChatist.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        return true
    }

    //MARK:-   Camera
    func showCamera (){
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.openAddPostViewController(image, video: videoURL ,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
        
    }
    
    
    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostViewController") as! AddPostViewController
        if video != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video])
        } else if image != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image])
        } else{
            
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }

    //MARK:- Other Method
    func showMenuOptions(identifier: String) {
        if identifier == "ChatViewController" {
            return
        }
        let chatVC  = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        AppUtility.shared.navigationController?.pushViewController(chatVC!, animated: true)
    }
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        
    }
    
    func setupView (){
        self.searchBar = UISearchBar()
        searchBar?.showsCancelButton = true
        searchBar?.delegate = self
        searchBar?.frame = CGRect(x: 0, y:0, width: self.view.width, height: 44)
        searchBar?.barStyle = .black
        
        self.navigationItem.hidesBackButton = true
        self.addLogo()
        
        AppUtility.shared.delegate = self
    }
    

    //MARK: - Webservice Call
    
    func getSelectedTabData() -> Void {
        
        if let user = User.loggedInUser() {
            var isShowLoading: Bool = true
            let strType: String = "1"
                if self.arrChatList.count > 0 {
                    isShowLoading = false
                }
            // If Already Called Service.
            if dataRequest != nil {
                dataRequest?.cancel()
                dataRequest = nil
            }
            
            // Set Parameters.
            let regParameters : Dictionary<String, Any>! = ["owner_id":user.ID,
                                                            "type":strType,
                                                            "user_id":user.ID]
            if isShowLoading == true {
                SVProgressHUD.show()
            }
            dataRequest = WebClient.requestWithUrl(url: K.URL.GET_CONVERSATIONS, parameters: regParameters, completion: { (response, error) in
                
                SVProgressHUD.dismiss()
                
                if error == nil{
                    let dictData = response as! Dictionary<String, Any>
                    let data = dictData["data"] as! Array<Dictionary<String, Any>>
                    
                    if data.count == 0 {
                        self.lblNoData.isHidden = false
                    }
                    else{
                        self.lblNoData.isHidden = true
                    }
                    
                    self.arrChatList = Mapper<ChatList>().mapArray(JSONArray: data)
                    self.lblNoData.text = "No Ongoing Chats Found"
                    self.tblChatist.reloadData()
                    
                }
                else{
                    //                    ISMessages.show(error?.localizedDescription, type: .warning)
                }

               
            })
        }
    }
    
    
   
    

}


extension ChatViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isSerching ? self.arrFilterFriend.count : self.arrChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        
        let data = self.isSerching ? arrFilterFriend[indexPath.row] : self.arrChatList[indexPath.row]
        cell.lblName.text = data.receiverName
        cell.imgProfile.setImageWithURL(data.receiverProfilePic ?? "", placeHolderImage:#imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock:nil)
        
        if data.message == ""{
            cell.lblLastMessage.text = "Media"
        }
        else{
            cell.lblLastMessage.text = data.message
        }
        
        cell.lblDate.text = NSDate(timeIntervalSince1970: TimeInterval(data.createdDate!)).timeAgoForB()
        
        if data.unreadMessageCount > 0 {
            cell.lblMsgCount.isHidden = false
            cell.lblMsgCount.text = String(data.unreadMessageCount)
        }
        else{
            cell.lblMsgCount.isHidden = true
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arrChatList[indexPath.row] as! ChatList
        data.unreadMessageCount = 0
        let messageList = AppUtility.GET_CONTROLLER(controllerName: "ChatMessageViewController") as! ChatMessageViewController
        // Set Conversaion Data.
        messageList.arrConversionList = self.arrChatList as! Array<ChatList>
        messageList.selectedUserId = data.displayUserId()
        messageList.selectedConversionId = data.conversationID
        messageList.selectedUserName = data.displayName()
        messageList.isMuted = data.isMuted
        messageList.isReported = data.isReported
        messageList.isBlocked = data.isBlocked
        self.navigationController?.pushViewController(messageList, animated: true)
        
    }
    
    
    
}
