//
//  EditVideoViewController.m
//
//  Created on 05/12/16.
//
//

#import <BitterWorld-Swift.h>
#import "EditVideoViewController.h"
#import "NSString+Category.h"

@interface EditVideoViewController ()

@end

@implementation EditVideoViewController
@synthesize trimmedVideoUrl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpSlider];
    [viewVideo setVideoFillMode:AVLayerVideoGravityResizeAspectFill];
    [viewVideo setDelegate:self];
    [viewVideo setLooping:FALSE];
    [viewVideo enableTimeUpdates];
    
    NSString *filePath = [[NSString temporaryPath] stringByAppendingPathComponent:@"selectedVideo.mp4"];
    
    if(_mediaURL)
    {
        [self convertVideoToMP4:_mediaURL withFilePath:filePath completion:^(BOOL success) {
            
            self.asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:filePath]];
            NSLog(@"duration %f", CMTimeGetSeconds(self.asset.duration));
            
            CGFloat assetDuration = CMTimeGetSeconds(self.asset.duration);
            [videoTrimmer setAsset:self.asset];
            [videoTrimmer setThemeColor:[UIColor clearColor]];
            [videoTrimmer setMinLength:5];
            CGFloat maxVideoLength = 60;
            [videoTrimmer setMaxLength:assetDuration < maxVideoLength ? assetDuration : maxVideoLength];
            [videoTrimmer setBorderWidth:5];
            [videoTrimmer setThemeColor:[UIColor clearColor]];
            [videoTrimmer hideTracker:TRUE];
            [videoTrimmer setDelegate:self];
            [videoTrimmer setLeftThumbImage:[UIImage imageNamed:@"video_slider_image"]];
            [videoTrimmer setRightThumbImage:[UIImage imageNamed:@"video_slider_image"]];
            // important: reset subviews
            [videoTrimmer resetSubviews];
            
            [self setupVideoWithAsset:self.asset];
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self pauseVideo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Methods

- (IBAction)btnCancelTapped:(id)sender {
    [AppUtility.shared.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

- (IBAction)btnUploadTapped:(id)sender {
    
    [self trimVideoWithCompletion:^{
          NSLog(@"%@",trimmedVideoUrl);
         [AppUtility.shared.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    }];
}

- (IBAction)btnPlayPauseTapped:(id)sender {
    
    if (viewVideo.isPlaying) {
        [self pauseVideo];
    }
    else {
        [self playVideo];
    }
}

#pragma mark - SetUp Slider

- (void)setUpSlider {
    
    [sliderTime addTarget:self action:@selector(scrubbingDidStart) forControlEvents:UIControlEventTouchDown];
    [sliderTime addTarget:self action:@selector(scrubbingDidChange) forControlEvents:UIControlEventValueChanged];
    [sliderTime addTarget:self action:@selector(scrubbingDidEnd) forControlEvents:UIControlEventTouchUpInside];
    [sliderTime addTarget:self action:@selector(scrubbingDidEnd) forControlEvents:UIControlEventTouchUpOutside];
    
}

- (void)scrubbingDidStart {
    [viewVideo startScrubbing];
    [sliderTime setThumbImage:[UIImage imageNamed:@"play_seek_icon"] forState:UIControlStateNormal];
}

- (void)scrubbingDidChange {
    
//    NSLog(@"%f",sliderTime.value);
    videoPlaybackTime = sliderTime.value;
    [viewVideo scrub:[self getCurrentPlaybackTime]];
    isScrubbing = TRUE;
    [self updateTimeLabel];
}

- (void)scrubbingDidEnd {
    [sliderTime setThumbImage:[UIImage imageNamed:@"play_seek_active_icon"] forState:UIControlStateNormal];
    [viewVideo stopScrubbing];
    isScrubbing = FALSE;
    [self updateTimeLabel];
}

#pragma mark - Other Methods

- (void)setupVideoWithAsset:(AVAsset *)asset {
    
    videoDuration = roundf(CMTimeGetSeconds(asset.duration));
    [viewVideo reset];
    
    NSInteger seconds = (NSInteger)videoDuration % 60;
    NSInteger minutes = ((NSInteger)videoDuration / 60) % 60;
    
    [lblTime setText:[NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds]];
    
    [viewVideo disableAirplay];
    [viewVideo setVideoFillMode:AVLayerVideoGravityResizeAspectFill];
    [viewVideo setDelegate:self];
    
    [viewVideo setAsset:asset];
}

- (void)playVideo {
    
    [viewVideo play];
    [btnPlayPause setSelected:TRUE];
}

- (void)pauseVideo {
    
    [viewVideo pause];
    [btnPlayPause setSelected:FALSE];
}

- (void)updateTimeLabel {
    NSString *strCurrentTime = [self timeFormatted:[self getCurrentPlaybackTime]];
    [lblTime setText:strCurrentTime];
    
}

- (CGFloat)getCurrentPlaybackTime {
    
    return videoPlaybackTime - videoTrimStartTime;
}

- (CGFloat)getVideoDuration {
    
    return videoTrimEndTime - videoTrimStartTime;
}

- (NSString *)timeFormatted:(NSInteger)totalSeconds {
    NSInteger seconds = totalSeconds % 60;
    NSInteger minutes = (totalSeconds / 60) % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld",(long)minutes, (long)seconds];
}

- (void)trimVideoWithCompletion:(void(^)())completion {
    
    if(!trimmedVideoUrl) {
        trimmedVideoUrl = [NSURL fileURLWithPath:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"video.mp4"]];
    }
    
    NSLog(@"%@",trimmedVideoUrl);
    if([[NSFileManager defaultManager] fileExistsAtPath:trimmedVideoUrl.path]) {
        [[NSFileManager defaultManager] removeItemAtURL:trimmedVideoUrl error:nil];
    }
    
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:self.asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        self.exportSession = [[AVAssetExportSession alloc] initWithAsset:self.asset presetName:AVAssetExportPresetPassthrough];
        self.exportSession.outputURL = trimmedVideoUrl;
        self.exportSession.outputFileType = AVFileTypeMPEG4;
        
        CMTime start = CMTimeMakeWithSeconds(videoTrimStartTime, self.asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds([self getVideoDuration], self.asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
           
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export cancelled");
                    break;
                case AVAssetExportSessionStatusCompleted: {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(completion) {
                            completion();
                        }
                    });
                    break;
                }
                default:
                    NSLog(@"NONE");
                    break;
            }
        }];
        
    }
}

- (void)convertVideoToMP4:(NSURL *)selectedVideoUrl withFilePath:(NSString *)filePath completion:(void(^)(BOOL success))completion {
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    NSURL *videoFullUrl = [NSURL fileURLWithPath:filePath];
    
    AVAsset *asset = [[AVURLAsset alloc] initWithURL:selectedVideoUrl options:nil];
//    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
//    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMPEG4, 0) == kCFCompareEqualTo) {
    if([[selectedVideoUrl.lastPathComponent lowercaseString] containsString:[@".mp4" lowercaseString]]) {
        NSData *videoData = [NSData dataWithContentsOfURL:selectedVideoUrl];
        [videoData writeToFile:videoFullUrl.absoluteString atomically:NO];
        if (completion) {
            completion(TRUE);
        }
    }
//    CFRelease((__bridge CFStringRef)(mediaType));//CRA
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality]) {
        __block UIBackgroundTaskIdentifier bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"bgTask" expirationHandler:^{
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }];
        _exportSession = [[AVAssetExportSession alloc]initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        
        _exportSession.shouldOptimizeForNetworkUse = YES;
        _exportSession.outputURL = videoFullUrl;
        _exportSession.outputFileType = AVFileTypeMPEG4;
        
        [_exportSession exportAsynchronouslyWithCompletionHandler:^{
            /*
             AVAssetExportSessionStatusUnknown,
             AVAssetExportSessionStatusWaiting,
             AVAssetExportSessionStatusExporting,
             */
            dispatch_async(dispatch_get_main_queue(), ^{
                switch ([_exportSession status]) {
                    case AVAssetExportSessionStatusFailed:
                    case AVAssetExportSessionStatusCancelled: {
                        if (completion) {
                            completion(FALSE);
                        }
                        _exportSession = nil;
                    }
                        break;
                    case AVAssetExportSessionStatusCompleted:{
                        if (completion) {
                            completion(TRUE);
                        }
                        _exportSession = nil;
                        break;
                    }
                    default:
                        break;
                }
                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
            });
        }];
    }
}

#pragma mark - ICGVideoTrimmerDelegate

- (void)trimmerView:(ICGVideoTrimmerView *)trimmerView didChangeLeftPosition:(CGFloat)startTime rightPosition:(CGFloat)endTime {
    if(startTime >= 0) {
        videoTrimStartTime = startTime;
        videoTrimEndTime = endTime;
        [sliderTime setMinimumValue:videoTrimStartTime];
        [sliderTime setMaximumValue:videoTrimEndTime];
        [sliderTime setValue:videoTrimStartTime];
        videoPlaybackTime = videoTrimStartTime;
        [lblTrimmedTime setText:[NSString stringWithFormat:@"%.2f - %.2f",videoTrimStartTime,videoTrimEndTime]];
        
        [viewVideo seekToTime:videoTrimStartTime completion:^{
            [self pauseVideo];
        }];
    }
}

#pragma mark - VIMVideoPlayer Delegate

- (void)videoPlayerIsReadyToPlayVideo:(VIMVideoPlayerView *)videoPlayer {
    
    videoDuration = CMTimeGetSeconds(videoPlayer.player.currentItem.duration);
    [sliderTime setValue:videoTrimStartTime animated:TRUE];
    [self updateTimeLabel];
    [viewVideo seekToTime:videoTrimStartTime completion:^{
        [self pauseVideo];
    }];
}

- (void)videoPlayerDidReachEnd:(VIMVideoPlayerView *)videoPlayer {
//    [viewVideo seekToTime:videoTrimStartTime];
//    videoPlaybackTime = videoTrimStartTime;
//    [self pauseVideo];
}

- (void)videoPlayer:(VIMVideoPlayerView *)videoPlayer timeDidChange:(CMTime)cmTime {
    if(isScrubbing) {
        return;
    }
    videoDuration = CMTimeGetSeconds(videoPlayer.player.currentItem.duration);
    CGFloat time = CMTimeGetSeconds(cmTime);
    if(floorf(time) >= videoTrimEndTime && viewVideo.isPlaying) {
        videoPlaybackTime = videoTrimStartTime;
        [sliderTime setValue:videoPlaybackTime animated:TRUE];
        [self pauseVideo];
        [viewVideo seekToTime:videoTrimStartTime completion:^{
        }];
    }
    else {
        videoPlaybackTime = ceilf(time);
    }

    [sliderTime setValue:videoPlaybackTime animated:TRUE];
    [self updateTimeLabel];

}

- (void)videoPlayer:(VIMVideoPlayerView *)videoPlayer didFailWithError:(NSError *)error {
    NSLog(@"Error : %@", [error localizedDescription]);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
