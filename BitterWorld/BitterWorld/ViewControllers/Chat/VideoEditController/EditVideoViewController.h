//
//  EditVideoViewController.h
//
//  Created on 05/12/16.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "VIMVideoPlayerView.h"
#import "ICGVideoTrimmer.h"

@interface EditVideoViewController : UIViewController <VIMVideoPlayerDelegate,ICGVideoTrimmerDelegate>
{
    IBOutlet VIMVideoPlayerView *viewVideo;
    IBOutlet UIView *viewVideoOptions;
    IBOutlet UISlider *sliderTime;
    IBOutlet UIButton *btnPlayPause;
    IBOutlet UILabel *lblTime;
    IBOutlet ICGVideoTrimmerView *videoTrimmer;
    CGFloat videoPlaybackTime;
    CGFloat videoDuration;
    BOOL isScrubbing;
    CGFloat videoTrimStartTime;
    CGFloat videoTrimEndTime;
    IBOutlet UILabel *lblTrimmedTime;
    
}

@property (strong, nonatomic) NSURL *trimmedVideoUrl;
@property (strong, nonatomic) NSURL *mediaURL;
@property (strong, nonatomic) AVAsset *asset;
@property (strong, nonatomic) AVAsset *assetTrimmed;
@property (strong, nonatomic) AVAssetExportSession *exportSession;

@end
