//
//  ShowImageViewController.swift
//  Bella
//
//  Created by Martin on 05/10/17.
//
//

import UIKit

class FullScreenImageCell: UICollectionViewCell {
    var scrollViewImage: UIScrollView?
    var imgView: UIImageView?
}

class ShowImageViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionViewImages: UICollectionView!
    
    var arrImages : [String] = []
    var selectedImageIndex: Int = 0
    
    var navigationTitle = ""
    
    // MARK: Methods to Show Image View Controller
    class func showImages(_ fileNames: [String], fromView viewController: UIViewController, at index: Int, imageType: String) {
        if fileNames.count > index {
            
            var fileURLs : [String] = []
            let fileName = fileNames[index]
            let fileUrl = URL(fileURLWithPath: fileName)
            let filePath: String = NSTemporaryDirectory() + fileUrl.lastPathComponent
            if FileManager.default.fileExists(atPath: filePath) {
                fileURLs.append(filePath)
            }
            if fileURLs.count > 0 {
                ShowImageViewController.presentFromViewController(from: viewController, images: fileURLs, selectedIndex: 0, withTitle: imageType)
            }
        }
    }
    
    class func presentFromViewController(from viewController: UIViewController, images arrImages: [String], selectedIndex: Int, withTitle title: String) {
        let objFullScreenImage = AppUtility.GET_CONTROLLER(controllerName: "ShowImageViewController") as! ShowImageViewController
        objFullScreenImage.navigationController?.isNavigationBarHidden = true
        objFullScreenImage.arrImages = arrImages
        objFullScreenImage.selectedImageIndex = selectedIndex
        objFullScreenImage.navigationTitle = title
       // viewController.presentPopupViewController(objFullScreenImage, animationType: .slideBottomTop)
        viewController.navigationController?.pushViewController(objFullScreenImage, animated: true)

        
    }
    
    
    class func pushFromViewController(from viewController: UIViewController, images arrImages: [String], selectedIndex: Int, withTitle title: String) {
        let objFullScreenImage = AppUtility.GET_CONTROLLER(controllerName: "ShowImageViewController") as! ShowImageViewController
        objFullScreenImage.arrImages = arrImages
        objFullScreenImage.selectedImageIndex = selectedIndex
        objFullScreenImage.navigationTitle = title
        viewController.navigationController?.pushViewController(objFullScreenImage, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add Top Bar View.
        //self.setTopBarNaviagtion()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Reload Collection View.
        self.collectionViewImages.reloadData()
        
        // Scroll To Current Selected Image.
        if self.selectedImageIndex < arrImages.count {
            collectionViewImages.scrollToItem(at: IndexPath(row: self.selectedImageIndex, section: 0), at: .centeredHorizontally, animated: false)
        }
    }
    
    //MARK: Add Top Bar
    func setTopBarNaviagtion() -> Void {
        let topNavBar = TopBarView(title: navigationTitle, leftOptionImages: ["white_back_icon"], showGradiantLayer: false)
        topNavBar.backgroundColor = .clear
        topNavBar.leftOptionDidTapped = { (barItem: UIButton) -> Void in
            self.navigationController?.popViewController(animated: true)
            self.dismissPopupViewControllerWithanimationType(.slideTopBottom)
        }
        self.view.addSubview(topNavBar)
    }
    
    //MARK: Collection View Delegate And Datasource
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width, height: collectionView.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let strCellIdentifier = "FullScreenImageCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: strCellIdentifier, for: indexPath) as! FullScreenImageCell
        
        if cell.scrollViewImage == nil {
            cell.scrollViewImage = UIScrollView(frame: cell.bounds)
            cell.scrollViewImage!.delegate = self
            cell.scrollViewImage!.clipsToBounds = true
            cell.addSubview(cell.scrollViewImage!)
        }
        
        // Remove All Subviews
        cell.scrollViewImage?.subviews.forEachEnumerated({ (idx, view) in
            view.removeFromSuperview()
        })
        
        cell.imgView = UIImageView(frame: cell.scrollViewImage!.bounds)
        cell.scrollViewImage?.addSubview((cell.imgView)!)
        
        let strUrl: String = arrImages[indexPath.item]
        
        func setupScrollView(_ selectedImage: UIImage?) -> Void {
            var image : UIImage
            if let img = selectedImage {
                image = img
            }
            else {
                image = #imageLiteral(resourceName: "placeholder_feed")
            }
            
            cell.imgView?.image = image
            var frame = CGRect.zero
            frame.size = image.size
            cell.imgView?.frame = frame
            
            cell.scrollViewImage?.contentSize = image.size
            let scrollViewFrame: CGRect = cell.scrollViewImage!.frame
            let scaleWidth: CGFloat = scrollViewFrame.size.width / cell.scrollViewImage!.contentSize.width
            let scaleHeight: CGFloat = scrollViewFrame.size.height / cell.scrollViewImage!.contentSize.height
            let minScale: CGFloat = min(scaleWidth, scaleHeight)
            cell.scrollViewImage?.minimumZoomScale = minScale
            cell.scrollViewImage?.maximumZoomScale = 2.0
            cell.scrollViewImage?.zoomScale = minScale
            cell.imgView?.frame = self.centeredFrame(for: cell.scrollViewImage!, andUIView: cell.imgView!)
            
        }
        
        if strUrl.contains(String.documentsPath()) {
            let image = UIImage(contentsOfFile: strUrl)
            setupScrollView(image)
        }
        else {
            let request = URLRequest(url: URL(fileURLWithPath: (strUrl as NSString).addingPercentEscapes(using: String.Encoding.ascii.rawValue)!))

            cell.imgView?.setImageWith(request, placeholderImage: #imageLiteral(resourceName: "ic_image_placeholder"), success: { (request, response, image) in
                setupScrollView(image)
            }, failure: { (request, response, error) in
            })
        }
        
        return cell
    }

    func centeredFrame(for scrollView: UIScrollView, andUIView imgView: UIImageView) -> CGRect {
        let boundsSize: CGSize = scrollView.bounds.size
        var contentsFrame: CGRect = imgView.frame
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        }
        else {
            contentsFrame.origin.x = 0.0
        }
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        }
        else {
            contentsFrame.origin.y = 0.0
        }
        return contentsFrame
    }
    
    // MARK: Scroll View Delegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        var image: UIImageView? = nil
        scrollView.subviews.forEachEnumerated { (idx, view) in
            if (view is UIImageView) {
                image = view as? UIImageView
            }
        }
        return image
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        var image: UIImageView? = nil
        scrollView.subviews.forEachEnumerated { (idx, view) in
            if (view is UIImageView) {
                image = view as? UIImageView
            }
        }
        
        // Set Image View in Center.
        if let img = image {
            img.frame = self.centeredFrame(for: scrollView, andUIView: img)
        }
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
    }

    
    @IBAction func btnClose (_ sender : UIButton){
        self.navigationController?.popViewController()
    }
    
}
