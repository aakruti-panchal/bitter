//
//  FilterCategoryViewController.swift
//  BitterWorld
//
//  Created by  " " on 30/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

public enum FilterOption {
    case Friends
    case Categories
    case Cities
    case None
}
@objc protocol FilterCategoryViewControllerDelegate:class {
    @objc optional func didFinishSelection(selectedItesm:AnyObject, selectedOption : Int)
}
class FilterCategoryViewController: UIViewController {

    weak var delegate   : FilterCategoryViewControllerDelegate?
    var selectedOption  : FilterOption = FilterOption.None
    
    @IBOutlet weak var tblList  : UITableView!

    var arrCategories           : Array<Categories> = []
    var arrFriends              : Array<Friends> = []
    var arrCities               : Array<Cities> = []
    
    var selectedCategories      : Array<Categories> = []
    var selectedFriends         : Array<Friends> = []
    var selectedCities          : Array<Cities> = []
    
    var totalRecordCount        : Int = 0
    var pageNumer               : Int = 1
    
//MARK:- View Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setView()
        
        switch self.selectedOption {
        case .Friends:
            self.getFriendsList(loadFromStart: true)
            break
        case .Categories:
            self.getCategories()
            break
        case .Cities:
            self.getCities()
            break
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

//MARK:-  IBAction Methods
extension FilterCategoryViewController {
    
    @IBAction func btnBackTapped(_ sender:Any) {
        self.navigationController?.popViewController()
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        
        self.selectedCategories     = []
        self.selectedFriends        = []
        self.selectedCities         = []
        /*
         1 for Friends
         2 for Categories
         3 for Cities
         */
        switch self.selectedOption {
        case .Friends:
            for i in 0..<(self.arrFriends.count) {
                if self.arrFriends[i].isSelected == true {
                    self.selectedFriends.append(self.arrFriends[i])
                }
            }
            self.delegate?.didFinishSelection!(selectedItesm: self.selectedFriends as AnyObject, selectedOption: 1)
            break
        case .Categories:
            for i in 0..<(self.arrCategories.count) {
                if self.arrCategories[i].isSelected == true {
                    self.selectedCategories.append(self.arrCategories[i])
                }
            }
            self.delegate?.didFinishSelection!(selectedItesm: self.selectedCategories as AnyObject, selectedOption: 2)
            break
        case .Cities:
            
            for i in 0..<(self.arrCities.count) {
                if self.arrCities[i].isSelected == true {
                    self.selectedCities.append(self.arrCities[i])
                }
            }
            self.delegate?.didFinishSelection!(selectedItesm: self.selectedCities as AnyObject, selectedOption: 3)
            break
        default:
            break
        }
        self.navigationController?.popViewController()
    }
}

//MARK:- Other Methods
extension FilterCategoryViewController {
    
    func setView() {
        self.addLogo()
        self.tblList.tableFooterView = UIView()
    }
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        //imageView.frame.size.width = 100;
        //imageView.frame.size.height = 45;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
}

//MARK:- UITable View DataSource & Delegate
extension FilterCategoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch self.selectedOption {
        case .Friends:
            return self.arrFriends.count
        case .Categories:
            return self.arrCategories.count
        case .Cities:
            return self.arrCities.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        switch self.selectedOption {
        case .Friends:
            let friend = arrFriends[indexPath.row]
            cell.lblName.text = friend.first_name
            cell.btnCheck.isSelected  =  friend.isSelected! ? true : false
            break
        case .Categories:
            let cat = arrCategories[indexPath.row]
            cell.lblName.text = cat.name
            cell.btnCheck.isSelected  =  cat.isSelected ? true : false
            break
        case .Cities:
            
            let city = arrCities[indexPath.row]
            cell.lblName.text = city.city
            cell.btnCheck.isSelected  =  city.isSelected ? true : false
            
            break
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        
        switch self.selectedOption {
        case .Friends:
            let friend = arrFriends[indexPath.row]
            friend.isSelected = !friend.isSelected!
            break
        case .Categories:
            let cat = arrCategories[indexPath.row]
            cat.isSelected = !cat.isSelected
            break
        case .Cities:
            
            let cat = arrCities[indexPath.row]
            cat.isSelected = !cat.isSelected
            break
        default:
            break
        }
        self.tblList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch self.selectedOption {
        case .Friends:
            if ((self.arrFriends.count)-1) == indexPath.row {
                if self.arrFriends.count < self.totalRecordCount {
                    self.getFriendsList(loadFromStart: false)
                }
            }
            break
        case .Categories:
            break
        case .Cities:
            break
        default:
            break
        }

    }
 
}

//MARK:- Service Call
extension FilterCategoryViewController {
    
    func getCategories (){
        _ = WebClient.requestWithUrl(url: K.URL.GET_CATEGORY, parameters: nil, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let cat =  Mapper<Categories>().map(JSON: dict) {
                        self.arrCategories.append(cat)
                    }
                }
                for i in 0..<(self.arrCategories.count) {
                    for j in 0..<(self.selectedCategories.count) {
                        if self.selectedCategories[j].ID == self.arrCategories[i].ID {
                            self.arrCategories[i].isSelected = true
                        }
                    }
                }
                self.tblList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    func getFriendsList(loadFromStart:Bool) {
        
        if loadFromStart == true {
            self.pageNumer = 1
        }
        
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "page":self.pageNumer]
        //let dictParam :Dictionary<String, Any> = [ "user_id" : 9, "page":self.pageNumer]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_FRIENDS, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                self.totalRecordCount = dictData["total_count"] as? Int ?? 0
                
                // Remove Previous Objects of first page
                if self.pageNumer == 1 {
                    self.arrFriends.removeAll(keepingCapacity: false)
                }
                for dict in arrData {
                    if let cat =  Mapper<Friends>().map(JSON: dict) {
                        
                        for j in 0..<(self.selectedFriends.count) {
                            if self.selectedFriends[j].user_id == cat.user_id {
                                cat.isSelected = true
                            }
                        }
                        self.arrFriends.append(cat)
                    }
                }
//                for i in 0..<(self.arrFriends.count) {
//                    for j in 0..<(self.selectedFriends.count) {
//                        if self.selectedFriends[j].user_id == self.arrFriends[i].user_id {
//                            self.arrFriends[i].isSelected = true
//                        }
//                    }
//                }
                self.tblList.reloadData()
                if self.pageNumer == 1{
                    self.tblList.scrollToTop(animated: true)
                }
                // Increase Page Counter
                self.pageNumer = self.pageNumer + 1;
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    func getCities() {
        
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_CITIES, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let cat =  Mapper<Cities>().map(JSON: dict) {
                        self.arrCities.append(cat)
                    }
                }
                for i in 0..<(self.arrCities.count) {
                    for j in 0..<(self.selectedCities.count) {
                        if self.selectedCities[j].city == self.arrCities[i].city {
                            self.arrCities[i].isSelected = true
                        }
                    }
                }
                self.tblList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
}
