//
//  SendToViewController.swift
//  BitterWorld
//
//  Created by  " " on 03/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class SendToViewController: UIViewController {

    @IBOutlet weak var tblSendTo            : UITableView!
    @IBOutlet weak var lblTitle             : UILabel!
    @IBOutlet weak var txtSearchText        : UITextField!
    @IBOutlet weak var imgSearch            : UIImageView!
    @IBOutlet weak var btnCloseSearch       : UIButton!
    @IBOutlet weak var lblReportPostTitle   : UILabel!
    @IBOutlet weak var CommentText          : UITextView!
    @IBOutlet weak var btnApply             : UIButton!
    
    @IBOutlet weak var sendToScrollView: TPKeyboardAvoidingScrollView!
    
    var arrFriends              : Array<Friends> = []
    var arrFriendsFilter        : Array<Friends> = []
    
    var arrReportPost : Array<ReportPost> = []
    
    var isSerching : Bool = false
    //let CommentText = UITextView()
    var isReportPost : Bool? = false
    var feedID : Int = 0
//    var selectedFriends         : Array<Friends> = []
    
    var totalRecordCount        : Int = 0
    var pageNumer               : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.sendToScrollView.isHidden = true
        //self.sendToScrollView.height = SF.screenHeight - 300
        
        if self.isReportPost! {
            self.gerReportPost()
            self.lblTitle.text = "Report Post"
            self.tblSendTo.separatorStyle = .none
            self.txtSearchText.isHidden = true
            self.imgSearch.isHidden = true
            self.btnCloseSearch.isHidden = true
            self.lblReportPostTitle.isHidden = false
            self.CommentText.text = "Add comment here"
            self.CommentText.textColor = UIColor.lightGray
            
            self.CommentText.borderWidth = 0.5
            self.CommentText.borderColor = K.Color.lightGray
            self.CommentText.cornerRadius = 2
            self.btnApply.setTitle("Submit",for: .normal)
            
        } else {
            
            self.lblTitle.text = "Send to"
            self.txtSearchText.isHidden = false
            self.imgSearch.isHidden = false
            self.btnCloseSearch.isHidden = false
            self.lblReportPostTitle.isHidden = true
            self.getFriendsList(loadFromStart: true)
            self.CommentText.height = 0
            self.CommentText.isHidden = true
            self.btnApply.setTitle("Send",for: .normal)
        }
        self.tblSendTo.estimatedRowHeight = 100
        self.tblSendTo.rowHeight = UITableViewAutomaticDimension
        self.tblSendTo.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
}


//MARK: - IBAction Methods.
extension SendToViewController {
    
    @IBAction func btnSearchClosedTapped(_ sender: Any) {
        self.txtSearchText.text = ""
        self.searchText(searchText: "")
        _ = self.txtSearchText.resignFirstResponder()
    }
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
        if self.isReportPost! {
            var strReportPost = ""
            for i in 0..<(self.arrReportPost.count) {
                if let id = self.arrReportPost[i].report_id, (self.arrReportPost[i].isSelected == true) {
                    if strReportPost.isEmpty {
                        strReportPost.append("\(id)")
                    } else {
                        strReportPost.append(", \(id)")
                    }
                }
            }
            print("strReportPost : ", strReportPost)
            var commentTxt = self.CommentText.text
            if (self.CommentText.textColor == UIColor.lightGray) { commentTxt = "" }
            if (commentTxt?.isEmpty)! {
                ISMessages.show("please enter reason!", type: .warning)
            } else {
                self.reportPost(feedId: self.feedID, reasonIds: strReportPost, reason: commentTxt!)
            }
            
        } else {
            var strFriends = ""
            if self.isSerching {
                for i in 0..<(self.arrFriendsFilter.count) {
                    if let id = self.arrFriendsFilter[i].user_id, (self.arrFriendsFilter[i].isSelected == true)  {
                        if strFriends.isEmpty {
                            strFriends.append("\(id)")
                        } else {
                            strFriends.append(", \(id)")
                        }
                    }
                }
            } else {
                for i in 0..<(self.arrFriends.count) {
                    if let id = self.arrFriends[i].user_id, (self.arrFriends[i].isSelected == true) {
                        if strFriends.isEmpty {
                            strFriends.append("\(id)")
                        } else {
                            strFriends.append(", \(id)")
                        }
                    }
                }
            }
            print("strFriends : ", strFriends)
//            if (strFriends.isEmpty) {
//                ISMessages.show("please select friend!", type: .warning)
//            } else {
                self.addSendTo(feedId: self.feedID, receiver_id: strFriends)
//            }
            
        }
    }
}

//MARK: - TableView DataSource & Delegate Methods.
extension SendToViewController: UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.isReportPost! {
            return self.arrReportPost.count
        }
        return (self.isSerching == true) ? self.arrFriendsFilter.count : self.arrFriends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:SendToCell? = tableView.dequeueReusableCell(withIdentifier: "SendToCell") as? SendToCell
        if (cell == nil) {
            let nib: NSArray = Bundle.main.loadNibNamed("SendToCell", owner: self, options: nil)! as NSArray
            cell = nib.object(at: 0) as? SendToCell
        }
        cell?.selectionStyle = .none
        if self.isReportPost! {
            let report = self.arrReportPost[indexPath.row]
            cell?.imgSendTo.width = 0
            cell?.lblSentToTitle.x = (cell?.imgSendTo.x)!
            cell?.lblSentToTitle.text = report.report_name
            cell?.btnClose.isSelected   =  report.isSelected! ? true : false
            
        } else {
            if self.isSerching == true {
                let friend = arrFriendsFilter[indexPath.row]
                cell?.lblSentToTitle.text   = friend.first_name
                cell?.btnClose.isSelected   =  friend.isSelected! ? true : false
                if let url = friend.profile_photo, !url.isEmpty {
                    cell?.imgSendTo.setImageWithURL(url, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
                }
            } else {
                let friend = arrFriends[indexPath.row]
                cell?.lblSentToTitle.text   = friend.first_name
                cell?.btnClose.isSelected   =  friend.isSelected! ? true : false
                if let url = friend.profile_photo, !url.isEmpty {
                    cell?.imgSendTo.setImageWithURL(url, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
                }
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        if self.isReportPost! {
            
            let friend = arrReportPost[indexPath.row]
            friend.isSelected = !friend.isSelected!
            self.tblSendTo.reloadData()
            
        } else {
            if self.isSerching == true {
                let friend = arrFriendsFilter[indexPath.row]
                friend.isSelected = !friend.isSelected!
                self.tblSendTo.reloadData()
            } else {
                let friend = arrFriends[indexPath.row]
                friend.isSelected = !friend.isSelected!
                self.tblSendTo.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.isReportPost! { } else {
            if self.isSerching == true {} else {
                if ((self.arrFriends.count)-1) == indexPath.row {
                    if self.arrFriends.count < self.totalRecordCount {
                        self.getFriendsList(loadFromStart: false)
                    }
                }
            }
        }
    }
    /*
    // Add Footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.isReportPost! {
            return 300.0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var footerView = UIView()
        if self.isReportPost! {
            footerView = UIView(frame:CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 180))
        } else {
            footerView = UIView(frame:CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        }
        self.CommentText.frame = CGRect(x: 0, y: 8, width: tableView.frame.size.width, height: 100)
        self.CommentText.delegate = self
        let loginButton = UIButton(type: .custom)
        loginButton.setTitle("SUBMIT", for: .normal)
        loginButton.addTarget(self, action: #selector(SendToViewController.submitAction), for: .touchUpInside)
        loginButton.setTitleColor(UIColor.white, for: .normal)
        loginButton.backgroundColor = K.Color.AppTheme_Primary
        loginButton.frame = CGRect(x: (tableView.frame.size.width / 2) - (WidthStatic.SurveySubmitButtonWidth / 2), y: 138, width: WidthStatic.SurveySubmitButtonWidth, height: 50)
        
        Common().addshadow(View: loginButton, shadowRadius: 1.0, shadowColor: UIColor.lightGray, shadowOpacity: 2.0, cornerRadius: 2.0)
        footerView.addSubview(self.CommentText)
        footerView.addSubview(loginButton)
        
        return footerView
    }
    
    @objc func submitAction() {
        print("Submit btn")
    }
    */

}

//MARK:- Service Call
extension SendToViewController {
    
        func getFriendsList(loadFromStart:Bool) {
            
            if loadFromStart == true {
                self.pageNumer = 1
            }
            
            let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "page":self.pageNumer]
            //let dictParam :Dictionary<String, Any> = [ "user_id" : 9, "page":self.pageNumer]
            
            _ = WebClient.requestWithUrl(url: K.URL.GET_FRIENDS, parameters: dictParam, completion: { (response, error) in
                if error == nil {
                    
                    let dictData = response as! Dictionary<String, Any>
                    let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                    self.totalRecordCount = dictData["total_count"] as? Int ?? 0

                    if self.pageNumer == 1 {
                        self.arrFriends.removeAll(keepingCapacity: false)
                    }
                    for dict in arrData {
                        if let cat =  Mapper<Friends>().map(JSON: dict) {
                            
//                            for j in 0..<(self.selectedFriends.count) {
//                                if self.selectedFriends[j].user_id == cat.user_id {
//                                    cat.isSelected = true
//                                }
//                            }
                            self.arrFriends.append(cat)
                        }
                    }

                    self.tblSendTo.reloadData()
                    if self.pageNumer == 1{
                        self.tblSendTo.scrollToTop(animated: true)
                    }
                    // Increase Page Counter
                    self.pageNumer = self.pageNumer + 1;
                }
                else {
                    ISMessages.show(error?.localizedDescription)
                }
                SVProgressHUD.dismiss()
            })
    }
    
    func gerReportPost() {
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_REPORT_REASON, parameters: nil, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let report =  Mapper<ReportPost>().map(JSON: dict) {
                        self.arrReportPost.append(report)
                    }
                }
                self.tblSendTo.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
        
    }
    
    func reportPost(feedId : Int, reasonIds : String, reason : String) {
        
        //user_id, feed_id, reason, reason_id
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : feedId, "reason" : reason, "reason_id" : reasonIds]
        
        _ = WebClient.requestWithUrl(url: K.URL.REPORT_POST, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    ISMessages.show(message, type: .success)
                }
            }
        })
    }

    func addSendTo(feedId : Int, receiver_id : String) {
        //URL: http://9834584578/clients/2018/bitterworld/api/web/v1/feed/send-feed
        //Params: owner_id, receiver_id, feed_id
        let dictParam :Dictionary<String, Any> = ["owner_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : feedId, "receiver_id" : receiver_id]
        
        _ = WebClient.requestWithUrl(url: K.URL.SEND_TO, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    ISMessages.show(message, type: .success)
                }
            }
        })
    }
    
}

//MARK: - UITextField Delegate methods
extension SendToViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString     = textField.text! as NSString
        let newString: NSString         = currentString.replacingCharacters(in: range, with: string) as NSString
        let newLength: Int              = newString.length
        print(newString)
        self.searchText(searchText: newString as String)
        return true
    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return true
//    }
    
    func searchText(searchText : String) {
        
        if (searchText.isNotEmpty()){
            isSerching = true
            self.arrFriendsFilter = self.arrFriends.filter({ (friend) -> Bool in
                return (friend.first_name?.localizedCaseInsensitiveContains(searchText))!
            })
            print(self.arrFriendsFilter.count)
            self.tblSendTo.reloadData()
        }
        else{
            isSerching = false
            self.tblSendTo.reloadData()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = self.txtSearchText.resignFirstResponder()
//        if let searchText = textField.text {
//            print("Return : ", searchText)
//        }
        self.searchText(searchText: textField.text!)
        
        return true
    }
}

//MARK : - UITextView delegate
extension SendToViewController:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView == self.CommentText) {
            if (self.CommentText.textColor == UIColor.lightGray) {
                self.CommentText.text = "";
                self.CommentText.textColor = K.Color.darkGray
            }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.text)
        if (textView == CommentText) {
            if  self.CommentText.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
                self.CommentText.text = "Add comment here"
                self.CommentText.textColor = UIColor.lightGray
            }
        }
    }
}


