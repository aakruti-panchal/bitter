//
//  HomeViewController.swift
//  BitterWorld
//
//  Created by  " " on 10/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import Floaty
import ObjectMapper


enum FeedType : Int {
    case Public    = 1
    case Friends    = 2
}




class HomeViewController: UIViewController,FloatyDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,GMSMapViewDelegate,UISearchBarDelegate,MenuProtocol {
    
    @IBOutlet weak var CollectionViewPost: UICollectionView!
    
    @IBOutlet var mapViewPlaces     : GMSMapView!
    @IBOutlet var viewFloating      : UIView!
    @IBOutlet var lblPublic         : UILabel!
    @IBOutlet var lblFriend         : UILabel!
    
    @IBOutlet weak var viewDataNoFound: UIView!
    
    @IBOutlet weak var btnNavSearch: UIButton!
    @IBOutlet weak var btnNavFilter: UIButton!
    
    
    var isOpenFilterView            : Bool! = false
    var arrFeed                     : Array<Feed> = []
    var coordinate                  : CLLocationCoordinate2D?
    var selectedPinIndex            : Int = 0
    var colletionViewHeight         : CGFloat = 310.0
    var isPublicSelected            : Bool! = true
    var isSearchApplied             : Bool! = false
    var totalRecordCount            : Int = 0
    var pageNumer                   : Int = 1

    //let floaty = Floaty()
    
    //Searchbar
    @IBOutlet weak var btnMapView   : UIButton!
    @IBOutlet weak var btnCamera    : UIButton!
    @IBOutlet var viewRight         : UIView!
    @IBOutlet var viewLeft          : UIView!
    @IBOutlet weak var tblUserList  : UITableView!
    
    var searchBar: UISearchBar?
    var arrUserList = Array<UserList>()
    var filteredSuggestions = Array<UserList>()

    var objFilterHome  : SearchParams = SearchParams()
    
    var selectedCategories      : Array<Categories> = []
    var selectedFriends         : Array<Friends> = []
    var selectedCities         : Array<Cities> = []
    var refresher:UIRefreshControl!
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.shared.navigationController = self.navigationController
        self.viewDataNoFound.isHidden = true
        self.getFeed(FeedType.Public,userId:0,loadFromStart:true)
        self.setView()
        self.tblUserList?.tableFooterView = UIView(frame:.zero)
        AppUtility.shared.delegate = self
        
    }
  
    func setMapCameraToCurrentLocation() {
        var camera:GMSCameraPosition!
        if AppUtility.shared.currentLocation.coordinate.latitude != 0.0 &&
            AppUtility.shared.currentLocation.coordinate.longitude != 0.0 {
            camera = GMSCameraPosition.camera(withTarget: AppUtility.shared.currentLocation.coordinate, zoom:16)
            
        }
        else {
            camera = GMSCameraPosition.camera(withTarget: self.mapViewPlaces.camera.target, zoom:16)
        }
        self.mapViewPlaces.camera = camera
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.shared.delegate = self
        let floaty = Floaty()
        AppUtility.shared.addFloatingButton(to: self.view, floaty: floaty)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewWillDisappear(_ animated: Bool) {
       // let floaty = Floaty()
       // AppUtility.shared.removeFloatingButton(to: self.view, floaty: floaty)
        super.viewWillDisappear(true)
    }
    //viewDidLayoutSubviews
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.reloadData()
    }
    
    
    
    //MARK:- IBActions Methods
    
    func showMenuOptions(identifier: String) {
        if identifier == "HomeViewController" {
            return
        }
        
        let chatVC  = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        AppUtility.shared.navigationController?.pushViewController(chatVC!, animated: true)
    }
    
    @IBAction func btnFilterTapped(_ sender:Any){
    
//        if User.loggedInUser() == nil {
//            AppUtility.shared.isShowClosebutton = true
//            LoginViewController.open(rootViewController: self, completion: { (success) in
//                if success == true {
//                    self.filterData()
//                }
//            })
//        }
//        else {
            self.filterData()
//        }
    }
    
    func filterData() {
        let filterVC  = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController")  as! FilterViewController
        filterVC.objFilter = self.objFilterHome
        filterVC.delegate = self
        if self.selectedFriends.count > 0 {
            filterVC.selectedFriends = self.selectedFriends
        }
        if self.selectedCategories.count > 0 {
            filterVC.selectedCategories = self.selectedCategories
        }
        if self.selectedCities.count > 0 {
            filterVC.selectedCities = self.selectedCities
        }
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    
    @IBAction func btnLogout(_ sender:Any){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnMapTapped(_ sender:Any){
        
//        if User.loggedInUser() == nil {
//            AppUtility.shared.isShowClosebutton = true
//            LoginViewController.open(rootViewController: self, completion: { (success) in
//                if success == true {
//                    self.openMap()
//                }
//            })
//        }
//        else {
            self.openMap()
//        }
    }

    func openMap() {
        if self.btnMapView.isSelected == true {
            
            self.CollectionViewPost.scrollToTop(animated: false)
            self.viewFloating.isHidden = false
            self.CollectionViewPost.isPagingEnabled = false
            self.btnMapView.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                
                self.mapViewPlaces.isHidden = true
                self.CollectionViewPost.frame = CGRect(x: 0, y: 64, width: SF.screenWidth, height: SF.screenHeight-64)
                if let layout = self.CollectionViewPost.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .vertical
                }
                self.reloadData()
                
            }, completion: {
                (value: Bool) in
            })
            
        } else {
            
            self.setMapPinAtCenter(0)
            self.viewFloating.isHidden = true
            self.btnMapView.isSelected = true
            self.CollectionViewPost.isPagingEnabled = true
            UIView.animate(withDuration: 0.5, animations: {
                if let layout = self.CollectionViewPost.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .horizontal
                    layout.minimumInteritemSpacing = 0
                    layout.minimumLineSpacing = 0
                    
                }
                self.mapViewPlaces.frame = CGRect(x: 0, y: 0, w: self.view.width, h: SF.screenHeight - self.colletionViewHeight + 20)
                self.mapViewPlaces.isHidden = false
                //self.CollectionViewPost.top = self.mapViewPlaces.bottom
                self.CollectionViewPost.y = SF.screenHeight - self.colletionViewHeight + 20
                self.CollectionViewPost.height = self.colletionViewHeight //(SF.screenHeight - self.mapViewPlaces.height)
                self.reloadData()
            }, completion: {
                (value: Bool) in
            })
        }
    }
    
    
    @IBAction func btnCameraTapped(_ sender:Any){
        
        if User.loggedInUser() == nil {
            AppUtility.shared.isShowClosebutton = true
            LoginViewController.open(rootViewController: self, completion: { (success) in
                if success == true {
                    self.showCamera()
                }
            })
        } else{
            self.showCamera()
        }

    }
    
    func showCamera (){
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.openAddPostViewController(image, video: videoURL ,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
        
    }
    

    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {


        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostViewController") as! AddPostViewController
        if video != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video])
        }
        else if image != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image])
        }
        else{
            
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }
    
  

//MARK:- Collectionview Delegate DataSource

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
//        let feed = self.arrFeed[indexPath.row]
//        return CGSize(width: self.CollectionViewPost.width, height: feed.commentCount == 0 ? 407.5 : 453.0)
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let feed = self.arrFeed[indexPath.row]
        if self.btnMapView.isSelected == true {
            return CGSize(width: self.CollectionViewPost.width, height: self.colletionViewHeight)
            
            
        } else {
            
            var dynHeight : CGFloat = feed.commentCount == 0 ? 395 : 435.0
            let font = UIFont(name: kPTSansRegularFontName, size: 14)!
            var descHeight: CGFloat = 0.0
            if self.arrFeed[indexPath.row].readMore == true {
                //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVCCell", for: indexPath) as! HomeVCCell
                //if let cell:HomeVCCell = self.CollectionViewPost.cellForItem(at: indexPath) as? HomeVCCell {
                    var descCountry = ""
                    if let desc = self.arrFeed[indexPath.row].desc, !desc.isEmpty {
                        descCountry.append(desc)
                    }
                    descHeight = descCountry.size(collectionView.width - 40, font: font, lineBreakMode: .byWordWrapping).height
                    print("descHeight : ",descHeight)
                    
//                } else {
//                    return CGSize(width: self.CollectionViewPost.width, height: dynHeight)
//                }
                
            } else {
                descHeight = font.lineHeight * 2
            }
            dynHeight = dynHeight + descHeight
            return CGSize(width: self.CollectionViewPost.width, height: dynHeight)
        }
    }
 
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVCCell", for: indexPath) as! HomeVCCell
        cell.viewFooterContent.isHidden = (self.btnMapView.isSelected == true) ? true : false
        cell.viewEnteComment.isHidden = (self.btnMapView.isSelected == true) ? true : false
        
        
        let feed = self.arrFeed[indexPath.row]
        if self.btnMapView.isSelected == true{
            //cell.viewContent.setCornerRadius(radius: 0)
            cell.viewContent.backgroundColor = UIColor.clear
        }
        else{
            //cell.viewContent.setCornerRadius(radius: 20.0)
            cell.viewContent.backgroundColor = UIColor.white
        }
        cell.feed = feed
        cell.viewEnteComment.addTapGesture { (gesture) in
            //ISMessages.show("Comming Soon", type: .warning)
            let commentVC  = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController")  as! CommentViewController
            commentVC.objFeed = feed
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        cell.btnChat.addTapGesture { (gesture) in
            //ISMessages.show("Comming Soon", type: .warning)
            let commentVC  = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController")  as! CommentViewController
            commentVC.objFeed = feed
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        
        cell.btnMoreOption.tag = indexPath.row
        //cell.btnMoreOption.addTarget(self, action: #selector(self.btnMorePressed(sender:)), for: .touchUpInside)
        cell.selectDelegate = self
        
        cell.layoutIfNeeded()
        return cell
        
    }
    

    
//    @objc func btnMorePressed(sender: UIButton) {
//        self.CollectionViewPost.reloadData()
//    }
    /*
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    */
    
    // Called before the cell is displayed
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if ((self.arrFeed.count)-1) == indexPath.row {
            if self.arrFeed.count < self.totalRecordCount {
                self.getFeed(self.isPublicSelected ? FeedType.Public :FeedType.Friends, userId: 0,loadFromStart: false)
            }
        }
        
        if btnMapView.isSelected == true{
            self.selectedPinIndex = indexPath.row
            self.setMapPinAtCenter(self.selectedPinIndex)

        }
        
    }

    //MARK: - Webservice Call

    

    func getFeed (_ withType : FeedType , userId : Int, isFilter : Bool? =
        false ,loadFromStart:Bool){
        
        
        if loadFromStart == true {
            self.pageNumer = 1
        }

        
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "feed_type":withType.rawValue ,
                                                   //"owner_id" : userId == 0 ? "" : userId
                                                    "owner_id" : self.objFilterHome.owner_id!,
                                                    "radius" : self.objFilterHome.radius!,
                                                    "latitude" : self.objFilterHome.latitude!,
                                                    "longitude" : self.objFilterHome.longitude!,
                                                    "city" : self.objFilterHome.city!,
                                                    "category_id" : self.objFilterHome.category_id!,
                                                    "keyword" : self.objFilterHome.keyword!,
                                                    "from_date" : self.objFilterHome.from_date!,
                                                    "to_date" : self.objFilterHome.to_date!,
                                                    "page":self.pageNumer
            
            

                                                ]
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.GET_FEED, parameters: dictParam , completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                self.totalRecordCount = dictData["total_count"] as? Int ?? 0
                
                // Remove Previous Objects of first page
                if self.pageNumer == 1 {
                    self.arrFeed.removeAll(keepingCapacity: false)
                    
                }

                for dict in arrData {
                    if let feed =  Mapper<Feed>().map(JSON: dict) {
                        feed.marker.map = self.mapViewPlaces
                        self.arrFeed.append(feed)
                    }
                }
                
                if self.arrFeed.count > 0 {
                    self.viewDataNoFound.isHidden = true
                } else {
                    self.viewDataNoFound.isHidden = false
                }
                self.reloadData()
                if self.pageNumer == 1{
                    self.CollectionViewPost.scrollToTop(animated: true)
                }
                
                self.setMapPinAtCenter(self.selectedPinIndex)
                
                // Increase Page Counter
                self.pageNumer = self.pageNumer + 1;

                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    
    //MARK:- Other Methods
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        //imageView.frame.size.width = 100;
        //imageView.frame.size.height = 45;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView

    }
    /*
     [self.collectionView layoutIfNeeded] immediately after calling [self.collectionView reloadData] or [self.collectionView.collectionViewLayout invalidateLayout].
     */
    func reloadData() {
        self.CollectionViewPost.layoutIfNeeded()
        self.CollectionViewPost.reloadData()
        self.CollectionViewPost.collectionViewLayout.invalidateLayout()
    }
    
    func getImageUrlArr(index : Int) -> [String]{
        
        var imgArr = [String]()
        if self.arrFeed.count > 0 {
            for i in 0..<(self.arrFeed[index].mediaData.count) {
                if let imgUrl = self.arrFeed[index].mediaData[i].photo, !imgUrl.isEmpty {
                    imgArr.append(imgUrl)
                }
            }
        }
        return imgArr
    }
    
    func showController(controllerName: String?){
        
        let filterVC  = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController")
        self.navigationController?.pushViewController(filterVC!, animated: true)
    }
    
    func setView() {
        
        self.addNavigationShadow()
        
        AppUtility.shared.isMenuPadding = false
        self.refresher = UIRefreshControl()
        self.CollectionViewPost!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.CollectionViewPost!.addSubview(refresher)
       
        
        
        self.addLogo()
        self.tblUserList.isHidden = true
        self.tblUserList?.tableFooterView = UIView(frame:.zero)
        self.searchBar = UISearchBar()
        searchBar?.showsCancelButton = true
        searchBar?.delegate = self
        searchBar?.frame = CGRect(x: 0, y:0, width: self.view.width, height: 44)
        searchBar?.barStyle = .black

        
        self.CollectionViewPost.register(UINib(nibName: "HomeVCCell", bundle: nil), forCellWithReuseIdentifier: "HomeVCCell")
        self.btnMapView.isSelected = false
        self.mapViewPlaces.isHidden = true
        if let layout = self.CollectionViewPost.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
        
        self.lblPublic.textColor = K.Color.black
        self.lblFriend.textColor = K.Color.lightGray
        
        lblFriend.addTapGesture { (gesture) in
            if self.isPublicSelected == true {
                self.lblFriend.textColor = K.Color.black
                self.lblPublic.textColor = K.Color.lightGray
                self.getFeed(FeedType.Friends,userId: 0,loadFromStart:true)
                self.isPublicSelected = false
            }
        }
        
        lblPublic.addTapGesture { (gesture) in
            if self.isPublicSelected == false {
                self.lblPublic.textColor = K.Color.black
                self.lblFriend.textColor = K.Color.lightGray
                self.getFeed(FeedType.Public,userId: 0,loadFromStart:true)
                self.isPublicSelected = true
            }
            
        }
        
        mapViewPlaces.isMyLocationEnabled = true
        mapViewPlaces.delegate = self
        self.mapViewPlaces.bringSubview(toFront: self.viewFloating)
        self.coordinate =  AppUtility.shared.currentLocation.coordinate
        setMapCameraToCurrentLocation()
    }
    
    func addNavigationShadow() {
        
        AppUtility.shared.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        AppUtility.shared.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        AppUtility.shared.navigationController?.navigationBar.layer.shadowRadius = 4.0
        AppUtility.shared.navigationController?.navigationBar.layer.shadowOpacity = 0.5
        AppUtility.shared.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    @objc func loadData() {
     
        self.getFeed(self.isPublicSelected ? FeedType.Public :FeedType.Friends, userId: (User.loggedInUser()?.ID)!, loadFromStart: true)
        
        stopRefresher()         //Call this to stop refresher
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    // Set MapView Pin in Center & Selected.
    
    func setMapPinAtCenter(_ selectedIndex: Int) -> Void {
        
        var placeCoordinate: CLLocationCoordinate2D?
  
            if self.arrFeed.count > selectedIndex {
                self.selectedPinIndex = selectedIndex
                self.arrFeed.forEachEnumerated({ (index, place) in
                    let marker = place.marker
                    let customMarker = CustomMarker.loadFromNib()
                    customMarker.IBImgaeUser.setCornerRadius(radius: customMarker.IBImgaeUser.width/2)
                    customMarker.IBImgaeUser.clipsToBounds = true
                    if selectedIndex == index{
                        //marker.icon = #imageLiteral(resourceName: "ic_red_pin")
                        customMarker.IBImgaeUser.setImageWithURL(self.arrFeed[selectedIndex].profilePhoto, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
                        customMarker.imgPin.image = #imageLiteral(resourceName: "ic_red_pin")
                        marker.iconView = customMarker
                        
                    }
                    else{
                        //marker.icon = #imageLiteral(resourceName: "ic_black_pin")
                        
                        customMarker.IBImgaeUser.setImageWithURL(self.arrFeed[index].profilePhoto, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
                        customMarker.imgPin.image = #imageLiteral(resourceName: "ic_black_pin")
                        marker.iconView = customMarker
                    }
                })
                placeCoordinate =  self.arrFeed[selectedIndex].coordinate
            }
        
       
        
        //Update Camera position to Current Selected Place
        if placeCoordinate != nil{
            let cameraPosition = GMSCameraPosition.camera(withTarget: placeCoordinate!, zoom:self.mapViewPlaces.camera.zoom)
            self.mapViewPlaces.animate(to: cameraPosition)
        }
    }
    
    //MARK: - Search User
    
    @IBAction func btnSearchTapped(_ sender:Any){
        
//        if User.loggedInUser() == nil {
//            AppUtility.shared.isShowClosebutton = true
//            LoginViewController.open(rootViewController: self, completion: { (success) in
//                if success == true {
//                    self.fullSearch()
//                }
//            })
//        }
//        else {
            self.fullSearch()
//        }
    }
    
    func fullSearch() {
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar?.alpha = 0.0
            self.viewFloating.isHidden = true
        }) { (finished) in
            self.navigationItem.leftBarButtonItem = nil
            //self.navigationItem.rightBarButtonItem = nil
            self.btnNavSearch.isHidden = true
            self.btnNavSearch.size.width = 0
            self.viewRight.width = self.btnNavFilter.size.width * 2
            self.navigationItem.titleView = self.searchBar
            let backButton = UIBarButtonItem(title: "", style: .plain, target: self.navigationController, action: nil)
            self.navigationItem.leftBarButtonItem = backButton
            
            self.searchBar?.alpha = 0.0
            //self.viewRight.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.searchBar?.alpha = 1.0
            }, completion: { (completed) in
                self.searchBar?.becomeFirstResponder()
                
            })
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar?.alpha = 0.0
            self.viewRight.isHidden = false
            self.viewFloating.isHidden = false
            if self.isSearchApplied {
                self.getFeed(self.isPublicSelected ? FeedType.Public :FeedType.Friends, userId: 0,loadFromStart:true)
                self.isSearchApplied = false
            }
        }) { (finished) in
            let RightNavBarButton = UIBarButtonItem(customView:self.viewRight)
            self.btnNavSearch.isHidden = false
            self.btnNavSearch.size.width = self.btnNavFilter.size.width
            self.viewRight.width = self.btnNavFilter.size.width * 3
            
            self.navigationItem.rightBarButtonItem = RightNavBarButton
            let LeftNavBarButton = UIBarButtonItem(customView:self.viewLeft)
            self.navigationItem.leftBarButtonItem = LeftNavBarButton
            self.addLogo()
            self.searchBar?.text = ""
            self.arrUserList = []
            self.tblUserList.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text \(searchBar.text!)")
        if (searchBar.text?.isValid)!{
            self.getUserList(keyword: searchBar.text)
        }
        else{
            self.arrUserList = []
            self.tblUserList.reloadData()
        }
        
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        return true
    }

    
    
    func getUserList(keyword:String?) {
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.GET_USERS_LIST, parameters: ["keyword":keyword ?? ""]) { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrUserList = []
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                print(arrData)
                for dict in arrData {
                    if let user =  Mapper<UserList>().map(JSON: dict) {
                        
                        self.arrUserList.append(user)
                    }
                }
                
                self.tblUserList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            
            
        }
    }
    
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrUserList.count > 0{
            self.tblUserList.isHidden = false
        }
        else{
            self.tblUserList.isHidden = true
        }
        
        return arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let user = arrUserList[indexPath.row]
        cell.textLabel?.text = user.name
        cell.textLabel?.textColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let user = arrUserList[indexPath.row]
        self.searchBar?.text = user.name
        self.getFeed(self.isPublicSelected ? FeedType.Public :FeedType.Friends, userId: user.ID, loadFromStart: true)
        self.arrUserList = []
        self.tblUserList.reloadData()
        self.isSearchApplied = true
        self.dismissKeyboard()
    }

}

//MARKselected Options
extension HomeViewController : FilterViewControllerDelegate {

    
    func didFinishFilter(filterItesm:AnyObject, selectedFriend:AnyObject, selectedCategory:AnyObject, selectedCity:AnyObject) {
        
        if let filter = filterItesm as? SearchParams {
            self.objFilterHome = filter
            if self.isPublicSelected == true {
                self.getFeed(FeedType.Public, userId: 0, isFilter: true,loadFromStart: true)
            } else {
                self.getFeed(FeedType.Friends, userId: 0, isFilter: true,loadFromStart: true)
            }
        }
        if let selectedFri = selectedFriend as? [Friends] {
            self.selectedFriends = selectedFri
        }
        if let selectedCat = selectedCategory as? [Categories] {
            self.selectedCategories = selectedCat
        }
        if let selectedCity = selectedCity as? [Cities] {
            self.selectedCities = selectedCity
        }
    }
    
    func didFinishFilterReset() {
        self.objFilterHome              = SearchParams()
        self.objFilterHome.isCleared    = true
        self.selectedCategories     = []
        self.selectedFriends        = []
        self.selectedCities         = []
        self.totalRecordCount       = 0
        self.pageNumer              = 1
        
        if self.isPublicSelected == true {
            self.getFeed(FeedType.Public,userId:0,loadFromStart:true)
        } else {
            self.getFeed(FeedType.Friends,userId:0,loadFromStart:true)
        }
    }
}

//MARK: - TextField Delegate Methods
extension HomeViewController : HomeVCCellDelegate {
    
    func didFinishSelection() {
        self.reloadData()
    }
}
