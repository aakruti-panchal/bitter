//
//  FilterViewController.swift
//  BitterWorld
//
//  Created by  " " on 30/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol FilterViewControllerDelegate:class {
    @objc optional func didFinishFilter(filterItesm:AnyObject, selectedFriend:AnyObject, selectedCategory:AnyObject, selectedCity:AnyObject)
    @objc optional func didFinishFilterReset()
}

class FilterViewController: UIViewController {

    weak var delegate:FilterViewControllerDelegate?
    
    @IBOutlet weak var btnFriend         : UIButton!
    @IBOutlet weak var btnCategory       : UIButton!
    @IBOutlet weak var btnCity           : UIButton!
    @IBOutlet weak var btnApply          : UIButton!
    
    @IBOutlet weak var txtToDate         : CustomTextField!
    @IBOutlet weak var txtFromDate       : CustomTextField!

    @IBOutlet weak var lblFriend         : UILabel!
    @IBOutlet weak var lblCategory       : UILabel!
    @IBOutlet weak var lblCity           : UILabel!
    
    //@IBOutlet weak var sliderMiles: UISlider!
    @IBOutlet weak var sliderMiles: NYSliderPopover!
    @IBOutlet weak var viewSliderMiles: UIView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var viewNearBy: UIView!
    
    @IBOutlet weak var lblMinMiles: UILabel!
    @IBOutlet weak var lblMaxMiles: UILabel!
    
    
    var objFilter : SearchParams = SearchParams()
    var isAllCleared : Bool? = false
    
    var selectedCategories           : Array<Categories> = []
    var selectedFriends              : Array<Friends> = []
    var selectedCities               : Array<Cities> = []
    
//MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLogo()
        //borderWidth:CGFloat? = 0,borderColor:UIColor? = UIColor.clear,
        Common().addshadow(View: self.viewSliderMiles, shadowRadius: 5.0, shadowColor: UIColor.white, shadowOpacity: 1.0, borderWidth: 0.5, borderColor : UIColor.lightGray,cornerRadius : 3.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        
        self.sliderMiles.popover.textLabel.text = ""
        
        self.sliderMiles.popover.clipsToBounds          = true
        self.sliderMiles.popover.backgroundColor        = UIColor.white
        Common().addshadow(View: self.sliderMiles.popover, shadowRadius: 5.0, shadowColor: UIColor.white, shadowOpacity: 1.0, borderWidth: 0.5, borderColor : UIColor.lightGray,cornerRadius : 3.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        self.sliderMiles.popover.isHidden = true
        
        /*
        self.sliderMiles.popover.borderWidth        = 0.5
        self.sliderMiles.popover.borderColor        = K.Color.lightGray
        self.sliderMiles.popover.shadowRadius       = 5.0
        self.sliderMiles.popover.clipsToBounds          = true
        self.sliderMiles.popover.layer.masksToBounds    = false
        self.sliderMiles.popover.backgroundColor    = UIColor.white
        self.sliderMiles.popover.shadowColor        = UIColor.white
        self.sliderMiles.popover.shadowOffset       = CGSize(width: 1.0, height: 1.0)
        self.sliderMiles.popover.shadowOpacity      = 1.0
        */

        self.setData()
        
        self.sliderMiles.setThumbImage(UIImage(named: "ic_slider"), for: .normal)
        //self.sliderMiles.height = 30.0
        
        //scrollView.contentHeight = self.viewNearBy.bottom + 100
        scrollView.contentSizeToFit()
        
//        sliderMiles.minimumTrackTintColor = .red
//        sliderMiles.maximumTrackTintColor = .gray
//        sliderMiles.setThumbImage(UIImage(named:"ic_slider"), for: .normal)
//        sliderMiles.setThumbImage(UIImage(named:"ic_slider"), for: .highlighted)
        
//        sliderMiles.trackRect(forBounds: CGRect(x: 0, y: 0, w: 100, h: 20))
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        btnApply.addShadow(offset: CGSize(width: 0.0, height: 0.0), radius: 4.0, color: K.Color.postbuttonshadowColor, opacity: 1.0, cornerRadius: 0)
    }
}

//MARK:- IBActions Methods
extension FilterViewController {
    
    @IBAction func btnBackTapped(_ sender:Any){
        self.navigationController?.popViewController()
    }
    
    @IBAction func btnFriendTapped (_ sender: UIButton){
        
        if User.loggedInUser() == nil {
            AppUtility.shared.isShowClosebutton = true
            LoginViewController.open(rootViewController: self, completion: { (success) in
                if success == true {
                    let catVC = SF.storyBoard.instantiateViewController(withIdentifier: "FilterCategoryViewController")  as! FilterCategoryViewController
                    catVC.selectedOption    = .Friends
                    catVC.delegate          = self
                    catVC.selectedFriends   = self.selectedFriends
                    self.navigationController?.pushViewController(catVC, animated: true)
                }
            })
        }
        else {
            let catVC = SF.storyBoard.instantiateViewController(withIdentifier: "FilterCategoryViewController")  as! FilterCategoryViewController
            catVC.selectedOption    = .Friends
            catVC.delegate          = self
            catVC.selectedFriends   = self.selectedFriends
            self.navigationController?.pushViewController(catVC, animated: true)
        }

    }
    @IBAction func btnCategoryTapped (_ sender: UIButton){
        
        let catVC = SF.storyBoard.instantiateViewController(withIdentifier: "FilterCategoryViewController")  as! FilterCategoryViewController
        catVC.selectedOption        = .Categories
        catVC.delegate              = self
        catVC.selectedCategories    = self.selectedCategories
        self.navigationController?.pushViewController(catVC, animated: true)

    }
    @IBAction func btnCityTapped (_ sender: UIButton){

        let catVC = SF.storyBoard.instantiateViewController(withIdentifier: "FilterCategoryViewController")  as! FilterCategoryViewController
        catVC.selectedOption        = .Cities
        catVC.delegate              = self
        catVC.selectedCities        = self.selectedCities
        self.navigationController?.pushViewController(catVC, animated: true)
    }

    @IBAction func btnApplyTapped (_ sender: UIButton){

//        if !self.btnFriend.isSelected && !self.btnCategory.isSelected && !self.btnCity.isSelected && (self.txtFromDate.text?.isEmpty)! && (self.txtToDate.text?.isEmpty)! {
//            self.isAllCleared = true
//        }
        
        if self.isAllCleared! {
            
            self.selectedCategories         = []
            self.selectedFriends            = []
            self.selectedCities             = []
            
            self.objFilter                  = SearchParams()
            self.btnFriend.isSelected       = false
            self.lblFriend.text             = ""
            self.txtFromDate.text           = ""
            self.txtToDate.text             = ""
            self.btnCategory.isSelected     = false
            self.lblCategory.text           = ""
            self.btnCity.isSelected         = false
            self.lblCity.text               = ""
            
            self.sliderMiles.setValue(self.sliderMiles.minimumValue)
            self.delegate?.didFinishFilterReset!()
            self.navigationController?.popViewController()
            
        } else {
            
            var strSelectedFriends          = ""
            var strSelectedCategories       = ""
            var strSelectedCities           = ""
            
            for i in 0..<(self.selectedFriends.count) {
                if let id = self.selectedFriends[i].user_id {
                    if strSelectedFriends.isEmpty {
                        strSelectedFriends.append("\(id)")
                    } else {
                        strSelectedFriends.append(",\(id)")
                    }
                }
            }
            for i in 0..<(self.selectedCategories.count) {
                if let id = self.selectedCategories[i].ID {
                    if strSelectedCategories.isEmpty {
                        strSelectedCategories.append("\(id)")
                    } else {
                        strSelectedCategories.append(",\(id)")
                    }
                }
            }
            for i in 0..<(self.selectedCities.count) {
                if let city = self.selectedCities[i].city {
                    if strSelectedCities.isEmpty {
                        strSelectedCities.append("\(city)")
                    } else {
                        strSelectedCities.append(",\(city)")
                    }
                }
            }
            if !strSelectedFriends.isEmpty {
                self.objFilter.owner_id = strSelectedFriends
            }
            if !strSelectedCategories.isEmpty {
                self.objFilter.category_id = strSelectedCategories
            }
            if !strSelectedCities.isEmpty {
                self.objFilter.city = strSelectedCities
            }
            if let sDate = self.txtFromDate.text {
                self.objFilter.from_date = sDate
            }
            if let eDate = self.txtToDate.text {
                self.objFilter.to_date = eDate
            }
            self.objFilter.isCleared = false
            delegate?.didFinishFilter!(filterItesm: self.objFilter as AnyObject,selectedFriend: self.selectedFriends as AnyObject,selectedCategory: self.selectedCategories as AnyObject, selectedCity: self.selectedCities as AnyObject)
            self.navigationController?.popViewController()
        }
        
    }
    
    @IBAction func btnResetAllTapped(_ sender:Any) {
        self.reSetData()
    }
}

//MARK:- Other Methods
extension FilterViewController {
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        //imageView.frame.size.width = 100;
        //imageView.frame.size.height = 45;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        
    }
    
    func setData() {
        
        self.lblMinMiles.textColor      = K.Color.lightGray
        self.lblMaxMiles.textColor      = K.Color.lightGray
        
        if let friendIds = self.objFilter.friends, !friendIds.isEmpty {
            self.lblFriend.text = friendIds
            self.btnFriend.isSelected = true
        }
        if let sDate = self.objFilter.from_date, !sDate.isEmpty {
            self.txtFromDate.text = sDate
        } else {
            self.txtFromDate.text = "" //(self.objFilter.isCleared)! ? "" : DateHelper().getCurrentDateTime(dateFormat: K.DateFormat.yyyyMMdd)
        }
        if let eDate = self.objFilter.to_date, !eDate.isEmpty {
            self.txtToDate.text = eDate
        } else {
            //self.txtToDate.text = DateHelper().getCurrentDateTime(dateFormat: K.DateFormat.yyyyMMdd)
            self.txtToDate.text = "" //(self.objFilter.isCleared)! ? "" : DateHelper().getCurrentDateTime(dateFormat: K.DateFormat.yyyyMMdd)
        }
        if let category = self.objFilter.categories, !category.isEmpty {
            self.lblCategory.text = category
            self.btnCategory.isSelected = true
        }
        if let citis = self.objFilter.city, !citis.isEmpty {
            self.lblCity.text = citis
            self.btnCity.isSelected = true
        }
        
        if let radius = self.objFilter.radius, radius > 0 {
            self.sliderMiles.value = Float(radius)
        }
        self.objFilter.latitude     = String(describing: AppUtility().locationManager.location?.coordinate.latitude)
        self.objFilter.longitude    = String(describing: AppUtility().locationManager.location?.coordinate.longitude)
        
    }
    
    func reSetData() {
        
        self.isAllCleared = true
        self.btnFriend.isSelected       = false
        self.lblFriend.text             = ""
        self.txtFromDate.text           = ""
        self.txtToDate.text             = ""
        self.btnCategory.isSelected     = false
        self.lblCategory.text           = ""
        self.btnCity.isSelected         = false
        self.lblCity.text               = ""
        
        self.sliderMiles.value = self.sliderMiles.minimumValue
        self.sliderMiles.popover.textLabel.text = ""
        self.sliderMiles.popover.isHidden = true
        //Common().addshadow(View: self.sliderMiles.popover.textLabel, shadowRadius: 2.0, shadowColor: UIColor.darkGray, shadowOpacity: 1.0, shadowOffset: CGSize(width: 1.0, height: 1.0))
        
    }

    func showDatePicker(textField:UITextField) {
        let selectAction = RMAction<UIDatePicker>(title: "Select", style: RMActionStyle.done) { controller in
            let date = controller.contentView.date
            let dateString = date.toString(format: K.DateFormat.yyyyMMdd)
            textField.text = dateString
            // call Filter webservice
            if (self.txtFromDate.text?.isValid)! && (self.txtToDate.text?.isValid)! {
                
                let fromDate = self.txtFromDate.text?.dateWith(K.DateFormat.birthDate) as! Date
                let toDate = self.txtToDate.text?.dateWith(K.DateFormat.birthDate) as! Date
                if fromDate > toDate  {
                    ISMessages.show("Please select proper date", type: .warning)
                }
                else{
                    
                }
            }
        }
        let cancelAction = RMAction<UIDatePicker>(title: "Cancel", style: RMActionStyle.cancel) { _ in
        }
        
        let datePicker = RMDateSelectionViewController(style: RMActionControllerStyle.white, select: selectAction , andCancel: cancelAction)
        datePicker?.disableBouncingEffects = true
        datePicker?.disableMotionEffects = true
        datePicker?.disableBlurEffects = true
        datePicker?.datePicker.datePickerMode = .date
        datePicker?.datePicker.maximumDate = NSDate() as Date
        self.navigationController?.present(datePicker!, animated: true, completion: nil)
        //AppUtility.shared.navigationController?.present(datePicker!, animated: true, completion: nil)
        
    }
}

//MARK:- Selected Options
extension FilterViewController : FilterCategoryViewControllerDelegate {
    
    func didFinishSelection(selectedItesm:AnyObject, selectedOption : Int) {
        
        
        
        switch selectedOption {
        case 1:
            if let selectedFri = selectedItesm as? [Friends] {
                self.selectedFriends = selectedFri
                
                var strSelectedFriends = ""
                for i in 0..<(self.selectedFriends.count) {
                    if let name = self.selectedFriends[i].first_name {
                        if strSelectedFriends.isEmpty {
                            strSelectedFriends.append("\(name)")
                        } else {
                            strSelectedFriends.append(", \(name)")
                        }
                    }
                }
                self.lblFriend.text = strSelectedFriends
                self.objFilter.friends = strSelectedFriends
            }
            self.btnFriend.isSelected = (self.selectedFriends.count > 0) ? true : false
            break
            
        case 2:
            if let selectedCat = selectedItesm as? [Categories] {
                self.selectedCategories = selectedCat
                
                var strSelectedCategories = ""
                for i in 0..<(self.selectedCategories.count) {
                    if let name = self.selectedCategories[i].name {
                        if strSelectedCategories.isEmpty {
                            strSelectedCategories.append("\(name)")
                        } else {
                            strSelectedCategories.append(", \(name)")
                        }
                    }
                }
                self.lblCategory.text = strSelectedCategories
                self.objFilter.categories = strSelectedCategories
            }
            self.btnCategory.isSelected = (self.selectedCategories.count > 0) ? true : false
            break
            
        case 3:

            if let selectedCity = selectedItesm as? [Cities] {
                self.selectedCities = selectedCity
                
                var strSelectedCities = ""
                for i in 0..<(self.selectedCities.count) {
                    if let city = self.selectedCities[i].city {
                        if strSelectedCities.isEmpty {
                            strSelectedCities.append("\(city)")
                        } else {
                            strSelectedCities.append(", \(city)")
                        }
                    }
                }
                self.lblCity.text = strSelectedCities
            }
            self.btnCity.isSelected = (self.selectedCities.count > 0) ? true : false
            break
            
        default:
            break
        }
    }
}

//MARK: - TextField Delegate Methods
extension FilterViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showDatePicker(textField: textField)
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:- Silder's Methods
extension FilterViewController {
    
    @IBAction func sliderChanged(_ sender: UISlider) {

        print(String(Int(sender.value)))
        self.objFilter.radius = Int(sender.value)
    }
    
//    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
//        let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
//        let sliderFrm : CGRect = slider .thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
//        return CGPoint(x: sliderFrm.origin.x + slider.frame.origin.x + 8, y: slider.frame.origin.y + 20)
//    }
}

class CustomSlider: UISlider {
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 5.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
}

