//
//  CommentViewController.swift
//  BitterWorld
//
//  Created by  " " on 09/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class CommentViewController: UIViewController , UITextViewDelegate {

    @IBOutlet weak var viewHeader               : UIView!
    @IBOutlet weak var imgUser                  : UIImageView!
    @IBOutlet weak var lblUserName              : UILabel!
    @IBOutlet weak var lblAddress               : UILabel!
    @IBOutlet weak var imgLocationIcon          : UIImageView!
    @IBOutlet weak var lblShopTitle             : UILabel!
    @IBOutlet weak var viewRatting              : FloatRatingView!
    
    @IBOutlet weak var tblCommentsList          : UITableView!
    @IBOutlet weak var viewFooter               : UIView!
    
    
    //@IBOutlet weak var viewFooterContent        : UIView!
    @IBOutlet var growingTextView               : RSKGrowingTextView!
    @IBOutlet weak var btnSendComment           : UIButton!
    
    var keyboardHeight: CGFloat             = 0.0
    private var isVisibleKeyboard           = true
    var arrComment : Array<Comment>         = []
    var objFeed : Feed                      = Feed()
    
    var totalRecordCount        : Int = 0
    var pageNumer               : Int = 1
    var isCallingGetMessage      : Bool = false
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.getCommentList(loadFromStart: true)
        self.setView()
        self.setData()
        
        self.viewFooter.borderWidth         = 1.0
        self.viewFooter.borderColor         = K.Color.lightGray
        self.viewFooter.cornerRadius        = 25.0
        
        self.growingTextView.text           = "Add a comment here"
        self.growingTextView.textColor      = UIColor.lightGray
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.registerForKeyboardNotifications()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterForKeyboardNotifications()
    }
}

//MARK:-  IBAction Methods
extension CommentViewController {
    
    @IBAction func btnBackTapped(_ sender:Any) {
        self.navigationController?.popViewController()
    }
    
    @IBAction func btnclickSendCommentTapped(_ sender: UIButton) {
        
        var commentTxt = self.growingTextView.text
        if (self.growingTextView.textColor == UIColor.lightGray) { commentTxt = "" }
        let trimmedString = commentTxt?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (trimmedString?.isEmpty)! {
            ISMessages.show("please enter comment!", type: .warning)
        } else {
            self.addComment(comment_text: trimmedString!)
        }
    }
}

//MARK:- Other Methods
extension CommentViewController {
    
    func setView() {
        //self.navigationController?.navigationBar.isTranslucent = false
        self.addLogo()
        self.configureInputBar()
        self.tblCommentsList.rowHeight              = UITableViewAutomaticDimension
        self.tblCommentsList.estimatedRowHeight     = 80
        self.tblCommentsList.tableFooterView        = UIView()
        
        self.imgUser.layer.masksToBounds    = false
        self.imgUser.roundSquareImage()
        
        self.lblUserName.textColor          = K.Color.white
        self.lblAddress.textColor           = K.Color.white
        self.lblShopTitle.textColor         = K.Color.white
        
        self.lblUserName.font               = UIFont(name: kPTSansRegularFontName, size: 15.21)
        self.lblAddress.font                = UIFont(name: kPTSansRegularFontName, size: 13.42)
        self.lblShopTitle.font              = UIFont(name: kPTSansRegularFontName, size: 13.5)
        
    }
    
    //MARK: - Comment Input view
    func configureInputBar() {
        growingTextView.heightChangeUserActionsBlock = { (oldHeight, newHeight) in
            self.viewFooter.height = self.growingTextView.height + 14
            self.viewFooter.y = self.view.height -  self.viewFooter.height - self.keyboardHeight
           
        }
    }
    
    func registerForKeyboardNotifications() {
        self.rsk_subscribeKeyboardWith(beforeWillShowOrHideAnimation: nil,
                                       willShowOrHideAnimation: { [unowned self] (keyboardRectEnd, duration, isShowing) -> Void in
                                        self.isVisibleKeyboard = isShowing
                                        self.keyboardHeight = keyboardRectEnd.height
                                        self.adjustContent(for: keyboardRectEnd)
            }, onComplete: { (finished, isShown) -> Void in
                self.isVisibleKeyboard = isShown
        }
        )
        
        self.rsk_subscribeKeyboard(willChangeFrameAnimation: { [unowned self] (keyboardRectEnd, duration) -> Void in
            self.keyboardHeight = keyboardRectEnd.height
            self.adjustContent(for: keyboardRectEnd)
            }, onComplete: nil)
    }
    private func adjustContent(for keyboardRect: CGRect) {
        let keyboardYPosition = self.isVisibleKeyboard ? keyboardRect.height : 0.0;
        tblCommentsList.contentInset.bottom = keyboardYPosition
        self.viewFooter.y = self.view.height - self.viewFooter.height - keyboardYPosition - 5
    }
    
    func unregisterForKeyboardNotifications() {
        self.rsk_unsubscribeKeyboard()
    }
    
    func reloadData() {

        self.tblCommentsList.reloadData()
        self.scrollToBottom(animated: true)
    }
    
     func scrollToBottom(animated: Bool) {
         if self.arrComment.count > 0 {
         self.tblCommentsList.scrollToRow(at: IndexPath(row: self.arrComment.count - 1, section: 0), at: .bottom, animated: animated)
         }
     }
    
    func setData() {
        
        if let imgUrl = self.objFeed.profilePhoto {
            imgUser.setImageWithURL(imgUrl, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        }
        if let name = self.objFeed.firstname {
            self.lblUserName.text = name
        }
        
        var cityCountry = ""
        if let city = self.objFeed.city, !city.isEmpty {
            cityCountry.append(city)
        }
        if let country = self.objFeed.country, !country.isEmpty {
            cityCountry.append(", \(country)")
        }
        if cityCountry.isEmpty {
            self.imgLocationIcon.isHidden = true
        } else {
            self.imgLocationIcon.isHidden = false
        }
        
        self.lblAddress.adjustsFontSizeToFitWidth = false
        let cityWidth = cityCountry.sizeWidth(self.lblAddress.size.height, font: UIFont(name: kPTSansRegularFontName, size: 15)!, lineBreakMode: .byWordWrapping).width
        
        if (self.objFeed.rating ?? 0 > 0) {
            
            //self.viewRating.isHidden = false
            self.viewRatting.rating = (self.objFeed.rating)!
            //let tmp = self.viewRating.left - self.imgUser.right
            let tmp = (SF.screenWidth - self.viewRatting.width) - self.imgUser.right
            if cityWidth > tmp {
                self.lblAddress.width = tmp - 40
            } else {
                self.lblAddress.width = cityWidth
            }
            
        } else {
            //viewRating.isHidden = true
            //lblAddress.width = self.viewHeader.width - (self.viewRating.width + 8.0)
            self.lblAddress.width = (cityWidth > (self.viewHeader.right - 80)) ? self.viewHeader.right - 80 : cityWidth
        }
        lblAddress.text = cityCountry
        self.imgLocationIcon.left = lblAddress.right
        if let cat = self.objFeed.categoryName {
            lblShopTitle.text = cat
        }
    }
    
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        //imageView.frame.size.width = 100;
        //imageView.frame.size.height = 45;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
}

//MARK: - TableView DataSource & Delegate Methods.
extension CommentViewController: UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:CommentVCCell? = tableView.dequeueReusableCell(withIdentifier: "CommentVCCell") as? CommentVCCell
        if (cell == nil) {
            let nib: NSArray = Bundle.main.loadNibNamed("CommentVCCell", owner: self, options: nil)! as NSArray
            cell = nib.object(at: 0) as? CommentVCCell
        }
        cell?.selectionStyle = .none
        cell?.initObj(objComment: self.arrComment[indexPath.row])
    
        return cell!
    }
    
}

//MARK:- Scroll View Delegate
extension CommentViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is UITableView, scrollView == self.tblCommentsList {
            let currentOffset = scrollView.contentOffset.y
            if currentOffset <= -20.0 && arrComment.count > 0 {
                let currentMinChatId = (arrComment.first?.feed_comment_id)!
                //if currentMinChatId > self.minChatId && self.isCallingGetMessage == false {
                if self.isCallingGetMessage == false {
                    self.isCallingGetMessage = true
                    self.pageNumer += 1;
                    self.getCommentList(loadFromStart: false)
                }
            }
        }
        else if scrollView is UITableView, scrollView == self.tblCommentsList {
            //self.imgCurrentChatIndicator.centerY = self.lastYOffset - scrollView.contentOffsetY
        }
    }
}
//MARK:- Service Call
extension CommentViewController {
    
    func getCommentList(loadFromStart:Bool) {
        
        if loadFromStart == true {
            self.pageNumer = 1
        }
        
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : self.objFeed.feedID, "page":self.pageNumer]
        //let dictParam :Dictionary<String, Any> = [ "user_id" : 22, "feed_id" : 19, "page":self.pageNumer]
        _ = WebClient.requestWithUrl(url: K.URL.GET_COMMENT_LIST, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData    = response as! Dictionary<String, Any>
                let arrData     = dictData["comment"] as! Array<Dictionary<String, Any>>
                self.totalRecordCount = dictData["comment_count"] as? Int ?? 0
                
                if self.pageNumer == 1 {
                    self.arrComment.removeAll(keepingCapacity: false)
                }
                for dict in arrData {
                    if let comment =  Mapper<Comment>().map(JSON: dict) {
                        self.arrComment.insertFirst(comment)
                        }
                }
                // Reset Flag
                self.isCallingGetMessage = false
                if loadFromStart == true {
                    self.reloadData()
                } else {
                    self.tblCommentsList.reloadData()
                }
                
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    
    func addComment(comment_text : String) {
        
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : self.objFeed.feedID, "comment_text" : comment_text]
        _ = WebClient.requestWithUrl(url: K.URL.ADD_COMMENT, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData    = response as! Dictionary<String, Any>
                let arrData     = dictData["data"] as! Dictionary<String, Any>
                if let feed =  Mapper<Comment>().map(JSON: arrData) {
                    self.arrComment.append(feed)
                }
                self.growingTextView.text   = ""
                self.reloadData()
                
//                self.totalRecordCount       = 0
//                self.pageNumer              = 1
//                self.growingTextView.text   = ""
//                self.getCommentList(loadFromStart: true)
                
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
}

//MARK : - UITextView delegate
extension CommentViewController {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView == self.growingTextView) {
            if (self.growingTextView.textColor == UIColor.lightGray) {
                self.growingTextView.text = "";
                self.growingTextView.textColor = K.Color.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.text)
        if (textView == growingTextView) {
            if  self.growingTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
                self.growingTextView.text = "Add a comment here"
                self.growingTextView.textColor = UIColor.lightGray
            }
        }
    }
}

