//
//  SendAudioViewController.h
//
//  Created on 08/12/16.
//
//

#import <UIKit/UIKit.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AVFoundation/AVFoundation.h>

#import "NSTimer+Addition.h"
#import "NSTimer+Blocks.h"

#import "EZAudio.h"
#import "CircleProgressView.h"

typedef enum {
    AudioPlayerStateNotStarted = 0,
    AudioPlayerStatePlaying = 1,
    AudioPlayerStatePause = 2,
    AudioPlayerStateStopped = 3,
    AudioPlayerStateNotDownloaded = 4,
} AudioPlayerState;

@interface SendAudioViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet UIView *viewRecordDailog;
    IBOutlet UILabel *lblRecordingTime;
    
    IBOutlet UIButton *btnRecord;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIButton *btnSend;
    
    AVAudioRecorder *audioRecorder;
    NSString *strAudioURL;
    NSString *strOriginalAudioURL;

    NSTimer *timerRecording;
    CGFloat audioDuration;

    BOOL isRecording;
    BOOL isSendAudio;
    
    // Audio Player Objects
    AVAudioPlayer *audioPlayer;
    AudioPlayerState playerState;
    
    IBOutlet UIView *viewAudio;
    //IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet CircleProgressView *circularProgress;
    IBOutlet UIProgressView *progressViewAudio;
    IBOutlet UILabel *lblAudioProgress;
    IBOutlet UIButton *btnAudioAction;
    IBOutlet UIImageView *imgCloseIcon;
}

@property (nonatomic,assign) BOOL enablePlayerMode;

@property (strong, nonatomic) NSString *strTrimmedAudioURL;
@property (strong, nonatomic) AVAssetExportSession *exportSession;

@property (nonatomic,strong) NSTimer *timer;
@property (nonatomic,strong) NSString *filePath;
@property (nonatomic,strong) NSString *localFilePath;


- (IBAction)audioActionClicked:(id)sender;

@end
