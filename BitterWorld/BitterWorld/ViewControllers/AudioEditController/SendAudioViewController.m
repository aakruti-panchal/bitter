//
//  SendAudioViewController.m
//
//  Created on 08/12/16.
//
//

#import <BitterWorld-Swift.h>
#import "SendAudioViewController.h"

@interface SendAudioViewController ()

@end

@implementation SendAudioViewController

@synthesize strTrimmedAudioURL;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    circularProgress.progressLayer.lineWidth = 1.0;
    
    viewAudio.layer.masksToBounds = NO;
    viewAudio.layer.shadowOffset = CGSizeMake(2, 2);
    viewAudio.layer.shadowRadius = 2.0;
    viewAudio.layer.shadowOpacity = 0.2;
    
    circularProgress.backgroundStrokeColor = [UIColor lightGrayColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    strOriginalAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"originalRecording.wav"];
    strAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"recording.wav"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL])
    {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    
    [self setupInitialDisplay];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
    [self.navigationController setNavigationBarHidden:YES];

}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear: true];
    [self.navigationController setNavigationBarHidden:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup Audio View

#pragma mark - Setup Methods

-(void)cancelEvents {
    [self stopAudioPlayer];
}

-(void)setupInitialDisplay {
    self.localFilePath = [NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),[_filePath lastPathComponent]];
    
    viewRecordDailog.hidden = _enablePlayerMode;
    viewAudio.hidden = !_enablePlayerMode;
    if (_enablePlayerMode) {
        isRecording = false;
        
        playerState = [[NSFileManager defaultManager] fileExistsAtPath:self.localFilePath] ? AudioPlayerStateNotStarted : AudioPlayerStateNotDownloaded;
        circularProgress.hidden = !(playerState == AudioPlayerStateNotDownloaded);
        
        if([[NSFileManager defaultManager] fileExistsAtPath:self.localFilePath]) {
            [self prepareToPlayAudio];
        }
        
        [self updateSlider];
        [btnAudioAction setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self audioStateString]]] forState:UIControlStateNormal];
        
//        __weak SendAudioViewController * weakSelf = self;
//        __weak UIButton * weakBtnCancel = btnCancel;
        
        
            // Commented
//        [imgCloseIcon addTapGestureWithTapNumber:1 action:^(UITapGestureRecognizer * _Nonnull gesture) {
//            [weakSelf btnCancelTapped:weakBtnCancel];
//        }];
        
        
        
        
        // Start Playing.
        [self audioActionClicked:btnAudioAction];
        
    }
    else {
        // Setup recording audio
        [self prepareToRecordAudio];
        
        // Start Recording
        isRecording = false;
        [self btnRecordingTapped:btnRecord];
    }
}


-(void)downloadFile {
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.localFilePath]) {
        //[activityIndicator startAnimating];
        btnAudioAction.enabled = false;
        
        [[NSURLSession sharedSession] downloadTaskWithURL:[NSURL URLWithString:self.filePath] completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                [[NSFileManager defaultManager] removeItemAtPath:self.localFilePath error:nil];
                self.localFilePath = nil;
                [ISMessages show:[error localizedDescription] type:ISAlertTypeError];
            }
            else {
                [[NSFileManager defaultManager] moveItemAtURL:location toURL:[NSURL fileURLWithPath:self.localFilePath] error:nil];
                btnAudioAction.enabled = true;
            }
            
            // Audio File.
            circularProgress.hidden = !error;
            playerState = error ?  AudioPlayerStateNotDownloaded : AudioPlayerStateNotStarted;
            [btnAudioAction setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self audioStateString]]] forState:UIControlStateNormal];
                
        }];
        
        /*
        [WebClientObjc downloadFile:self.filePath atPath:self.localFilePath downloadProgress:^(NSProgress *downloadProgress) {
            circularProgress.timeLimit = downloadProgress.totalUnitCount;
            circularProgress.elapsedTime = downloadProgress.completedUnitCount;
        } downloadFileCompletionBlock:^(NSURL *filePath, NSData *fileData, NSError *error) {
            //[activityIndicator stopAnimating];
            if (error) {
                [[NSFileManager defaultManager] removeItemAtPath:self.localFilePath error:nil];
                self.localFilePath = nil;
                [ISMessages show:[error localizedDescription] type:ISAlertTypeError];
            }
            else {
                btnAudioAction.enabled = true;
            }
            
            // Audio File.
            circularProgress.hidden = !error;
            playerState = error ?  AudioPlayerStateNotDownloaded : AudioPlayerStateNotStarted;
            [btnAudioAction setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self audioStateString]]] forState:UIControlStateNormal];
        }];
         */
    }
    
}

#pragma mark Audio Action Method
- (IBAction)audioActionClicked:(id)sender {
    circularProgress.hidden = true;
    if (playerState == AudioPlayerStateNotDownloaded) {
        circularProgress.hidden = false;
        [self downloadFile];
    }
    else if (playerState == AudioPlayerStateNotStarted){
        lblAudioProgress.text = [NSString stringWithFormat:@"%.2f",audioPlayer.duration];
        [self playAudioPlayer];
        [self addObserverToAudioPlayer];
    }
    else if (playerState == AudioPlayerStatePlaying) {
        [self pauseAudioPlayer];
    }
    else if (playerState == AudioPlayerStatePause) {
        [self playAudioPlayer];
        
    }
    else if (playerState == AudioPlayerStateStopped) {
        [self playAudioPlayer];
        
    }
    else{
        [self playAudioPlayer];
    }
}

#pragma mark Audio Observer Methods

-(void)addObserverToAudioPlayer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.50 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
    [self.timer fire];
    [self updateSlider];
}
-(void)removeAudioPlayerObserver{
    [self.timer invalidate];
}

-(void)updateSlider{
    
    CGFloat progress = audioPlayer.currentTime/audioPlayer.duration;
    progressViewAudio.progress = isnan(progress) ? 0.0 : progress;
    lblAudioProgress.text = [NSString stringWithFormat:@"%.2f/%.2f",audioPlayer.currentTime, audioPlayer.duration];
}

#pragma mark audio Life Cycle Methods

- (void)prepareToPlayAudio {
    if (self.localFilePath) {
        NSURL *url = [NSURL fileURLWithPath:self.localFilePath];
        NSError *error;
        if (!audioPlayer) {
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            if (error) {
                NSLog(@"Error in audioPlayer: %@",[error localizedDescription]);
            } else {
                audioPlayer.delegate = self;
                [audioPlayer prepareToPlay];
                
            }
        }
    }
}

- (void)playAudioPlayer {
    if (!audioPlayer) {
        [self prepareToPlayAudio];
    }
    
    if(audioPlayer) {
        playerState = AudioPlayerStatePlaying;
        [audioPlayer play];
        [self.timer resumeTimer];
    }
    [btnAudioAction setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self audioStateString]]] forState:UIControlStateNormal];
}

- (void)pauseAudioPlayer {
    if (playerState == AudioPlayerStatePlaying) {
        playerState = AudioPlayerStatePause;
        [audioPlayer pause];
        [self.timer pauseTimer];
        
        [btnAudioAction setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self audioStateString]]] forState:UIControlStateNormal];
    }
}

- (void)stopAudioPlayer {
    
    if (playerState == AudioPlayerStatePlaying) {
        playerState = AudioPlayerStateStopped;
        [audioPlayer stop];
        [btnAudioAction setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self audioStateString]]] forState:UIControlStateNormal];
        [self removeAudioPlayerObserver];
        
    }
}

#pragma mark - Audio Player Delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    progressViewAudio.progress = 1.0;
    lblAudioProgress.text = [NSString stringWithFormat:@"%.2f/%.2f",audioPlayer.duration, audioPlayer.duration];
    [self stopAudioPlayer];
    playerState = AudioPlayerStateNotStarted;
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    [self stopAudioPlayer];
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    [self pauseAudioPlayer];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player {
    [self playAudioPlayer];
}

#pragma mark Other Audio Methods
-(NSString*)audioStateString {
    switch (playerState) {
        case AudioPlayerStateNotDownloaded:
            return @"ic_play_btn_icon";
            break;
        case AudioPlayerStateNotStarted:
            return @"ic_play_btn_icon";
            break;
        case AudioPlayerStatePause:
            return @"ic_play_btn_icon";
            break;
        case AudioPlayerStatePlaying:
            return @"ic_pause_btn_icon";
            break;
        case AudioPlayerStateStopped:
            return @"ic_pause_btn_icon";
            break;
        default:
            break;
    }
}

#pragma mark - Buttons

- (IBAction)btnCancelTapped:(id)sender {
    isSendAudio = FALSE;
    if (isRecording) {
        [self btnRecordingTapped:btnRecord];
    }
    else {
        strTrimmedAudioURL = nil;
        //[UIApplication sharedApplication].delegate
        [AppUtility.shared.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    }
}

- (IBAction)btnSendTapped:(id)sender {
    isSendAudio = TRUE;
    if (isRecording) {
        [self btnRecordingTapped:btnRecord];
    }
    else {
        if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
            [self trimAudioWithCompletion:^{
//                [AppUtility.shared.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
            }];
        }
    }
}

- (IBAction)btnRecordingTapped:(id)sender {
    if (isRecording) {
        [self stopRecording];
    }
    else {
        [self startRecording];
    }
    isRecording = !isRecording;
    ((UIButton *)sender).selected = !isRecording;
}

#pragma mark - Other Methods

- (NSString *)timeFormatted:(CGFloat)interval isWithMinutes:(BOOL)isWithMinutes {
    unsigned long milliseconds = interval * 1000;
    unsigned long seconds = milliseconds / 1000;
    milliseconds %= 1000;
    unsigned long minutes = seconds / 60;
    seconds %= 60;
//    unsigned long hours = minutes / 60;
//    minutes %= 60;
    
    NSString *strMillisec = @(milliseconds).stringValue;
    if (strMillisec.length > 2) {
        strMillisec = [strMillisec substringToIndex:2];
    }
    
    if(isWithMinutes) {
        return [NSString stringWithFormat:@"%02ld:%02ld.%02ld",(long)minutes, (long)seconds, (long)[strMillisec integerValue]];
    }
    else {
        return [NSString stringWithFormat:@"%02ld.%02ld",(long)seconds, (long)[strMillisec integerValue]];
    }
}

- (void)trimAudioWithCompletion:(void(^)(void))completion {
    
    if(!strTrimmedAudioURL) {
        strTrimmedAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"trimmerRecording.wav"];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:strTrimmedAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strTrimmedAudioURL error:nil];
    }
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strAudioURL]];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        self.exportSession.outputURL = [NSURL fileURLWithPath:strTrimmedAudioURL];
        self.exportSession.outputFileType = AVFileTypeWAVE;
        
        CMTime start = CMTimeMakeWithSeconds(0, asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(CMTimeGetSeconds(asset.duration), asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export cancelled");
                    break;
                case AVAssetExportSessionStatusCompleted: {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error;
                        if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
                            [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
                        }
                        [[NSFileManager defaultManager] copyItemAtPath:strTrimmedAudioURL toPath:strAudioURL error:&error];
                        if(completion) {
                            completion();
                        }
                    });
                    break;
                }
                default:
                    NSLog(@"NONE");
                    break;
            }
        }];
    }
}

#pragma mark - Audio Recorder

- (void)prepareToRecordAudio {
    NSURL *outputFileURL = [NSURL fileURLWithPath:strOriginalAudioURL];
    if([[NSFileManager defaultManager] fileExistsAtPath:outputFileURL.path]) {
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
    }
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue :[NSNumber  numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:11025.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    
    // Initiate and prepare the recorder
    NSError *error = nil;
    audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    if (error) {
        NSLog(@"Audio Session Error: %@", error.localizedDescription);
    }
    else {
        audioRecorder.delegate = self;
        audioRecorder.meteringEnabled = YES;
        [audioRecorder prepareToRecord];
    }
}

- (void)startRecording {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    [audioRecorder record];
    [self startRcorderTimer];
}

- (void)stopRecording {
    [audioRecorder stop];
    [self stopRecorderTimer];
}

- (void)startRcorderTimer {
    __block NSDate *date = [NSDate date];
    timerRecording = [NSTimer scheduledTimerWithTimeInterval:0.1 block:^{
        audioDuration = [[NSDate date] timeIntervalSinceDate:date];
        [lblRecordingTime setText:[self timeFormatted:audioDuration isWithMinutes:TRUE]];
    } repeats:YES];
}

- (void)stopRecorderTimer {
    if (timerRecording) {
        [timerRecording invalidate];
        timerRecording = nil;
    }
}

#pragma mark - Audio Recorder Delegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag {
    // Stop recording
    [audioRecorder stop];
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strOriginalAudioURL]];
    audioDuration = CMTimeGetSeconds(asset.duration);
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:NO error:nil];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    NSError *error;
    [[NSFileManager defaultManager] copyItemAtPath:strOriginalAudioURL toPath:strAudioURL error:&error];
    
    // Send Audio File.
    strTrimmedAudioURL = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL] &&
        isSendAudio) {
        [self trimAudioWithCompletion:^{
//            [AppUtility.shared.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
        }];
    }
    else {
//        [AppUtility.shared.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    }
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error {
    
}


@end
