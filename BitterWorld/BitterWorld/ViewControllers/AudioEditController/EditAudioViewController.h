//
//  EditAudioViewController.h
//
//  Created on 08/12/16.
//
//

#import <UIKit/UIKit.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AVFoundation/AVFoundation.h>
#import "EZAudio.h"
typedef void(^AudioSelectionCompletion)(NSString *);

@interface EditAudioViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet UIView *viewStartRecord;
    IBOutlet UIView *viewRecording;
    IBOutlet UIView *viewEditRecording;
    IBOutlet UILabel *lblRecordingTime;
    
    IBOutlet UIView *viewAudioPlotContainer;
    IBOutlet EZAudioPlot *audioPlot;
    IBOutlet UIImageView *imgViewLeftThumb;
    IBOutlet UIImageView *imgViewRightThumb;
    IBOutlet UIImageView *imgViewAudioIndicatorLine;
    IBOutlet UILabel *lblAudioStartTime;
    IBOutlet UILabel *lblAudioEndTime;
    IBOutlet UILabel *lblAudioDuration;
    
    IBOutlet UIButton *btnCancel;
    IBOutlet UIButton *btnUpload;
    
    IBOutlet UIButton *btnReset;
    IBOutlet UIButton *btnPlayPause;
    IBOutlet UIButton *btnResetAudio;
    IBOutlet UIButton *btnEditTrimmedAudio;

    EZAudioFile *audioFile;
    AVAudioPlayer *audioPlayer;
    AVAudioRecorder *audioRecorder;
    NSString *strAudioURL;
    NSString *strOriginalAudioURL;

    NSTimer *timerRecording;
    NSTimer *timerAudioPlayer;
    
    BOOL videoTrimTimeChanged;
    CGFloat audioDuration;
    CGFloat audioTrimStartTime;
    CGFloat audioTrimEndTime;
    
    BOOL isPlaying;
    BOOL isAudioTrimmed;
}

@property (strong, nonatomic) NSString *strTrimmedAudioURL;
@property (nonatomic, assign) CGFloat audioDuration;

@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property (nonatomic, copy) AudioSelectionCompletion audioSelectionCompletion;


@end
