//
//  EditAudioViewController.m
//
//  Created on 08/12/16.
//
//

#import <BitterWorld-Swift.h>
#import "NSTimer+Blocks.h"
#import "EditAudioViewController.h"


CGFloat const minimumAudioDuration = 1;
CGFloat const maximumAudioDuration = 1;

@interface EditAudioViewController ()

@end

@implementation EditAudioViewController

@synthesize strTrimmedAudioURL;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [viewStartRecord setHidden:FALSE];
    [viewRecording setHidden:TRUE];
    [viewEditRecording setHidden:TRUE];
    [imgViewAudioIndicatorLine setHidden:TRUE];
    [btnUpload setHidden:YES];
    
    strOriginalAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"originalRecording.wav"];
    strAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"recording.wav"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL])
    {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    
    
    [self prepareToRecordAudio];
    
    UIPanGestureRecognizer *panGestureLeftThumb = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [panGestureLeftThumb setDelegate:self];
    [imgViewLeftThumb addGestureRecognizer:panGestureLeftThumb];
    
    UIPanGestureRecognizer *panGestureRightThumb = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [panGestureRightThumb setDelegate:self];
    [imgViewRightThumb addGestureRecognizer:panGestureRightThumb];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buttons

- (IBAction)btnCancelTapped:(id)sender {
    strTrimmedAudioURL = nil;
    //[[AppUtility shared].navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

- (IBAction)btnUploadTapped:(id)sender {
    if ([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [self stopAudioPlayer];
        if(isAudioTrimmed) {
           // [[AppUtility shared].navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
        }
        else {
            [self trimAudioWithCompletion:^{
                isAudioTrimmed = TRUE;
                [self resetAudioPlayer];
                [self hideUnhideControls:TRUE];
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
                //[[AppUtility shared].navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
            }];
        }
    }
   
}


- (IBAction)btnStartRecordingTapped:(id)sender {
    [self startRecording];
}

- (IBAction)btnStopRecordingTapped:(id)sender {
    [self stopRecording];
}

- (IBAction)btnResetTapped:(id)sender {

     isAudioTrimmed = FALSE;
    if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    [[NSFileManager defaultManager] copyItemAtPath:strOriginalAudioURL toPath:strAudioURL error:nil];
    [self resetAudioPlayer];
}

- (IBAction)btnPlayPauseTapped:(id)sender {
    if(isPlaying) {
        [self pauseAudioPlayer];
    }
    else {
        [self playAudioPlayer];
        [audioPlayer setCurrentTime:audioTrimStartTime];
    }
}

- (IBAction)btnResetAudioTapped:(id)sender {
    [self resetAudioPlayer];
    [self hideUnhideControls:FALSE];
    [self startRecording];
}

- (IBAction)btnEditTrimmedVideoTapped:(id)sender {
    if (((UIButton *) sender).selected) {
        isAudioTrimmed = FALSE;
        [self resetAudioPlayer];
        
        [self hideUnhideControls:FALSE];
    }
    else {
        [self btnTrimFinalAudio];
    }
    
}

- (void)btnTrimFinalAudio {
    [self stopAudioPlayer];
    [self trimAudioWithCompletion:^{
        isAudioTrimmed = TRUE;
        [self resetAudioPlayer];
        [self hideUnhideControls:TRUE];
    }];
}

#pragma mark - Gesture

- (void)handlePan:(UIPanGestureRecognizer *)sender {
    [self stopAudioPlayer];
    if(sender.state == UIGestureRecognizerStateChanged) {
        UIView *view = sender.view;
        [view.superview bringSubviewToFront:view];
        CGPoint translation = [sender translationInView:view];
        CGPoint viewPosition = view.center;
        viewPosition.x += translation.x;
        
        [self calculateAudioTrimTime];

        if(audioTrimEndTime - audioTrimStartTime > minimumAudioDuration ||
           ([view isEqual:imgViewLeftThumb] && translation.x <= 0) ||
           ([view isEqual:imgViewRightThumb] && translation.x >= 0)) {
            view.center = viewPosition;
            if(view.x <= (-view.width/2)) {
                [view setX:-view.width/2];
            }
            else if(view.centerX >= viewAudioPlotContainer.width){
                [view setX:viewAudioPlotContainer.width - (view.width/2)];
            }
        }
        else {
            [self calculateAudioTrimTime];
        }
        [sender setTranslation:CGPointZero inView:imgViewLeftThumb];
    }
    else {
        [self calculateAudioTrimTime];
    }
}

#pragma mark - Other Methods

- (NSString *)timeFormatted:(CGFloat)interval isWithMinutes:(BOOL)isWithMinutes {
    unsigned long milliseconds = interval * 1000;
    unsigned long seconds = milliseconds / 1000;
    milliseconds %= 1000;
    unsigned long minutes = seconds / 60;
    seconds %= 60;
//    unsigned long hours = minutes / 60;
//    minutes %= 60;
    
    NSString *strMillisec = @(milliseconds).stringValue;
    if (strMillisec.length > 2) {
        strMillisec = [strMillisec substringToIndex:2];
    }
    
    if(isWithMinutes) {
        return [NSString stringWithFormat:@"%02ld:%02ld.%02ld",(long)minutes, (long)seconds, (long)[strMillisec integerValue]];
    }
    else {
        return [NSString stringWithFormat:@"%02ld.%02ld",(long)seconds, (long)[strMillisec integerValue]];
    }
}

- (void)setupAudioPlot {
    
    audioPlot.backgroundColor = [UIColor colorWithRed:(231)/255.0f green:(231)/255.0f blue:(231)/255.0f alpha:1];
    audioPlot.color = [UIColor colorWithRed:(155)/255.0f green:(161)/255.0f blue:(186)/255.0f alpha:1];
    
    audioPlot.plotType = EZPlotTypeBuffer;
    audioPlot.shouldFill = YES;
    audioPlot.shouldMirror = YES;
    audioPlot.shouldOptimizeForRealtimePlot = NO;
    audioPlot.waveformLayer.shadowOffset = CGSizeMake(0.0, 1.0);
    audioPlot.waveformLayer.shadowRadius = 0.0;
    audioPlot.waveformLayer.shadowColor = [UIColor clearColor].CGColor;
    audioPlot.waveformLayer.shadowOpacity = 1.0;

    audioFile = [EZAudioFile audioFileWithURL:[NSURL fileURLWithPath:strAudioURL]];
    [audioFile getWaveformDataWithCompletionBlock:^(float **waveformData, int length) {
        [audioPlot updateBuffer:waveformData[0] withBufferSize:length];
    }];
}

- (void)trimAudioWithCompletion:(void(^)(void))completion {
    
    if(!strTrimmedAudioURL) {
        strTrimmedAudioURL = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"trimmerRecording.wav"];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:strTrimmedAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strTrimmedAudioURL error:nil];
    }
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strAudioURL]];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        self.exportSession.outputURL = [NSURL fileURLWithPath:strTrimmedAudioURL];
        self.exportSession.outputFileType = AVFileTypeWAVE;
        
        _audioDuration = audioTrimEndTime - audioTrimStartTime;

        CMTime start = CMTimeMakeWithSeconds(audioTrimStartTime, asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(_audioDuration, asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
        [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([self.exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export cancelled");
                    break;
                case AVAssetExportSessionStatusCompleted: {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError *error;
                        if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
                            [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
                        }
                        [[NSFileManager defaultManager] copyItemAtPath:strTrimmedAudioURL toPath:strAudioURL error:&error];
                        if(completion) {
                            completion();
                        }
                    });
                    break;
                }
                default:
                    NSLog(@"NONE");
                    break;
            }
        }];
    }
}

- (void)calculateAudioTrimTime {
    
    audioTrimStartTime = imgViewLeftThumb.centerX * audioDuration / viewAudioPlotContainer.width;
    audioTrimEndTime = imgViewRightThumb.centerX * audioDuration / viewAudioPlotContainer.width;
    [lblAudioStartTime setText:[self timeFormatted:audioTrimStartTime isWithMinutes:FALSE]];
    [lblAudioEndTime setText:[self timeFormatted:audioTrimEndTime isWithMinutes:FALSE]];
    
    lblAudioStartTime.width = 100;
    [lblAudioStartTime sizeToFit];
    
    lblAudioEndTime.width = 100;
    [lblAudioEndTime sizeToFit];
    
    [lblAudioStartTime setCenterX:imgViewLeftThumb.centerX];
    [lblAudioEndTime setCenterX:imgViewRightThumb.centerX];
    
    if(lblAudioStartTime.x < 0) {
        [lblAudioStartTime setX:0];
    }
    else if(lblAudioStartTime.x > viewAudioPlotContainer.width - lblAudioStartTime.width) {
        [lblAudioStartTime setX:viewAudioPlotContainer.width - lblAudioStartTime.width];
    }
    if(lblAudioEndTime.x < 0) {
        [lblAudioEndTime setX:0];
    }
    else if(lblAudioEndTime.x > viewAudioPlotContainer.width - lblAudioEndTime.width) {
        [lblAudioEndTime setX:viewAudioPlotContainer.width - lblAudioEndTime.width];
    }
}

- (void)hideUnhideControls:(BOOL)isHidden {
    [btnEditTrimmedAudio setSelected:isHidden];
    [imgViewLeftThumb setHidden:isHidden];
    [imgViewRightThumb setHidden:isHidden];
}

#pragma mark - Audio Recorder

- (void)prepareToRecordAudio {
    NSURL *outputFileURL = [NSURL fileURLWithPath:strOriginalAudioURL];
    if([[NSFileManager defaultManager] fileExistsAtPath:outputFileURL.path]) {
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
    }
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue :[NSNumber  numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:11025.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    
    /*[recordSetting setValue:[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
     [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
     [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];*/
    
    // Initiate and prepare the recorder
    NSError *error = nil;
    audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    if (error) {
        NSLog(@"Audio Session Error: %@", error.localizedDescription);
    }
    else {
        audioRecorder.delegate = self;
        audioRecorder.meteringEnabled = YES;
        [audioRecorder prepareToRecord];
    }
}

- (void)startRecording {
    [btnUpload setHidden:YES];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    [audioRecorder record];
    [self startRcorderTimer];
    
    [viewStartRecord setHidden:TRUE];
    [viewRecording setHidden:FALSE];
    [viewEditRecording setHidden:TRUE];
}

- (void)stopRecording {
    [audioRecorder stop];
    [btnUpload setHidden:NO];
    [self stopRecorderTimer];
}

- (void)startRcorderTimer {
    __block NSDate *date = [NSDate date];
    timerRecording = [NSTimer scheduledTimerWithTimeInterval:0.1 block:^{
        audioDuration = [[NSDate date] timeIntervalSinceDate:date];
        [lblRecordingTime setText:[self timeFormatted:audioDuration isWithMinutes:TRUE]];
    } repeats:YES];
}

- (void)stopRecorderTimer {
    if (timerRecording) {
        [timerRecording invalidate];
        timerRecording = nil;
    }
}

#pragma mark - Audio Player

- (void)resetAudioPlayer {
    [self stopAudioPlayer];
    audioPlayer = nil;
    [self setupAudioPlot];
    [self prepareToPlayAudio];
    [imgViewLeftThumb setCenterX:viewAudioPlotContainer.x];
    [imgViewRightThumb setCenterX:viewAudioPlotContainer.width];
    [self calculateAudioTrimTime];
}

- (void)prepareToPlayAudio {
    if (strAudioURL) {
        NSURL *url = [NSURL fileURLWithPath:strAudioURL];
        NSError *error;
        if (!audioPlayer) {
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            if (error) {
                NSLog(@"Error in audioPlayer: %@",[error localizedDescription]);
            } else {
                audioPlayer.delegate = self;
                [audioPlayer prepareToPlay];
                audioDuration = audioPlayer.duration;
                [lblAudioDuration setText:[self timeFormatted:audioDuration isWithMinutes:TRUE]];
            }
        }
    }
}

- (void)playAudioPlayer {
    if (!audioPlayer) {
        [self prepareToPlayAudio];
    }
    
    if(audioPlayer) {
        isPlaying = YES;
        [audioPlayer play];
        [btnPlayPause setSelected:TRUE];
        [self startAudioTimerToDisplay];
    }
}

- (void)pauseAudioPlayer {
    if (isPlaying) {
        isPlaying = NO;
        [audioPlayer pause];
        [btnPlayPause setSelected:FALSE];
        [imgViewAudioIndicatorLine setHidden:TRUE];
        [self stopAudioTimerToDisplay];
    }
}

- (void)stopAudioPlayer {
    if (isPlaying) {
        isPlaying = NO;
        [audioPlayer stop];
        [btnPlayPause setSelected:FALSE];
        [imgViewAudioIndicatorLine setHidden:TRUE];
        [self stopAudioTimerToDisplay];
    }
}

- (void)startAudioTimerToDisplay {
    if (timerAudioPlayer) {
        [timerAudioPlayer invalidate];
        timerAudioPlayer = nil;
    }
    
    timerAudioPlayer = [NSTimer scheduledTimerWithTimeInterval:.1 block:^{
        imgViewAudioIndicatorLine.centerX = viewAudioPlotContainer.width * audioPlayer.currentTime / audioDuration;
        [imgViewAudioIndicatorLine setHidden:FALSE];
        if(audioPlayer.currentTime >= audioTrimEndTime) {
            [self stopAudioPlayer];
        }
    } repeats:YES];
}

- (void)stopAudioTimerToDisplay {
    if (timerAudioPlayer) {
        [timerAudioPlayer invalidate];
        timerAudioPlayer = nil;
    }
}


#pragma mark - Audio Recorder Delegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag {
    // Stop recording
    [audioRecorder stop];
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:strOriginalAudioURL]];
    audioDuration = CMTimeGetSeconds(asset.duration);
    [self calculateAudioTrimTime];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:NO error:nil];

    [viewStartRecord setHidden:TRUE];
    [viewRecording setHidden:TRUE];
    [viewEditRecording setHidden:FALSE];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:strAudioURL]) {
        [[NSFileManager defaultManager] removeItemAtPath:strAudioURL error:nil];
    }
    NSError *error;
    [[NSFileManager defaultManager] copyItemAtPath:strOriginalAudioURL toPath:strAudioURL error:&error];
    
    [self resetAudioPlayer];
    [self prepareToPlayAudio];

    [self setupAudioPlot];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error {
    
}

#pragma mark - Audio Player Delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self stopAudioPlayer];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    [self stopAudioPlayer];
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    [self pauseAudioPlayer];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player {
    [self playAudioPlayer];
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint velocity = [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:gestureRecognizer.view];
    return fabs(velocity.x) > fabs(velocity.y);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
