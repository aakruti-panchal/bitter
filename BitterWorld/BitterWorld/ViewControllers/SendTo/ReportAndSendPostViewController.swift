//
//  RateUsVC.swift
//  Way2Web
//
//  Created by Kaira Support on 23/02/17.
//  Copyright © 2017 Kaira NewMac. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

enum ReportSentToAction {
    case Closed
    case Apply
    case any
}

class ReportAndSendPostViewController: UIViewController {

    @IBOutlet weak var viewBottomConstraint     : NSLayoutConstraint!
    @IBOutlet weak var subViewHeight            : NSLayoutConstraint!
    @IBOutlet weak var subView                  : UIView!
    
    @IBOutlet weak var tblSendTo            : UITableView!
    @IBOutlet weak var lblTitle             : UILabel!
    @IBOutlet weak var txtSearchText        : UITextField!
    @IBOutlet weak var imgSearch            : UIImageView!
    @IBOutlet weak var btnCloseSearch       : UIButton!
    @IBOutlet weak var lblReportPostTitle   : UILabel!
    @IBOutlet weak var viewDevider          : UIView!
    @IBOutlet weak var CommentText          : UITextView!
    @IBOutlet weak var btnApply             : UIButton!
    
    var viewHeight                      : CGFloat = 0.0
    var alertCompletion                 : ((ReportSentToAction) -> ())!
    var arrFriends                      : Array<Friends> = []
    var arrFriendsFilter                : Array<Friends> = []
    var arrReportPost                   : Array<ReportPost> = []
    
    var isSerching                      : Bool = false
    var isReportPost                    : Bool? = false
    var feedID                          : Int = 0
    
    var totalRecordCount                : Int = 0
    var pageNumer                       : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setView()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.view.frame.size.width = (appDelegate.window?.frame.width)!
        showAlert()
    }
}

//MARK: - View Touch Method
extension ReportAndSendPostViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first! as UITouch
        if(touch.view == self.view){
            self.hiddeAlert { (result) -> Void in
                self.alertCompletion(.any)
            }
        }
    }
}

//MARK: - IBAction Methods.
extension ReportAndSendPostViewController {
    
    @IBAction func btnSearchClosedTapped(_ sender: Any) {
        self.txtSearchText.text = ""
        self.searchText(searchText: "")
        _ = self.txtSearchText.resignFirstResponder()
    }
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
        if self.isReportPost! {
            var strReportPost = ""
            for i in 0..<(self.arrReportPost.count) {
                if let id = self.arrReportPost[i].report_id, (self.arrReportPost[i].isSelected == true) {
                    if strReportPost.isEmpty {
                        strReportPost.append("\(id)")
                    } else {
                        strReportPost.append(", \(id)")
                    }
                }
            }
            var commentTxt = self.CommentText.text
            if (self.CommentText.textColor == UIColor.lightGray) { commentTxt = "" }
            self.reportPost(feedId: self.feedID, reasonIds: strReportPost, reason: commentTxt!)
//            if (commentTxt?.isEmpty)! {
//                ISMessages.show("please enter comment!", type: .warning)
//            } else {
//                self.reportPost(feedId: self.feedID, reasonIds: strReportPost, reason: commentTxt!)
//            }
            
        } else {
            var strFriends = ""
            if self.isSerching {
                for i in 0..<(self.arrFriendsFilter.count) {
                    if let id = self.arrFriendsFilter[i].user_id, (self.arrFriendsFilter[i].isSelected == true)  {
                        if strFriends.isEmpty {
                            strFriends.append("\(id)")
                        } else {
                            strFriends.append(", \(id)")
                        }
                    }
                }
            } else {
                for i in 0..<(self.arrFriends.count) {
                    if let id = self.arrFriends[i].user_id, (self.arrFriends[i].isSelected == true) {
                        if strFriends.isEmpty {
                            strFriends.append("\(id)")
                        } else {
                            strFriends.append(", \(id)")
                        }
                    }
                }
            }
            if (strFriends.isEmpty) {
                ISMessages.show("please select friend!", type: .warning)
            } else {
                self.addSendTo(feedId: self.feedID, receiver_id: strFriends)
            }
            
        }
    }
}

//MARK: - TableView DataSource & Delegate Methods.
extension ReportAndSendPostViewController: UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.isReportPost! {
            return self.arrReportPost.count
        }
        return (self.isSerching == true) ? self.arrFriendsFilter.count : self.arrFriends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:SendToCell? = tableView.dequeueReusableCell(withIdentifier: "SendToCell") as? SendToCell
        if (cell == nil) {
            let nib: NSArray = Bundle.main.loadNibNamed("SendToCell", owner: self, options: nil)! as NSArray
            cell = nib.object(at: 0) as? SendToCell
        }
        cell?.selectionStyle = .none
        if self.isReportPost! {
            let report = self.arrReportPost[indexPath.row]
            cell?.imgSendTo.width = 0
            cell?.lblSentToTitle.x = (cell?.imgSendTo.x)!
            cell?.lblSentToTitle.text = report.report_name
            cell?.btnClose.isSelected   =  report.isSelected! ? true : false
            
        } else {
            if self.isSerching == true {
                let friend = arrFriendsFilter[indexPath.row]
                cell?.lblSentToTitle.text   = friend.first_name
                cell?.btnClose.isSelected   =  friend.isSelected! ? true : false
                if let url = friend.profile_photo, !url.isEmpty {
                    cell?.imgSendTo.setImageWithURL(url, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
                }
            } else {
                let friend = arrFriends[indexPath.row]
                cell?.lblSentToTitle.text   = friend.first_name
                cell?.btnClose.isSelected   =  friend.isSelected! ? true : false
                if let url = friend.profile_photo, !url.isEmpty {
                    cell?.imgSendTo.setImageWithURL(url, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
                }
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if self.isReportPost! {
            
            let friend = arrReportPost[indexPath.row]
            friend.isSelected = !friend.isSelected!
            self.tblSendTo.reloadData()
            
        } else {
            if self.isSerching == true {
                let friend = arrFriendsFilter[indexPath.row]
                friend.isSelected = !friend.isSelected!
                self.tblSendTo.reloadData()
            } else {
                let friend = arrFriends[indexPath.row]
                friend.isSelected = !friend.isSelected!
                self.tblSendTo.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.isReportPost! { } else {
            if self.isSerching == true {} else {
                if ((self.arrFriends.count)-1) == indexPath.row {
                    if self.arrFriends.count < self.totalRecordCount {
                        self.getFriendsList(loadFromStart: false)
                    }
                }
            }
        }
    }
    
}

//MARK:- Service Call
extension ReportAndSendPostViewController {
    
    func getFriendsList(loadFromStart:Bool) {
        
        if loadFromStart == true {
            self.pageNumer = 1
        }
        
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "page":self.pageNumer]
        //let dictParam :Dictionary<String, Any> = [ "user_id" : 9, "page":self.pageNumer]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_FRIENDS, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                self.totalRecordCount = dictData["total_count"] as? Int ?? 0
                
                if self.pageNumer == 1 {
                    self.arrFriends.removeAll(keepingCapacity: false)
                }
                for dict in arrData {
                    if let cat =  Mapper<Friends>().map(JSON: dict) {
                        
//                            for j in 0..<(self.selectedFriends.count) {
//                                if self.selectedFriends[j].user_id == cat.user_id {
//                                    cat.isSelected = true
//                                }
//                            }
                        self.arrFriends.append(cat)
                    }
                }
                
                self.tblSendTo.reloadData()
                if self.pageNumer == 1{
                    self.tblSendTo.scrollToTop(animated: true)
                }
                // Increase Page Counter
                self.pageNumer = self.pageNumer + 1;
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    func gerReportPost() {
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_REPORT_REASON, parameters: nil, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let report =  Mapper<ReportPost>().map(JSON: dict) {
                        self.arrReportPost.append(report)
                    }
                }
                self.tblSendTo.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
        
    }
    
    func reportPost(feedId : Int, reasonIds : String, reason : String) {
        
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : feedId, "reason" : reason, "reason_id" : reasonIds]
        
        _ = WebClient.requestWithUrl(url: K.URL.REPORT_POST, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    ISMessages.show(message, type: .success)
                }
            }
            self.hiddeAlert { (result) -> Void in
                self.alertCompletion(.Closed)
            }
        })
    }
    
    func addSendTo(feedId : Int, receiver_id : String) {

        let dictParam :Dictionary<String, Any> = ["owner_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : feedId, "receiver_id" : receiver_id]
        
        _ = WebClient.requestWithUrl(url: K.URL.SEND_TO, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    ISMessages.show(message, type: .success)
                }
            }
            self.hiddeAlert { (result) -> Void in
                self.alertCompletion(.Closed)
            }
        })
    }
}

//MARK: - UITextField Delegate methods
extension ReportAndSendPostViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString     = textField.text! as NSString
        let newString: NSString         = currentString.replacingCharacters(in: range, with: string) as NSString
        //let newLength: Int              = newString.length
        print(newString)
        self.searchText(searchText: newString as String)
        return true
    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return true
//    }
    
    func searchText(searchText : String) {
        
        if (searchText.isNotEmpty()) {
            isSerching = true
            self.btnCloseSearch.isHidden = false
            self.arrFriendsFilter = self.arrFriends.filter({ (friend) -> Bool in
                return (friend.first_name?.localizedCaseInsensitiveContains(searchText))!
            })
            print(self.arrFriendsFilter.count)
            self.tblSendTo.reloadData()
        }
        else{
            isSerching = false
            self.btnCloseSearch.isHidden = true
            self.tblSendTo.reloadData()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = self.txtSearchText.resignFirstResponder()
        self.searchText(searchText: textField.text!)
        return true
    }
}

//MARK : - UITextView delegate methods
extension ReportAndSendPostViewController:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView == self.CommentText) {
            if (self.CommentText.textColor == UIColor.lightGray) {
                self.CommentText.text = "";
                self.CommentText.textColor = K.Color.darkGray
            }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.text)
        if (textView == CommentText) {
            if  self.CommentText.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
                self.CommentText.text = "Add comment here"
                self.CommentText.textColor = UIColor.lightGray
            }
        }
    }
}

//MARK:- Other function
extension ReportAndSendPostViewController {
    
    func setView() {
        
        self.setFont()
        self.viewHeight                     = SF.screenHeight - (SF.screenHeight / 4)
        self.subViewHeight.constant         = self.viewHeight
        
        IQKeyboardManager.shared.enable                         = true
        IQKeyboardManager.shared.enableAutoToolbar              = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder   = true
        IQKeyboardManager.shared.previousNextDisplayMode        = .alwaysShow
        
        self.viewBottomConstraint.constant  = -self.viewHeight
        self.view.frame                     = (appDelegate.window?.screen.bounds)!
        
        self.tblSendTo.estimatedRowHeight   = 100
        self.tblSendTo.rowHeight            = UITableViewAutomaticDimension
        self.tblSendTo.tableFooterView      = UIView()
        
        self.btnCloseSearch.isHidden = true
    }
    
    func setFont() {
        
        Common().addshadow(View: self.btnApply, shadowRadius: 5.0, shadowColor: UIColor.black, shadowOpacity: 1.0, cornerRadius : 3.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        self.lblTitle.textColor                 = K.Color.white
        self.lblReportPostTitle.textColor       = K.Color.lightGray
        self.lblTitle.font                      = UIFont(name: kPTSansRegularFontName, size: 17)
        self.lblReportPostTitle.font            = UIFont(name: kPTSansRegularFontName, size: 14)
    }
    
    func setData() {
        
        if self.isReportPost! {
            
            self.gerReportPost()
            self.viewDevider.isHidden           = true
            self.lblTitle.text                  = "Report Post"
            self.tblSendTo.separatorStyle       = .none
            self.txtSearchText.isHidden         = true
            self.imgSearch.isHidden             = true

            self.lblReportPostTitle.isHidden    = false
            self.CommentText.text               = "Add comment here"
            self.CommentText.textColor          = UIColor.lightGray
            
            self.CommentText.borderWidth        = 0.5
            self.CommentText.borderColor        = K.Color.lightGray
            self.CommentText.cornerRadius       = 2
            self.btnApply.setTitle("Submit",for: .normal)
            
        } else {
            
            self.lblTitle.text                  = "Send to"
            self.txtSearchText.isHidden         = false
            self.imgSearch.isHidden             = false

            self.lblReportPostTitle.isHidden    = true
            self.getFriendsList(loadFromStart: true)
            self.CommentText.height             = 0
            self.CommentText.isHidden           = true
            self.btnApply.setTitle("Send",for: .normal)
        }
    }
    
    func showAlert() {
        UIView .animate(withDuration: 0.4, delay: 0.0, options:UIViewAnimationOptions.curveEaseInOut, animations: {
            
            self.viewBottomConstraint.constant=0
            self.view.layoutIfNeeded()
        }, completion: {
            (value: Bool) in
        })
    }
    
    func hiddeAlert(ServiceCallBack: @escaping (_ result: Bool?)-> Void! ) {
        
        UIView .animate(withDuration: 0.2, delay: 0.0, options:UIViewAnimationOptions.curveEaseInOut, animations: {
            
            IQKeyboardManager.shared.enable                         = false
            IQKeyboardManager.shared.enableAutoToolbar              = false
            IQKeyboardManager.shared.shouldShowToolbarPlaceholder   = false
            IQKeyboardManager.shared.previousNextDisplayMode        = .alwaysHide
            self.viewBottomConstraint.constant                      = -self.viewHeight
            self.view.layoutIfNeeded()
        }, completion: {
            (value: Bool) in
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
            return ServiceCallBack(true)
        })
    }
    
    class func showRateUsAlert(isReportPost : Bool, feedID : Int, completion completionHandler: @escaping ((ReportSentToAction) -> () )) {
        
        let objReportSendTo = ReportAndSendPostViewController(nibName: "ReportAndSendPostViewController", bundle: nil)
        objReportSendTo.alertCompletion = completionHandler
        
        appDelegate.window?.addSubview(objReportSendTo.view)
        appDelegate.window?.rootViewController?.addChildViewController(objReportSendTo)
        objReportSendTo.didMove(toParentViewController: appDelegate.window?.rootViewController)
        
        objReportSendTo.isReportPost    = isReportPost
        objReportSendTo.feedID          = feedID
        objReportSendTo.setData()
        
    }
}
