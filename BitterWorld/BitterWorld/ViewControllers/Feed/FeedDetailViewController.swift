//
//  FeedDetailViewController.swift
//  BitterWorld
//
//  Created by  " " on 11/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import ObjectMapper

class FeedDetailViewController: UIViewController {

    
    @IBOutlet weak var scrollViewFeedDetail     : UIScrollView!
    @IBOutlet weak var viewMainView             : UIView!
    @IBOutlet weak var viewContent              : UIView!
    @IBOutlet weak var viewHeader               : UIView!
    @IBOutlet weak var imgUser                  : UIImageView!
    @IBOutlet weak var lblUserName              : UILabel!
    @IBOutlet weak var lblAddress               : UILabel!
    @IBOutlet weak var imgLocationIcon          : UIImageView!
    @IBOutlet weak var lblShopTitle             : UILabel!
    @IBOutlet weak var viewRatting              : FloatRatingView!
    
    @IBOutlet weak var viewFooterContent        : UIView!
    @IBOutlet weak var viewDescription          : UIView!
    @IBOutlet weak var lblDescription           : UILabel!
    
    @IBOutlet weak var btnLike                  : UIButton!
    @IBOutlet weak var btnChat                  : UIButton!
    @IBOutlet weak var btnSentComment           : UIButton!
    @IBOutlet weak var btnMoreOption            : UIButton!
    
    @IBOutlet weak var lblNoOfComments          : UILabel!
    @IBOutlet weak var lblDate                  : UILabel!
    @IBOutlet weak var lblComments              : UILabel!
    
    @IBOutlet weak var viewEnteComment          : UIView!
    @IBOutlet weak var imgComment               : UIImageView!
    
    @IBOutlet weak var viewDate                 : UIView!
    
    @IBOutlet var collectionViewMedia           : UICollectionView!
    @IBOutlet weak var pageControl              : UIPageControl!
    
    var objFeed : Feed = Feed()
    var feedID : Int = 0

    var menuOptionNameArray : [String] = ["Save Post","Share Post","Report Post"]
    var menuOptionImageNameArray : [String] = ["ic_save_post","ic_share","ic_flag_post"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.viewEnteComment.y = SF.screenHeight - 50
        self.scrollViewFeedDetail.isHidden = true
        self.viewEnteComment.isHidden = true
        self.getFeedDetail(feedId: self.feedID)
        self.setView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

//MARK:-  IBAction Methods
extension FeedDetailViewController {
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController()
    }
    
    @IBAction func handleButtonTap(_ sender: UIButton) {
        
        self.menuOptionImageNameArray = (self.objFeed.is_reported == 1) ? ["ic_save_post","ic_share","ic_flag_post_red"] : ["ic_save_post","ic_share","ic_flag_post"]
        
        FTPopOverMenu.showForSender(sender: sender, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray as [String], done: { (selectedIndex) -> () in
            print(selectedIndex)
            
            switch selectedIndex {
            case 0:
                //Save
                break
            case 1:
                //Share
                self.openPopup(isReportPost: false, feedID: self.objFeed.feedID)
                break
            case 2:
                //Report Post
                self.openPopup(isReportPost: true, feedID: self.objFeed.feedID)
                break
            default:
                break
            }
        }) {
        }
    }
}

//MARK:- Other Methods
extension FeedDetailViewController {
    
    func setView() {
        
        self.addLogo()
        
        let nib = UINib(nibName: "PostMediaCell", bundle: nil)
        self.collectionViewMedia.register(nib, forCellWithReuseIdentifier: "PostMediaCell")
        self.collectionViewMedia.delegate = self
        self.collectionViewMedia.dataSource = self
        self.collectionViewMedia.reloadData()
        self.collectionViewMedia.bringSubview(toFront: pageControl)
        
        self.imgUser.layer.masksToBounds    = false
        self.imgUser.roundSquareImage()
        self.imgComment.layer.masksToBounds = false
        self.imgComment.roundSquareImage()
        
        self.viewEnteComment.addTapGesture { (gesture) in
            let commentVC  = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController")  as! CommentViewController
            commentVC.objFeed = self.objFeed
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        self.btnChat.addTapGesture { (gesture) in
            let commentVC  = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController")  as! CommentViewController
            commentVC.objFeed = self.objFeed
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        
        
        Common().addshadow(View: self.viewEnteComment, shadowRadius: 5.0, shadowColor: UIColor.lightGray, shadowOpacity: 1.0, cornerRadius : (self.imgComment.height / 2) , shadowOffset: CGSize(width: 1.0, height: 1.0))
        
        self.lblUserName.textColor          = K.Color.white
        self.lblAddress.textColor           = K.Color.white
        self.lblShopTitle.textColor         = K.Color.white
        
        self.lblUserName.font               = UIFont(name: kPTSansRegularFontName, size: 15.21)
        self.lblAddress.font                = UIFont(name: kPTSansRegularFontName, size: 13.42)
        self.lblShopTitle.font              = UIFont(name: kPTSansRegularFontName, size: 13.5)
        
        self.lblDescription.textColor       = K.Color.lightTextColor
        self.lblDescription.font            = UIFont(name: kPTSansRegularFontName, size: 15)
        
        btnLike.addTapGesture { (gesture) in
            self.likeWebserviceCall(self.objFeed.feedID)
        }
        
    }
    
    func setData() {
        
        if let imgUrl = self.objFeed.profilePhoto {
            imgUser.setImageWithURL(imgUrl, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        }
        if let name = self.objFeed.firstname {
            self.lblUserName.text = name
        }
        
        var cityCountry = ""
        if let city = self.objFeed.city, !city.isEmpty {
            cityCountry.append(city)
        }
        if let country = self.objFeed.country, !country.isEmpty {
            cityCountry.append(", \(country)")
        }
        if cityCountry.isEmpty {
            self.imgLocationIcon.isHidden = true
        } else {
            self.imgLocationIcon.isHidden = false
        }
        
        self.lblAddress.adjustsFontSizeToFitWidth = false
        let cityWidth = cityCountry.sizeWidth(self.lblAddress.size.height, font: UIFont(name: kPTSansRegularFontName, size: 15)!, lineBreakMode: .byWordWrapping).width
        
        if (self.objFeed.rating ?? 0 > 0) {
            
            //self.viewRating.isHidden = false
            self.viewRatting.rating = (self.objFeed.rating)!
            //let tmp = self.viewRating.left - self.imgUser.right
            let tmp = (SF.screenWidth - self.viewRatting.width) - self.imgUser.right
            if cityWidth > tmp {
                self.lblAddress.width = tmp - 40
            } else {
                self.lblAddress.width = cityWidth
            }
            
        } else {
            //viewRating.isHidden = true
            //lblAddress.width = self.viewHeader.width - (self.viewRating.width + 8.0)
            self.lblAddress.width = (cityWidth > (self.viewHeader.right - 80)) ? self.viewHeader.right - 80 : cityWidth
        }
        lblAddress.text = cityCountry
        self.imgLocationIcon.left = lblAddress.right
        if let cat = self.objFeed.categoryName {
            lblShopTitle.text = cat
        }
        if let media = self.objFeed.mediaData, media.count > 0 {
            self.pageControl.numberOfPages = media.count
        }
        
        self.collectionViewMedia.reloadData()
        
        //******************
        
        
        var descCountry = ""
        if let desc = self.objFeed.desc, !desc.isEmpty {
            descCountry.append(desc)
        }
        let font = self.lblDescription.font!
        let descHeight = descCountry.size(self.lblDescription.size.width, font: font, lineBreakMode: .byWordWrapping).height
        self.lblDescription.height = descHeight
        self.lblDescription.text = descCountry
        
        
        
        let strComment = self.objFeed.commentCount == 1 ? "Comment" : "Comments"
        let strBroken = self.objFeed.likeCount == 1 ? "Broken Heart" : "Broken Hearts"
        
        btnLike.isSelected = self.objFeed.isLike == true ? true : false
        
//        if self.objFeed.likeCount > 0 && self.objFeedFeed.commentCount ?? 0 > 0{
//            self.lblNoOfComments.text = "\(self.objFeed.likeCount ) \(strBroken) • " + "\(self.objFeed.commentCount, trComment)"
        if self.objFeed.likeCount > 0 && self.objFeed.commentCount > 0 {
                self.lblNoOfComments.text = "\(self.objFeed.likeCount) \(strBroken) • " + "\(self.objFeed.commentCount) \(strComment)"
            self.lblNoOfComments.isHidden = false
        } else if self.objFeed.likeCount > 0{
            
            self.lblNoOfComments.text = "\(self.objFeed.likeCount ) \(strBroken) "
            self.lblNoOfComments.isHidden = false
            
        } else if self.objFeed.commentCount > 0{
            
            self.lblNoOfComments.text = "\(self.objFeed.commentCount ) \(strComment) "
            self.lblNoOfComments.isHidden = false
            
        } else{
            self.lblNoOfComments.isHidden = true
        }
        self.lblDate.text =     Date(timeIntervalSince1970:(self.objFeed.createdDate)!).toString(format: K.DateFormat.feedCreate)
        
        /*
        if self.objFeed.commentCount == 0 {
            self.lblComments.height = 0
            lblComments.isHidden = true
            //viewEnteComment.y = lblComments.y
        } else {
            let fontComment = self.lblComments.font!
            if let comments = self.objFeed.comments, comments.count > 0 {
                if let commentTxt = self.objFeed.comments.first?.comment_text, !commentTxt.isEmpty {
                    var comnt = ""
                    comnt.append(commentTxt)
                    let commentHeight = comnt.size(self.lblComments.size.width, font: fontComment, lineBreakMode: .byWordWrapping).height
                    self.lblComments.text = comnt
                    self.lblComments.height = commentHeight
                }
            }
            lblComments.isHidden = false
            //viewEnteComment.y = lblComments.bottom + 10
        }
        */
        self.lblComments.text = ""
        self.lblComments.height = 0
        
        
        self.viewDescription.layer.masksToBounds = true
        
        self.viewDescription.y = self.viewDescription.y - 5   //mehul
        
        self.viewDescription.height = self.lblDescription.height + 20
        self.viewFooterContent.y = self.viewDescription.bottom + 1
        self.viewFooterContent.height = self.lblComments.bottom
        self.viewContent.height = self.viewFooterContent.bottom
        
        self.viewMainView.height = self.viewHeader.height + self.viewContent.height
        self.scrollViewFeedDetail.autocalculateContentHeight()
        
//        self.viewDescription.addBorderLeft(size: 1, color: UIColor.lightGray)
//        self.viewDescription.addBorderRight(size: 1, color: UIColor.lightGray)
//        self.viewDescription.addBorderBottom(size: 1, color: UIColor.lightGray)
//        self.viewDescription.roundCorners([.bottomLeft, .bottomRight], radius: 10)
//        Common().addshadow(View: self.viewFooterContent, shadowRadius: 5.0, shadowColor: UIColor.lightGray, shadowOpacity: 1.0, cornerRadius : 3.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        
        Common().addshadow(View: self.viewDescription, borderWidth: 1.0, borderColor : K.Color.lightGray, cornerRadius : 5.0)
        self.viewDescription.addBorderTop(size: 1, color: UIColor.clear)
    }
    
    func addLogo() {
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        //imageView.frame.size.width = 100;
        //imageView.frame.size.height = 45;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
}


//MARK:- UIScrollViewDataSource & Delegate Methods.
extension FeedDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let media = self.objFeed.mediaData, media.count > 0 {
            return media.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewMedia.width, height:188.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostMediaCell", for: indexPath) as! PostMediaCell
        cell.setData(for: indexPath, arrMedia: self.objFeed.mediaData ?? [], mediaCount:0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewMedia {
            
            
            
            //For Image full screen
            if self.objFeed.mediaData[indexPath.row].mediaType == 1 {
                self.openFullImageScreen()
                
            } else if self.objFeed.mediaData[indexPath.row].mediaType == 2 {
                //feed_video
                if let myURL = self.objFeed.mediaData[indexPath.row].videofile, !myURL.isEmpty  {
                    let videoURL = URL(string: myURL)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    AppUtility.shared.navigationController?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            } else if self.objFeed.mediaData[indexPath.row].mediaType == 3 {
                //feed_audio
                if let myURL = self.objFeed.mediaData[indexPath.row].audio, !myURL.isEmpty {
                    
                    let videoURL = URL(string: myURL)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    AppUtility.shared.navigationController?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
        self.changeDotColor()
    }
    
    func changeDotColor() -> Void {
        for (index, dot) in pageControl.subviews.enumerated() {
            if index == pageControl.currentPage {
                dot.backgroundColor = .red
                dot.layer.cornerRadius = dot.frame.size.height / 2;
            } else {
                dot.backgroundColor = .white
                dot.layer.cornerRadius = dot.frame.size.height / 2
                dot.layer.borderColor = UIColor.white.cgColor
                // dot.layer.borderWidth = 1.0
            }
        }
    }
}


//MARK:- FullImageScreen Methods
extension FeedDetailViewController  {
    
    func openFullImageScreen() {
        
        var imgArr = [String]()
        if self.objFeed.mediaData.count > 0 {
            for i in 0..<(self.objFeed.mediaData.count) {
                if self.objFeed.mediaData[0].mediaType == 1 {
                    if let imgUrl = self.objFeed.mediaData[i].photo, !imgUrl.isEmpty {
                        imgArr.append(imgUrl)
                    }
                }
            }
            if imgArr.count > 0 {
                let objFullView         = FullImageView(nibName:"FullImageView",bundle: nil)
                objFullView.imageArray  = imgArr
                AppUtility.shared.navigationController?.present(objFullView, animated: true, completion: nil)
            }
        }
    }
}

extension FeedDetailViewController {
    
    func openPopup(isReportPost : Bool, feedID : Int) {
        
        ReportAndSendPostViewController.showRateUsAlert(isReportPost : isReportPost, feedID : feedID) { (completionHandler) in
            switch completionHandler{
            case .Closed:
                break
            case .Apply:
                break
            case .any:
                break
            }
        }
    }
    
}

//MARK:- Service Call
extension FeedDetailViewController  {
    
    func getFeedDetail(feedId : Int) {
        
        //URL: http://9834584578/clients/2018/bitterworld/api/web/v1/feed/get-feed-detail
        //Params: user_id, feed_id
        
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : feedId]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_FEED_DETAIL, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Dictionary<String, Any>
                
                if let feed =  Mapper<Feed>().map(JSON: arrData) {
                    self.objFeed = feed
                    self.setData()
                }
                self.scrollViewFeedDetail.isHidden = false
                self.viewEnteComment.isHidden = false
                
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    
    func likeWebserviceCall(_ feedId: Int?) -> Void {
        
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0, "feed_id":feedId ?? 0
        ]
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.LIKE_DISLIKE, parameters: dictParam , completion: { (response, error) in
            if error == nil {
                SVProgressHUD.dismiss()
                
                if self.objFeed.isLike == true {
                    self.objFeed.likeCount = self.objFeed.likeCount - 1
                }
                else{
                    self.objFeed.likeCount = self.objFeed.likeCount + 1
                }
                self.objFeed.isLike = self.objFeed.isLike == true ? false : true
                self.btnLike.isSelected = self.objFeed.isLike == true ? true : false
                
                let strComment = self.objFeed.commentCount == 1 ? "Comment" : "Comments"
                let strBroken = self.objFeed.likeCount == 1 ? "Broken Heart" : "Broken Hearts"
                
                self.btnLike.isSelected = self.objFeed.isLike == true ? true : false
                
                if self.objFeed.likeCount > 0 && self.objFeed.commentCount > 0{
                    self.lblNoOfComments.text = "\(self.objFeed.likeCount > 0 ? strBroken : "")  " + "\(self.objFeed.commentCount > 0 ? strComment : "" ) "
                    self.lblNoOfComments.isHidden = false
                }
                else if self.objFeed.likeCount > 0{
                    self.lblNoOfComments.text = "\(self.objFeed.likeCount) \(strBroken) "
                    self.lblNoOfComments.isHidden = false
                }
                else if self.objFeed.commentCount > 0{
                    self.lblNoOfComments.text = "\(self.objFeed.commentCount) \(strComment) "
                    self.lblNoOfComments.isHidden = false
                }
                else{
                    self.lblNoOfComments.isHidden = true
                }
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
}
