//
//  SuggestedFriendViewController.swift
//  BitterWorld
//
//  Created by  " " on 03/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class SuggestedFriendViewController: UIViewController,UISearchBarDelegate {

    @IBOutlet weak var tblFriendList    : UITableView!
    
    @IBOutlet weak var btnSearch          : UIButton!
    @IBOutlet weak var btnBack          : UIButton!
    
    @IBOutlet var viewRight         : UIView!
    @IBOutlet var viewLeft          : UIView!

    
    var arrSuggestedFriend: Array<Friends> = []
    var arrFilterFriend: Array<Friends> = []
    var facebookIds = ""
    var contacts = ""
    var isSerching : Bool = false

    //search bar
    var searchBar: UISearchBar?

    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchBar = UISearchBar()
        searchBar?.showsCancelButton = true
        searchBar?.delegate = self
        searchBar?.frame = CGRect(x: 0, y:0, width: self.view.width, height: 44)
        searchBar?.barStyle = .black

        self.navigationItem.hidesBackButton = true
        
        self.syncContactAndFacebookFriend()
        self.addLogo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    //MARK : - IBAction
    
    @IBAction func btnSearchTapped (_sender : UIButton){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar?.alpha = 0.0
        }) { (finished) in
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.titleView = self.searchBar
            self.searchBar?.alpha = 1.0
            self.viewRight.isHidden = true
            self.searchBar?.becomeFirstResponder()
        }
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar?.alpha = 0.0
            self.viewRight.isHidden = false
            
            let RightNavBarButton = UIBarButtonItem(customView:self.viewRight)
            self.navigationItem.rightBarButtonItem = RightNavBarButton
            let LeftNavBarButton = UIBarButtonItem(customView:self.viewLeft)
            self.navigationItem.leftBarButtonItem = LeftNavBarButton
            self.addLogo()
            self.searchBar?.text = ""
            self.isSerching = false
            self.tblFriendList.reloadData()
        }) { (finished) in
        
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text \(searchBar.text!)")
        if (searchBar.text?.isValid)!{
            isSerching = true
            self.arrFilterFriend = self.arrSuggestedFriend.filter({ (friend) -> Bool in
                return (friend.first_name?.localizedCaseInsensitiveContains(searchBar.text!))!
            })
            self.tblFriendList.reloadData()
        }
        else{
            isSerching = false
            self.tblFriendList.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        return true
    }

    
   
    // MARK: - Webservice

    func syncContactAndFacebookFriend (){
        
        let dictParam = ["user_id":User.loggedInUser()?.ID ?? 0,
                         "contacts":contacts,
                         "facebook_id":facebookIds,
                         "see_all":1] as [String : Any]
        
        
        SVProgressHUD.show()
        
        _ = WebClient.requestWithUrl(url: K.URL.SYNC_CONTACT, parameters: dictParam, completion: { (response, error) in
            
            SVProgressHUD.dismiss()
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Dictionary<String, Any>
                let arrFriend = arrData["suggested_friends"]
                self.arrSuggestedFriend = Mapper<Friends>().mapArray(JSONArray: arrFriend as! [[String : Any]])
             
                self.tblFriendList.reloadData()
            }
            else{
                ISMessages.show(error?.localizedDescription, type: .warning)
                
            }
            
            
        })
        
    }
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        
    }
    
    
    @IBAction func btnBackTapped (_ sender : UIButton)
    {
        self.navigationController?.popViewController()
    }
    
    

}

extension SuggestedFriendViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isSerching ? self.arrFilterFriend.count : self.arrSuggestedFriend.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFriendCell", for: indexPath) as! MyFriendCell
        let myfriend = self.isSerching ? arrFilterFriend[indexPath.row] : arrSuggestedFriend[indexPath.row]
            cell.lblName.text = myfriend.first_name
            cell.imgProfile.setImageWithURL(myfriend.profile_photo, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        
        cell.lblUnfriend.addTapGesture { (gesture) in
            
            SVProgressHUD.show()
            let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                       "friend_id" : myfriend.user_id ?? 0]
            
            _ = WebClient.requestWithUrl(url: K.URL.ADD_FRIEND, parameters: dictParam, completion: { (response, error) in
                SVProgressHUD.dismiss()
                if error == nil {
                    
                    //self.arrSuggestedFriend.remove(element: myfriend)
                    let dictData = response as! Dictionary<String, Any>
                    ISMessages.show(dictData["message"] as! String, type: .success)
                    self.syncContactAndFacebookFriend()
                    
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("FriendRequestSent"), object: nil)

                    
                }
                else {
                    ISMessages.show(error?.localizedDescription)
                }
                
            })
            
        }
            return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
      
        print(indexPath.row)
    }
    
    
}
