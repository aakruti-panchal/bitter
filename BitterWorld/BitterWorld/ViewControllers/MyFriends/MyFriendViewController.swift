//
//  MyFriendViewController.swift
//  BitterWorld
//
//  Created by  " " on 01/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import Contacts
import FBSDKLoginKit
import FBSDKCoreKit
import ObjectMapper
import Floaty

enum Type : Int {
    case MYFRIEND   = 1
    case INVITED    = 2
    case PENDING    = 3
    
}

class MyFriendViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate,MenuProtocol {
   
    

    @IBOutlet weak var collectionViewSuggestedFriend : UICollectionView!
    
    @IBOutlet weak var tblFriendList    : UITableView!
    
    @IBOutlet weak var btnFriend        : UIButton!
    @IBOutlet weak var btnInvited       : UIButton!
    @IBOutlet weak var btnPending       : UIButton!
    
    @IBOutlet weak var lblSeeAll       : UILabel!
    @IBOutlet weak var lblNoDataFound  : UILabel!
    @IBOutlet weak var lblSuggestedFriend  : UILabel!
    @IBOutlet weak var lblNoFriendFound : UILabel!
    
    var requestType : StatusType = .ACCEPT
    var selectedTab : Type = .MYFRIEND
    var arrContacts : Array<String> = []
    var arrData : Array<Dictionary<String, Any>> = []
    var arrFacaebokFriend : Array<Dictionary<String, Any>> = []

    var arrSuggestedFriend: Array<Friends> = []
    var arrMyFriend: Array<Friends> = []
    var arrInvited: Array<Friends> = []
    var arrPending: Array<Friends> = []
    
    var profileImage: UIImage?
    var facebookIds = ""
    
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,CNContactImageDataAvailableKey,CNContactThumbnailImageDataKey
            ] as [Any]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLogo()
        self.setupView()
        self.getAllContact()
        AppUtility.shared.delegate = self
 
        self.lblSuggestedFriend.addTapGesture { (gesture) in
            let suggestVC = self.storyboard?.instantiateViewController(withIdentifier:"SuggestedFriendViewController") as! SuggestedFriendViewController
            suggestVC.contacts = self.arrContacts.joined(separator: ",")
            suggestVC.facebookIds = self.facebookIds
            self.navigationController?.pushViewController(suggestVC, animated: true)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let floaty = Floaty()
        AppUtility.shared.addFloatingButton(to: self.view, floaty: floaty)

        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(syncContactAndFacebookFriend), name: Notification.Name("FriendRequestSent"), object: nil)

        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     //MARK:- IBActions
    
    @IBAction func btnMyFriendTapped (_ sender : UIButton)
    {
        if self.selectedTab != .MYFRIEND{
            self.getFriendsList()
        }
        
        self.selectedTab = .MYFRIEND
        
        sender.backgroundColor = K.Color.black
        btnInvited.backgroundColor = K.Color.white
        btnPending.backgroundColor = K.Color.white

        sender.setTitleColor(UIColor.white, for: .normal)
        btnInvited.setTitleColor(K.Color.lightTextColor, for: .normal)
        btnPending.setTitleColor(K.Color.lightTextColor, for: .normal)
        
    }
    
    
    @IBAction func btnInvitedTapped (_ sender : UIButton)
    {
        if self.selectedTab != .INVITED{
           self.getInvitedList()
        }
        
        self.selectedTab = .INVITED
        sender.backgroundColor = K.Color.black
        btnFriend.backgroundColor = K.Color.white
        btnPending.backgroundColor = K.Color.white

        sender.setTitleColor(UIColor.white, for: .normal)
        btnFriend.setTitleColor(K.Color.lightTextColor, for: .normal)
        btnPending.setTitleColor(K.Color.lightTextColor, for: .normal)
        
        
    }
    
    
    @IBAction func btnPendingTapped (_ sender : UIButton)
    {
        if self.selectedTab != .PENDING{
            self.getPendingList()
        }
        
        self.selectedTab = .PENDING
        sender.backgroundColor = K.Color.black
        btnFriend.backgroundColor = K.Color.white
        btnInvited.backgroundColor = K.Color.white
        
        sender.setTitleColor(UIColor.white, for: .normal)
        btnFriend.setTitleColor(K.Color.lightTextColor, for: .normal)
        btnInvited.setTitleColor(K.Color.lightTextColor, for: .normal)

    }
    

    @IBAction func btnBackTapped (_ sender : UIButton)
    {
        //AppUtility.shared.navigationController?.popViewController()
        //self.navigationController?.popViewController()
        self.showCamera()
    }
    
    func showMenuOptions(identifier : String) -> Void {
        
        if identifier == "MyFriendViewController" {
            return
        }
        
        let myFrndVC  = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        AppUtility.shared.navigationController?.pushViewController(myFrndVC!, animated: true)
    }
    
    //MARK:- Other Methods
    
    func getAllContact (){
        
        for contact in contacts {
            
            let name = contact.givenName + " " + contact.familyName
            for phoneNumber in contact.phoneNumbers {
                var dict : Dictionary<String, Any>! = [:]
                dict["name"] = name
                dict["phone"] = phoneNumber.value.stringValue.ignoreExtraCharacters()
                if let imageData = contact.thumbnailImageData {
                    print("image \(String(describing: UIImage(data: imageData)))")
                    profileImage = UIImage(data: imageData)
                    
                } else {
                    print("No image available")
                    
                }
                //dict["profile"] = profileImage
                arrData.append(dict)
            }
            
        }
        arrContacts = arrData.map({$0["phone"]}) as! Array<String>
        
        if (FBSDKAccessToken.current() != nil) {
            self.getFBFriendsList()
        }
        
        self.syncContactAndFacebookFriend()
        self.getFriendsList()
    }
    
    func getFBFriendsList (){
        let anyParametersYouWant:Dictionary = ["limit":20]
        makeFBRequestToPath(aPath: "/me/friends", withParameters: anyParametersYouWant as Dictionary<String, AnyObject>, success: { (results:Array<Dictionary<String, Any>>?) -> () in
            self.arrFacaebokFriend = results!
            var arrFbIds : Array<String> = []
            for dict in results!{
                arrFbIds.append(dict["id"] as! String)
            }
            self.facebookIds = arrFbIds.joined(separator: ",")
            print("Found friends are: \(results)")
            
        }) { (error:NSError?) -> () in
            print("Oops! Something went wrong \(error)")
        }
    }
    
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        
    }
    
    func setupView (){
        btnFriend.addShadow(offset: CGSize(width: 0.0, height: 0.0), radius: 4.0, color: K.Color.postbuttonshadowColor, opacity: 1.0, cornerRadius: 0)
        btnInvited.addShadow(offset: CGSize(width: 0.0, height: 0.0), radius: 4.0, color: K.Color.postbuttonshadowColor, opacity: 1.0, cornerRadius: 0)
        btnPending.addShadow(offset: CGSize(width: 0.0, height: 0.0), radius: 4.0, color: K.Color.postbuttonshadowColor, opacity: 1.0, cornerRadius: 0)
        
        self.btnMyFriendTapped(self.btnFriend)
    }
    
    
    //MARK: - Collectionview Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrSuggestedFriend.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestedFriendCell", for: indexPath) as! SuggestedFriendCell
        
        let suggestedFrnd = arrSuggestedFriend[indexPath.row]
        //cell.setShadow()
        cell.lblName.text = suggestedFrnd.first_name
        cell.imgProfile.setImageWithURL(suggestedFrnd.profile_photo, placeHolderImage:#imageLiteral(resourceName: "ic_image_placeholder_square"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        if (suggestedFrnd.isContactUser == 1){
            cell.imgFriend.image = #imageLiteral(resourceName: "ic_phone")
        }
        if (suggestedFrnd.isFBUser == 1){
            cell.imgFriend.image = #imageLiteral(resourceName: "ic_fb_1")
        }
        else{
        cell.imgFriend.image = #imageLiteral(resourceName: "ic_friendsoffriend")
        }
        
        cell.viewAddFriend.addTapGesture { (gesture) in
            SVProgressHUD.show()
            let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                       "friend_id" : suggestedFrnd.user_id ?? 0]
            
            _ = WebClient.requestWithUrl(url: K.URL.ADD_FRIEND, parameters: dictParam, completion: { (response, error) in
                SVProgressHUD.dismiss()
                if error == nil {
                    
                    let dictData = response as! Dictionary<String, Any>
                    self.syncContactAndFacebookFriend()
                    //self.getFriendsList()
                    self.getInvitedList()
                    ISMessages.show(dictData["message"] as! String, type: .success)
                    
                }
                else {
                    ISMessages.show(error?.localizedDescription)
                }
                
            })
            
        }
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let suggestedFrnd = arrSuggestedFriend[indexPath.row]
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        profileVC.isMyProfile = false
        profileVC.otherUserProfileID = suggestedFrnd.user_id ?? 0
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    
    
    
    //MARK:- WebService Call
    
    @objc func syncContactAndFacebookFriend (){
        
        let dictParam = ["user_id":User.loggedInUser()?.ID ?? 0,
                         "contacts":arrContacts.joined(separator: ","),
                         "facebook_id":facebookIds,
                         "see_all":0] as [String : Any]

        
        SVProgressHUD.show()
        
        _ = WebClient.requestWithUrl(url: K.URL.SYNC_CONTACT, parameters: dictParam, completion: { (response, error) in
            
            SVProgressHUD.dismiss()
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Dictionary<String, Any>
                let arrFriend = arrData["suggested_friends"]
                let isMore = arrData["is_more"] as! Int
                self.lblSeeAll.isHidden = isMore == 0 ? true : false
                self.arrSuggestedFriend = Mapper<Friends>().mapArray(JSONArray: arrFriend as! [[String : Any]])
                self.lblNoDataFound.isHidden = self.arrSuggestedFriend.count == 0 ? false : true
                self.lblSuggestedFriend.isHidden = self.arrSuggestedFriend.count == 0 ? true :false
                self.collectionViewSuggestedFriend.reloadData()
            }
            else{
                ISMessages.show(error?.localizedDescription, type: .warning)
                
            }

            
        })
        
    }
    
    func getFriendsList() {
         SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_FRIENDS, parameters: dictParam, completion: { (response, error) in
             SVProgressHUD.dismiss()
            if error == nil {
                self.arrMyFriend = []
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let myfrnd =  Mapper<Friends>().map(JSON: dict) {
                        self.arrMyFriend.append(myfrnd)
                    }
                }
                
                self.lblNoFriendFound.isHidden = arrData.count == 0 ? false : true
                self.lblNoFriendFound.text = "No Friends Found"
                self.tblFriendList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            
        })
    }
    
    
    func getPendingList() {
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_PENDING_REQUEST, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrPending = []
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let myfrnd =  Mapper<Friends>().map(JSON: dict) {
                        self.arrPending.append(myfrnd)
                    }
                }
                self.lblNoFriendFound.isHidden = arrData.count == 0 ? false : true
               self.lblNoFriendFound.text = "No Pending Request Found"
                self.tblFriendList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    
    func getInvitedList() {
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_INVITED_REQUEST, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrInvited = []
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let myfrnd =  Mapper<Friends>().map(JSON: dict) {
                        self.arrInvited.append(myfrnd)
                    }
                }
                self.lblNoFriendFound.isHidden = arrData.count == 0 ? false : true
                 self.lblNoFriendFound.text = "No Invitation Sent"
                self.tblFriendList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }

    
    func AcceptRejectFriendRequest(status:StatusType,friend:Friends) {

        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "friend_id" :friend.user_id ?? 0,
                                                   "status" : status.rawValue ]

        _ = WebClient.requestWithUrl(url: K.URL.ACCEPT_REJECT, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {

                self.getFriendsList()
                self.getPendingList()
                
//                if status == .ACCEPT{
//                    self.arrPending.remove(element: friend)
//                    self.arrMyFriend.append(friend)
//
//                }
//                else{
//                    self.arrPending.remove(element: friend)
//
//                }
                
                self.tblFriendList.reloadData()

                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
        
    }
    
    func cancelRequest (friend:Friends){
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "friend_id" : friend.user_id ?? 0
        ]
        _ = WebClient.requestWithUrl(url: K.URL.CANCEL_REQUEST, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrInvited = []
                let dictData = response as! Dictionary<String, Any>
                ISMessages.show(dictData["message"] as! String, type: .success)
                
                self.syncContactAndFacebookFriend()
                self.getInvitedList()
                
                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            
        })
    }
    
}

extension MyFriendViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTab == .MYFRIEND {
            return self.arrMyFriend.count
        }
        else if  selectedTab == .INVITED
        {
            return self.arrInvited.count
        }
        else{
            return self.arrPending.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedTab == .MYFRIEND {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyFriendCell", for: indexPath) as! MyFriendCell
            let myfriend = arrMyFriend[indexPath.row]
            cell.lblName.text = myfriend.first_name
            cell.imgProfile.setImageWithURL(myfriend.profile_photo, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            
            cell.imgProfile.addTapGesture { (sender) in
                let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                profileVC.isMyProfile = false
                profileVC.otherUserProfileID = myfriend.user_id ?? 0
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
            
            //My Friend
            cell.viewUnFriend.addTapGesture(action: { (gesture) in
                
                SVProgressHUD.show()
                
                
                let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                           "friend_id" : myfriend.user_id ?? 0]
                
                print(dictParam)
                _ = WebClient.requestWithUrl(url: K.URL.UNFRIEND, parameters: dictParam, completion: { (response, error) in
                    SVProgressHUD.dismiss()
                    if error == nil {
                        
                        let dictData = response as! Dictionary<String, Any>
                        ISMessages.show(dictData["message"] as! String, type: .success)
                        
                        self.syncContactAndFacebookFriend()
                        self.getFriendsList()
                        
                        
                    }
                    else {
                        ISMessages.show(error?.localizedDescription)
                    }
                    
                })
                
            })
            
            return cell
        }
            
       else if  selectedTab == .INVITED
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InvitedCell", for: indexPath) as! InvitedCell
            let invitedUser = arrInvited[indexPath.row]
            cell.lblName.text = invitedUser.first_name
            cell.imgProfile.setImageWithURL(invitedUser.profile_photo, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            
            cell.viewCancelRequest.addTapGesture(action: { (gesture) in
                
                self.cancelRequest(friend: invitedUser)
                
            })
            
            
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PendingCell", for: indexPath) as! PendingCell
            let pendingUser = arrPending[indexPath.row]
            cell.lblName.text = pendingUser.first_name
            cell.imgProfile.setImageWithURL(pendingUser.profile_photo, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            
            cell.viewConfirm.addTapGesture(action: { (gesture) in
                self.AcceptRejectFriendRequest(status: .ACCEPT,friend: pendingUser)
            })
            
            cell.viewDelete.addTapGesture(action: { (gesture) in
                self.AcceptRejectFriendRequest(status: .REJECT,friend: pendingUser)
            })
            
            return cell

            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedTab == .MYFRIEND || selectedTab == .INVITED {
            return 60
        }
        else{
            return 80
        }
    }
    
    //MARK:- Facebook Friend List
    
    func makeFBRequestToPath(aPath:String, withParameters:Dictionary<String, AnyObject>, success successBlock: @escaping (Array<Dictionary<String, Any>>?) -> (), failure failureBlock: @escaping (NSError?) -> ())
    {
        //create array to store results of multiple requests
        let recievedDataStorage:Array<Dictionary<String, Any>> = Array()
        
        //run requests with array to store results in
        p_requestFromPath(path: aPath, parameters: withParameters, storage: recievedDataStorage, success: successBlock, failure: failureBlock)
    }
    
    
    func p_requestFromPath(path:String, parameters params:Dictionary<String, AnyObject>, storage friends:Array<Dictionary<String, Any>>, success successBlock: @escaping (Array<Dictionary<String, Any>>?) -> (), failure failureBlock: @escaping (NSError?) -> ())
    {
        //create requests with needed parameters
        var friends = friends
        let req = FBSDKGraphRequest(graphPath: path, parameters: params, tokenString: FBSDKAccessToken.current().tokenString, version: nil, httpMethod: "GET")
        req?.start(completionHandler: { (connection, result, error) in
            if(error == nil)
            {
                print("result \(String(describing: result))")
                
                let result:Dictionary<String, AnyObject> = result as! Dictionary<String, AnyObject>
                if (result["data"] as! NSArray).count > 0{
                    
                    //add recieved data to array
                    friends.append(contentsOf: result["data"]! as! Array<Dictionary<String, Any>>)
                    //then get parameters of link for the next page of data
                    
                    //let nextCursor:String? = result["paging"]!["next"]! as? String
                    var nextCursor = ""
                    if let dictPaging = result["paging"] as? Dictionary<String,Any> {
                        nextCursor = (dictPaging["next"] as? String ?? "")!
                    }
                    if nextCursor.isValid == true
                    {
                        let paramsOfNextPage:Dictionary = FBSDKUtility.dictionary(withQueryString: nextCursor)
                        
                        if paramsOfNextPage.keys.count > 0
                        {
                            self.p_requestFromPath(path: path, parameters: paramsOfNextPage as! Dictionary<String, AnyObject>, storage: friends, success:successBlock, failure: failureBlock)
                            //just exit out of the method body if next link was found
                            return
                        }
                    }
                    
                    successBlock(friends)
                }
                
                
            }
            else
            {
                //if error pass it in a failure block and exit out of method
                print("error \(String(describing: error))")
                failureBlock(error! as? NSError)
            }
        })
    }
    
}

//MARK:- Take image & AddPost
extension MyFriendViewController {
    
    func showCamera() {
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.openAddPostViewController(image, video: videoURL ,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
    }
    
    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {
        
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostViewController") as! AddPostViewController
        if video != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video])
        }
        else if image != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image])
        }
        else{
            
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }
}


