//
//  ProfileViewController.swift
//  BitterWorld
//
//  Created by rane on 04/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import Floaty

class ProfileViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MenuProtocol {
    
    @IBOutlet weak var scrollViewProfile: UIScrollView!
    
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    
    @IBOutlet weak var collectionProfile: UICollectionView!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewImageAndData: UIView!
    
    @IBOutlet weak var viewProfilePicContainer  : UIView!
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var btnEditProfilePic: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet weak var imgLocationRed: UIImageView!
    @IBOutlet weak var lblEmailID: UILabel!
    
    @IBOutlet weak var btnBlock: UIButton!
 
    
    
    @IBOutlet weak var viewRequest      : UIView!
    @IBOutlet weak var btnRequest       : UIButton!  // used for cancel and send request
    @IBOutlet weak var btnUnFriend      : UIButton!
    @IBOutlet weak var btnAcceptRequest : UIButton!
    @IBOutlet weak var btnRejectRequest : UIButton!
    
    @IBOutlet weak var viewEditProfile: UIView!
    @IBOutlet weak var btnEditProfile: UIButton!
    
    @IBOutlet weak var viewFriendsList: UIView!
    @IBOutlet weak var viewFriendListHeader: UIView!
    @IBOutlet weak var imgFriendsRed: UIImageView!
    @IBOutlet weak var lblFriends: UILabel!
    @IBOutlet weak var lblFriendsCount: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    
    @IBOutlet weak var collectionViewFriends: UICollectionView!
    var colletionViewHeight         : CGFloat = 290.0
    
    @IBOutlet weak var viewPostsHeader: UIView!
    @IBOutlet weak var imgPostRed: UIImageView!
    @IBOutlet weak var lblPosts: UILabel!
    @IBOutlet weak var lblPostsCount: UILabel!
    
    @IBOutlet weak var lblNoDataFound       : UILabel!
    
    
    
    //Varibles
    var isMyProfile         : Bool = true
    var otherUserProfileID  : Int = 0
    
    var arrFriendList       : Array<Friends> = []
    var arrFeedList         : Array<Feed> = []
    var userData            : User?
    
    var totalCount          : Int = 0
    var currentPageNumber   : Int = 0
    var dataTask            : DataRequest!

    var totalFriendsCount   : Int = 0
    var selectedProfileImage: UIImage?
    
    
    var isFriend    : Int = 0
    var isInvited   : Int = 0
    var isPending   : Int = 0
    var isBlocked   : Int = 0
    
    static var ProfileViewControllerVC = "ProfileViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setView()
        self.collectionProfile.register(UINib(nibName: "HomeVCCell", bundle: nil), forCellWithReuseIdentifier: "HomeVCCell")
        
        lblNoDataFound.isHidden = true
        self.currentPageNumber = 0
        AppUtility.shared.delegate = self
        //get Profile Data
        self.viewHeader.isHidden = true
        self.getProfileData()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ProfileViewController.ProfileViewControllerVC), object: nil, queue: OperationQueue.main) { (Notification) in
            if Notification.name.rawValue == ProfileViewController.ProfileViewControllerVC {
                self.getProfileData()
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.scrollViewProfile.height = self.viewHeader.height + 10 + self.collectionProfile.contentHeight + 10 //mehul
        self.collectionProfile.height = self.collectionProfile.contentHeight
        self.scrollViewProfile.autocalculateContentHeight()  //mehul
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppUtility.shared.delegate = self
        if isMyProfile {
            let floaty = Floaty()
            AppUtility.shared.addFloatingButton(to: self.view, floaty: floaty)
        }
    }
    
    
    //MARK: - Web service
    
    func getProfileData() -> Void
    {
        
        if(currentPageNumber == 0){
            currentPageNumber = 1
        }
        
        if((dataTask != nil) && dataTask.task?.state == .running) {
            return
        }
        if(currentPageNumber == 1) {
            SVProgressHUD.show()
        }
        
        var reqParam = ["page"    : currentPageNumber] as Dictionary<String, Any>
        
        if User.loggedInUser() != nil {
            reqParam ["user_id"] = User.loggedInUser()?.ID ?? 0
        }
        
        if(isMyProfile){
            reqParam ["owner_id"] = User.loggedInUser()?.ID ?? 0
        }else{
             reqParam ["owner_id"] = otherUserProfileID
        }
        
        
        dataTask = WebClient.requestWithUrl(url: K.URL.GET_PROFILE_DATA, parameters: reqParam, completion: { (responseObject, error) in
            
            SVProgressHUD.dismiss()
            
            if error == nil {
                if(self.currentPageNumber == 1) {
                    self.arrFeedList.removeAll(keepingCapacity: false)
                }
                
                let dictData = responseObject as! Dictionary<String, Any>
                
                
                self.userData =  User(dict: dictData["profile_info"] as! [String : Any])
                
                
                let arrData = ((responseObject as! NSDictionary).value(forKey: "data")) as! Array<Any>
                let arrTemp = Mapper<Feed>().mapArray(JSONArray: arrData as! [[String : Any]])
                self.arrFeedList.append(contentsOf: arrTemp)
                
                
                let arrDataFriends = ((responseObject as! NSDictionary).value(forKey: "friends")) as! Array<Any>
                let arrTempFriends = Mapper<Friends>().mapArray(JSONArray: arrDataFriends as! [[String : Any]])
                self.arrFriendList.append(contentsOf: arrTempFriends)
        
                
                self.totalFriendsCount = ((responseObject as! NSDictionary).value(forKey: "friends_count")) as! Int
                self.totalCount = ((responseObject as! NSDictionary).value(forKey: "total_count")) as! Int
                
                self.isFriend = ((responseObject as! NSDictionary).value(forKey: "is_friend")) as! Int
                self.isInvited = ((responseObject as! NSDictionary).value(forKey: "is_invited")) as! Int
                self.isPending = ((responseObject as! NSDictionary).value(forKey: "is_pending")) as! Int
                self.isBlocked = ((responseObject as! NSDictionary).value(forKey: "is_blocked")) as! Int
    
                
                self.setupLayout()
                
                self.collectionProfile.reloadData()
                
                
                
            }else{
                
                ISMessages.show(error?.localizedDescription, type: .warning)
            }
            
            self.viewHeader.isHidden = false
            //self.lblNoDataFound.isHidden = self.arrFeedList.count != 0
            self.collectionProfile.reloadData()
        })
        
    }
    
    
    //MARK: - Other Methods

    func setView() {
        
        self.addLogo()
    }
    func addLogo() {
        
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
    
    func showMenuOptions(identifier: String) {
      
      
        if identifier == "ProfileViewController" {
            return
        }
        let chatVC  = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        AppUtility.shared.navigationController?.pushViewController(chatVC!, animated: true)
    }

    func setupLayout() -> Void {
        let img = UIImage(named: isMyProfile ? "ic_addvideo" : "ic_backbutton_top")
        self.btnBack.setImage(img, for: .normal)
        

        
        if(isMyProfile)
        {
            userData = User.loggedInUser()
            self.viewEditProfile.isHidden = false
            self.viewRequest.isHidden = true
            self.btnBlock.isHidden = true
            self.btnChat.isHidden = true
            self.btnEditProfilePic.isHidden = false
            
        }else
        {
            self.btnChat.isHidden = false
            self.btnEditProfilePic.isHidden = true
            self.updateRequestUI()
        }
    
        self.lblUserName.text = userData?.fullName
        self.lblLocationName.text = "\(userData?.city ?? "") \(userData?.country ?? "")"
        self.lblLocationName.sizeToFit()
        self.imgLocationRed.x = self.lblLocationName.right + 5
        self.imgLocationRed.isHidden = (self.lblLocationName.text?.isEmpty)!
        
        self.lblEmailID.y = self.lblLocationName.bottom                 //mehul
        self.viewRequest.y = self.lblEmailID.bottom + 10                //mehul
        self.viewEditProfile.y = self.lblEmailID.bottom + 10            //mehul
        
        self.lblEmailID.text = userData?.email ?? ""
        
        self.imgProfilePic.setImageWithURLObjC(userData?.profilePic ?? "", placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), completionBlock: nil)

        let path = UIBezierPath(roundedRect:self.imgProfilePic.bounds,
                                byRoundingCorners:[.topRight, .bottomRight],
                                cornerRadii: CGSize(width: 8, height:  8))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        self.imgProfilePic.layer.mask = maskLayer
        
        self.lblFriendsCount.text = " \(self.totalFriendsCount)"
        
        if self.totalFriendsCount > 10 {
            self.btnSeeAll.isHidden = false
        }
        else{
            self.btnSeeAll.isHidden = true
        }
        
        
        self.collectionProfile.y = self.viewHeader.bottom + 10      //mehul
        
        self.lblPostsCount.text = " \(self.totalCount)"
        
        

        // Load friends list
        self.collectionViewFriends.reloadData()
        
    }
    

    func getAttributeTitleForButton(ButtonTitle:String) -> NSAttributedString {
        
        let attributedString = NSAttributedString(string: ButtonTitle,
                                                         attributes: [NSAttributedStringKey.foregroundColor : UIColor.darkGray,NSAttributedStringKey.underlineStyle:1])
        
        return attributedString
    }
    
    func updateRequestUI() -> Void {
        
        self.viewEditProfile.isHidden = true
        self.viewRequest.isHidden = false
        
        self.btnBlock.isHidden = false
        self.btnRequest.isHidden = true
        self.btnUnFriend.isHidden = true
        self.btnAcceptRequest.isHidden = true
        self.btnRejectRequest.isHidden = true
        
        if(isBlocked == 1){
            //you have already block this user
            self.btnBlock.setAttributedTitle(self.getAttributeTitleForButton(ButtonTitle: "Unblock"), for: .normal)
        }else{
            self.btnBlock.setAttributedTitle(self.getAttributeTitleForButton(ButtonTitle: "Block"), for: .normal)
        }
        
        if(isFriend == 1)
        {  // Is alredy friends
            self.btnUnFriend.isHidden = false
            
        }else
        {
            if(isPending == 1)
            { // check you have alredy receive request from x user and you are looking x's profile
                self.btnAcceptRequest.isHidden = false
                self.btnRejectRequest.isHidden = false
                
            }else
            {
                self.btnRequest.isHidden = false
                
                if(isInvited == 1){ //Already request pending
                    self.btnRequest.setImage(#imageLiteral(resourceName: "cancel_request_icon"), for: .normal)
                    self.btnRequest.setTitle("Cancel Request", for: .normal)
                }else
                { //send new friend request
                    self.btnRequest.setImage(#imageLiteral(resourceName: "send_friend_request_icon"), for: .normal)
                    self.btnRequest.setTitle("Send Request", for: .normal)
                }
            }
            
        }
    }
    

    //MARK: - CollectionView Delegates Methods
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView == self.collectionProfile) {

            let feed = self.arrFeedList[indexPath.row]
            var dynHeight : CGFloat = feed.commentCount == 0 ? 395 : 435.0
            let font = UIFont(name: kPTSansRegularFontName, size: 14)!
            var descHeight: CGFloat = 0.0
            if self.arrFeedList[indexPath.row].readMore == true {
                var descCountry = ""
                if let desc = self.arrFeedList[indexPath.row].desc, !desc.isEmpty {
                    descCountry.append(desc)
                }
                descHeight = descCountry.size(collectionView.width - 40, font: font, lineBreakMode: .byWordWrapping).height
                print("descHeight : ",descHeight)

            } else {
                descHeight = font.lineHeight * 2
            }
            dynHeight = dynHeight + descHeight
            return CGSize(width: self.collectionProfile.width, height: dynHeight)
        } else {
            return CGSize(width: 100, height: 100)
        }
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
     {
        if(collectionView == collectionViewFriends)
        {
         return self.arrFriendList.count
        }else
        {
        //return 0
        return self.arrFeedList.count
        }
     }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if(collectionView == collectionViewFriends)
        {
            let objFriends = arrFriendList[indexPath.row]
            
            let strIdentifier = "FriendListCell"
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: strIdentifier, for: indexPath) as! FriendListCell
            
            cell.objFriendList = objFriends
            
            return cell
        }else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVCCell", for: indexPath) as! HomeVCCell

            cell.viewFooterContent.isHidden = false
            cell.viewEnteComment.isHidden = false
            
            let feed = self.arrFeedList[indexPath.row]
            cell.viewContent.backgroundColor = UIColor.white
            cell.feed = feed
            cell.viewEnteComment.addTapGesture { (gesture) in
                let commentVC  = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController")  as! CommentViewController
                commentVC.objFeed = feed
                self.navigationController?.pushViewController(commentVC, animated: true)
            }
            
            cell.btnChat.addTapGesture { (gesture) in
                let commentVC  = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController")  as! CommentViewController
                commentVC.objFeed = feed
                self.navigationController?.pushViewController(commentVC, animated: true)
            }
    
            cell.btnMoreOption.tag = indexPath.row
            cell.selectDelegate = self
            cell.layoutIfNeeded()
            return cell
        }
    }
    
    
    //MARK: - Buttons Actions
    
    @IBAction func btnEditProfilePicClick(_ sender: Any) {
        
        AppUtility.shared.showImageSelectionOption(viewController: self, isCropEnable: false) { (selectedImage) in
            if(selectedImage != nil)
            {
                self.selectedProfileImage = selectedImage
                self.uploadProfileImage()
            }
        }
        
    }
    
    @IBAction func btnBlockClick(_ sender: Any) {
        
        let req = ["user_id": User.loggedInUser()?.ID! ?? 0,
                   "blocked_user_id" : userData?.ID! ?? 0] as Dictionary<String,Any>
        
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.BLOCK_UNBLOCK_USER, parameters: req, completion: { (responseObject, error) in
            SVProgressHUD.dismiss()
            
            if(error == nil)
            {
                if(self.isBlocked == 1)
                {
                    self.isBlocked = 0
                }else{
                    self.isBlocked = 1
                }
                self.updateRequestUI()
                
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
        })
    }
    
    
    @IBAction func btnRequestClick(_ sender: Any) {
        
        var url = K.URL.ADD_FRIEND
        
        if(isInvited == 1)
        {
            url = K.URL.CANCEL_REQUEST
        }
        
        let req = ["user_id": User.loggedInUser()?.ID! ?? 0,
                   "friend_id" : userData?.ID! ?? 0] as Dictionary<String,Any>
        
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: url, parameters: req, completion: { (responseObject, error) in
            SVProgressHUD.dismiss()
            
            if(error == nil)
            {
                if(self.isInvited == 1)
                {
                    self.isInvited = 0
                }else{
                    self.isInvited = 1
                }
                self.updateRequestUI()
                
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
        })
    }
    
    @IBAction func btnUnFriendClick(_ sender: Any) {
        
        let req = ["user_id": User.loggedInUser()?.ID! ?? 0,
                   "friend_id" : userData?.ID! ?? 0] as Dictionary<String,Any>
        
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.UNFRIEND, parameters: req, completion: { (responseObject, error) in
            SVProgressHUD.dismiss()
            
            if(error == nil)
            {
                if(self.isFriend == 1)
                {
                    self.isFriend = 0
                }else{
                    self.isFriend = 1
                }
                self.updateRequestUI()
                
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
        })
        
    }
    @IBAction func btnAcceptRequestClick(_ sender: Any) {
        self.acceptRejectRequest(isFromAccept: true)
    }
    
    @IBAction func btnRejectRequestClick(_ sender: Any) {
        self.acceptRejectRequest(isFromAccept: false)
    }
    
    
    
//    ADD_FRIEND  //    CANCEL_REQUEST
//    ACCEPT_REJECT
//    UNFRIEND
//    BLOCK_UNBLOCK_USER
    
//    Add Friend: .../user/add-friend Params: user_id, friend_id
//    Accept or Reject request: .../user/accept-reject-request Params: user_id, friend_id, status(1=accept, 2=reject)
//    Unfriend to user: .../user/un-friend Params: user_id, friend_id
//    Cancel Request: URL: .../user/cancel-request Params: user_id, friend_id
    
    @IBAction func btnEditProfileClick(_ sender: Any) {
        
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        editProfileVC.userData = self.userData
        self.navigationController?.pushViewController(editProfileVC, animated: true)
        
    }
    
    @IBAction func btnSeeAllClick(_ sender: Any) {
        
        let friendVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        friendVC.userId = isMyProfile ? (User.loggedInUser()?.ID)! : otherUserProfileID
        friendVC.userName = self.lblUserName.text ?? ""
        self.navigationController?.pushViewController(friendVC, animated: true)
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
        
        if isMyProfile {
            self.showCamera()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func btnChatClick(_ sender: Any) {
        
        let messageList = AppUtility.GET_CONTROLLER(controllerName: "ChatMessageViewController") as! ChatMessageViewController
        // Set Conversaion Data.
        //messageList.arrConversionList = self.arrChatList as! Array<ChatList>
        //messageList.selectedUserId = data.displayUserId()
        //messageList.selectedConversionId = data.conversationID
        //messageList.selectedUserName = data.displayName()
        self.navigationController?.pushViewController(messageList, animated: true)
        
        
    }
    
    
    //MARK:-   Camera
    func showCamera (){
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.openAddPostViewController(image, video: videoURL ,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
        
    }
    
    
    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostViewController") as! AddPostViewController
        if video != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video])
        } else if image != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image])
        } else{
            
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }

    
    
    func acceptRejectRequest(isFromAccept:Bool) -> Void {
        
        var req = ["user_id": User.loggedInUser()?.ID! ?? 0,
                   "friend_id" : userData?.ID! ?? 0] as Dictionary<String,Any>
        
        if(isFromAccept)
        {
            req["status"] = 1
        }else
        {
            req["status"] = 2
        }
        
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.CANCEL_REQUEST, parameters: req, completion: { (responseObject, error) in
            SVProgressHUD.dismiss()
            
            if(error == nil)
            {
                if(isFromAccept)
                {
                    self.isFriend = 1
                    self.isPending = 0
                }
                self.updateRequestUI()
                
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
        })
        
    }
    
    func uploadProfileImage(){
       
        SVProgressHUD.show()
                WebClient.uploadMedia(mediaType: "profile_photo",dirName:"profile_photo" ,image: self.selectedProfileImage, completion: { (responseObject, fileName, error) in
                    if error == nil {
                        self.imgProfilePic.image = self.selectedProfileImage
                        self.editProfile(profilePhotoFileName: fileName ?? "")
                    }
                    else{
                        SVProgressHUD.dismiss()
                        ISMessages.show(error?.localizedDescription, type: .warning)
                    }
        })
    }
    
    //MARK: - WebService
    
    func editProfile(profilePhotoFileName:String) -> Void {
        
        let reqParam = ["user_id"    : User.loggedInUser()?.ID ?? 0,
                        "profile_photo" : profilePhotoFileName] as Dictionary<String, Any>
        
        
        _ = WebClient.requestWithUrl(url: K.URL.UPDATE_PROFILE, parameters: reqParam, completion: { (responseObject, error) in
            SVProgressHUD.dismiss()
            if(error == nil)
            {
                if  let dictResponse = (responseObject as? Dictionary<String,Any>)? [K.Key.Data] as? Dictionary<String,Any>{
                    
                    let user =  User(dict: dictResponse)
                    user.save()
                    ISMessages.show("Profile photo updated successfully")
                }
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
        })
    }
    
}

//MARK: - TextField Delegate Methods   //mehul
extension ProfileViewController : HomeVCCellDelegate {
    
    func reloadData() {
        //self.collectionProfile.layoutIfNeeded()
        self.collectionProfile.reloadData()
        //self.collectionProfile.collectionViewLayout.invalidateLayout()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
//            // Your code with delay
//            self.collectionProfile.height = self.collectionProfile.contentHeight
//            self.scrollViewProfile.autocalculateContentHeight()
//            self.view.layoutIfNeeded()
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.collectionProfile.height = self.collectionProfile.contentHeight
            self.scrollViewProfile.autocalculateContentHeight()
            self.view.layoutIfNeeded()
        })
        
    }
    
    func didFinishSelection() {
        self.reloadData()
    }
}
