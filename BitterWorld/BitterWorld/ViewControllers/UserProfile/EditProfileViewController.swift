//
//  EditProfileViewController.swift
//  BitterWorld
//
//  Created by rane on 04/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class EditProfileViewController: UIViewController {

    @IBOutlet weak var scrollViewMain: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var btnBack: UIBarButtonItem!
    
    @IBOutlet weak var viewFullName     : UIView!
    @IBOutlet weak var lblFullName      : UILabel!
    @IBOutlet weak var txtFullName      : UITextField!
    
    @IBOutlet weak var viewNickName     : UIView!
    @IBOutlet weak var lblNickName      : UILabel!
    @IBOutlet weak var txtNickName      : UITextField!
    @IBOutlet weak var lblAddnickName   : UILabel!
    
    @IBOutlet weak var btnNickNameTick  : UIButton!
    
    
    @IBOutlet weak var viewEmail        : UIView!
    @IBOutlet weak var lblEmail         : UILabel!
    @IBOutlet weak var txtEmail         : UITextField!
    
    @IBOutlet weak var viewMobileNo     : UIView!
    @IBOutlet weak var lblMobileNo      : UILabel!
    @IBOutlet weak var txtMobileNo      : UITextField!
    
    @IBOutlet weak var viewGender       : UIView!
    @IBOutlet weak var lblGender        : UILabel!
    
    @IBOutlet weak var btnMale          : UIButton!
    @IBOutlet weak var btnFemale        : UIButton!
    
    @IBOutlet weak var btnDone: UIButton!

    
    //Varibles
    var isMaleSelected      : Bool = true
    var isNickNameSelected  : Bool = false
    
    var userData : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()

    }

    //MARK:- Other Methods
    
    func setupLayout() -> Void {
        
        if(userData != nil)
        {
            self.txtFullName.text   = userData?.fullName ?? ""
            self.txtNickName.text   = userData?.userName ?? ""
            self.txtEmail.text      = userData?.email ?? ""
            self.txtMobileNo.text   = userData?.phone ?? ""
            
            if(userData?.gender == 0){
                //female
                self.btnMaleFemaleClick(self.btnFemale)
            }else{
                //male
                self.btnMaleFemaleClick(self.btnMale)
            }
        
            self.lblEmail.isEnabled = false
            scrollViewMain.contentSizeToFit()
        }
        
        
    }


    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNickNameTickClick(_ sender: Any) {
        
        if((sender as? UIButton)?.isSelected == true)
        {
            isNickNameSelected = false
            (sender as? UIButton)?.isSelected = false
        }else
        {
            isNickNameSelected = true
            (sender as? UIButton)?.isSelected = true
        }
    }

    @IBAction func btnMaleFemaleClick(_ sender: Any) {
      
        if(sender as? UIButton == btnMale)
        {
            isMaleSelected = true
            btnMale.isSelected = true
            btnFemale.isSelected = false
        }else
        {
            isMaleSelected = false
            btnFemale.isSelected = true
            btnMale.isSelected = false
        }
  
    }
    
    @IBAction func btnDoneClick(_ sender: Any) {
        
        if dataValidation() == true {
          self.editProfile()
        }
    }
    
    func dataValidation() -> Bool {
        
        var strMessage : String = ""
        
        if txtFullName.text?.isValid == false {
            strMessage = K.Message.enterYourName
        }
        else if txtNickName.text?.isValid == false {
            strMessage = K.Message.enterUsername
        }
        else if txtMobileNo.text?.isValid == false {
            strMessage = K.Message.enterPhone
        }
        if(strMessage != ""){
            ISMessages.show(strMessage, type: .warning)
            return false
        }else{
            return true
        }
    }
    
    
    //MARK: - WebService
    
    func editProfile() -> Void {
        
        SVProgressHUD.show()
        
        let reqParam = ["user_id"    : User.loggedInUser()?.ID ?? 0,
                        "first_name" : txtFullName.text!,
                        "nick_name"  : txtNickName.text!,
                        "phone"      : txtMobileNo.text!,
                        "use_nickname" : isNickNameSelected == true ? 1 :0,
                        "gender"       : isMaleSelected == true ? 1 :0] as Dictionary<String, Any>
        
        
        _ = WebClient.requestWithUrl(url: K.URL.UPDATE_PROFILE, parameters: reqParam, completion: { (responseObject, error) in
            
            if(error == nil)
             {
                if  let dictResponse = (responseObject as? Dictionary<String,Any>)? [K.Key.Data] as? Dictionary<String,Any>{
                    
                    let user =  User(dict: dictResponse)
                    user.save()
                    ISMessages.show("Profile updated successfully")
                    self.navigationController?.popViewController(animated: true)
                }
            }else
            {
                ISMessages.show(error?.localizedDescription)
            }
            
            SVProgressHUD.dismiss()
        })
    }
    

}
