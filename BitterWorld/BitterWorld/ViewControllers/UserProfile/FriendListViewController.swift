//
//  FriendListViewController.swift
//  BitterWorld
//
//  Created by rane on 04/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper




class FriendListViewController: UIViewController,UISearchBarDelegate {

    
    @IBOutlet weak var viewHeader           : UIView!
    @IBOutlet weak var lblUserFriendsList   : UILabel!
    @IBOutlet weak var imgRedFriendIcon     : UIImageView!
    @IBOutlet weak var lblFriends           : UILabel!
    @IBOutlet weak var lblFriendsCount      : UILabel!
    @IBOutlet weak var lblNoFriendFound      : UILabel!
    
    @IBOutlet weak var tblFriendList    : UITableView!
    @IBOutlet weak var btnBack          : UIButton!

    @IBOutlet var viewRight         : UIView!
    @IBOutlet var viewLeft          : UIView!
    
    
    var arrMyFriend: Array<Friends> = []
    var arrFilterFriend: Array<Friends> = []
    var isSerching : Bool = false
    var userId : Int = 0
    var userName : String = ""
    //search bar
    var searchBar: UISearchBar?
    
    var otherUserId : Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar = UISearchBar()
        searchBar?.showsCancelButton = true
        searchBar?.delegate = self
        searchBar?.frame = CGRect(x: 0, y:0, width: self.view.width, height: 44)
        searchBar?.barStyle = .black
        
        self.navigationItem.hidesBackButton = true
        self.lblUserFriendsList.text = "\(userName)'s Friend list"
        self.getFriendsList()
        self.addLogo()
        
    }

    
    // MARK: - Webservice
    
    
    func getFriendsList() {
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "login_user_id" :  User.loggedInUser()?.ID ?? 0,
                                                   "user_id" : userId ]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_FRIENDS, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrMyFriend = []
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let myfrnd =  Mapper<Friends>().map(JSON: dict) {
                        self.arrMyFriend.append(myfrnd)
                    }
                }
                
                self.lblFriendsCount.text = "\(self.arrMyFriend.count)"
                self.lblNoFriendFound.isHidden = arrData.count == 0 ? false : true
                self.lblNoFriendFound.text = "No Friends Found"
                self.tblFriendList.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            
        })
    }
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        
    }
    
    
    @IBAction func btnBackTapped (_ sender : UIButton)
    {
        AppUtility.shared.navigationController?.popViewController()
    }
    
    
    //MARK : - IBAction
    
    @IBAction func btnSearchTapped (_sender : UIButton){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar?.alpha = 0.0
        }) { (finished) in
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.titleView = self.searchBar
            self.searchBar?.alpha = 1.0
            self.viewRight.isHidden = true
            self.searchBar?.becomeFirstResponder()
        }
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar?.alpha = 0.0
            self.viewRight.isHidden = false
            
            let RightNavBarButton = UIBarButtonItem(customView:self.viewRight)
            self.navigationItem.rightBarButtonItem = RightNavBarButton
            let LeftNavBarButton = UIBarButtonItem(customView:self.viewLeft)
            self.navigationItem.leftBarButtonItem = LeftNavBarButton
            self.addLogo()
            self.searchBar?.text = ""
            self.isSerching = false
            self.tblFriendList.reloadData()
        }) { (finished) in
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text \(searchBar.text!)")
        if (searchBar.text?.isValid)!{
            isSerching = true
            self.arrFilterFriend = self.arrMyFriend.filter({ (friend) -> Bool in
                return (friend.first_name?.localizedCaseInsensitiveContains(searchBar.text!))!
            })
            self.tblFriendList.reloadData()
        }
        else{
            isSerching = false
            self.tblFriendList.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        return true
    }
    
    


    // MARK: - Friend Cancle Request
    
    func cancelRequest (friend:Friends){
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "friend_id" : friend.user_id ?? 0
        ]
        _ = WebClient.requestWithUrl(url: K.URL.CANCEL_REQUEST, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrMyFriend = []
                let dictData = response as! Dictionary<String, Any>
                ISMessages.show(dictData["message"] as! String, type: .success)
                
               self.getFriendsList()
                
                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            
        })
    }
    
    func AcceptRejectFriendRequest(status:StatusType,friend:Friends) {
        
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "friend_id" :friend.user_id ?? 0,
                                                   "status" : status.rawValue ]
        
        _ = WebClient.requestWithUrl(url: K.URL.ACCEPT_REJECT, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                
                self.getFriendsList()
                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
        
    }

}

extension FriendListViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isSerching ? self.arrFilterFriend.count : self.arrMyFriend.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFriendCell", for: indexPath) as! MyFriendCell
        let myfriend = self.isSerching ? arrFilterFriend[indexPath.row] : arrMyFriend[indexPath.row]
        cell.lblName.text = myfriend.first_name
        cell.imgProfile.setImageWithURL(myfriend.profile_photo, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        

        if myfriend.isFriend == 0 && myfriend.isPending == 0 && myfriend.isInvited == 0 {
            
            if myfriend.user_id == User.loggedInUser()?.ID {
                cell.viewUnFriend.isHidden = true
            }
            else{
                cell.viewUnFriend.isHidden = false
            }
            
            
            cell.viewPending.isHidden = true
            cell.viewCancel.isHidden = true
            
        }
        else {
            
            if myfriend.isFriend == 1 {
                cell.viewUnFriend.isHidden = true
                cell.viewPending.isHidden = true
                cell.viewCancel.isHidden = true
            }
            
            
            if myfriend.isPending == 1{
                cell.viewPending.isHidden = false
                cell.viewUnFriend.isHidden = true
                cell.viewCancel.isHidden = true
            }
            
            
            if myfriend.isInvited == 1 {
                cell.viewCancel.isHidden = false
                cell.viewPending.isHidden = true
                cell.viewUnFriend.isHidden = true
                
            }
            
            
            
        }
        
        
        cell.lblAccept.addTapGesture { (gesture) in
            print("Accept")
            self.AcceptRejectFriendRequest(status: .ACCEPT, friend: myfriend)
        }
        
        cell.lblReject.addTapGesture { (gesture) in
            self.AcceptRejectFriendRequest(status: .REJECT, friend: myfriend)
        }
        
        
        cell.lblCancelRequest.addTapGesture { (gesture) in
            self.cancelRequest(friend: myfriend)
        }
        
            
        cell.lblUnfriend.addTapGesture { (gesture) in
            
            SVProgressHUD.show()
            let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                       "friend_id" : myfriend.user_id ?? 0]
            
            _ = WebClient.requestWithUrl(url: K.URL.ADD_FRIEND, parameters: dictParam, completion: { (response, error) in
                SVProgressHUD.dismiss()
                if error == nil {
                    
                    //self.arrSuggestedFriend.remove(element: myfriend)
                    let dictData = response as! Dictionary<String, Any>
                    ISMessages.show(dictData["message"] as! String, type: .success)
                    self.getFriendsList()
                    
                }
                else {
                    ISMessages.show(error?.localizedDescription)
                }
                
            })
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
    }
    
    
}
