//
//  AddPostMediaViewController.swift


//

import UIKit
//import Sharaku
private let maxVideoLimitInSeconds : Double = 10

class AddPostMediaViewController: UIViewController {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnSwitchCamera: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewMediaSelection: UIView!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnAudio: UIButton!
    
    var camera = LLSimpleCamera()
    var videoLengthInSeconds: Double = 0
    private var timer: Timer?
    var mediaSelectionCompletion: ((_ image: UIImage?, _ videoURL: URL? , _ audioURL: URL?) -> (Void))?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setTopBarNaviagtion()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.camera.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup Topbar Options
    
    func setTopBarNaviagtion() -> Void {
        let topbarView = TopBarView(title: "Photo", leftOptionImages: ["ic_close"], rightOptionImages: [])
        topbarView.backgroundColor = .white
        topbarView.leftOptionDidTapped = { (barItem: UIButton) -> Void in
            self.dismiss(animated: true, completion: { 
                
            })
        }
        self.view.addSubview(topbarView)
    }
    
    // MARK: - Other Methods
    
    func initialSetup() {
        lblTime.isHidden = true
        btnSwitchCamera.isHidden = false
        btnPhoto.isSelected = true
        
        // Modified By Martin.
        self.camera = LLSimpleCamera(quality: AVCaptureSession.Preset.medium.rawValue, position: LLCameraPositionRear, videoEnabled: true)
        self.camera.attach(to: self, withFrame: CGRect(x:0, y:self.viewContainer.y, width: self.viewContainer.width, height: self.viewContainer.height))
        self.camera.fixOrientationAfterCapture = true
        
        if (self.camera.position == LLCameraPositionRear && self.btnFlash.isHidden) {
            self.btnFlash.isHidden = false
        }
        
        self.camera.onDeviceChange = {(camera, device) -> Void in
            if (camera?.isFlashAvailable())! {
                self.btnFlash.isHidden = false
                if camera?.flash == LLCameraFlashOff {
                    self.btnFlash.setImage(#imageLiteral(resourceName: "off_flashcam_icon"), for : .normal)
                }
                else if camera?.flash == LLCameraFlashAuto {
                    self.btnFlash.setImage(#imageLiteral(resourceName: "auto_flashcam_icon"), for : .normal)
                }
                else {
                    self.btnFlash.setImage(#imageLiteral(resourceName: "flash_cam_icon"), for : .normal)
                }
            }
            else {
                self.btnFlash.isHidden = true
            }
        }
        
        self.camera.onError = {(camera, error) -> Void in
            print("Camera error: \(error?.localizedDescription ?? "")")
            if (error!._domain == LLSimpleCameraErrorDomain) {
                if error!._code == 10 || error!._code == 11 {
                    self.lblMessage.text = K.Message.weNeedPermission
                    self.lblMessage.isHidden = false
                    
                    self.btnSwitchCamera.isEnabled = false
                    self.btnFlash.isEnabled = false
                    self.btnCapture.isEnabled = false
                }
            }
        }
        
        if(LLSimpleCamera.isFrontCameraAvailable() && LLSimpleCamera.isRearCameraAvailable()){
            lblMessage.isHidden = true
        }
        else{
        }
        self.view.bringSubview(toFront: viewContainer)
    }
    
    func captureImage() {
        if (!self.camera.isRecording) {
            self.camera.capture({(camera, image, metadata, error) -> Void in
                if error == nil {
                    
                    let imageToBeFiltered = image
                    let vc = SHViewController(image: imageToBeFiltered!)
                    vc.delegate = self
                    self.present(vc, animated:true, completion: nil)
                    
                    
                   // self.redirectToPreviousScreen(image: image,videoURL: nil,audioURL: nil)
                }
                else {
                    print("An error has occured: \(error?.localizedDescription ?? "")")
                }
            }, exactSeenImage: true)
        }
        else{
            //Tap on btnCapture to Stop Video
            self.resetUIAfterVideoRecord()
        }
    }
    
    func videoRecord() {
        if !self.camera.isRecording {
            if (self.camera.position == LLCameraPositionRear && !self.btnFlash.isHidden) {
                self.btnFlash.isHidden = true
            }
            self.btnSwitchCamera.isHidden = true
            videoLengthInSeconds = 0
            self.lblTime.isHidden = false
            
            self.startTimer()
            
            //Set Stop image
            self.btnCapture.setImage(#imageLiteral(resourceName: "video_stop_button_icon"), for: .normal)
            
            // start recording
            let outputURL: URL? = AppUtility.shared.getDocumentsDirectory().appendingPathComponent("test").appendingPathExtension("mov")
            
            self.camera.startRecording(withOutputUrl: outputURL, didRecord: { (camera, url, error) in
                if error == nil {
                    //Redirect to Video Preview
                    SVProgressHUD.show()
                    let attachmentVideoUrl = AppUtility.shared.getDocumentsDirectory().appendingPathComponent("attachmentVideo.mp4")
                    AppUtility.shared.convertVideoToMp4(outputURL!, withFileUrl: attachmentVideoUrl, completion: { (success) in
                        SVProgressHUD.dismiss()
                        self.redirectToPreviousScreen(image: nil, videoURL: attachmentVideoUrl,audioURL: nil)
                    })
                }
                else {
                }
            })
        }
        else {
        }
    }
    
    func startTimer() {
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
        // Disable Other Options.
        self.btnGallery.isEnabled = false
        self.btnPhoto.isEnabled = false
    }
    
    func stopTimer() {
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
        
        // Enable Other Options.
        self.btnGallery.isEnabled = true
        self.btnPhoto.isEnabled = true
    }
    
    @objc func updateCounter() {
        videoLengthInSeconds += 1
        lblTime.text = stringFromTimeInterval() as String
        
        if videoLengthInSeconds == maxVideoLimitInSeconds {
            self.resetUIAfterVideoRecord()
        }
    }
    
    func stringFromTimeInterval() -> NSString {
        // let hours = Int(totalSeconds / 3600)
        let minutes = Int((videoLengthInSeconds.truncatingRemainder(dividingBy: 3600)) / 60)
        let seconds = Int(videoLengthInSeconds.truncatingRemainder(dividingBy: 60))
        let str = NSString(format: "%0.2d:%0.2d",minutes,seconds)
        return str
    }
    
    func resetUIAfterVideoRecord() {
        if (self.camera.position == LLCameraPositionRear && self.btnFlash.isHidden) {
            self.btnFlash.isHidden = false
        }
        self.lblTime.isHidden = true
        self.btnSwitchCamera.isHidden = false
        self.btnCapture.setImage(#imageLiteral(resourceName: "pic_button_icon"), for: .normal)
        self.camera.stopRecording()
        self.stopTimer()
    }
    
    func redirectToPreviousScreen(image: UIImage?,videoURL: URL?,audioURL: URL?) {
        self.dismiss(animated: true, completion: {
            
            var videoThumb : UIImage?
            if videoURL != nil {
                videoThumb = AppUtility.shared.thumbFromVieoURL(videoURL: videoURL!)
                self.mediaSelectionCompletion?(videoThumb, videoURL, audioURL)
            }
            else if audioURL != nil {
                self.mediaSelectionCompletion?(videoThumb, videoURL, audioURL)
            }
            else {
                self.mediaSelectionCompletion?(image, videoURL, audioURL)
            }
        })
    }
    
    // MARK: - Buttons
    
    @IBAction func btnSwitchCameraTapped(_ sender: Any) {
        if camera.position == LLCameraPositionRear {
            btnFlash.isHidden = false
        }
        else{
            btnFlash.isHidden = true
        }
        self.camera.togglePosition()
    }
    
    @IBAction func btnFlashTapped(_ sender: Any) {
        if self.camera.flash == LLCameraFlashOff {
            _ = self.camera.updateFlashMode(LLCameraFlashOn)
            self.btnFlash.setImage(#imageLiteral(resourceName: "flash_cam_icon"), for : .normal)
        }
        else if self.camera.flash == LLCameraFlashOn {
            _ = self.camera.updateFlashMode(LLCameraFlashAuto)
            self.btnFlash.setImage(#imageLiteral(resourceName: "auto_flashcam_icon"), for : .normal)
        }
        else {
            _ = self.camera.updateFlashMode(LLCameraFlashOff)
            self.btnFlash.setImage(#imageLiteral(resourceName: "off_flashcam_icon"), for : .normal)
        }
    }

    @IBAction func btnCaptureTapped(_ sender: Any) {
        if btnPhoto.isSelected {
            captureImage()
        }
        else {
            resetUIAfterVideoRecord()
            
            /*if !self.camera.isRecording {
                videoRecord()
            }
            else {
                resetUIAfterVideoRecord()
            }*/
        }
    }
    
    @IBAction func btnGalleryTapped(_ sender: Any) {
        if !self.camera.isRecording {
            AppUtility.shared.showMediaSelectionOptionWithoutCamera(viewController: self) { (videoURL, image) in
                if image == nil {
                    self.redirectToPreviousScreen(image: nil,videoURL: videoURL,audioURL: nil)
                }
                else {
                    let imageToBeFiltered = image
                    let vc = SHViewController(image: imageToBeFiltered!)
                    vc.delegate = self
                    self.present(vc, animated:true, completion: nil)

//                    self.redirectToPreviousScreen(image: image,videoURL: nil,audioURL: nil)
                }
            }
        }
    }
    
    @IBAction func btnPhotoTapped(_ sender: Any) {
        if !self.camera.isRecording {
            self.btnCapture.setImage(#imageLiteral(resourceName: "pic_button_icon"), for: .normal)
            btnPhoto.isSelected = true
            btnVideo.isSelected = false
        }
    }
    
    @IBAction func btnVideoTapped(_ sender: Any) {
        if !self.camera.isRecording {
            btnPhoto.isSelected = false
            btnVideo.isSelected = true
            btnAudio.isUserInteractionEnabled = false

            videoRecord()
            
            //Set Video Record image
            //self.btnCapture.setImage(#imageLiteral(resourceName: "video_button_icon"), for: .normal)
        }
    }
    
    @IBAction func btnAudioTapped(_ sender: Any) {
        let editAudioViewController = AppUtility.GET_CONTROLLER(controllerName: "EditAudioViewController") as! EditAudioViewController
        editAudioViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
       self.presentPopupViewController(editAudioViewController, animationType: .slideBottomTop, backgroundTouch: false, dismissed: {
            if let trimmedUrl = editAudioViewController.strTrimmedAudioURL {
                var attachmentAudioFilePath     : String! = ""
                attachmentAudioFilePath = NSString.temporaryPath().appendingPathComponent("attachmentAudio.wav")
                // Remove Previously added file.
                if FileManager.default.fileExists(atPath: attachmentAudioFilePath) {
                    try? FileManager.default.removeItem(atPath: attachmentAudioFilePath)
                }
                try? FileManager.default.copyItem(atPath: trimmedUrl, toPath: attachmentAudioFilePath)
               
                self.redirectToPreviousScreen(image: nil, videoURL: nil, audioURL: URL(fileURLWithPath: attachmentAudioFilePath))
               
            }
        })
        
        
    }
}

extension AddPostMediaViewController: SHViewControllerDelegate {
    func shViewControllerImageDidFilter(image: UIImage) {
        // Filtered image will be returned here.
        self.dismiss(animated: true, completion: nil)
         self.redirectToPreviousScreen(image: image,videoURL: nil,audioURL: nil)
    }
    
    func shViewControllerDidCancel() {
        // This will be called when you cancel filtering the image.
    }
}
