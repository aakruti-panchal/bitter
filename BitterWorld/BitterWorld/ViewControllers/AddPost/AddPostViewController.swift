//
//  AddPostViewController.swift
//  BitterWorld
//
//  Created by  " " on 18/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import ObjectMapper

class AddPostViewController: UIViewController ,UIScrollViewDelegate,LocationPickerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var btnPrivate: UIButton!
    @IBOutlet weak var btnPublic: UIButton!
    
    @IBOutlet weak var lblSelectedLocation: UILabel!
    @IBOutlet weak var lblAddmedia        : UILabel!
    @IBOutlet weak var lblCategory        : UILabel!
    
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var viewCategory: UIView!
    
    @IBOutlet weak var scrollview: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var CollectionViewMedia: UICollectionView!

    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    var selectedLocationCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D()
    var arrMedia            : [[String : Any?]] = []
    var arrCategories         : Array<Categories> = []
    var categoryId : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCategories()
        self.setupNavigation()
        
        self.scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.btnPost.bottom + 50)
        self.lblAddmedia.isHidden = true
        self.ratingView.rating = 1.0
        viewLocation.addTapGesture { (gesture) in
        
            self.selectedLocationCoordinate = AppUtility.shared.currentLocation.coordinate
            let locationPicker = self.storyboard?.instantiateViewController(withIdentifier: "LocationPickerViewController") as! LocationPickerViewController
            locationPicker.isPresented = false
            locationPicker.delegate = self
            self.navigationController?.pushViewController(locationPicker, animated: true)
            
        }
        
        
        viewCategory.addTapGesture { (gesture) in
            let arrCountryName = self.arrCategories.map({$0.name}) as! Array<String>
            if arrCountryName.count > 0 {
                RMPickerViewController.show(withTitle: "Select Country", selectedIndex: 0, array: arrCountryName, doneBlock: { (index, object) in
                    print(index)
                    let selectedCategory = self.arrCategories[index]
                    self.categoryId = selectedCategory.ID!
                    self.lblCategory.text = object as? String
                    

                }, cancel: {

                })
            }
        }
        
    }
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK:- Collectionview Delegate DataSource
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.CollectionViewMedia.width, height: self.CollectionViewMedia.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCell", for: indexPath) as! MediaCell
        let dict = arrMedia[indexPath.item]
        let mediaType = dict[K.Key.mediaTypekey] as! PostMediaType
        let isUploaded = dict[K.Key.isUploadedKey] as? Bool ?? false
        
        if isUploaded == false {
            if ((dict[K.Key.videoUrlkey] is URL) || (dict[K.Key.audioUrlkey] is URL)){
                
                if (mediaType == .video) {
                    cell.imgPlay.image = #imageLiteral(resourceName: "ic_play")
                }
                else{
                    cell.imgPlay.image = #imageLiteral(resourceName: "ic_record_btn_icon")
                }
                cell.imgPlay.isHidden = false
                
            }
            else {
                cell.imgPlay.isHidden = true
            }
            if (mediaType == .audio) {
                cell.imgeMedia.image = #imageLiteral(resourceName: "audio_placeholder")
            }
            else{
            cell.imgeMedia.image = dict[K.Key.postImagekey] as? UIImage
            }
            
            
        }
        else {
           
        }
        
        self.showHideMedia()
        cell.btnRemove.addTapGesture { (gesture) in
            // Remove and reload data.
            self.arrMedia.remove(at : indexPath.item)
            self.CollectionViewMedia.reloadData()
            self.showHideMedia()
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        self.pageControl.currentPage = indexPath.row
        self.changeDotColor()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrMedia[indexPath.item]
        if dict[K.Key.videoUrlkey] is URL {
            let player = AVPlayer(url: dict[K.Key.videoUrlkey] as! URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    //MARK:- Location Picker Delegate
    
    func didSelectedCoordinate(_ coordinate: CLLocationCoordinate2D, withFormatedAddress address: String!, withPlaceId googlePlaceId: String!) {
        self.selectedLocationCoordinate = coordinate
        self.lblSelectedLocation.text = address
        
    }
    
    //MARK: -  IBAction
    
    @IBAction func btnBackTapped(_ sender:Any){
        self.navigationController?.popViewController()
    }
    
    @IBAction func btnCameraTapped (_ sender : Any){
        
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                // Reset Filter View Flag
                self.openAddPostViewController(image, video: videoURL,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
        
    }
    
    @IBAction func btnPublicPrivateTapped(_ sender : UIButton){
        if (sender == self.btnPublic){
            sender.isSelected = true
            self.btnPrivate.isSelected = false
        }
        else{
            sender.isSelected = !sender.isSelected
            self.btnPublic.isSelected = false
        }
    }
    
    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {
        
        if video != nil {
            self.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video,])
        }
        else if image != nil {
            self.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image,])
        }
        else{
            
            self.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        
        self.pageControl.numberOfPages = arrMedia.count
        self.CollectionViewMedia.reloadData()
        self.scrollToLastItem()
    }
    
    func scrollToLastItem() {
        
        let lastSection = CollectionViewMedia.numberOfSections - 1
        
        let lastRow = CollectionViewMedia.numberOfItems(inSection: lastSection)
        
        let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
        
        self.CollectionViewMedia.scrollToItem(at: indexPath, at: .right, animated: true)
        
    }
    
    
    @IBAction func btnPostTapped (_ sender : Any){
        
        self.view.endEditing(true)
        if validatePostData() {
            self.postWebservice()
        }
    }
    
    func postWebservice()  {
        
        SVProgressHUD.show()

        let type = "type"
        let file = "feed_photo"
        let thumb = "thumb_photo"
        let filevideo = "feed_video"
        let fileaudio = "feed_audio"
        //let duration = "duration"
        
        let feedImageFolderName = "feed_pics"
        let feedVideoFolderName = "feed_video"
        let feedAudioFolderName = "feed_audio"
        let thumbFolderName = "thumb_photo"
        
        var arrMediaData: Array<Dictionary<String, Any>> = []
        let group = DispatchGroup()

        for dict in self.arrMedia {
            
            let discoveryMediaType = dict[K.Key.mediaTypekey] as? PostMediaType
            var dictMedia: [String: Any] = [//type : discoveryMediaType!.rawValue,
                                            file : "",
                                            thumb: "",
                                            filevideo:"",
                                            fileaudio:""]
            
            
            if discoveryMediaType == PostMediaType.video ||
                discoveryMediaType == PostMediaType.audio {
                //Thumb of video
                if let image = dict[K.Key.postImagekey] as? UIImage {
                    group.enter()
                    WebClient.uploadMedia(url:K.URL.UPLOAD_MEDIA, mediaType: thumbFolderName,dirName:thumbFolderName,image: image, fileUrl: nil, completion: { (responseObject, fileName, error) in
                        if error == nil{
                            dictMedia[thumb] = fileName
                        }
                        else {
                            ISMessages.show(error?.localizedDescription)
                        }
                        group.leave()
                    })
                }
                group.enter()
                let mediaUrl = dict[K.Key.videoUrlkey] as? URL
                let audioUrl = dict[K.Key.audioUrlkey] as? URL
             
                    
                    if discoveryMediaType == PostMediaType.video
                    {
                        //video
                        WebClient.uploadMedia(url:K.URL.UPLOAD_MEDIA, mediaType: feedVideoFolderName,dirName:feedVideoFolderName, image: nil, fileUrl: mediaUrl, completion: { (responseObject, fileName, error) in
                            if error == nil{
                                dictMedia[filevideo] = fileName
                                arrMediaData.append(dictMedia)
                            }
                            else {
                                ISMessages.show(error?.localizedDescription)
                            }
                            group.leave()
                        })
                        
                    }
                    else{
                        
                        // Audio
                        WebClient.uploadMedia(url:K.URL.UPLOAD_MEDIA, mediaType: feedAudioFolderName,dirName:feedAudioFolderName, image: nil, fileUrl: audioUrl, completion: { (responseObject, fileName, error) in
                            if error == nil{
                                dictMedia[fileaudio] = fileName
                                arrMediaData.append(dictMedia)
                            }
                            else {
                                ISMessages.show(error?.localizedDescription)
                            }
                            group.leave()
                        })
                        
                    }
                    
                    
                
            }
            
            else {
                
                
                group.enter()
                if let image = dict[K.Key.postImagekey] as? UIImage {
                    WebClient.uploadMedia(url:K.URL.UPLOAD_MEDIA, mediaType: feedImageFolderName, dirName:feedImageFolderName,image: image, fileUrl: dict[K.Key.videoUrlkey] as? URL, completion: { (responseObject, fileName, error) in
                        if error == nil{
                            dictMedia[file] = fileName
                            arrMediaData.append(dictMedia)
                        }
                        else {
                            ISMessages.show(error?.localizedDescription)
                        }
                        group.leave()
                    })
                }
                else {
                    dictMedia[file] = (dict[K.Key.videoUrlkey] as? URL)?.lastPathComponent
                    arrMediaData.append(dictMedia)
                    group.leave()
                }
            }

            
        }
        
        
        
        group.notify(queue: DispatchQueue.main) {
            print("************",arrMediaData)
            
            let discoveryMedia : String = arrMediaData.jsonString()!
            print(discoveryMedia)
            let params =
                [   "user_id": User.loggedInUser()?.ID ?? 0,
                    "category_id": self.categoryId,
                    "description": self.txtViewDescription.text,
                    "feed_type": self.btnPrivate.isSelected ? 2 : 1,
                     "location": self.lblSelectedLocation.text ?? "",
                     "latitude": self.selectedLocationCoordinate.latitude,
                     "longitude":self.selectedLocationCoordinate.longitude,
                     "feed_pics":discoveryMedia,
                     "rating":self.ratingView.rating ,
                     "tags":""
            ] as [String : Any]
         
         
          
            
            _ = WebClient.requestWithUrl(url: K.URL.POST_FEED, parameters: params, completion: { (response, error) in
                if error == nil {
                   
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: ProfileViewController.ProfileViewControllerVC), object: ProfileViewController.ProfileViewControllerVC)
                    let dictData = response as! Dictionary<String, Any>
                    let msg = dictData["message"] as! String
                    ISMessages.show(msg)
                   
                    self.navigationController?.popViewController()
                }
                else {
                    ISMessages.show(error?.localizedDescription)
                }
                SVProgressHUD.dismiss()
            })
            
        }
        
     
        

    }
    
    //MARK: -  Other method
    
    func setupNavigation() -> Void {
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        //imageView.frame.size.width = 100;
        //imageView.frame.size.height = 45;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
        self.navigationController?.navigationBar.hideBottomHairline()
        
        self.navigationController?.navigationBar.shadowColor = K.Color.shadowColor
        self.navigationController?.navigationBar.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.shadowRadius = 4.0
        self.navigationController?.navigationBar.shadowOpacity = 1.0
        self.navigationController?.navigationBar.layer.masksToBounds = false

        btnPost.addShadow(offset: CGSize(width: 0.0, height: 0.0), radius: 4.0, color: K.Color.postbuttonshadowColor, opacity: 1.0, cornerRadius: 0)
        
        getAddress { (address) in
           self.lblSelectedLocation.text = address
        }
        self.pageControl.numberOfPages = arrMedia.count
    }
    
    
    func getAddress(handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: AppUtility.shared.currentLocation.coordinate.latitude, longitude: AppUtility.shared.currentLocation.coordinate.longitude)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                address += locationName + ", "
            }
            
            // Street address
            if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
                address += street + ", "
            }
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address += city + ", "
            }
            
            // Zip code
            if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
                address += zip + ", "
            }
            
            // Country
            if let country = placeMark?.addressDictionary?["Country"] as? String {
                address += country
            }
            
            // Passing address back
            handler(address)
        })
    }
    
    func changeDotColor() -> Void {
        for (index, dot) in pageControl.subviews.enumerated() {
            if index == pageControl.currentPage {
                dot.backgroundColor = .red
                dot.layer.cornerRadius = dot.frame.size.height / 2;
            } else {
                dot.backgroundColor = .white
                dot.layer.cornerRadius = dot.frame.size.height / 2
                dot.layer.borderColor = UIColor.white.cgColor
               // dot.layer.borderWidth = 1.0
            }
        }
    }
    
    func showHideMedia() -> Void {
    
        if (self.arrMedia.count == 0){
            self.lblAddmedia.isHidden = false
        }
        else{
            self.lblAddmedia.isHidden = true
        }
    }
    
    func validatePostData() -> Bool {
        self.view.endEditing(true)
        var strMessage : String = ""
            if txtViewDescription.text?.isValid == false {
                strMessage = K.Message.enterDescription
            }
            else if arrMedia.count == 0{
                strMessage = K.Message.selectMedia
            }
        
        if(strMessage != ""){
            ISMessages.show(strMessage, type: .warning)
            return false
        }else{
            return true
        }
    }
    
    //MARK: - Webservice Call
    
    func getCategories (){
        _ = WebClient.requestWithUrl(url: K.URL.GET_CATEGORY, parameters: nil, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                for dict in arrData {
                    if let cat =  Mapper<Categories>().map(JSON: dict) {
                        
                        self.arrCategories.append(cat)
                        self.lblCategory.text = self.arrCategories.first?.name
                    }
                }
               
                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
}
