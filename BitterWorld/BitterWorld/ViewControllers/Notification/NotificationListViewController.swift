//
//  NotificationListViewController.swift
//  BitterWorld
//
//  Created by  " " on 14/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import Floaty
import ObjectMapper

class NotificationListViewController: UIViewController, MenuProtocol {

    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var viewNotFound: UIView!
    
    var arrNotifications              : Array<Notifications> = []
    var totalRecordCount            : Int = 0
    var pageNumer                   : Int = 1
    
    var requestType : StatusType = .ACCEPT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNotFound.isHidden = true
        self.getNotificationList(loadFromStart: true)
        self.setView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let floaty = Floaty()
        AppUtility.shared.addFloatingButton(to: self.view, floaty: floaty)
    }

    func showMenuOptions(identifier: String) {
        if identifier == "NotificationListViewController" {
            return
        }
        let chatVC  = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        AppUtility.shared.navigationController?.pushViewController(chatVC!, animated: true)
    }
}

//MARK:- IBAction Methods
extension NotificationListViewController {
    
    @IBAction func btnCameraTapped (_ sender : UIButton) {
        self.showCamera()
    }
}

//MARK: - TableView DataSource & Delegate Methods.
extension NotificationListViewController: UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:NotificationListVCCell? = tableView.dequeueReusableCell(withIdentifier: "NotificationListVCCell") as? NotificationListVCCell
        if (cell == nil) {
            let nib: NSArray = Bundle.main.loadNibNamed("NotificationListVCCell", owner: self, options: nil)! as NSArray
            cell = nib.object(at: 0) as? NotificationListVCCell
        }
        cell?.selectionStyle = .none

        cell?.btnConfirm.tag    = indexPath.row
        cell?.btnDelete.tag     = indexPath.row
        
        cell?.btnConfirm.addTarget(self, action: #selector(self.btnClickedConfirm(_:)), for: .touchUpInside)
        cell?.btnDelete.addTarget(self, action: #selector(self.btnClickedDelete(_:)), for: .touchUpInside)
        cell?.initObj(objNotifications: self.arrNotifications[indexPath.row])
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            if let notiID = self.arrNotifications[indexPath.row].notification_id {
                
                self.deleteNotification(notification_id: notiID , indexPath: indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //((self.arrFriends.count)-1) == indexPath.row
        if indexPath.row == ((self.arrNotifications.count)-1) && (self.arrNotifications.count < self.totalRecordCount) {
            //self.pageNumer += 1;
            //self.totalRecordCount = self.totalRecordCount - 1
            
            
            self.getNotificationList(loadFromStart: false)
        }
    }
    
    //click on update push notification status.
    @objc func btnClickedConfirm(_ sender: UIButton) {
        
        let indexPath: IndexPath = IndexPath(row: sender.tag, section: 0)
        //let cell = self.tblNotification.cellForRow(at: indexPath) as! NotificationListVCCell
        if let friendID = self.arrNotifications[indexPath.row].notification_from {
            self.AcceptRejectFriendRequest(status: .ACCEPT,friend: friendID, indexPath : indexPath)
        }
    }
    @objc func btnClickedDelete(_ sender: UIButton) {
        
        let indexPath: IndexPath = IndexPath(row: sender.tag, section: 0)
        //let cell = self.tblNotification.cellForRow(at: indexPath) as! NotificationListVCCell
        if let friendID = self.arrNotifications[indexPath.row].notification_from {
            self.AcceptRejectFriendRequest(status: .REJECT,friend: friendID, indexPath : indexPath)
        }
    }
    
}

//MARK:- Other Methods
extension NotificationListViewController {
    
    func setView() {
        self.navigationItem.hidesBackButton = true
        self.addLogo()
        AppUtility.shared.delegate = self
        
        self.tblNotification.rowHeight              = UITableViewAutomaticDimension
        self.tblNotification.estimatedRowHeight     = 150
        self.tblNotification.tableFooterView        = UIView()
        
    }

    func addLogo () {
        
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
}

//MARK:- Take image & AddPost
extension NotificationListViewController {
    func showCamera () {
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.openAddPostViewController(image, video: videoURL ,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
    }
    
    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {
        
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostViewController") as! AddPostViewController
        if video != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video])
        }
        else if image != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image])
        }
        else{
            
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }
}

//MARK:- Service Call
extension NotificationListViewController {
    
    func reloadeAllData() {
        self.arrNotifications = []
        self.totalRecordCount = 0
        self.pageNumer = 1
        self.getNotificationList(loadFromStart: true)
    }
    
    func getNotificationList(loadFromStart:Bool) {
        
        //URL: http://9834584578
        //Params: user_id, page
        if loadFromStart == true {
            self.pageNumer = 1
        }
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "page":self.pageNumer]
        
        _ = WebClient.requestWithUrl(url: K.URL.GET_NOTIFICATIONLIST, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                let arrData = dictData["data"] as! Array<Dictionary<String, Any>>
                self.totalRecordCount = dictData["total_records"] as? Int ?? 0
                
                // Remove Previous Objects of first page
                if self.pageNumer == 1 {
                    self.arrNotifications.removeAll(keepingCapacity: false)
                }
                let notification = Mapper<Notifications>().mapArray(JSONArray: arrData )
                self.arrNotifications.append(contentsOf: notification)
                self.viewNotFound.isHidden = (self.arrNotifications.count > 0) ? true : false
                self.pageNumer = self.pageNumer + 1;
                self.tblNotification.reloadData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    func AcceptRejectFriendRequest(status:StatusType,friend:Int, indexPath : IndexPath) {
        
        SVProgressHUD.show()
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0,
                                                   "friend_id" :friend,
                                                   "status" : status.rawValue ]
        
        _ = WebClient.requestWithUrl(url: K.URL.ACCEPT_REJECT, parameters: dictParam, completion: { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.arrNotifications.remove(at: indexPath.row)
                //self.totalRecordCount = self.totalRecordCount - 1
                self.tblNotification.deleteRows(at: [indexPath], with: .automatic)
                self.reloadeAllData()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    func deleteNotification(notification_id:Int, indexPath : IndexPath) {
        
        //URL: http://9834584578
        //Param: notification_id
        
        let dictParam :Dictionary<String, Any> = ["notification_id" : notification_id]
        _ = WebClient.requestWithUrl(url: K.URL.DELETE_NOTIFICATION, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    ISMessages.show(message, type: .success)
                }

                self.arrNotifications.remove(at: indexPath.row)
//                self.totalRecordCount = self.totalRecordCount - 1
                self.tblNotification.deleteRows(at: [indexPath], with: .automatic)
                self.reloadeAllData()
                
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
        
    }
    
    
}
