//
//  SignupViewController.swift
//  BitterWorld
//
//  Created by  " " on 10/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import AccountKit

class SignupViewController: UIViewController,AKFViewControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var viewFullName                : UIView!
    @IBOutlet weak var viewNickName                : UIView!
    @IBOutlet weak var viewEmail                   : UIView!
    @IBOutlet weak var viewmobile                  : UIView!
    @IBOutlet weak var viewPassword                : UIView!
    @IBOutlet weak var viewConfirmPassword         : UIView!
    @IBOutlet weak var viewTermsCondition          : UIView!
    
    @IBOutlet weak var lblTermsCondition           : UILabel!
    
    @IBOutlet weak var btnSubmit                   : UIButton!
    @IBOutlet weak var btnAlreadyHaveaccountSignin : UIButton!
    @IBOutlet weak var btnShowPassword             : UIButton!
    @IBOutlet weak var btnShowConfirmPassword      : UIButton!
    
    @IBOutlet weak var imgProfile                  : UIImageView!
    
    @IBOutlet weak var txtFullName              : CustomTextField!
    @IBOutlet weak var txtnickName              : CustomTextField!
    @IBOutlet weak var txtEmail                 : CustomTextField!
    @IBOutlet weak var txtMobile                : CustomTextField!
    @IBOutlet weak var txtPassword              : CustomTextField!
    @IBOutlet weak var txtConfirmPassword       : CustomTextField!
    
    
    @IBOutlet weak var scrollview               : TPKeyboardAvoidingScrollView!

    
    
    //variable
    var dictSocialMediaInfo         : Dictionary<String, Any>?
    var isTermsConditionSelected    : Bool = true
    var isProfileImageModified      : Bool = false
    var accountKit = AKFAccountKit(responseType: .accessToken)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.initSetup()
        
        imgProfile.addTapGesture { (sender) in
            AppUtility.shared.showImageSelectionOption(viewController: self, isCropEnable: false) { (selectedImage) in
                if(selectedImage != nil)
                {
                    self.imgProfile.image = selectedImage
                    self.isProfileImageModified  = true
                }
            }
        }
        
        self.setSocialData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    
    @IBAction func btnBackTapped(_ sender:Any){
      self.navigationController?.popViewController()
    }

    @IBAction func btnRegisterTapped(_ sender:Any){
        
        self.view.endEditing(true)
        if validDataForSignUp() {
            // check User is Alreay Registered or Not
            self.checkUserExistOrNot()
        }
        
 
    }
    
    @IBAction func btnShowHidePassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if (sender == btnShowPassword)
        {
            txtPassword.isSecureTextEntry = !sender.isSelected
        }
        else{
            txtConfirmPassword.isSecureTextEntry = !sender.isSelected
        }
        
    }

    @IBAction func btnTermsAndConditionTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        isTermsConditionSelected = sender.isSelected
        
    }
    
    @IBAction func btnAlreadyHaveAccountTapped(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnTermsTapped(_ sender: UIButton) {
        
        let cmsVC = self.storyboard?.instantiateViewController(withIdentifier: "CMSViewController") as! CMSViewController
        cmsVC.CMSType = "TERMS_CONDITIONS"
        self.navigationController?.pushViewController(cmsVC, animated: true)
        
    }
    
    @IBAction func btnPrivacyPolicyTapped(_ sender: UIButton) {
        
        let cmsVC = self.storyboard?.instantiateViewController(withIdentifier: "CMSViewController") as! CMSViewController
        cmsVC.CMSType = "PRIVACY_POLICY"
        self.navigationController?.pushViewController(cmsVC, animated: true)
        
    }

    //MARK:- Validation
    
    func validDataForSignUp() -> Bool {
        self.view.endEditing(true)
        var strMessage : String = ""
        
        if(dictSocialMediaInfo == nil){
            
            if txtFullName.text?.isValid == false {
                strMessage = K.Message.enterYourName
            }
            else if txtnickName.text?.isValid == false {
                strMessage = K.Message.enterUsername
            }
           
            else if txtEmail.text?.isValid == false {
                strMessage = K.Message.enterEmail
            }
            else if txtEmail.text?.isEmail == false {
                strMessage = K.Message.enterValidEmail
            }
            else if (txtMobile.text?.length == 0 || (txtMobile.text?.length)! < 10){
                strMessage = K.Message.enterPhone
            }
            else if txtPassword.text?.isValid == false {
                strMessage = K.Message.enterPassword
            }
            else if txtPassword.text!.length < 6 {
                strMessage = K.Message.passwordMinLength
            }
            else if txtPassword.text! != txtConfirmPassword.text! {
                strMessage = K.Message.passwordDoesNotMatched
            }
            else if !isTermsConditionSelected {
                strMessage = K.Message.agreeTermsOfService
            }
            
            
        }else{
            if txtFullName.text?.isValid == false {
                strMessage = K.Message.enterYourName
            }
            else if txtnickName.text?.isValid == false {
                strMessage = K.Message.enterUsername
            }
                
            else if txtEmail.text?.isValid == false {
                strMessage = K.Message.enterEmail
            }
            else if txtEmail.text?.isEmail == false {
                strMessage = K.Message.enterValidEmail
            }
            else if (txtMobile.text?.length == 0 || (txtMobile.text?.length)! < 10){
                strMessage = K.Message.enterPhone
            }
            else if !isTermsConditionSelected {
                strMessage = K.Message.agreeTermsOfService
            }
           
        }
        
        if(strMessage != ""){
            ISMessages.show(strMessage, type: .warning)
            return false
        }else{
            return true
        }
    }
    //MARK: - Other Method
    func initSetup (){
        let str = "I accept Terms of Use And Privacy Policy"
        let atttString : NSMutableAttributedString = NSMutableAttributedString(string: str)
        atttString.setAttributedString(textForAttribute: "Terms of Use", withColor: K.Color.termsCondition, fontSize: 12.0, fontName: kPTSansBoldFontName)
        atttString.setAttributedString(textForAttribute: "And", withColor: K.Color.termsCondition, fontSize: 12.0, fontName: kPTSansRegularFontName)
        atttString.setAttributedString(textForAttribute: "Privacy Policy", withColor: K.Color.termsCondition, fontSize: 12.0, fontName: kPTSansBoldFontName)
        lblTermsCondition.attributedText = atttString
        
        self.scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.btnAlreadyHaveaccountSignin.bottom + 50)
    }
    
    func setSocialData (){
       
        if (dictSocialMediaInfo != nil){
            viewPassword.isHidden = true
            viewConfirmPassword.isHidden = true
            viewTermsCondition.y = viewmobile.bottom + 20
            btnSubmit.y = viewTermsCondition.bottom + 20
            btnAlreadyHaveaccountSignin.y = btnSubmit.bottom + 20
            if let email = dictSocialMediaInfo!["email"] {
                txtEmail.text = email as? String
            }
            if let profileUrl = dictSocialMediaInfo!["profile_photo"] {
                imgProfile.setImageWithURL(profileUrl as? String, placeHolderImage: nil, activityIndicatorViewStyle: .gray, completionBlock:nil)
                
            }
        }
        
        
       
    }
    
    //MARK: - Webservice
    func checkUserExistOrNot()  {
        SVProgressHUD.show()
        let param = ["email": txtEmail.text!,
                     "phone": txtMobile.text!,
                     "nick_name": txtnickName.text!
            ] as [String : Any]
        _ = WebClient.requestWithUrl(url: K.URL.PRE_SIGNUP, parameters:param) { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                self.showOTPEnterScreen()
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
        }
    }
    
    
    func uploadProfileImage(_ strPhoneNumber : String){
        if let image = imgProfile.image {
           if isProfileImageModified
           {
                SVProgressHUD.show()
                WebClient.uploadMedia(mediaType: "profile_photo",dirName:"profile_photo" ,image: image, completion: { (responseObject, fileName, error) in
                    if error == nil {
                        self.signUP(strPhoneNumber, fileName ?? "")
                    }
                    else{
                        SVProgressHUD.dismiss()
                        ISMessages.show(error?.localizedDescription, type: .warning)
                    }
                })
            }
           else{
                self.signUP(strPhoneNumber, "")
            }
        }
        else {
            self.signUP(strPhoneNumber, "")
        }
        
    }
    
    func signUP(_ strPhoneNumber: String,_ profilePhoto: String) -> Void {
        self.view.endEditing(true)
        SVProgressHUD.show()
        var param = ["first_name": txtFullName.text!,
                     "nick_name" : txtnickName.text!,
                     "email": txtEmail.text!,
                     "password":txtPassword.text!,
                     "repeat_password":txtConfirmPassword.text!,
                     "phone": strPhoneNumber,
                     "device_type":"ios",
                     "device_token":"",
                     "latitude" : AppUtility.shared.currentLocation.coordinate.latitude ,
                     "longitude" : AppUtility.shared.currentLocation.coordinate.longitude ,
                     "profile_photo": profilePhoto] as [String : Any]
        
        if self.dictSocialMediaInfo != nil{
            param["social_id"] = self.dictSocialMediaInfo?["social_id"]
            param["social_type"] = self.dictSocialMediaInfo?["social_type"]
        }
        _ = WebClient.requestWithUrl(url: K.URL.SIGN_UP, parameters: param) { (response, error) in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dictResponse = (response as? Dictionary<String, Any>)?[K.Key.Data] as? Dictionary<String,Any> {
                    let user = User(dict: dictResponse)
                    user.save()
                }
                //AppUtility.updateDeviceToken()
                
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                 self.navigationController?.pushViewController(homeVC, animated: true)
                
            }
            else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    
    // MARK: - Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ ."
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtnickName{
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        return true
        
    }
    
    //MARK: - AKFViewControllerDelegate
    
    func showOTPEnterScreen() {
        accountKit.logOut()
        let viewController =  accountKit.viewControllerForPhoneLogin()
        viewController.delegate = self
        viewController.uiManager = AKFSkinManager.init(skinType: AKFSkinType.classic, primaryColor: K.Color.darkGray)
        present(viewController, animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        accountKit.requestAccount { (account, error) in
            if let phoneNumber = account?.phoneNumber?.stringRepresentation() {
                self.uploadProfileImage(phoneNumber)
            }
        }
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        print(error.localizedDescription)
    }
    
}

extension NSMutableAttributedString {
    
    
    func setAttributedString (textForAttribute: String, withColor color: UIColor,fontSize : CGFloat ,fontName : String)
    {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        let yourAttributes = [NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.font:UIFont(name: fontName, size: fontSize)]
        self.addAttributes(yourAttributes, range: range)
        
    }
    
    
}
