//
//  CMSViewController.swift
//  BitterWorld
//
//  Created by  " " on 12/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class CMSViewController: UIViewController {
    @IBOutlet weak var webViewContent : UIWebView!
    @IBOutlet weak var lblTitle : UILabel!
    var CMSType: String!
    var CMSTitle: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCMSContent()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Webview Delegate
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SVProgressHUD.dismiss()
    }
    //MARK: - Webservice
    func getCMSContent() {
        SVProgressHUD.show()
        let reqParam = ["cms_key" : self.CMSType]  as [String : Any]
        _ = WebClient.requestWithUrl(url: K.URL.GET_CMS, parameters: reqParam) { (response, error) in
            if error == nil {
                let data = response as! Dictionary<String, Any>
                let dictData = data["data"] as! Dictionary<String,Any>
                //self.lblTitle.text = dictData["title"] as! String
                let stringHTML = dictData["description"]
                self.webViewContent.loadHTMLString(stringHTML as! String, baseURL: nil)
            } else if let err = error {
                ISMessages.show(err.localizedDescription, type: .error)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func btnBackTapped (_ sender: UIButton){
        self.navigationController?.popViewController()
    }

}
