//
//  LoginViewController.swift
//  BitterWorld
//
//  Created by  " " on 10/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import Google

typealias SignInCompletion = (_ suceess : Bool?) -> Void

class LoginViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate  {
    
    

    @IBOutlet weak var txtEmail             : CustomTextField!
    @IBOutlet weak var txtPassword          : CustomTextField!
    @IBOutlet weak var txtEmailForgotPassword: CustomTextField!
    
    @IBOutlet weak var btnForgotPasword             : UIButton!
    @IBOutlet weak var btnLogin                     : UIButton!
    @IBOutlet weak var btnCreateAccount             : UIButton!
    @IBOutlet weak var btnFacebook                  : UIButton!
    @IBOutlet weak var btnTwitter                   : UIButton!
    @IBOutlet weak var btngmail                     : UIButton!
    @IBOutlet weak var btnClose                     : UIButton!
    @IBOutlet weak var btnSkip                      : UIButton!
    @IBOutlet weak var scrollViewForgotPassword: TPKeyboardAvoidingScrollView!

    
    var dictSocialMediaInfo : Dictionary<String, Any>?
    var signInCompletion : SignInCompletion?
    
    
    
    class func open(rootViewController : UIViewController, completion : SignInCompletion?) {
    
        let loginVC = SF.storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.signInCompletion = completion
        let navController = UINavigationController(rootViewController: loginVC)
        rootViewController.present(navController, animated: true, completion: {
            
        })
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if((User.loggedInUser()) != nil) {
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.setViewControllers([homeVC], animated: false)
            
        }
       scrollViewForgotPassword.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- IBAction
    @IBAction func btnLoginTapped(_ sender:Any){
        
        if(self.validDataForLogin())
        {
            // Remove social dict info
            dictSocialMediaInfo = nil
            signIn()

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppUtility.shared.isShowClosebutton
        {
            self.btnSkip.isHidden = true
            self.btnClose.isHidden = false
        }
        else{
            self.btnClose.isHidden = true
            self.btnSkip.isHidden = false
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func btnCreateaccountTapped(_ sender: Any) {
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: Any) {
        self.txtEmailForgotPassword.text = ""
        scrollViewForgotPassword.isHidden = false
    }
    
    
    @IBAction func btnCloseForgotPassword(_ sender: Any) {
        scrollViewForgotPassword.isHidden = true
    }
    
    @IBAction func btnSubmitForgotPassword(_ sender: Any) {
        self.view.endEditing(true)
        if (txtEmailForgotPassword.text?.isEmail == false)
        {
            ISMessages.show(K.Message.enterValidEmail, type: .warning)
        }
        else{
            
            
            let param : Dictionary<String, Any> = ["email" : txtEmailForgotPassword.text!]
              SVProgressHUD.show()
            _ = WebClient.requestWithUrl(url: K.URL.FORGOT_PASSWORD, parameters: param) { (response, error) in
                SVProgressHUD.dismiss()
                if error == nil {
                    let dictData = response as! Dictionary<String, Any>
                    ISMessages.show(dictData["message"] as! String)
                    self.dismissPopupViewControllerWithanimationType(.slideTopBottom)
                    self.scrollViewForgotPassword.isHidden = true
                }
                else{
                    ISMessages.show(error?.localizedDescription, type: .warning)
                }
            }
        }
        
    }
    
    @IBAction func btnShowHidePassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func btnFacebookTapped(_ sender: Any) {
        self.signInWithFacebook()
    }
    
    @IBAction func btnTwitterTapped(_ sender: Any) {
        self.signInWithTwitter()
    }
    
    @IBAction func btnGmailTapped(_ sender: Any) {
        self.signInWithGoogle()
    }
    
    @IBAction func btnSkipTapped(_ sender: Any) {
        
        StandardUserDefaults.set(true, forKey: "skipIgnore")
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.setViewControllers([homeVC], animated: false)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Webservice Call
    
    func signIn() -> Void {
        SVProgressHUD.show()
        var param : Dictionary<String, Any> = ["email" : txtEmail.text!,
                                               "password" : txtPassword.text!]
        if self.dictSocialMediaInfo != nil {
            //param["email"] = self.dictSocialMediaInfo?["email"]
            param["social_id"] = self.dictSocialMediaInfo?["social_id"]
            param["social_type"] = self.dictSocialMediaInfo?["social_type"]
        }
        
        _ = WebClient.requestWithUrl(url: K.URL.LOGIN, parameters: param) { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                let user =  User(dict: dictData[K.Key.Data] as! [String : Any])
                user.save()
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.setViewControllers([homeVC], animated: false)

                
//                self.dismiss(animated: true, completion: {
//                    self.signInCompletion?(true)
//                })
            }
            else if (error as NSError?)?.code == 2 {
                
                print("Social Sign Up")
                let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
                signupVC.dictSocialMediaInfo = self.dictSocialMediaInfo
                 //self.navigationController?.setViewControllers([signupVC], animated: false)
                self.navigationController?.pushViewController(signupVC, animated: true)
                
            }else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    
    //MARK:- Social Login
    func signInWithFacebook() {
        FBSDKAccessToken.setCurrent(nil)
        let fetchUserInfoFromFaceBook = {() -> Void in
            let graphResquest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,first_name,last_name,email,gender,birthday"])
            
            _ = graphResquest?.start { connection, result, error in
                let dictionary = result as! Dictionary<String, Any>
                self.dictSocialMediaInfo = [:]
                if let picture = dictionary["picture"] as? [String:Any] ,
                    let imgData = picture["data"] as? [String:Any] ,
                    let imgUrl = imgData["url"] as? String {
                    self.dictSocialMediaInfo?["profile_photo"] = imgUrl
                }
                
                self.dictSocialMediaInfo?["full_name"] = (dictionary["first_name"] as? String)! + " " + (dictionary["last_name"] as? String)!
                //self.dictSocialMediaInfo["last_name"] = dictionary["last_name"]
                self.dictSocialMediaInfo?["email"] = dictionary["email"] ?? ""
                self.dictSocialMediaInfo?["social_id"] = dictionary["id"]
                self.dictSocialMediaInfo?["social_type"] = SocialMediaType.facebook.rawValue
                self.signIn()
            }
        }
        if FBSDKAccessToken.current() != nil {
            fetchUserInfoFromFaceBook()
        }else {
            let loginManager = FBSDKLoginManager()
            loginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self, handler: { (signInResult, error) in
                if error != nil {
                    print("Process error")
                }
                else if (signInResult?.isCancelled)! {
                    print("Cancelled")
                }
                else {
                    fetchUserInfoFromFaceBook()
                    print("logged in")
                }
            })
        }
    }
    
    func signInWithTwitter() {
        TWTRTwitter.sharedInstance().logIn(with: self) { (session, error) in
            if session != nil {
                self.dictSocialMediaInfo = [:]
                self.dictSocialMediaInfo?["full_name"] = session?.userName
                self.dictSocialMediaInfo?["social_id"] = session?.userID
                self.dictSocialMediaInfo?["social_type"] = SocialMediaType.twitter.rawValue
                self.signIn()
            }else {
                print("Cancelled")
               // ISMessages.show(error?.localizedDescription)
            }
        }
    }
    
    
    func signInWithGoogle(){
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
        
    }

    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        self.dictSocialMediaInfo?.removeAll()
        
        if (error == nil) {
            
            self.dictSocialMediaInfo = [:]
            self.dictSocialMediaInfo?["email"] =  user.profile.email ?? ""
            self.dictSocialMediaInfo?["full_name"] = user.profile.name
            self.dictSocialMediaInfo?["social_id"] = user.userID
            self.dictSocialMediaInfo?["social_type"] = SocialMediaType.google.rawValue
            self.signIn()
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        
    }
    
    //MARK: -  validation  login
    
    func validDataForLogin() -> Bool {
        
        var strMessage : String = ""
        
        if txtEmail.text?.isValid == false {
            strMessage = K.Message.enterEmail
        }
        else if txtEmail.text?.isEmail == false {
            strMessage = K.Message.enterValidEmail
        }
        else if txtPassword.text?.isValid == false {
            strMessage = K.Message.enterPassword
        }
        if(strMessage != ""){
            ISMessages.show(strMessage, type: .warning)
            return false
        }else{
            return true
        }
    }
    
    // MARK: - other method
    
    func redirectToHome()  {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
}
