//
//  SettingsViewController.swift
//  BitterWorld
//
//  Created by  " " on 14/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import Floaty
import ObjectMapper

class SettingsViewController: UIViewController, MenuProtocol {

    @IBOutlet weak var tblSetings: UITableView!
    var objSettings : [Settings] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView ()
        self.getSettingList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let floaty = Floaty()
        AppUtility.shared.addFloatingButton(to: self.view, floaty: floaty)
        //self.tblChatist.reloadData()
    }
    
    func showMenuOptions(identifier: String) {
        if identifier == "SettingsViewController" {
            return
        }
        let chatVC  = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        AppUtility.shared.navigationController?.pushViewController(chatVC!, animated: true)
    }

    func getSettingList() {
        self.objSettings = []
        if let response:ServiceResponseArray = Mapper<ServiceResponseArray<Settings>>().map(JSONString: SettingsMenuList.SettingsList) {
            self.objSettings = response.Data!
        }
        self.tblSetings.reloadData()
    }
    
}

//MARK:- IBAction Methods
extension SettingsViewController {
    
    @IBAction func btnCameraTapped (_ sender : UIButton) {
        
        self.showCamera()
    }
    
    @IBAction func btnLogoutTapped (_sender : UIButton) {
        
        let alert = UIAlertController(title: "Logout", message: AppMessage.LogoutMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: ButtonTitle.btnLogout, style: UIAlertActionStyle.default, handler: { action in
            User.delete()
            self.redirectToHome()
        }))
        alert.addAction(UIAlertAction(title: ButtonTitle.btnCancel, style: UIAlertActionStyle.destructive, handler: { action in
            print("Cancel")
        }))
        self.present(alert, animated: true, completion: nil)
    }

    
    func redirectToHome()  {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
}

//MARK: - TableView DataSource & Delegate Methods.
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.objSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:SettingsVCCell? = tableView.dequeueReusableCell(withIdentifier: "SettingsVCCell") as? SettingsVCCell
        if (cell == nil) {
            let nib: NSArray = Bundle.main.loadNibNamed("SettingsVCCell", owner: self, options: nil)! as NSArray
            cell = nib.object(at: 0) as? SettingsVCCell
        }
        cell?.selectionStyle = .none
        cell?.btnNotificationStatus.tag = indexPath.row
        cell?.btnNotificationStatus.addTarget(self, action: #selector(self.btnClickedPushNotification(_:)), for: .touchUpInside)
        cell?.initObj(objSettings: self.objSettings[indexPath.row], index: indexPath.row)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*
         0. Push Notification
         1. Change Password
         2. Invite Friends
         3. Terms of Use
         4. Privacy Policy
         5. Rate App
         */
        
        switch indexPath.row {
        case 0:
            break
        case 1:
            let objChangePass  = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController")  as! ChangePasswordViewController
            self.navigationController?.pushViewController(objChangePass, animated: true)
            break
        case 2:
            Common().shareYourApp(vc: self, strURL: Links.rateusappstorelink, strMessage: "BitterWorld")
            break
        case 3:
            let cmsVC = self.storyboard?.instantiateViewController(withIdentifier: "CMSViewController") as! CMSViewController
            cmsVC.CMSType = "TERMS_CONDITIONS"
            self.navigationController?.pushViewController(cmsVC, animated: true)
            break
        case 4:
            let cmsVC = self.storyboard?.instantiateViewController(withIdentifier: "CMSViewController") as! CMSViewController
            cmsVC.CMSType = "PRIVACY_POLICY"
            self.navigationController?.pushViewController(cmsVC, animated: true)
            break
        case 5:
            Common().openBrouserLink(urlString: Links.rateusappstorelink, vc: self)
            break
        default:
            break
        }
    }
    
    //click on update push notification status.
    @objc func btnClickedPushNotification(_ sender: UIButton) {

        let indexPath: IndexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tblSetings.cellForRow(at: indexPath) as! SettingsVCCell
        
        self.setPushNotification()
        if User.loggedInUser()?.isPushNotification == 1 {
            
            UIView.animate(withDuration: 15.0, delay: 15.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                cell.btnNotificationStatus.isSelected = false
            }, completion: nil)
            
        } else {
            
            UIView.animate(withDuration: 15.0, delay: 15.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                cell.btnNotificationStatus.isSelected = true
            }, completion: nil)
        }
        
    }
}

//MARK:- Other Methods
extension SettingsViewController {
    
    func setupView () {
        
        self.navigationItem.hidesBackButton = true
        self.addLogo()
        AppUtility.shared.delegate = self
        
        self.tblSetings.rowHeight              = UITableViewAutomaticDimension
        self.tblSetings.estimatedRowHeight     = 150
        self.tblSetings.tableFooterView        = UIView()
        
    }
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
}

//MARK:- Take image & AddPost
extension SettingsViewController {
    func showCamera () {
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostMediaViewController") as! AddPostMediaViewController
        addPostVC.mediaSelectionCompletion = { (image, videoURL,audioURL) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                self.openAddPostViewController(image, video: videoURL ,audio: audioURL)
            })
        }
        self.present(addPostVC, animated: true) {
        }
    }
    
    func openAddPostViewController(_ image: UIImage?, video: URL?,audio: URL?) -> Void {
        
        
        let addPostVC = self.storyboard?.instantiateViewController(withIdentifier:"AddPostViewController") as! AddPostViewController
        if video != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : video,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.video])
        }
        else if image != nil {
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : image ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : nil,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.image])
        }
        else{
            
            addPostVC.arrMedia.append([K.Key.ImageIdkey : 0,
                                       K.Key.postImagekey : nil ,
                                       K.Key.videoUrlkey : nil,
                                       K.Key.audioUrlkey : audio,
                                       K.Key.isUploadedKey: false,
                                       K.Key.mediaTypekey : PostMediaType.audio])
            
        }
        self.navigationController?.pushViewController(addPostVC, animated: true)
    }
}

//MARK:- Service Call
extension SettingsViewController {
    
    func setPushNotification() {
//        URL : http://9834584578/clients/2018/bitterworld/api/web/v1/user/update-notification-setting
//        param: user_id
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0]
        
        _ = WebClient.requestWithUrl(url: K.URL.UPDATE_NOTIFICATION, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    let user = User.loggedInUser()
                    user?.isPushNotification = (user?.isPushNotification == 1) ? 0 : 1
                    user?.save()
                    ISMessages.show(message, type: .success)
                }
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
}



