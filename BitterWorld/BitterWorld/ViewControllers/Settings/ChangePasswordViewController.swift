//
//  ChangePasswordViewController.swift
//  BitterWorld
//
//  Created by  " " on 14/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet var scrollView                : TPKeyboardAvoidingScrollView!
    @IBOutlet var activityIndicator         : UIActivityIndicatorView!
    @IBOutlet weak var lblCurrentPassTitle  : UILabel!
    @IBOutlet weak var txtCurrentPass       : CustomTextField!
    @IBOutlet weak var lblNewPassTitle      : UILabel!
    @IBOutlet weak var txtNewPass           : CustomTextField!
    @IBOutlet weak var lblConfirmPassTitle  : UILabel!
    @IBOutlet weak var txtConfirmPass       : CustomTextField!
    @IBOutlet weak var btnUpdate            : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:-  IBAction Methods
extension ChangePasswordViewController {
    
    @IBAction func btnBackTapped(_ sender:Any) {
        self.navigationController?.popViewController()
    }
    
    @IBAction func btnClickUpdate(_ sender: UIButton) {
        if self.validateFileds() {
            self.changePassword()
        }
    }
}

//MARK:- Validation Metyhods
extension ChangePasswordViewController {
    func validateFileds() -> Bool {
        var isValidate : Bool = true
        if txtCurrentPass.text?.isEmpty == true {
            ISMessages.show("Please enter current password.", type: .warning)
            isValidate = false
        }
        else if txtNewPass.text?.isEmpty == true {
            ISMessages.show("Please enter new password.", type: .warning)
            isValidate = false
        }
        else if txtConfirmPass.text?.isEmpty == true {
            ISMessages.show("Please enter confirm password.", type: .warning)
            isValidate = false
        }
        else if (txtNewPass.text != txtConfirmPass.text) {
            ISMessages.show("New password and Confirm password does not matched.", type: .warning)
            isValidate = false
        }
        return isValidate
    }
}

//MARK:- Other Methods
extension ChangePasswordViewController {
    
    func setView() {
        
        self.addLogo()
        
        self.txtCurrentPass.text                = ""
        self.txtNewPass.text                    = ""
        self.txtConfirmPass.text                = ""
        
        self.lblCurrentPassTitle.textColor      = K.Color.lightTextColor
        self.lblNewPassTitle.textColor          = K.Color.lightTextColor
        self.lblConfirmPassTitle.textColor      = K.Color.lightTextColor
        self.lblCurrentPassTitle.font           = UIFont(name: kPTSansRegularFontName, size: 15)
        self.lblNewPassTitle.font               = UIFont(name: kPTSansRegularFontName, size: 15)
        self.lblConfirmPassTitle.font           = UIFont(name: kPTSansRegularFontName, size: 15)
        
        self.txtCurrentPass.addBorder(width: 1, color: K.Color.GrayTone2)
        self.txtCurrentPass.setCornerRadius(radius: 2)
        self.txtCurrentPass.placeholder = ""
        
        self.txtNewPass.addBorder(width: 1, color: K.Color.GrayTone2)
        self.txtNewPass.setCornerRadius(radius: 2)
        self.txtNewPass.placeholder = ""
        
        self.txtConfirmPass.addBorder(width: 1, color: K.Color.GrayTone2)
        self.txtConfirmPass.setCornerRadius(radius: 2)
        self.txtConfirmPass.placeholder = ""
        
        Common().addshadow(View: self.btnUpdate, shadowRadius: 5.0, shadowColor: UIColor.black, shadowOpacity: 1.0, cornerRadius : 3.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        
        self.scrollView.autocalculateContentHeight()
    }
    
    func addLogo (){
        let logo = UIImage(named: "ic_logo_topbar-new.png") as UIImage?
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.titleView = imageView
    }
}

//MARK:- Service Call
extension ChangePasswordViewController {
    
    func changePassword() {
        
        //URL: http://9834584578/clients/2018/bitterworld/api/web/v1/user/change-password
        //Params: user_id, old_password, new_password

        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "old_password" : self.txtCurrentPass.text! , "new_password" : self.txtNewPass.text!]
        
        _ = WebClient.requestWithUrl(url: K.URL.CHANGE_PASSWORD, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                
                let dictData = response as! Dictionary<String, Any>
                if let status = dictData["status"] as? Int {
                    if status == 1 {
                        if let message = dictData["message"] as? String {
                            ISMessages.show(message, type: .success)
                        }
                        self.txtCurrentPass.text        = ""
                        self.txtNewPass.text            = ""
                        self.txtConfirmPass.text        = ""
                        self.navigationController?.popViewController()
                    }
                }
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
        
    }
    
}





