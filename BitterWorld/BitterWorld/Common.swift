//
//  Common.swift
//  BitterWorld
//
//  Created by  " " on 20/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import Foundation
import SafariServices

class Common {

    
    func addshadow(View:UIView,shadowRadius:CGFloat? = 0,shadowColor:UIColor? = UIColor.darkGray,shadowOpacity:Float? = 0,borderWidth:CGFloat? = 0,borderColor:UIColor? = UIColor.clear,cornerRadius:CGFloat? = 0, shadowOffset:CGSize? = CGSize.zero, IsMaskToBounds:Bool = false) {
        
        View.layer.shadowRadius = shadowRadius!;
        View.layer.shadowColor = shadowColor?.cgColor;
        View.layer.shadowOffset = shadowOffset!
        View.layer.shadowOpacity = shadowOpacity!;
        View.layer.borderWidth = borderWidth!;
        View.layer.borderColor = borderColor?.cgColor;
        View.layer.cornerRadius = cornerRadius!;
        View.layer.masksToBounds = IsMaskToBounds
    }
    //MARK:- Set Font in Label
    func getFont(fontSize: CGFloat = 14,fontStyle: String = FontStyle.Regular) -> UIFont {
        
                
        if fontStyle == FontStyle.Bold {
            return UIFont(name: "HelveticaNeue" + FontStyle.Bold, size: fontSize)!
        }else if fontStyle == FontStyle.Regular {
            return UIFont(name: "HelveticaNeue", size: fontSize)!
        } else if fontStyle == FontStyle.Medium || fontStyle == FontStyle.SemiBold {
            return UIFont(name: "HelveticaNeue" + FontStyle.Medium, size: fontSize)!
        } else if fontStyle == FontStyle.Italic {
            return UIFont(name: "HelveticaNeue" + "-LightItalic", size: fontSize)!
        } else if fontStyle == FontStyle.Light {
            return UIFont(name: "HelveticaNeue" + FontStyle.Light, size: fontSize)!
        } else {
            return UIFont(name: "HelveticaNeue", size: fontSize)!
        }
        //return UIFont(name: "HelveticaNeue", size: fontSize)!
    }
    
    //MARK:  Open Safari
    func openBrouserLink(urlString : String, vc:UIViewController){
        
        if #available(iOS 9.0, *) {
            if let url = NSURL(string: urlString) {
                let svc = SFSafariViewController(url: url as URL, entersReaderIfAvailable: true)
                vc.present(svc, animated: true, completion: nil)
            }
        } else {
            
            if let url = NSURL(string: urlString) {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    func shareYourApp(vc:UIViewController, strURL: String, strMessage:String = ""){
        
        if strMessage != "" {
            let activityController = UIActivityViewController(activityItems: [strMessage,NSURL(string: strURL)!], applicationActivities: nil)
            vc.present(activityController, animated: true, completion: nil)
        } else {
            let activityController = UIActivityViewController(activityItems: [NSURL(string: strURL)!], applicationActivities: nil)
            vc.present(activityController, animated: true, completion: nil)
        }
        
        
    }
}

class DateHelper {
    
    func getCurrentDateTime(dateFormat:String)-> String
    {
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = NSTimeZone.local
        let strcurrentdate = dateFormatter.string(from: date as Date)
        return strcurrentdate;
    }
    
    func getCurrentDateTime_Date(dateFormat:String)-> NSDate
    {
        var date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        date = dateFormatter.date(from: dateFormatter.string(from: date as Date))! as NSDate
        return date;
    }

}
