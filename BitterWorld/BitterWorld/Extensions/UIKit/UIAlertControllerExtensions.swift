//
//  UIAlertControllerExtensions.swift
//  EZSwiftExtensions
//
//  Created by Lucas Farah on 23/02/16.
//  Copyright (c) 2016 Lucas Farah. All rights reserved.
//
import UIKit

extension UIAlertController {
    /// EZSE: Easy way to present UIAlertController
    func show() {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
            rootViewController.present(self, animated: true, completion: nil)
        }
        else {
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(self, animated: true, completion: nil)
        }
    }
    
    class func show(_ title: String, message: String, type:UIAlertControllerStyle, cancelButtonTitle:String?, destructiveButtonTitle:String?, buttons:[String]?, tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: type, cancelButtonTitle: cancelButtonTitle, destructiveButtonTitle: destructiveButtonTitle, buttons: buttons, tapBlock: tapBlock)
        alert.show()
        return alert
    }
    
    class func alert(_ title: String, message: String, cancelButtonTitle:String?, destructiveButtonTitle:String?, buttons:[String]?, tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert, cancelButtonTitle: cancelButtonTitle, destructiveButtonTitle: destructiveButtonTitle, buttons: buttons, tapBlock: tapBlock)
        alert.show()
        return alert
    }
    
    class func actionSheet(_ title: String, message: String, cancelButtonTitle:String?, destructiveButtonTitle:String?, buttons:[String]?, tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet, cancelButtonTitle: cancelButtonTitle, destructiveButtonTitle: destructiveButtonTitle, buttons: buttons, tapBlock: tapBlock)
        alert.show()
        return alert
    }

}

private extension UIAlertController {
    convenience init(title: String?, message: String?, preferredStyle: UIAlertControllerStyle, cancelButtonTitle:String?, destructiveButtonTitle:String?, buttons:[String]?, tapBlock:((UIAlertAction,Int) -> Void)?) {
        self.init(title: title, message: message, preferredStyle:preferredStyle)
        var buttonIndex = 0
        if cancelButtonTitle != nil {
            let action = UIAlertAction(title: cancelButtonTitle, preferredStyle: .cancel, buttonIndex: buttonIndex, tapBlock: tapBlock)
            buttonIndex += 1
            self.addAction(action)
        }
        if destructiveButtonTitle != nil {
            let action = UIAlertAction(title: destructiveButtonTitle, preferredStyle: .destructive, buttonIndex: buttonIndex, tapBlock: tapBlock)
            buttonIndex += 1
            self.addAction(action)
        }
        if buttons != nil {
            for buttonTitle in buttons! {
                let action = UIAlertAction(title: buttonTitle, preferredStyle: .default, buttonIndex: buttonIndex, tapBlock: tapBlock)
                buttonIndex += 1
                self.addAction(action)
            }
        }
    }
}

private extension UIAlertAction {
    convenience init(title: String?, preferredStyle: UIAlertActionStyle, buttonIndex:Int, tapBlock:((UIAlertAction,Int) -> Void)?) {
        self.init(title: title, style: preferredStyle) {
            (action:UIAlertAction) in
            if let block = tapBlock {
                block(action,buttonIndex)
            }
        }
    }
}
