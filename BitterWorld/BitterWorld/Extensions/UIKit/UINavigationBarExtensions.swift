//
//  UINavigationBarExtensions.swift
//  SwifterSwift
//
//  Created by Omar Albeik on 8/22/16.
//  Copyright © 2016 Omar Albeik. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit


// MARK: - Methods
public extension UINavigationBar {
	
	/// SwifterSwift: Set Navigation Bar title, title color and font.
	///
	/// - Parameters:
	///   - font: title font
	///   - color: title text color (default is .black).
	public func setTitleFont(_ font: UIFont, color: UIColor = UIColor.black) {
		var attrs = [NSAttributedStringKey: Any]()
		attrs[NSAttributedStringKey.font] = font
		attrs[NSAttributedStringKey.foregroundColor] = color
		titleTextAttributes = attrs
	}
	
	/// SwifterSwift: Make navigation bar transparent.
	///
	/// - Parameter withTint: tint color (default is .white).
	public func makeTransparent(withTint: UIColor = .white) {
		setBackgroundImage(UIImage(), for: .default)
		shadowImage = UIImage()
        isTranslucent = true
		tintColor = withTint
		titleTextAttributes = [NSAttributedStringKey.foregroundColor: withTint]
        barTintColor = UIColor.clear
//        backgroundColor = UIColor.clear
	}
	
	/// SwifterSwift: Set navigationBar background and text colors
	///
	/// - Parameters:
	///   - background: backgound color
	///   - text: text color
	public func setColors(background: UIColor, text: UIColor) {
        isTranslucent = false
//        backgroundColor = background
		barTintColor = background
		setBackgroundImage(UIImage(), for: UIBarMetrics.default)
		tintColor = text
		titleTextAttributes = [NSAttributedStringKey.foregroundColor: text]
	}
    
    func hideBottomHairline() {
        self.hairlineImageView?.isHidden = true
    }
    
    func showBottomHairline() {
        self.hairlineImageView?.isHidden = false
    }
}
    
    extension UIView {
        fileprivate var hairlineImageView: UIImageView? {
            return hairlineImageView(in: self)
        }
        
        fileprivate func hairlineImageView(in view: UIView) -> UIImageView? {
            if let imageView = view as? UIImageView, imageView.bounds.height <= 1.0 {
                return imageView
            }
            
            for subview in view.subviews {
                if let imageView = self.hairlineImageView(in: subview) { return imageView }
            }
            
            return nil
        }
    }
#endif
