//
//  CustomFont.swift
//  TrackReport
//
//  Created by Henryp on 29/03/17.
//
//

import UIKit


var kPTSansRegularFontName    = "PTSans-Regular"
var kPTSansBoldFontName       = "PTSans-Bold"

var kSorceSansRegularFontName    = "SourceSansPro-Regular"
var kSorceSansSemiboldFontName       = "SourceSansPro-SemiBold"


func fontNameFromType(fontType : String) -> String {
    switch fontType.lowercased() {
        
    case "ptregular":
        return kPTSansRegularFontName
    case "ptbold":
        return kPTSansBoldFontName
    case "sregular":
        return kSorceSansRegularFontName
    case "ssemibold":
        return kSorceSansSemiboldFontName

    default:
        return kPTSansRegularFontName
    }
}

extension UITextField {
    @IBInspectable var fontType : String?  {
        get {
            return self.font?.familyName
        }
        set {
            self.font = UIFont(name: fontNameFromType(fontType: newValue!), size: (self.font?.pointSize)!)
            
        }
    }
    
    @IBInspectable var returnButton : Bool  {
        get {
            return true
        }
        set {
            if newValue {
                let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
                let flexableItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, w: UIScreen.main.bounds.size.width, h: 40))
                toolbar.tintColor = UIColor.darkGray
                toolbar.barTintColor = UIColor.lightGray
                toolbar.setItems([flexableItem,doneItem], animated: false)
                //toolbar.setBackgroundImage(UIImage(named: ""), forToolbarPosition: .any, barMetrics: .default)
                self.inputAccessoryView = toolbar
            }
        }
    }
    
    @objc func hideKeyboard() -> Void {
        self.resignFirstResponder()
    }

}

extension UILabel {
    @IBInspectable var fontType : String?  {
        get {
            return self.font?.familyName
        }
        set {
            self.font = UIFont(name: fontNameFromType(fontType: newValue!), size: (self.font?.pointSize)!)
        }
    }
}

extension UITextView {
    @IBInspectable var fontType : String?  {
        get {
            return self.font?.familyName
        }
        set {
            self.font = UIFont(name: fontNameFromType(fontType: newValue!), size: (self.font?.pointSize)!)
        }
    }
    
    @IBInspectable var returnButton : Bool  {
        get {
            return true
        }
        set {
            if newValue {
                let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
                let flexableItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, w: UIScreen.main.bounds.size.width, h: 40))
                toolbar.tintColor = UIColor.darkGray
                toolbar.barTintColor = UIColor.lightGray
                toolbar.setItems([flexableItem,doneItem], animated: false)
                //toolbar.setBackgroundImage(UIImage(named: ""), forToolbarPosition: .any, barMetrics: .default)
                self.inputAccessoryView = toolbar
            }
        }
    }
    
    @objc func hideKeyboard() -> Void {
        self.resignFirstResponder()
    }
    
}

extension UIButton {
    @IBInspectable var fontType : String?  {
        get {
            return self.titleLabel?.font?.familyName
        }
        set {
            self.titleLabel?.font = UIFont(name: fontNameFromType(fontType: newValue!), size: (self.titleLabel?.font?.pointSize)!)
        }
    }
}

