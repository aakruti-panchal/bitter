//
//  SuggestedFriendCell.swift
//  BitterWorld
//
//  Created by  " " on 01/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class SuggestedFriendCell: UICollectionViewCell {
    
    @IBOutlet weak var viewContent      : UIView!
    @IBOutlet weak var viewAddFriend    : UIView!

    @IBOutlet weak var imgFriend        : UIImageView!
    @IBOutlet weak var imgProfile       : UIImageView!
    @IBOutlet weak var viewImgProfile   : UIView!
    @IBOutlet weak var lblName          : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         viewImgProfile.addShadow(offset: CGSize(width: 0.0, height: 5.0), radius: 4.0, color: K.Color.friendBoxShadow, opacity: 1.0, cornerRadius: 0)
        
       // viewContent.addShadow(offset: CGSize(width: 0.0, height: 5.0), radius: 4.0, color: UIColor.red, opacity: 1.0, cornerRadius: 0)
        
       
    }
    
    func setShadow() -> Void {
        self.contentView.layer.cornerRadius = 0.0
        self.contentView.layer.borderWidth = 0.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.layer.shadowOffset = CGSize(width: 10, height: 0)
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
}

class MyFriendCell: UITableViewCell {
    
    
    @IBOutlet weak var imgProfile       : UIImageView!
    @IBOutlet weak var lblName          : UILabel!
    @IBOutlet weak var viewUnFriend     : UIView!
    @IBOutlet weak var lblUnfriend      : UILabel!
    
    @IBOutlet weak var viewCancel       : UIView!
    @IBOutlet weak var lblCancelRequest : UILabel!
    
    @IBOutlet weak var viewPending      : UIView!
    @IBOutlet weak var lblAccept      : UILabel!
    @IBOutlet weak var lblReject      : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        imgProfile.setCornerRadius(radius: imgProfile.width/2)
    }
    
}


class PendingCell: UITableViewCell {
    
    
    @IBOutlet weak var imgProfile       : UIImageView!
    @IBOutlet weak var lblName          : UILabel!
    @IBOutlet weak var viewConfirm     : UIView!
    @IBOutlet weak var viewDelete     : UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfile.setCornerRadius(radius: imgProfile.width/2)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}



class InvitedCell: UITableViewCell {
    
    
    @IBOutlet weak var imgProfile       : UIImageView!
    @IBOutlet weak var lblName          : UILabel!
    @IBOutlet weak var viewCancelRequest    : UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfile.setCornerRadius(radius: imgProfile.width/2)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
