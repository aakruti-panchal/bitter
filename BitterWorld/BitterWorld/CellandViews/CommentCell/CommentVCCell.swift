//
//  CommentVCCell.swift
//  BitterWorld
//
//  Created by  " " on 09/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class CommentVCCell: UITableViewCell {

    @IBOutlet weak var imgUser          : UIImageView!
    @IBOutlet weak var lblName          : UILabel!
    @IBOutlet weak var lblComment       : UILabel!
    @IBOutlet weak var lblTime          : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblName.text       = ""
        self.lblComment.text    = ""
        self.lblTime.text       = ""
        
        self.lblName.textColor          = K.Color.black
        self.lblComment.textColor       = K.Color.commentText
        self.lblTime.textColor          = K.Color.commentTime
        
        self.lblName.font               = UIFont(name: kPTSansBoldFontName, size: 14)
        self.lblComment.font            = UIFont(name: kPTSansRegularFontName, size: 15)
        self.lblTime.font               = UIFont(name: kPTSansRegularFontName, size: 12)
        
        self.imgUser.layer.masksToBounds    = false
        self.imgUser.borderColor            = K.Color.white
        self.imgUser.borderWidth            = 0.5
        self.imgUser.roundSquareImage()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func initObj(objComment : Comment) {

        self.separatorInset = .zero;
        
        if let imgUrl = objComment.profile_photo {
            imgUser.setImageWithURL(imgUrl, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        }
        if let name = objComment.first_name { self.lblName.text = name }
        if let comment = objComment.comment_text { self.lblComment.text = comment }
        if let createdDate = objComment.created_date {
            self.lblTime.text = NSDate(timeIntervalSince1970: TimeInterval(createdDate)).timeAgoForB()
        }
    }
    
}
