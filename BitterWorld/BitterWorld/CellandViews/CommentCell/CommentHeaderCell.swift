//
//  CommentHeaderCell.swift
//  BitterWorld
//
//  Created by  " " on 09/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class CommentHeaderCell: UITableViewCell {

    @IBOutlet weak var viewHeader           : UIView!
    @IBOutlet weak var imgUser              : UIImageView!
    @IBOutlet weak var lblName              : UILabel!
    @IBOutlet weak var lblAddress           : UILabel!
    @IBOutlet weak var lblShopTitle         : UILabel!
    @IBOutlet weak var imgAddrPlace         : UIImageView!
    @IBOutlet weak var viewRating           : FloatRatingView!
    
    
    @IBOutlet weak var viewContent          : UIView!
    @IBOutlet weak var viewDescription           : UIView!
    @IBOutlet weak var lblDescription           : UILabel!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet var collectionViewMedia: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgUser.layer.masksToBounds = false
        self.imgUser.roundSquareImage()
        
        let nib = UINib(nibName: "PostMediaCell", bundle: nil)
        collectionViewMedia.register(nib, forCellWithReuseIdentifier: "PostMediaCell")
        self.collectionViewMedia.delegate = self
        self.collectionViewMedia.dataSource = self
        collectionViewMedia.reloadData()
        collectionViewMedia.bringSubview(toFront: pageControl)
        
        
       
        
        //set color
        self.lblDescription.textColor       = K.Color.lightTextColor
        
        self.btnReadMore.setTitleColor(K.Color.lightGray, for: .normal)
        //set font
        self.lblAddress.font                = UIFont(name: kPTSansRegularFontName, size: 15)
        self.btnReadMore.titleLabel?.font   =  UIFont(name: kPTSansRegularFontName, size: 15)
        
        
        self.lblName.font                   = UIFont(name: kPTSansRegularFontName, size: 15.21)
        self.lblAddress.font                = UIFont(name: kPTSansRegularFontName, size: 13.42)
        self.lblShopTitle.font              = UIFont(name: kPTSansRegularFontName, size: 13.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var feed : Feed? {
        didSet {
            
            pageControl.numberOfPages = (feed?.mediaData.count)!
            imgUser.setImageWithURL(feed?.profilePhoto, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            lblName.text = feed?.firstname
            
            var cityCountry = ""
            if let city = feed?.city, !city.isEmpty {
                cityCountry.append(city)
            }
            if let country = feed?.country, !country.isEmpty {
                cityCountry.append(", \(country)")
            }
            if cityCountry.isEmpty {
                self.imgAddrPlace.isHidden = true
            } else {
                self.imgAddrPlace.isHidden = false
            }
            //let cityCountry1 = "\(feed?.city ?? "") , \(feed?.country ?? "")"
            
            self.lblAddress.adjustsFontSizeToFitWidth = false
            let cityWidth = cityCountry.sizeWidth(self.lblAddress.size.height, font: UIFont(name: kPTSansRegularFontName, size: 15)!, lineBreakMode: .byWordWrapping).width
            
            if (feed?.rating ?? 0 > 0) {
                
                //self.viewRating.isHidden = false
                self.viewRating.rating = (self.feed?.rating)!
                //let tmp = self.viewRating.left - self.imgUser.right
                let tmp = (SF.screenWidth - self.viewRating.width) - self.imgUser.right
                
                if cityWidth > tmp {
                    self.lblAddress.width = tmp - 40
                } else {
                    self.lblAddress.width = cityWidth
                }
            } else {
                //viewRating.isHidden = true
                //lblAddress.width = self.viewHeader.width - (self.viewRating.width + 8.0)
                self.lblAddress.width = (cityWidth > (self.viewHeader.right - 80)) ? self.viewHeader.right - 80 : cityWidth
                
            }
            
            //lblAddress.backgroundColor = UIColor.red
            lblAddress.text = cityCountry
            self.imgAddrPlace.left = lblAddress.right
            
            lblShopTitle.text = feed?.categoryName
            
            /*
            let strComment = feed?.commentCount == 1 ? "Comment" : "Comments"
            let strBroken = feed?.likeCount == 1 ? "Broken Heart" : "Broken Hearts"
            
            btnLike.isSelected = feed?.isLike == true ? true : false
            
            if feed?.likeCount ?? 0 > 0 && feed?.commentCount ?? 0 > 0{
                self.lblNoOfComments.text = "\(feed?.likeCount ?? 0) \(strBroken) • " + "\(feed?.commentCount ?? 0 ) \(strComment)"
                
                self.lblNoOfComments.isHidden = false
            }
            else if feed?.likeCount ?? 0 > 0{
                
                self.lblNoOfComments.text = "\(feed?.likeCount ?? 0) \(strBroken) "
                
                self.lblNoOfComments.isHidden = false
                
            }
            else if feed?.commentCount ?? 0 > 0{
                
                self.lblNoOfComments.text = "\(feed?.commentCount ?? 0) \(strComment) "
                
                self.lblNoOfComments.isHidden = false
                
            }
            else{
                self.lblNoOfComments.isHidden = true
            }
            
            
            self.lblDescription.text = feed?.desc
            print(self.lblDescription.numberOfVisibleLines)
            let font = self.lblDescription.font!
            if self.feed?.readMore == true {
                
                //self.lblDescription.numberOfLines = 2
                var descCountry = ""
                if let desc = feed?.desc, !desc.isEmpty {
                    descCountry.append(desc)
                }
                let descHeight = descCountry.size(self.lblDescription.size.width, font: font, lineBreakMode: .byWordWrapping).height
                print("descHeight : ",descHeight)
                self.lblDescription.height = descHeight
                
                self.btnReadMore.isHidden = true
                
                
            } else {
                let descHeight = font.lineHeight * 2
                self.lblDescription.height = descHeight
                
                
                self.btnReadMore.isHidden = (self.lblDescription.numberOfVisibleLines > 2) ? false : true
            }
            
            self.viewDescription.height = self.lblDescription.height + 20
            self.viewFooterContent.y = self.viewDescription.bottom + 10
            self.viewFooterContent.height = self.viewEnteComment.bottom
            self.viewContent.height = self.viewFooterContent.bottom
            
            //            self.viewFooterContent.backgroundColor = UIColor.clear
            //            self.viewContent.backgroundColor = UIColor.green
            
            self.viewContent.width = self.width - 20
            self.viewContent.roundCorners([.bottomLeft, .bottomRight], radius: 20)
            
            self.viewDescription.addBorderLeft(size: 1, color: UIColor.lightGray)
            self.viewDescription.addBorderRight(size: 1, color: UIColor.lightGray)
            self.viewDescription.addBorderBottom(size: 1, color: UIColor.lightGray)
            self.viewDescription.roundCorners([.bottomLeft, .bottomRight], radius: 5)
            
            //let descWidth = descCountry.sizeWidth(self.lblDescription.size.height, font: UIFont(name: kPTSansRegularFontName, size: 15)!, lineBreakMode: .byWordWrapping).height
            self.lblDate.text =     Date(timeIntervalSince1970:(feed?.createdDate)!).toString(format: K.DateFormat.feedCreate)
            
            if feed?.commentCount == 0{
                lblComments.isHidden = true
                viewEnteComment.y = lblComments.y
            }
            else{
                lblComments.isHidden = false
                viewEnteComment.y = lblComments.bottom + 10
            }
             */
            collectionViewMedia.reloadData()
            
        }
    }
    
}


extension CommentHeaderCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feed?.mediaData.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewMedia.width, height:188.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostMediaCell", for: indexPath) as! PostMediaCell
        cell.setData(for: indexPath, arrMedia: feed?.mediaData ?? [], mediaCount:0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewMedia {
            
            //For Image full screen
            if self.feed?.mediaData[indexPath.row].mediaType == 1 {
                self.openFullImageScreen()
                
            } else if self.feed?.mediaData[indexPath.row].mediaType == 2 {
                //feed_video
                if let myURL = self.feed?.mediaData[indexPath.row].videofile, !myURL.isEmpty  {
                    let videoURL = URL(string: myURL)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    AppUtility.shared.navigationController?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            } else if self.feed?.mediaData[indexPath.row].mediaType == 3 {
                //feed_audio
                if let myURL = self.feed?.mediaData[indexPath.row].audio, !myURL.isEmpty {
                    
                    let videoURL = URL(string: myURL)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    AppUtility.shared.navigationController?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
        self.changeDotColor()
    }
    
    func changeDotColor() -> Void {
        for (index, dot) in pageControl.subviews.enumerated() {
            if index == pageControl.currentPage {
                dot.backgroundColor = .red
                dot.layer.cornerRadius = dot.frame.size.height / 2;
            } else {
                dot.backgroundColor = .white
                dot.layer.cornerRadius = dot.frame.size.height / 2
                dot.layer.borderColor = UIColor.white.cgColor
                // dot.layer.borderWidth = 1.0
            }
        }
    }
    
    func openFullImageScreen(){
        
        var imgArr = [String]()
        if let count = self.feed?.mediaData.count {
            for i in 0..<(count) {
                if self.feed?.mediaData[0].mediaType == 1 {
                    if let imgUrl = self.feed?.mediaData[i].photo, !imgUrl.isEmpty {
                        imgArr.append(imgUrl)
                    }
                }
            }
            
            if imgArr.count > 0 {
                let objFullView         = FullImageView(nibName:"FullImageView",bundle: nil)
                objFullView.imageArray  = imgArr
                AppUtility.shared.navigationController?.present(objFullView, animated: true, completion: nil)
            }
        }
    }
}

