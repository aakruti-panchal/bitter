//
//  SettingsVCCell.swift
//  BitterWorld
//
//  Created by  " " on 14/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class SettingsVCCell: UITableViewCell {

    @IBOutlet weak var viewContents: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnNotificationStatus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblName.textColor  = K.Color.black
        self.lblName.font       = UIFont(name: kPTSansRegularFontName, size: 15)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initObj(objSettings : Settings, index : Int) {
        
        self.btnNotificationStatus.isHidden     = (index == 0) ? false : true
        self.btnNotificationStatus.isSelected   = (User.loggedInUser()?.isPushNotification == 1) ? true : false
        self.accessoryType                      = (index == 0) ? .none : .disclosureIndicator
        self.separatorInset = .zero;
        
        if let name = objSettings.Name {
            self.lblName.text = name
        }
        
    }
}
