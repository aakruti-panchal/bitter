//
//  NotificationListVCCell.swift
//  BitterWorld
//
//  Created by  " " on 15/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class NotificationListVCCell: UITableViewCell {

    @IBOutlet weak var viewMain             : UIView!
    @IBOutlet weak var imgNotification      : UIImageView!
    @IBOutlet weak var lblComments          : UILabel!
    @IBOutlet weak var btnConfirm           : UIButton!
    @IBOutlet weak var btnDelete            : UIButton!
    @IBOutlet weak var lblDateTime          : UILabel!
    
    @IBOutlet weak var viewBtns: UIView!
    @IBOutlet weak var viewBtnsHeight: NSLayoutConstraint!
    
    let notification_type = "friendrequest"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imgNotification.layer.masksToBounds    = false
        self.imgNotification.borderColor            = K.Color.lightGray
        self.imgNotification.borderWidth            = 0.5
        self.imgNotification.roundSquareImage()
        
        self.lblComments.textColor  = K.Color.black
        self.lblComments.font       = UIFont(name: kPTSansRegularFontName, size: 15)
        
        self.lblDateTime.textColor  = K.Color.lightGray
        self.lblDateTime.font       = UIFont(name: kPTSansRegularFontName, size: 12)
        
        self.btnConfirm.backgroundColor        = K.Color.black
        self.btnConfirm.setTitleColor(K.Color.white, for: .normal)
        
        self.btnDelete.backgroundColor        = K.Color.white
        self.btnDelete.setTitleColor(K.Color.darkGray, for: .normal)
        
        Common().addshadow(View: self.btnConfirm, shadowRadius: 5.0, shadowColor: UIColor.white, shadowOpacity: 1.0, borderWidth: 0.5, borderColor : K.Color.darkGray, cornerRadius : 1.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        Common().addshadow(View: self.btnDelete, borderWidth: 0.5, borderColor : K.Color.darkGray, cornerRadius : 1.0)
        
        
        self.lblComments.text = ""
        self.lblDateTime.text = ""
        //Common().addshadow(View: self.viewMain, shadowRadius: 5.0, shadowColor: UIColor.lightGray, shadowOpacity: 1.0, cornerRadius : 3.0 , shadowOffset: CGSize(width: 1.0, height: 1.0))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func initObj(objNotifications : Notifications) {
        
        self.separatorInset = .zero;
        if let imgUrl = objNotifications.profile_photo {
            self.imgNotification.setImageWithURL(imgUrl, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        }
        if let commentTxt = objNotifications.notification_text {
            self.lblComments.text = commentTxt
        }
        if let strDate = objNotifications.created_date {
            self.lblDateTime.text =     Date(timeIntervalSince1970:strDate).toString(format: K.DateFormat.NotificationDateTime)
        }
        
        if let notiType = objNotifications.notification_type, notiType == self.notification_type, let is_friend = objNotifications.is_friend, is_friend == 1 {
            self.viewBtns.isHidden = false
            self.viewBtnsHeight.constant = 50.0
        } else {
            self.viewBtns.isHidden = true
            self.viewBtnsHeight.constant = 0
        }
        
    }
    
}
