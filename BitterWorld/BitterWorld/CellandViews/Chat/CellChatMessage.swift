//
//  CellChatMessage.swift

import UIKit

class CellChatMessage: UITableViewCell {
    
    @IBOutlet weak var profileImageContainer: UIView!
    @IBOutlet weak var imgViewProfilePic    : UIImageView!
    @IBOutlet weak var imgViewStatus        : UIImageView!
    @IBOutlet weak var viewBubbleContainer  : UIView!
    @IBOutlet weak var imgViewBubble        : UIImageView!
    @IBOutlet weak var lblMessage           : UILabel!
    @IBOutlet weak var lblMessageTime       : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    class func getChatStaticHeight() -> CGFloat {
        
        return 60
        //return 45
    }
}
