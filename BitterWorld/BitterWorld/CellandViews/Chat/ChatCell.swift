//
//  ChatCell.swift
//  BitterWorld
//
//  Created by  " " on 08/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblMsgCount: UILabel!
    
    @IBOutlet var viewInfoContainer: UIView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblShortName: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var lblLastMessage: UILabel!
    
    @IBOutlet var selectedView: UIView!
    @IBOutlet var selectedLineView: UIView!
    @IBOutlet var imgSelectedArrow: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgProfile.setCornerRadius(radius: self.imgProfile.width/2)
        self.imgProfile.layer.borderColor = K.Color.extraLightGray.cgColor
        self.imgProfile.layer.borderWidth = 1.0
        self.lblMsgCount.setCornerRadius(radius: self.lblMsgCount.width/2)
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
