//
//  ChatFeedCell.swift

//
//  Created by on 05/04/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

class ChatFeedCell : UITableViewCell {
    
    //MARK: - IBOutlet
    @IBOutlet weak var imgUserProfile       : UIImageView!
    
    @IBOutlet weak var viewImageContainer   : UIView!
    @IBOutlet weak var imgBubble            : UIImageView!
    @IBOutlet weak var imgReceived          : UIImageView!
    @IBOutlet weak var lblTime              : UILabel!
    @IBOutlet weak var btnDownaload         : UIButton!
    @IBOutlet weak var viewMediaContainer   : UIView!
    @IBOutlet weak var viewDarkLayer        : UIView!
    @IBOutlet weak var activityIndicator    : UIActivityIndicatorView!
    @IBOutlet weak var lblCaption           : UILabel!
    
    
    //MARK: - Controller File Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgUserProfile?.roundView()
        self.btnDownaload?.setCornerRadius(radius: 5)
        self.viewMediaContainer?.setCornerRadius(radius: 5)
        
    }
    
    func displayCellForSender(isSender : Bool, isShowProfileImage: Bool = false) -> Void {
        
        self.imgUserProfile?.isHidden = !isShowProfileImage
        
        let offset : CGFloat = 10.0
        if(isSender == true) {
            self.imgUserProfile.right = offset
            
            if isShowProfileImage == true {
                self.viewImageContainer.right = self.imgUserProfile.width + offset
                self.lblTime.right = self.imgUserProfile.width + offset + 17
            }
            else {
                self.viewImageContainer.right = offset
                self.lblTime.right = offset + 17
            }
            
            self.viewMediaContainer.right = 19
            self.lblCaption.x = self.viewMediaContainer.x
            self.imgBubble.image = UIImage(named: "ic_greychatbox")
            self.lblTime.textAlignment = .right
            self.lblCaption.textColor = UIColor.black
        }
        else {
            self.imgUserProfile.x = offset
            
            if isShowProfileImage == true {
                self.viewImageContainer.x = self.imgUserProfile.right
            }
            else {
                self.viewImageContainer.x = self.imgUserProfile.x
            }
            
            self.lblTime.x = self.viewMediaContainer.x + 10
            self.viewMediaContainer.x = 19
            self.lblCaption.x = self.viewMediaContainer.x
            self.imgBubble.image = UIImage(named: "ic_redchatbox")
            self.lblTime.textAlignment = .left
            self.lblCaption.textColor = UIColor.white
        }
    }
    
    class func getChatStaticHeight() -> CGFloat {
        return 220
    }
    
}
