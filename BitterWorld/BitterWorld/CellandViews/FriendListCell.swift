//
//  FriendListCell.swift
//  BitterWorld
//
//  Created by rane on 07/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class FriendListCell: UICollectionViewCell {
 
    @IBOutlet weak var imgProfilePic    : UIImageView!
    @IBOutlet weak var lblName          : UILabel!
    @IBOutlet weak var lblPostCount     : UILabel!
    
    var objFriendList : Friends! {
        
        didSet {
            
            self.lblName.text = objFriendList?.first_name
            self.imgProfilePic.setImageWithURLObjC(objFriendList?.profile_photo ?? "", placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), completionBlock: nil)
            self.lblPostCount.text = "\(objFriendList?.totalFeeds ?? 0) Post"
            
        }
        
    }
    
}
