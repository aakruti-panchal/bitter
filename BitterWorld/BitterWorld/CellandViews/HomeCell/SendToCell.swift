//
//  SendToCell.swift
//  BitterWorld
//
//  Created by  " " on 03/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class SendToCell: UITableViewCell {

    @IBOutlet weak var imgSendTo: UIImageView!
    @IBOutlet weak var lblSentToTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //Common().addshadow(View: self.imgSendTo, borderWidth :0, borderColor : UIColor.clear, cornerRadius : self.imgSendTo.bounds.height)
        self.imgSendTo.layer.masksToBounds = false
        self.imgSendTo.roundSquareImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
