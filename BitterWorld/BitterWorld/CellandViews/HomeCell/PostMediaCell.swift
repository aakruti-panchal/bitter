

//
//  PostMediaCell.swift
//  BitterWorld
//
//  Created by  " " on 25/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit

class PostMediaCell: UICollectionViewCell {
    @IBOutlet weak var imgMeida              : UIImageView!
    @IBOutlet weak var imgPlay              : UIButton!

    
    func setData(for indexPath: IndexPath, arrMedia: Array<MediaData>,mediaCount : Int) {
        
        
        let postMedia = arrMedia[indexPath.item]
        
        
        if postMedia.mediaType == MediaType.image.rawValue {
            imgMeida.setImageWithURL(postMedia.photo, placeHolderImage: #imageLiteral(resourceName: "ic_image_placeholder"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            imgPlay.isHidden = true
        }
        else if postMedia.mediaType == MediaType.video.rawValue {
            imgMeida.setImageWithURL(postMedia.videoThumb, placeHolderImage: #imageLiteral(resourceName: "ic_image_placeholder"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            imgPlay.isHidden = false
            imgPlay.setImage(UIImage(named:"ic_play.png"), for: .normal)
        }
        else {
            imgMeida.image = #imageLiteral(resourceName: "audio_placeholder")
            imgPlay.isHidden = false
            imgPlay.setImage(UIImage(named:"ic_record_btn_icon"), for: .normal)
        }

        
    }
    
    
}
