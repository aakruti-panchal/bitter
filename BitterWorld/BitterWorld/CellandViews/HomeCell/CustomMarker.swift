//
//  CustomMarker.swift
//  Sample Custom Map
//
//  Created by Akruti Panchal on 30/03/18.
//  Copyright © 2018 Akruti Panchal. All rights reserved.
//

import UIKit
import GoogleMaps

class CustomMarker: UIView {
    @IBOutlet weak var IBImgaeUser: UIImageView!
    @IBOutlet weak var imgPin: UIImageView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomMarker", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
//    override init(_ image: UIImage?) {
//        if let imageVal = image {
//            self.IBImgaeUser.image = imageVal
//        }
//    }
 
}
