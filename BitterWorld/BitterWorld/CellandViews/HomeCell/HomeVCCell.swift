//
//  HomeVCCell.swift
//  BitterWorld
//
//  Created by  " " on 20/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
//import FTPopOverMenu_Swift
import AVKit
import AVFoundation

@objc protocol HomeVCCellDelegate:class {
    @objc optional func didFinishSelection()
}

class HomeVCCell: UICollectionViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    weak var selectDelegate   : HomeVCCellDelegate?
    
    @IBOutlet weak var viewMainView         : UIView!
    @IBOutlet weak var viewHeader           : UIView!
    @IBOutlet weak var viewContent          : UIView!
    
    @IBOutlet weak var imgUser              : UIImageView!
    @IBOutlet weak var lblName              : UILabel!
    @IBOutlet weak var lblAddress           : UILabel!
    @IBOutlet weak var lblShopTitle         : UILabel!
    
    @IBOutlet weak var imgAddrPlace         : UIImageView!
    
    @IBOutlet weak var viewRating           : FloatRatingView!
    
    @IBOutlet weak var viewPager            : UIView!
    @IBOutlet weak var viewFooterContent    : UIView!

    
    @IBOutlet weak var viewDescription           : UIView!
    //@IBOutlet weak var lblDescription           : UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnSentComment: UIButton!
    @IBOutlet weak var btnMoreOption: UIButton!
    
    @IBOutlet weak var lblNoOfComments: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    
    
    @IBOutlet weak var viewEnteComment: UIView!
    @IBOutlet weak var imgComment: UIImageView!
    
    @IBOutlet weak var viewDate: UIView!
    
    @IBOutlet var collectionViewMedia: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var btnReadMore: UIButton!
    
    
    var menuOptionNameArray : [String] = ["Save Post","Share Post","Report Post"]
    
    var menuOptionImageNameArray : [String] = ["ic_save_post","ic_share","ic_flag_post"]
    
    ///**************
    let sentToViewController1 = SF.storyBoard.instantiateViewController(withIdentifier: "SendToViewController") as! SendToViewController
    //let popupController1 = CNPPopupController()
    
    //**************
    /*
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = .coverVerticalFromBottom
        presenter.dismissTransitionType = .coverHorizontalFromRight
        return presenter
    }()
    */
    
//    let customType = PresentationType.custom(width: ModalSize.full, height: ModalSize.fluid(percentage: 0.20), center: .center)
//    let presenter: Presentr = {
//        let presenter = Presentr(presentationType: customType)
//        presenter.transitionType = .coverVerticalFromTop
//        presenter.dismissTransitionType = .crossDissolve
//    }()
    let presenter: Presentr = {
        let width = ModalSize.full
        let height = ModalSize.custom(size: Float(SF.screenHeight - 300))
        let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: 300))
        let customType = PresentationType.custom(width: width, height: height, center: center)
        
        let customPresenter = Presentr(presentationType: customType)
        customPresenter.transitionType = .coverVertical
        customPresenter.dismissTransitionType = .coverVertical
        customPresenter.roundCorners = false
        customPresenter.backgroundColor = .black
        customPresenter.backgroundOpacity = 0.5
        customPresenter.dismissOnSwipe = true
        customPresenter.dismissOnSwipeDirection = .bottom
        return customPresenter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib() //borderColor

        self.lblName.text           = ""
        self.lblAddress.text        = ""
        self.lblShopTitle.text      = ""
        self.lblNoOfComments.text   = ""
        self.lblDate.text           = ""
        self.lblComments.text       = ""
        
        self.imgUser.layer.masksToBounds = false
        self.imgComment.layer.masksToBounds = false
        self.imgUser.roundSquareImage()
        self.imgComment.roundSquareImage()
      
        let nib = UINib(nibName: "PostMediaCell", bundle: nil)
        collectionViewMedia.register(nib, forCellWithReuseIdentifier: "PostMediaCell")
        self.collectionViewMedia.delegate = self
        self.collectionViewMedia.dataSource = self
        collectionViewMedia.reloadData()
        collectionViewMedia.bringSubview(toFront: pageControl)
        
        
        btnLike.addTapGesture { (gesture) in
    
            self.likeWebserviceCall(self.feed?.feedID)
        }
        
        //set color
        self.lblDescription.textColor       = K.Color.lightTextColor
        self.lblNoOfComments.textColor      = K.Color.lightGray
        self.lblDate.textColor              = K.Color.lightGray
        self.lblComments.textColor          = K.Color.lightTextColor
        self.btnReadMore.setTitleColor(K.Color.lightGray, for: .normal)
        //set font
        self.lblAddress.font                = UIFont(name: kPTSansRegularFontName, size: 15)
        self.btnReadMore.titleLabel?.font   =  UIFont(name: kPTSansRegularFontName, size: 15)
        self.lblNoOfComments.font           = UIFont(name: kPTSansRegularFontName, size: 13)
        self.lblDate.font                   = UIFont(name: kPTSansRegularFontName, size: 13)
        self.lblComments.font               = UIFont(name: kPTSansRegularFontName, size: 13)
        
        self.lblName.font                   = UIFont(name: kPTSansRegularFontName, size: 15.21)
        self.lblAddress.font                = UIFont(name: kPTSansRegularFontName, size: 13.42)
        self.lblShopTitle.font              = UIFont(name: kPTSansRegularFontName, size: 13.5)
        
        let config = FTConfiguration.shared
        config.textColor = K.Color.lightGray
        config.backgoundTintColor = UIColor.white
        config.borderColor = K.Color.termsCondition
        config.menuWidth = 150
        config.menuSeparatorColor = UIColor.clear
        config.textAlignment = .left
        config.textFont = UIFont.systemFont(ofSize: 14)
        config.menuRowHeight = 50
        config.cornerRadius = 0
        
//        [textView setTextContainerInset:UIEdgeInsetsZero];
        lblDescription.textContainerInset = .zero
        lblDescription.textContainer.lineFragmentPadding = 0;
        
        self.viewDescription.addBorder(width: 1, color: UIColor.lightGray, radius: 5)
        
//        self.viewContent.width = self.width - 20
//        self.viewDescription.width = self.width - 20
//        self.viewContent.roundCorners([.bottomLeft, .bottomRight], radius: 20)
//
//        self.viewDescription.addBorderLeft(size: 1, color: UIColor.lightGray)
//        self.viewDescription.addBorderRight(size: 1, color: UIColor.lightGray)
//        self.viewDescription.addBorderBottom(size: 1, color: UIColor.lightGray)
//        self.viewDescription.roundCorners([.bottomLeft, .bottomRight], radius: 5)
        
        
    }
    
    @IBAction func btnSentToTapped(_ sender: UIButton) {
        
        self.openPopup(isReportPost: false, feedID: (self.feed?.feedID)!)
        
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.reloadInputViews()
//    }
    
    var feed : Feed? {
        didSet {
            
            
            pageControl.numberOfPages = (feed?.mediaData.count)!
            imgComment.setImageWithURL(User.loggedInUser()?.profilePic, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            imgUser.setImageWithURL(feed?.profilePhoto, placeHolderImage: #imageLiteral(resourceName: "ic_placeholder_user"), activityIndicatorViewStyle: .gray, completionBlock: nil)
            
            
            
            lblName.text = feed?.firstname
            
            var cityCountry = ""
            if let city = feed?.city, !city.isEmpty {
                cityCountry.append(city)
            }
            if let country = feed?.country, !country.isEmpty {
                cityCountry.append(", \(country)")
            }
            if cityCountry.isEmpty {
                self.imgAddrPlace.isHidden = true
            } else {
                self.imgAddrPlace.isHidden = false
            }
            //let cityCountry1 = "\(feed?.city ?? "") , \(feed?.country ?? "")"
            
            self.lblAddress.adjustsFontSizeToFitWidth = false
            let cityWidth = cityCountry.sizeWidth(self.lblAddress.size.height, font: UIFont(name: kPTSansRegularFontName, size: 15)!, lineBreakMode: .byWordWrapping).width
            
            if (feed?.rating ?? 0 > 0) {
                
                //self.viewRating.isHidden = false
                
                self.viewRating.rating = (self.feed?.rating)!
                
                //let tmp = self.viewRating.left - self.imgUser.right
                let tmp = (SF.screenWidth - self.viewRating.width) - self.imgUser.right
                
                if cityWidth > tmp {
                    self.lblAddress.width = tmp - 40
                } else {
                    self.lblAddress.width = cityWidth
                }
                
            } else {
                //viewRating.isHidden = true
                //lblAddress.width = self.viewHeader.width - (self.viewRating.width + 8.0)
                self.lblAddress.width = (cityWidth > (self.viewHeader.right - 80)) ? self.viewHeader.right - 80 : cityWidth
            }
            
            //lblAddress.backgroundColor = UIColor.red
            lblAddress.text = cityCountry
            self.imgAddrPlace.left = lblAddress.right
            
            lblShopTitle.text = feed?.categoryName
            
            
            let strComment = feed?.commentCount == 1 ? "Comment" : "Comments"
            let strBroken = feed?.likeCount == 1 ? "Broken Heart" : "Broken Hearts"
            
            btnLike.isSelected = feed?.isLike == true ? true : false
            
            if feed?.likeCount ?? 0 > 0 && feed?.commentCount ?? 0 > 0{
                self.lblNoOfComments.text = "\(feed?.likeCount ?? 0) \(strBroken) • " + "\(feed?.commentCount ?? 0 ) \(strComment)"
                
                self.lblNoOfComments.isHidden = false
            }
            else if feed?.likeCount ?? 0 > 0{
                
                self.lblNoOfComments.text = "\(feed?.likeCount ?? 0) \(strBroken) "
                
                self.lblNoOfComments.isHidden = false
                
            }
            else if feed?.commentCount ?? 0 > 0{
                
                self.lblNoOfComments.text = "\(feed?.commentCount ?? 0) \(strComment) "
                
                self.lblNoOfComments.isHidden = false
                
            }
            else{
                self.lblNoOfComments.isHidden = true
            }
            
            
            self.lblDescription.text = feed?.desc
            print(self.lblDescription.numberOfVisibleLines)
            let font = self.lblDescription.font!
            if self.feed?.readMore == true {
                
                //self.lblDescription.numberOfLines = 2
                var descCountry = ""
                if let desc = feed?.desc, !desc.isEmpty {
                    descCountry.append(desc)
                }
                let descHeight = descCountry.size(self.lblDescription.size.width, font: font, lineBreakMode: .byWordWrapping).height
                print("descHeight : ",descHeight)
                self.lblDescription.height = descHeight
                
                self.btnReadMore.isHidden = true
                
                
            } else {
                let descHeight = font.lineHeight * 2
                self.lblDescription.height = descHeight
                self.btnReadMore.isHidden = (self.lblDescription.numberOfVisibleLines > 2) ? false : true
            }
            
            
            if feed?.commentCount == 0 {
                self.lblComments.height = 0
                lblComments.isHidden = true
                viewEnteComment.y = lblComments.y
            } else {
                //****************
                
                let fontComment = self.lblComments.font!
                if let count = self.feed?.comments.count, count > 0 {
                    //if self.feed?.comments.count > 0 {
                    if let commentTxt = self.feed?.comments.first?.comment_text, !commentTxt.isEmpty {

                        var comnt = ""
                        comnt.append(commentTxt)
                        var commentHeight = comnt.size(self.lblComments.size.width, font: fontComment, lineBreakMode: .byWordWrapping).height
                        print("comments : ",comnt)
                        self.lblComments.text = comnt
                        if self.lblComments.numberOfVisibleLines > 2 {
                            commentHeight = font.lineHeight * 2
                        }
                        self.lblComments.height = commentHeight
                    }
                }
                //****************
                
                lblComments.isHidden = false
                viewEnteComment.y = lblComments.bottom + 10
            }
            
//            lblComments.backgroundColor = UIColor.red
//            lblComments.textColor = UIColor.white
            
//            self.viewDescription.y = self.viewDescription.y - 5   //mehul
//            self.lblDescription.y = self.lblDescription.y + 5    //mehul
//            self.btnReadMore.y = self.btnReadMore.y + 5         //mehul
            
            self.viewDescription.height = self.lblDescription.height + 25
            self.viewFooterContent.y = self.viewDescription.bottom + 10
            self.viewFooterContent.height = self.viewEnteComment.bottom
            self.viewContent.height = self.viewFooterContent.bottom
            
            self.viewContent.width = self.width - 20
            self.viewContent.roundCorners([.bottomLeft, .bottomRight], radius: 20)
            


            
            
            //let descWidth = descCountry.sizeWidth(self.lblDescription.size.height, font: UIFont(name: kPTSansRegularFontName, size: 15)!, lineBreakMode: .byWordWrapping).height
            self.lblDate.text =     Date(timeIntervalSince1970:(feed?.createdDate)!).toString(format: K.DateFormat.feedCreate)
            
            
            self.viewContent.height = self.viewFooterContent.bottom
            collectionViewMedia.reloadData()
            
            
            
//            self.viewDescription.addBorderLeft(size: 1, color: UIColor.lightGray)
//            self.viewDescription.addBorderRight(size: 1, color: UIColor.lightGray)
//            self.viewDescription.addBorderBottom(size: 1, color: UIColor.lightGray)
            //self.viewDescription.layer.borderWidth  = 1
            //self.viewDescription.layer.borderColor  = UIColor.lightGray.cgColor
            //self.viewDescription.roundCorners([.bottomLeft, .bottomRight], radius: 5)
            //self.viewDescription.layer.masksToBounds = false
//            Common().addshadow(View: self.viewDescription, borderWidth: 0.5, borderColor : UIColor.lightGray, cornerRadius : 5.0 )
//            self.viewDescription.addBorderTop(size: 0.1, color: UIColor.clear)
            
//            let path = UIBezierPath(roundedRect:self.viewDescription.bounds,
//                                    byRoundingCorners:[.bottomLeft, .bottomRight],
//                                    cornerRadii: CGSize(width: 5, height:  5))
//
//            let maskLayer = CAShapeLayer()
//            self.viewDescription.layer.borderWidth  = 1
//            self.viewDescription.layer.borderColor  = UIColor.lightGray.cgColor
//            maskLayer.path = path.cgPath
//            self.viewDescription.layer.mask = maskLayer
            
//            self.viewDescription.backgroundColor    = UIColor.red
//            self.lblDescription.backgroundColor     = UIColor.green
//            self.viewDescription.layer.borderWidth  = 1
//            self.viewDescription.layer.borderColor  = UIColor.lightGray.cgColor
//            self.viewDescription.addBorder(width: 1, color: UIColor.lightGray, radius: 5)
//            self.viewDescription.roundCorners([.bottomLeft, .bottomRight], radius: 5)
//            self.viewDescription.superview?.bringSubview(toFront: self.viewDescription)

//            Common().addshadow(View: self.viewDescription, borderWidth: 1.0, borderColor : K.Color.lightGray, cornerRadius : 5.0)
//            self.viewDescription.addBorderTop(size: 1, color: UIColor.clear)
            
            
            
//            let rectShape = CAShapeLayer()
//            rectShape.bounds = self.viewDescription.frame
//            rectShape.position = self.viewDescription.center
//            rectShape.path = UIBezierPath(roundedRect: self.viewDescription.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 5, height: 5)).cgPath
//            self.viewDescription.layer.backgroundColor = UIColor.clear.cgColor //UIColor.green.cgColor
//
//                self.viewDescription.layer.masksToBounds    = false
//            self.viewDescription.layer.borderWidth          = 1
//            self.viewDescription.layer.borderColor          = UIColor.lightGray.cgColor
//            //Here I'm masking the textView's layer with rectShape layer
//            self.viewDescription.layer.mask = rectShape
            
            
//            self.viewDescription.backgroundColor = UIColor.red
//            self.lblDescription.backgroundColor = UIColor.green
//            self.viewContent.backgroundColor = UIColor.yellow
            
            
        }
    }
    
    // MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feed?.mediaData.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: collectionViewMedia.width, height:188.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostMediaCell", for: indexPath) as! PostMediaCell
         cell.setData(for: indexPath, arrMedia: feed?.mediaData ?? [], mediaCount:0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewMedia {
            

            
            //For Image full screen
            if self.feed?.mediaData[indexPath.row].mediaType == 1 {
                self.openFullImageScreen()
                
            } else if self.feed?.mediaData[indexPath.row].mediaType == 2 {
                //feed_video
                if let myURL = self.feed?.mediaData[indexPath.row].videofile, !myURL.isEmpty  {
                    let videoURL = URL(string: myURL)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    AppUtility.shared.navigationController?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            } else if self.feed?.mediaData[indexPath.row].mediaType == 3 {
                //feed_audio
                if let myURL = self.feed?.mediaData[indexPath.row].audio, !myURL.isEmpty {
                    
                    let videoURL = URL(string: myURL)
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    AppUtility.shared.navigationController?.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
    
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
        self.changeDotColor()
    }

    func changeDotColor() -> Void {
        for (index, dot) in pageControl.subviews.enumerated() {
            if index == pageControl.currentPage {
                dot.backgroundColor = .red
                dot.layer.cornerRadius = dot.frame.size.height / 2;
            } else {
                dot.backgroundColor = .white
                dot.layer.cornerRadius = dot.frame.size.height / 2
                dot.layer.borderColor = UIColor.white.cgColor
                // dot.layer.borderWidth = 1.0
            }
        }
    }
    
    func openFullImageScreen(){
        
        var imgArr = [String]()
        if let count = self.feed?.mediaData.count {
            for i in 0..<(count) {
                if self.feed?.mediaData[0].mediaType == 1 {
                    if let imgUrl = self.feed?.mediaData[i].photo, !imgUrl.isEmpty {
                        imgArr.append(imgUrl)
                    }
                }
            }
            
            if imgArr.count > 0 {
                let objFullView         = FullImageView(nibName:"FullImageView",bundle: nil)
                objFullView.imageArray  = imgArr
                AppUtility.shared.navigationController?.present(objFullView, animated: true, completion: nil)
            }
        }
    }
}

//MARK:- IBAction Methods
extension HomeVCCell {
    
    
    @IBAction func btnReadmoreTapped(_ sender: UIButton) {
        
        
        var descCountry = ""
        if let desc = feed?.desc, !desc.isEmpty {
            descCountry.append(desc)
        }
        let descHeight = descCountry.size(self.lblDescription.size.width, font: self.lblDescription.font!, lineBreakMode: .byWordWrapping).height
        print("descHeight : ",descHeight)
        self.lblDescription.height = descHeight
        self.viewDescription.height = descHeight + 25
        self.viewFooterContent.y = self.viewDescription.bottom + 10
        self.viewFooterContent.height = self.viewEnteComment.bottom
        self.btnReadMore.isHidden = true
        
        print(self.height)
        
        
        if let id = self.feed?.feedID, id > 0 {
            //print("FeedID : \(id) | Feed name : \(name)")
            //self.lblDescription.numberOfLines = 0
            self.feed?.readMore = true
        }
        self.selectDelegate?.didFinishSelection!()

    }
    
    @IBAction func handleButtonTap(_ sender: UIButton) {
        
        self.menuOptionImageNameArray = (self.feed?.is_reported == 1) ? ["ic_save_post","ic_share","ic_flag_post_red"] : ["ic_save_post","ic_share","ic_flag_post"]
        
        FTPopOverMenu.showForSender(sender: sender, with: menuOptionNameArray, menuImageArray: menuOptionImageNameArray as [String], done: { (selectedIndex) -> () in
            print(selectedIndex)

            switch selectedIndex {
            case 0:
                //Save
                break
            case 1:
                //Share
                
                break
            case 2:
                //Report Post
                self.openPopup(isReportPost: true, feedID: (self.feed?.feedID)!)
                break
            default:
                break
            }
            
        }) {
        }
    }
    
}





//MARK:- Service Call
extension HomeVCCell {
    
    func likeWebserviceCall(_ feedId: Int?) -> Void {
        
        let dictParam :Dictionary<String, Any> = [ "user_id" : User.loggedInUser()?.ID ?? 0, "feed_id":feedId ?? 0
        ]
        SVProgressHUD.show()
        _ = WebClient.requestWithUrl(url: K.URL.LIKE_DISLIKE, parameters: dictParam , completion: { (response, error) in
            if error == nil {
                SVProgressHUD.dismiss()
                
                if self.feed?.isLike == true {
                    self.feed?.likeCount = (self.feed?.likeCount)! - 1
                }
                else{
                    self.feed?.likeCount = (self.feed?.likeCount)! + 1
                }
                
                
                self.feed?.isLike = self.feed?.isLike == true ? false : true
                self.btnLike.isSelected = self.feed?.isLike == true ? true : false
                
                
                let strComment = self.feed?.commentCount == 1 ? "Comment" : "Comments"
                let strBroken = self.feed?.likeCount == 1 ? "Broken Heart" : "Broken Hearts"
                
                self.btnLike.isSelected = self.feed?.isLike == true ? true : false
                
                if self.feed?.likeCount ?? 0 > 0 && self.feed?.commentCount ?? 0 > 0{
                    self.lblNoOfComments.text = "\(self.feed?.likeCount ?? 0 > 0 ? strBroken : "")  " + "\(self.feed?.commentCount ?? 0 > 0 ? strComment : "" ) "
                    self.lblNoOfComments.isHidden = false
                }
                else if self.feed?.likeCount ?? 0 > 0{
                    self.lblNoOfComments.text = "\(self.feed?.likeCount ?? 0) \(strBroken) "
                    self.lblNoOfComments.isHidden = false
                }
                else if self.feed?.commentCount ?? 0 > 0{
                    self.lblNoOfComments.text = "\(self.feed?.commentCount ?? 0) \(strComment) "
                    self.lblNoOfComments.isHidden = false
                }
                else{
                    self.lblNoOfComments.isHidden = true
                }
            } else {
                ISMessages.show(error?.localizedDescription)
            }
            SVProgressHUD.dismiss()
        })
    }
    
    func reportPost(feedId : Int, reason : String) {
        
        //user_id, feed_id, reason
        let dictParam :Dictionary<String, Any> = ["user_id" : User.loggedInUser()?.ID ?? 0, "feed_id" : feedId, "reason" : reason]
        
        _ = WebClient.requestWithUrl(url: K.URL.REPORT_POST, parameters: dictParam, completion: { (response, error) in
            if error == nil {
                let dictData = response as! Dictionary<String, Any>
                if let message = dictData["message"] as? String {
                    ISMessages.show(message, type: .success)
                    
                }
                
            }
        })
        
    }
    
}

extension HomeVCCell {
    
    func initObj() {
        
    }
    //Common().addshadow(View: self.viewPager, shadowRadius: 2.0, shadowColor: K.Color.Light_Grey , shadowOpacity: 1.0,borderWidth : 1,borderColor : K.Color.Light_Grey , cornerRadius: 5.0, shadowOffset: CGSize(width: 1.0, height: 1.0))
    //  Common().addshadow(View: self.viewAttached, cornerRadius: (self.viewAttached.frame.size.width/2), IsMaskToBounds: true)
    //self.viewDescription.sendSubview(toBack: self.viewPager)
    
    //Common().addshadow(View: self.viewEnteComment, borderWidth : 1,borderColor : K.Color.Light_Grey, cornerRadius: (self.imgComent.frame.size.width/2), IsMaskToBounds: true)
    // self.viewFooterContent.roundCorners([.bottomLeft, .bottomRight], radius: self.imgComent.frame.size.width/2)
    
}


extension HomeVCCell : CNPPopupControllerDelegate {
    
    func popupControllerWillDismiss(_ controller: CNPPopupController) {
        print("Popup controller will be dismissed")
    }
    
    func popupControllerDidPresent(_ controller: CNPPopupController) {
        print("Popup controller presented")
    }
    
}


extension HomeVCCell {
    
    func openPopup(isReportPost : Bool, feedID : Int) {
        
        ReportAndSendPostViewController.showRateUsAlert(isReportPost : isReportPost, feedID : feedID) { (completionHandler) in
            switch completionHandler{
            case .Closed:
                break
            case .Apply:
                break
            case .any:
                break
            }
        }
    }
    
}


extension UITextView {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font!.pointSize))
        return rHeight / charSize
    }
}

extension UILabel {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font!.pointSize))
        return rHeight / charSize
    }
}
