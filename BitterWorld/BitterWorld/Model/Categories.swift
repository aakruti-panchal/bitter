//
//  Category.swift
//  BitterWorld
//
//  Created by  " " on 19/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class Categories: NSObject,Mappable {
    
    var ID           : Int? = 0
    var name         : String?
    var isSelected   : Bool = false
   
    
    required init?(map: Map) {
        
    }

    
    func mapping(map: Map) {
        ID <- map["category_id"]
        name <- map["title"]
      
    }


}
