
import Foundation
import ObjectMapper
enum MessageType : Int {
    case text = 0
    case image = 1
    case video = 2
    case audio = 3
    case feed = 4
}

public class ChatMessage : NSObject {
	
    var chatId                  : Int! = 0
    var conversationId          : Int! = 0
    var createdDate             : Double! = 0
    
    var message                 : String! = ""
    var messageType             : MessageType! = .text
    var file                    : String! = ""
    
    var ownerId                 : Int! = 0
    var ownerName               : String! = ""
    var ownerProfilePic         : String! = ""
    
    var receiverId              : Int! = 0
    var receiverName            : String! = ""
    var receiverProfilePic      : String! = ""
    var receiverSeen            : Int! = 0
    
    var thumbnail               : String! = ""
    var feedId                  : Int! = 0
    
    var feed_data : [FeedData] = []

    
    public class func modelsFromDictionaryArray(array:NSArray) -> [ChatMessage]
    {
        var models:[ChatMessage] = []
        for item in array
        {
            models.append(ChatMessage(dictionary: item as! NSDictionary)!)
        }
        return models
    }


	required public init?(dictionary: NSDictionary) {

        chatId = dictionary.value(forKey: "chat_id") as? Int ?? 0
        conversationId = dictionary.value(forKey: "conversation_id") as? Int ?? 0
        createdDate = dictionary.value(forKey: "created_date") as? Double ?? 0
        
        message = dictionary.value(forKey: "message") as? String ?? ""
        
        messageType = (dictionary.value(forKey: "message_type") as? Int).map { MessageType(rawValue: $0) } ?? .text
        
        
//        if type == 0 {
//            messageType = .text
//        }
//        else if type == 1 {
//            messageType = .image
//        }
//        else if type == 2 {
//            messageType = .video
//        }
//        else if type == 3 {
//            messageType = .audio
//        }
        file = dictionary.value(forKey: "file") as? String ?? ""
        
        ownerId = dictionary.value(forKey: "owner_id") as? Int ?? 0
        ownerName = dictionary.value(forKey: "owner_name") as? String ?? ""
        ownerProfilePic = dictionary.value(forKey: "owner_profile_pic") as? String ?? ""
        
        receiverId = dictionary.value(forKey: "receiver_id") as? Int ?? 0
        receiverName = dictionary.value(forKey: "receiver_name") as? String ?? ""
        receiverProfilePic = dictionary.value(forKey: "receiver_profile_pic") as? String ?? ""
        receiverSeen = dictionary.value(forKey: "receiver_seen") as? Int ?? 0
        
        thumbnail = dictionary.value(forKey: "thumbnail") as? String ?? ""
        
        feedId = dictionary.value(forKey: "feed_id") as? Int ?? 0
        
         let arrActivity = dictionary["feed_data"] as? NSArray ?? []
         feed_data = FeedData.modelsFromDictionaryArray(array: arrActivity)
        
        print(feed_data.first?.desc)
	}


	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

        dictionary.setValue(self.chatId, forKey: "chat_id")
        dictionary.setValue(self.conversationId, forKey: "conversation_id")
        dictionary.setValue(self.createdDate, forKey: "created_date")
        
        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.messageType, forKey: "message_type")
        dictionary.setValue(self.file, forKey: "file")
        
        dictionary.setValue(self.ownerId, forKey: "owner_id")
        dictionary.setValue(self.ownerName, forKey: "owner_name")
        dictionary.setValue(self.ownerProfilePic, forKey: "owner_profile_pic")
        
        dictionary.setValue(self.receiverId, forKey: "receiver_id")
        dictionary.setValue(self.receiverName, forKey: "receiver_name")
        dictionary.setValue(self.receiverProfilePic, forKey: "receiver_profile_pic")
        dictionary.setValue(self.receiverSeen, forKey: "receiver_seen")
        
        dictionary.setValue(self.thumbnail, forKey: "thumbnail")
        dictionary.setValue(self.feedId, forKey: "feed_id")
        
		return dictionary
	}

    
    func isSender() -> Bool {
        let user = User.loggedInUser()
        if user?.ID == self.ownerId {
           return true
        }
        return false
    }
    
}

class FeedData {
    
    public var feed_id : Int? = 0
    public var desc: String? = ""
    
    var pics : [Pics] = []

    public class func modelsFromDictionaryArray(array:NSArray) -> [FeedData] {
        var models:[FeedData] = []
        for item in array {
            models.append(FeedData(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        feed_id = dictionary["feed_id"] as? Int ?? 0
        desc = dictionary["description"] as? String ?? ""
        
        let arrPics = dictionary["pics"] as? NSArray ?? []
        pics = Pics.modelsFromDictionaryArray(array: arrPics)
        
        
    }

}


class Pics {
    
    public var feed_pics_id : Int? = 0
    public var file_type : Int? = 0
    public var feed_photo: String? = ""
    public var thumb_photo: String? = ""
    public var feed_video: String? = ""
    public var feed_audio: String? = ""
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Pics] {
        var models:[Pics] = []
        for item in array {
            models.append(Pics(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        feed_pics_id = dictionary["feed_pics_id"] as? Int ?? 0
        file_type = dictionary["file_type"] as? Int ?? 0
        feed_photo = dictionary["feed_photo"] as? String ?? ""
        thumb_photo = dictionary["thumb_photo"] as? String ?? ""
        feed_video = dictionary["feed_video"] as? String ?? ""
        feed_audio = dictionary["feed_audio"] as? String ?? ""
    }
    
}




