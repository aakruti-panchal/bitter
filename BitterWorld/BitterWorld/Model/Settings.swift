//
//  Settings.swift
//  BitterWorld
//
//  Created by  " " on 14/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import Foundation
import ObjectMapper

class Settings : Mappable {
    
    lazy var ID                     : Int? = 0
    lazy var Name                   : String? = ""
    lazy var ImagePath              : String? = ""
    //lazy var pushNotificationOn     : Bool? = false
    
    required init(){
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        self.ID                     <- map["ID"]
        self.Name                   <- map["Name"]
        self.ImagePath              <- map["ImagePath"]
        //self.pushNotificationOn     <- map["pushNotificationOn"]
        
        
    }
}


class ServiceResponseArray<T: Mappable>: Mappable {
    
    lazy var IsSuccess      : Bool? = false
    lazy var Code           : String? = ""
    lazy var Message        : String? = ""
    lazy var Data           : [T]? = []
    
    init?() {
        
    }
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        IsSuccess           <- map["IsSuccess"]
        Code                <- map["Code"]
        Message             <- map["Message"]
        Data                <- map["Data"]
        
    }
}
