//
//  Friends.swift
//  BitterWorld
//
//  Created by  " " on 01/05/18.
//  Copyright © 2018  " ". All rights reserved.
//


import Foundation
import ObjectMapper


class Friends: NSObject,Mappable {
    
    lazy var user_id            : Int? = 0
    lazy var first_name         : String? = ""
    lazy var nick_name          : String? = ""
    lazy var use_nickname       : Int? = 0
    lazy var phone              : String? = ""
    lazy var profile_photo      : String? = ""
    lazy var isSelected         : Bool? = false
    lazy var isContactUser      : Int? = 0
    lazy var isFBUser           : Int? = 0
    lazy var totalFeeds         : Int? = 0
    
    lazy var isFriend         : Int? = -1
    lazy var isInvited        : Int? = -1
    lazy var isPending        : Int? = -1
    
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        user_id                 <- map["user_id"]
        first_name              <- map["first_name"]
        nick_name               <- map["nick_name"]
        use_nickname            <- map["use_nickname"]
        phone                   <- map["phone"]
        profile_photo           <- map["profile_photo"]
        isSelected              <- map["isSelected"]
        isContactUser           <- map["is_contact_user"]
        isFBUser                <- map["is_fb_user"]
        totalFeeds              <- map["total_feeds"]
        isFriend                <- map["is_friend"]
        isInvited               <- map["is_invited"]
        isPending               <- map["is_pending"]
    }
}

class ReportPost: Mappable {
    
    //reason_id":1,"label
    lazy var report_id            : Int? = 0
    lazy var report_name         : String? = ""
    lazy var isSelected         : Bool? = false

    required init(){
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        report_id           <- map["reason_id"]
        report_name         <- map["label"]
        isSelected          <- map["isSelected"]

        
    }
    
}

