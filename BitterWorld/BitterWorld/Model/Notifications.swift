//
//  Notifications.swift
//  BitterWorld
//
//  Created by  " " on 15/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import Foundation
import ObjectMapper

class Notifications : Mappable {
    
    lazy var notification_id            : Int? = 0
    lazy var notification_from          : Int? = 0
    lazy var first_name                 : String? = ""
    lazy var nick_name                  : String? = ""
    lazy var use_nickname               : Int? = 0
    lazy var profile_photo              : String? = ""
    lazy var notification_to            : Int? = 0
    lazy var notification_type          : String? = ""
    lazy var notification_text          : String? = ""
    lazy var is_read                    : Int? = 0
    lazy var is_friend                  : Int? = 0
    lazy var is_pending                 : Int? = 0
    lazy var is_invited                 : Int? = 0
    lazy var created_date               : Double? = 0
    lazy var primary_id                 : Int? = 0
    
    required init(){
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        self.notification_id            <- map["notification_id"]
        self.notification_from          <- map["notification_from"]
        self.first_name                 <- map["first_name"]
        self.nick_name                  <- map["nick_name"]
        self.use_nickname               <- map["use_nickname"]
        self.profile_photo              <- map["profile_photo"]
        self.notification_to            <- map["notification_to"]
        self.notification_type          <- map["notification_type"]
        self.notification_text          <- map["notification_text"]
        self.is_read                    <- map["is_read"]
        self.is_friend                  <- map["is_friend"]
        self.is_pending                 <- map["is_pending"]
        self.is_invited                 <- map["is_invited"]
        self.created_date               <- map["created_date"]
        self.primary_id                 <- map["primary_id"]
        
    }
}
