//
//  UserList.swift
//  BitterWorld
//
//  Created by  " " on 30/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class UserList: NSObject ,Mappable{

    var ID           : Int! = 0
    var name         : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["user_id"]
        name <- map["first_name"]
        
    }
}
