//
//  Filter.swift
//  BitterWorld
//
//  Created by  " " on 01/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class SearchParams: Mappable {
    
    lazy var feed_type          : Int? = 0
    lazy var radius             : Int? = 0
    lazy var latitude           : String? = ""
    lazy var longitude          : String? = ""
    lazy var city               : String? = ""
    lazy var category_id        : String? = ""
    lazy var owner_id           : String? = ""
    lazy var keyword            : String? = ""
    lazy var from_date          : String? = ""
    lazy var to_date            : String? = ""
    
    lazy var friends : String? = ""
    lazy var categories : String? = ""
    
    lazy var isCleared : Bool? = false
    
    required init(){
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        feed_type           <- map["feed_type"]
        radius              <- map["radius"]
        latitude            <- map["latitude"]
        longitude           <- map["longitude"]
        city                <- map["city"]
        category_id         <- map["category_id"]
        owner_id            <- map["owner_id"]
        keyword             <- map["keyword"]
        from_date           <- map["from_date"]
        to_date             <- map["to_date"]
        
        friends             <- map["friends"]
        categories          <- map["categories"]
        isCleared          <- map["isCleared"]
        
    }
}





