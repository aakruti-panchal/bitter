
//
//  Feed.swift
//  BitterWorld
//
//  Created by  " " on 24/04/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class Feed: NSObject , Mappable{

    var feedID: Int! = 0
    var userId : Int! = 0
    var firstname : String?
    var profilePhoto : String?
    var categoryId : Int! = 0
    var categoryName : String?
    var title : String?
    var desc : String?
    var rating : Float! = 0.0
    var feedType : Int! = 0
    var city : String?
    var state : String?
    var country : String?
    var location : String?
    var is_reported : Int? = 0
    
    //var latitude: Double! = 0.0
    //var longitude: Double! = 0.0
    var latitude: String?
    var longitude: String?
    var coordinate: CLLocationCoordinate2D?
    var marker = GMSMarker()
    var mediaData: Array<MediaData>! = []
    
    //Comment
    var comments: Array<Comment>! = []
    
    var likeCount : Int = 0
    var commentCount : Int = 0
    var isLike : Bool = false
    var createdDate: Double! = 0.0
    
    var readMore : Bool? = false
    
    required override init(){
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        feedID <- map["feed_id"]
        userId <- map["user_id"]
        mediaData <- map["pics"]
        comments <- map["comments"]
        firstname <- map["first_name"]
        profilePhoto <- map["profile_photo"]
        categoryId <- map["category_id"]
        categoryName <- map["category_title"]
        title <- map["title"]
        desc <- map["description"]
        createdDate <- map["created_date"]
        rating <- map["rating"]
        feedType <- map["feed_type"]
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        location <- map["location"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        is_reported <- map["is_reported"]
        
        commentCount <- map["comment_count"]
        likeCount <- map["like_count"]
        isLike <- map["is_liked"]
        readMore <- map["readMore"]
      
        
        coordinate = CLLocationCoordinate2DMake(Double(latitude ?? "")!, Double(longitude ?? "")!)

        marker.position = coordinate!
        marker.icon = #imageLiteral(resourceName: "Ic_location")
        marker.title = location


        var myData = Dictionary<String, Any>()
        myData["latitude"] = coordinate?.latitude
        myData["longitude"] = coordinate?.longitude
        myData["feedId"] = feedID
        myData["placeName"] = location
        marker.userData = myData
        
    }
}

class MediaData: NSObject, Mappable {
    
    var ID: Int! = 0
    var mediaType: Int! = 0
    var photo: String?
    var videofile: String?
    var videoThumb: String?
    var audio: String?
    var likeCount : Int = 0
    var commentCount : Int = 0
    var isLike : Bool = false
    var duration: Float! = 0.0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["feed_pics_id"]
        mediaType <- map["file_type"]
        photo <- map["feed_photo"]
//        if mediaType == nil {
//            mediaType = .image
//        }
        videoThumb <- map["thumb_photo"]
        videofile <- map["feed_video"]
        audio <- map["feed_audio"]
        likeCount <- map["like_count"]
        commentCount <- map["comment_count"]
        isLike <- map["is_liked"]

    }
    
}
