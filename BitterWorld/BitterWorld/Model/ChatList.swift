//
//  ChatList.swift
//  MapAll
//
//  Created by Darshan V on 13/12/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import ObjectMapper

class ChatList: NSObject,Mappable {

    var chatID                      : Int! = 0
    var conversationID              : Int! = 0
    var createdDate                 : Double! = 0.0
    var message                     : String?
    var receiverID                  : Int! = 0
    var receiverName                : String?
    var receiverProfilePic          : String?
    var receiverSeen                : Int! = 0
    var senderID                    : Int! = 0
    var senderName                  : String?
    var sednerProfilePic          : String?
    var unreadMessageCount          : Int! = 0
    var isMuted                     : Int! = 0
    var isReported                     : Int! = 0
    var isBlocked                     : Int! = 0
    
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        chatID <- map["chat_id"]
        conversationID <- map["conversation_id"]
        createdDate <- map["created_date"]
        message <- map["message"]
        receiverID <- map["receiver_id"]
        receiverName <- map["receiver_name"]
        receiverProfilePic <- map["receiver_profile_pic"]
        receiverSeen <- map["receiver_seen"]
        senderID <- map["owner_id"]
        senderName <- map["owner_name"]
        sednerProfilePic <- map["owner_profile_pic"]
        unreadMessageCount <- map["unread_message_count"]
        isReported <- map ["is_reported"]
        isMuted <- map ["is_muted"]
        //isBlocked <- map ["is_blocked"]
        

    }
    
    
    
    func displayName() -> String {
        if let user = User.loggedInUser(), let rName = self.receiverName, let oName = self.senderName {
            if self.receiverID == user.ID {
                return oName
            }
            else {
                return rName
            }
        }
        return ""
    }
    func displayUserId() -> Int {
        if let user = User.loggedInUser(), let rChatId = self.receiverID, let oChatId = self.senderID {
            if self.receiverID == user.ID {
                return oChatId
            }
            else {
                return rChatId
            }
        }
        return 0
    }
    func displayImage() -> String {
        if let user = User.loggedInUser(), let rImage = self.receiverProfilePic, let oImage = self.sednerProfilePic {
            if self.receiverID == user.ID {
                return oImage
            }
            else {
                return rImage
            }
        }
        return ""
    }
    
}
