//
//  Comment.swift
//  BitterWorld
//
//  Created by  " " on 09/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

enum StatusType : Int {
    case ACCEPT   = 1
    case REJECT    = 2
}

class Comment : Mappable {

    lazy var feed_comment_id            : Int? = 0
    lazy var user_id                    : Int? = 0
    lazy var comment_text               : String? = ""
    lazy var first_name                 : String? = ""
    lazy var nick_name                  : String? = ""
    lazy var use_nickname               : String? = ""
    lazy var profile_photo              : String? = ""
    lazy var created_date               : Double! = 0.0
    lazy var comment_like_count         : Int? = 0
    lazy var is_friend                  : Bool? = false
    lazy var is_liked                   : Bool? = false
    
    required init(){
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        self.feed_comment_id        <- map["feed_comment_id"]
        self.user_id                <- map["user_id"]
        self.comment_text           <- map["comment_text"]
        self.first_name             <- map["first_name"]
        self.nick_name              <- map["nick_name"]
        self.use_nickname           <- map["use_nickname"]
        self.profile_photo          <- map["profile_photo"]
        self.created_date           <- map["created_date"]
        self.comment_like_count     <- map["comment_like_count"]
        self.is_friend              <- map["is_friend"]
        self.is_liked               <- map["is_liked"]
        
    }
}
