//
//  Cities.swift
//  BitterWorld
//
//  Created by  " " on 01/05/18.
//  Copyright © 2018  " ". All rights reserved.
//

import UIKit
import ObjectMapper

class Cities: NSObject,Mappable {
    
    lazy var city           : String? = ""
    lazy var latitude       : String? = ""
    lazy var longitude      : String? = ""
    lazy var state          : String? = ""
    lazy var country        : String? = ""
    lazy var isSelected     : Bool = false
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        city            <- map["city"]
        latitude        <- map["latitude"]
        longitude       <- map["longitude"]
        state           <- map["state"]
        country         <- map["countr"]
        isSelected      <- map["isSelected"]
    }
}


