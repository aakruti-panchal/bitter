//
//  User.swift
//
//
//  Created by Harry on 22/05/17.
//
//

import UIKit
import ObjectMapper

private let kUserID             = "user_id"
private let KNickName           = "nick_name"
private let kFullName           = "first_name"
private let kEmail              = "email"
private let kProfilePic         = "profile_photo"

private let kSocialType         = "social_type"
private let kSocialID           = "social_id"

private let kCreatedDate        = "created_date"
private let kDOB                = "dob"
private let kPhone              = "phone"

private let kUserDeviceToken    = "device_token"
private let kUserDeviceType     = "device_type"
private let kUserServiceToken   = "service_token"
private let kUserPushnotification = "is_pushnotification"

private let kgender             = "gender"
private let kisUseSignature     = "use_signature"
private let kAddress            = "address"
private let kcountryCodePhone    = "countrycode_phone"

private let kCity               = "city"
private let kState              = "state"
private let kCountry            = "country"


class User: NSObject, NSCoding {

    var ID                  : Int! = 0
    var userName            : String?
    var fullName            : String?
    var email               : String?
    var profilePic          : String?
    var socialType          : String?
    var socialID            : String?
    
    var createdDate         : String?
    var dob                 : String?
    var phone               : String?
    
    var deviceToken         : String! = ""
    var deviceType          : String! = "ios"
    var serviceToken        : String! = ""
    //var userType            : String! = ""
    var isPushNotification : Int! = 0
    
    var gender              : Int! = 0
    var useSignature        : Int! = 0
    var address             : String! = ""
    var countryCodePhone    : String! = ""
    
    var city                : String! = ""
    var state               : String! = ""
    var country             : String! = ""
    


    public init(dict : [String : Any]) {
        self.ID                 = dict[kUserID] as? Int
        self.userName           = dict[KNickName] as? String
        self.fullName           = dict[kFullName] as? String
        self.email              = dict[kEmail] as? String
        self.profilePic         = dict[kProfilePic] as? String
        self.socialType         = dict[kSocialType] as? String
        self.socialID           = dict[kSocialID] as? String
        self.createdDate        =  dict[kCreatedDate] as? String
        self.dob                =  dict[kDOB] as? String
        self.phone              =  dict[kPhone] as? String
        self.deviceType         =  dict[kUserDeviceToken] as? String
        self.serviceToken       =  dict[kUserServiceToken] as? String
        //self.userType           = dict[kUserType] as? String
        self.isPushNotification  = dict[kUserPushnotification] as? Int
        
        self.gender             = dict[kgender] as? Int
        self.useSignature       = dict[kisUseSignature] as? Int
        self.address            =  dict[kAddress] as? String
        self.countryCodePhone   =  dict[kcountryCodePhone] as? String
        
        self.city               =  dict[kCity] as? String ?? ""
        self.state              =  dict[kState] as? String ?? ""
        self.country            =  dict[kCountry] as? String ?? ""
        
        
    }

    //required public
    required init?(coder aDecoder: NSCoder) {
        
        self.ID                = aDecoder.decodeObject(forKey: kUserID) as? Int ?? 0
        self.userName          = aDecoder.decodeObject(forKey: KNickName) as? String
        self.fullName          = aDecoder.decodeObject(forKey: kFullName) as? String
        self.email             = aDecoder.decodeObject(forKey: kEmail) as? String
        self.profilePic        = aDecoder.decodeObject(forKey: kProfilePic) as? String
        self.socialType        = aDecoder.decodeObject(forKey: kSocialType) as? String
        self.socialID          = aDecoder.decodeObject(forKey: kSocialID) as? String
        
        self.createdDate       = aDecoder.decodeObject(forKey:kCreatedDate) as? String
        self.dob               = aDecoder.decodeObject(forKey:kDOB) as? String
        self.phone             = aDecoder.decodeObject(forKey:kPhone) as? String
        
        self.serviceToken      = aDecoder.decodeObject(forKey: kUserServiceToken) as? String
        self.deviceToken       = aDecoder.decodeObject(forKey: kUserDeviceType) as? String
        self.deviceType        = aDecoder.decodeObject(forKey: kUserDeviceType) as? String
       // self.userType          = aDecoder.decodeObject(forKey: kUserType) as? String
       
        self.isPushNotification           = aDecoder.decodeObject(forKey: kUserPushnotification) as? Int ?? 0
        
        self.gender             = aDecoder.decodeObject(forKey: kgender) as? Int ?? 0
        self.useSignature       = aDecoder.decodeObject(forKey: kisUseSignature) as? Int ?? 0
        self.address            = aDecoder.decodeObject(forKey: kAddress) as? String
        self.countryCodePhone   = aDecoder.decodeObject(forKey: kcountryCodePhone) as? String
        
        
        self.city   = aDecoder.decodeObject(forKey: kCity) as? String ?? ""
        self.state   = aDecoder.decodeObject(forKey: kState) as? String ?? ""
        self.country   = aDecoder.decodeObject(forKey: kCountry) as? String ?? ""
        
    }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.ID, forKey: kUserID)
        aCoder.encode(self.userName, forKey: KNickName)
        aCoder.encode(self.fullName, forKey: kFullName)
        aCoder.encode(self.email, forKey: kEmail)
        aCoder.encode(self.profilePic, forKey: kProfilePic)
        aCoder.encode(self.socialType, forKey: kSocialType)
        aCoder.encode(self.socialID, forKey: kSocialID)
        aCoder.encode(self.serviceToken, forKey: kUserServiceToken)
        //aCoder.encode(self.userType, forKey: kUserType)
        aCoder.encode(self.deviceToken, forKey: kUserDeviceToken)
        aCoder.encode(self.deviceType, forKey: kUserDeviceType)
        aCoder.encode(self.createdDate,forKey:kCreatedDate)
        aCoder.encode(self.dob,forKey:kDOB)
        aCoder.encode(self.phone,forKey:kPhone)
        aCoder.encode(self.isPushNotification, forKey: kUserPushnotification)
        
        aCoder.encode(self.gender, forKey: kgender)
        aCoder.encode(self.useSignature, forKey: kisUseSignature)
        aCoder.encode(self.address, forKey: kAddress)
        aCoder.encode(self.countryCodePhone, forKey: kcountryCodePhone)
        
        
        aCoder.encode(self.city, forKey: kCity)
        aCoder.encode(self.state, forKey: kState)
        aCoder.encode(self.country, forKey: kCountry)
        
    }

    func save() -> Void {
        StandardUserDefaults.setCustomObject(obj: self, key: K.Key.LoggedInUser)
    }
    
    class func delete() -> Void {
        StandardUserDefaults.removeObject(forKey: K.Key.LoggedInUser)
        StandardUserDefaults.synchronize()
    }
    
    public static func loggedInUser() -> User? {
        let user  = StandardUserDefaults.getCustomObject(key: K.Key.LoggedInUser) as? User
        return user
    }
}
