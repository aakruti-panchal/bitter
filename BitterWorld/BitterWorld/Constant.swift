//
//  Constant.swift
//
//  Created by Henryp on 15/11/16.
//
//

import UIKit
import Foundation

let appDelegate = UIApplication.shared.delegate as! AppDelegate


public let BASE_URL     = "http://8734875646574657"



struct K {
    static let TWITTER_CONSUMER_KEY     = "hajhdfhBNjuyqtwuyDGT" // Client Account
    static let TWITTER_CONSUMER_SECRET     = "lyRTbwFDeNcUOuAe77dcfpJkE1Jcni7beAP1qLYmXPpwt41d4v" // client Account

    static let FACEBOOK_KEY     = "91823987498347983" // Client Acccount
    static let ITUNE_APP_ID     = ""
    
   
    static let GOOGLE_API_KEY   = "ABiSDT-yudmt5qDREiek1xyuiaxpjeNzPoZZXA" //Client Account

    
    static let GoogleMapDefaultZoomLevel : Float = 12.0
    static let GoogleMapDefaultZoomLevelForLocationPicker : Float = 18.0
    
    static let HugeIntValue : Int = 99999
    struct MediaDir {
        static let PROFILE_PHOTO            = "profile_photo"
        static let ACTIVITY_PHOTO           = "activity_photo"
        static let CHAT_PHOTO               = "chat"
        static let TYPE_PHOTO               = "type_photo"
        static let FEED_SOUND               = "feed_sound"
        static let FEED_VIDEO               = "feed_video"
        static let THUMB_PHOTO              = "thumb_photo"
        static let SOUND_FILE               = "sound_file"
    }
    
    //struct: Contains all the URLs
    struct URL {
        
        static let ITUNE_APP_URL                 = "itms-apps://itunes.apple.com/app/id\(K.ITUNE_APP_ID)"
        static let SHARE_APP_URL                = "http://www.google.com"
        static let LOGIN                        = BASE_URL + "user/login"
        static let SIGN_UP                      = BASE_URL + "user/signup"
        static let FORGOT_PASSWORD              = BASE_URL + "user/forgot-password"
        static let CHANGE_PASSWORD              = BASE_URL + "user/change-password"
        static let UPLOAD_MEDIA                 = BASE_URL + "master/upload-commonfile"
        static let PRE_SIGNUP                   = BASE_URL + "user/validate-element"
        
        static let POST_FEED                    = BASE_URL + "feed/post-feed"
        
        static let GET_CATEGORY                 = BASE_URL + "master/get-categories"
        static let GET_FRIENDS                  = BASE_URL + "user/get-friends"
        static let GET_CITIES                   = BASE_URL + "feed/get-cities"
        static let GET_FEED_DETAIL              = BASE_URL + "feed/get-feed-detail"
        
        static let GET_FEED                     = BASE_URL + "feed/get-feeds"
        static let LIKE_DISLIKE                 = BASE_URL + "feed/like-dislike"
        static let GET_USERS_LIST               = BASE_URL + "user/get-users"
        
        static let SYNC_CONTACT                 = BASE_URL + "user/sync-contact"
        static let GET_PENDING_REQUEST          = BASE_URL + "user/get-pending-req"
        static let GET_INVITED_REQUEST          = BASE_URL + "user/get-invited-req"
        
        
        //by rane
        static let ADD_FRIEND                   = BASE_URL + "user/add-friend" // for sent friend request
        static let ACCEPT_REJECT                = BASE_URL + "user/accept-reject-request" //Accept reject friend request
        static let UNFRIEND                     = BASE_URL + "user/un-friend" // unfriend the friend
        static let CANCEL_REQUEST               = BASE_URL + "user/cancel-request" //cancel request you have sent
        static let BLOCK_UNBLOCK_USER           = BASE_URL + "user/block-unblock" //block unblock user
        
        static let SEND_TO              = BASE_URL + "feed/send-feed"
        
        static let GET_REPORT_REASON            = BASE_URL + "master/get-reporting-reason"
        static let REPORT_POST                  = BASE_URL + "feed/report-feed"
        static let GET_MASTERS_LIST             = BASE_URL + "master/get-master"


        static let INVITE_USER                  = BASE_URL + "friend/invite-user"
        static let CHANGE_NOTIFICATION          = BASE_URL + "user/change-settings"
        static let GET_CMS                      = BASE_URL + "master/get-cms"
        static let NOTIFICATION_LIST            = BASE_URL + "master/get-notifications"
        static let UPDATE_TOKEN                 = BASE_URL + "user/update-token"
        
        static let LOGOUT                       = BASE_URL + "user/logout"
        
        static let GET_PROFILE_DATA             = BASE_URL + "feed/get-profile-data"
        static let UPDATE_PROFILE               = BASE_URL + "user/update-profile"

        static let GET_COMMENT_LIST             = BASE_URL + "feed/get-comments"
        static let ADD_COMMENT              = BASE_URL + "feed/add-comment"

        //chat
        
        static let GET_CONVERSATIONS            = BASE_URL + "chat/get-conversations"
        static let ADD_MESSAGE                  = BASE_URL + "chat/add-message"
        static let GET_CHAT_LIST                = BASE_URL + "chat/get-chat-list"
        static let DELETE_CHAT_MESSAGE          = BASE_URL + "chat/delete-chat"
        static let USER_ACTION                 = BASE_URL + "user/user-actions"
        
        //Settings
        static let UPDATE_NOTIFICATION          = BASE_URL + "user/update-notification-setting"
        
        
        //notification
        static let GET_NOTIFICATIONLIST         = BASE_URL + "master/get-notification"
        static let DELETE_NOTIFICATION          = BASE_URL + "master/remove-notification"
    }
        
    struct Notification {
        static let CartItemCountDidChange         = "CartItemCountDidChangeNotification"
    }
    
    //struct: Contains All notification types
    struct NotificationType {
        static let NewRequest                   = ""
    }

    //struct: Contains Keys
    struct Key {
        
        static let Data                         = "data"
        static let LoggedInUser                 = "LoggedInUserKey"
        static let DeviceToken                  = "DeviceToken"
        static let RecentSearchString           = "RecentSearchKey"
        
        static let ImageIdkey                   = "picsIdsKey"
        static let postImagekey                 = "imageKey"
        static let videoUrlkey                  = "videoUrlKey"
        static let audioUrlkey                  = "audioUrlKey"
        static let mediaTypekey                 = "mediaTypeKey"
        static let discoveryMediaDuration       = "mediaDurationKey"
        static let isUploadedKey                = "mediaIsUploaded"

        static let CMS_TERMS_POLICY             = "terms"
        static let CMS_PRIVACY_POLICY           = "privacy"
        static let CMS_APP_WORK                 = "howitworks"

    }
    
    //struct: Contains All the messages shown to user
    struct Message {
        static let OK                           = "OK"
        static let Cancel                       = "Cancel"
        static let YES                          = "Yes"
        static let NO                           = "No"
        
        static let enterYourName                        = "Please enter full name"
        static let enterUsername                        = "Please enter nick name"
        static let enterPassword                        = "Please enter password"
        static let enterEmail                           = "Please enter email address"
        static let enterValidEmail                      = "Please enter valid email address"
        static let enterPhone                           = "Please enter valid mobile number"
        static let enter10DigitNumber                   = "Please enter valid phone number"

        static let enterPhoneorEmail                    = "Please enter email or phone number"
        static let enterConfirmPassword                 = "Please enter confirm password"
        static let passwordDoesNotMatched               = "Password and confirm password doesn't match"
        static let passwordMinLength                    = "Password should be at least 6 characters long"
        static let agreeTermsOfService                  = "Please agree terms of use"
        static let chooseyourintrest                    = "Please choose your interest"
        static let cantGenerateThumb                    = "Can't generate thumb image of recorded video"
        static let selectLocation                       = "Please select location"
        static let enterDescription                     = "Please enter description"
        static let selectMedia                          = "Please select valid media"
        
        static let weNeedPermission                     = "We need permission for the camera and microphone."
        
    }
    
    //struct: Contains Color used in application
    struct Color {
        static let white                   = UIColor(hexString: "FFFFFF")!
        static let yellow                  = UIColor(hexString: "EBB100")!
        static let green                   = UIColor(hexString: "485654")!
        static let black                   = UIColor(hexString: "000000")!
        static let darkGray                = UIColor(hexString: "2E2E2E")!
        static let gray                    = UIColor(hexString: "7B7B7B")!
        static let lightGray               = UIColor(hexString: "C1C1C1")!
        static let extraLightGray          = UIColor(hexString: "DDDDDD")!
        
        static let termsCondition          = UIColor(hexString: "E6E6E6")!
        static let shadowColor             = UIColor(hexString: "EDEDED")!
        static let postbuttonshadowColor   = UIColor(hexString: "A0A0A0")!
        static let lightTextColor          = UIColor(hexString: "777777")!
        
        static let friendBoxShadow          = UIColor(hexString: "E3E3E3")!
        

        static let commentText          = UIColor(hexString: "333333")!
        static let commentTime          = UIColor(hexString: "A9A9A9")!
        
        static let GrayTone2               = UIColor(hexString: "DDDDDD")!
        
        static let Dark_Blue        = UIColor(red: 62/255, green: 75/255, blue: 94/255 , alpha :1)
        //General Light Color
        static let Light_Grey       = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 0.5)
        //General Dark Color
        static let Dark_Pink        = UIColor(red: 255/255, green: 51/255, blue: 102/255, alpha: 1.0)
        static let Dark_Green       =  UIColor(red: 68/255, green: 175/255, blue: 86/255 , alpha :1.0)
        static let Dark_Orange      =  UIColor(red: 209/255, green: 92/255, blue: 50/255, alpha: 1.0)
        static let Dark_Gray        = UIColor(red: 67/255, green: 65/255, blue: 73/255 , alpha :1)
        static let Red              = UIColor.red
        static let Green            = UIColor(red: 41/255, green: 169/255, blue: 88/255, alpha: 1.0)
        static let FaceBook         = UIColor(red: 79/255, green: 95/255, blue: 157/255, alpha: 1.0)
        static let GooglePlus       = UIColor(red: 184/255, green: 55/255, blue: 32/255, alpha: 1.0)
        static let LinkedIn         = UIColor(red: 70/255, green: 115/255, blue: 178/255, alpha: 1.0)
        static let Pinterest        = UIColor(red: 161/255, green: 44/255, blue: 33/255, alpha: 1.0)
        static let Twitter          = UIColor(red: 101/255, green: 157/255, blue: 240/255, alpha: 1.0)
        static let Instagram        = UIColor(red: 198/255, green: 104/255, blue: 80/255, alpha: 1.0)
        
        static var AppTheme_Primary         = UIColor(red: 255/255, green: 51/255, blue: 102/255, alpha: 1.0)
    }
    
    
    //struct: Contains all date and time format
    
    struct DateFormat {
        static let common                   = "MM/dd/yyyy"
        static let birthDate                = "MM/dd/yyyy"
        static let yyyyMMdd                 = "yyyy-MM-dd"
        static let time                     = "h:mm a"
        static let time24Hours              = "H:mm"
         static let DiscoveryTime           = "MM/dd/yyyy 'at' hh:mm a"
        static let NotificationTime         = "dd MMM''yy | hh:mm a"
        
        static let feedCreate               = "MMM dd','yyyy"
        static let NotificationDateTime     = "MMM dd','yyyy '|' hh:mm a"
        
    }
    
    struct ImageFolderNameOnServer {
        static let ProfilePhoto     = "profile_photo"
    }
    
    public static var imagesFolderPath: String {
        return SF.documentsPath + "/Images"
    }
    
    public static func imagePath(imageName : String!) -> String {
        return  K.imagesFolderPath + "/" + imageName
    }
    
    public static var topBarHeight : CGFloat! {
        return Display.typeIsLike == .iphoneX ? 84.0 : 64.0
    }
    
}



enum NotificationType : Int {
    case FriendRequest    = 1
    case AcceptFriendRequest    = 2
    case ChatMessage    = 3
    case VisitPlace = 4
    case FavoritePlace = 5
    case CreateDiscovery = 6
    case WishList = 7
}


enum FilterUserTypes : Int {
    case me         = 1
    case friends    = 2
    case others     = 3
}

enum FilterRatings : Int {
    case one     = 1
    case two     = 2
    case three   = 3
    case four    = 4
    case five    = 5
}

enum PostMediaType : String {
    case image    = "image"
    case video    = "video"
    case audio    = "audio"
}

enum MediaType : Int {
    case image    = 1
    case video    = 2
    case audio    = 3
}

enum DiscoveryPrivacyType : String {
    case everyone   = "everyone"
    case friends    = "friends"
    case noOne      = "no_one"
}

public enum DisplayType {
    case unknown
    case iphone4
    case iphone5
    case iphone6
    case iphone6plus
    static let iphone7 = iphone6
    static let iphone7plus = iphone6plus
    static let iphone8 = iphone6
    static let iphone8plus = iphone6plus
    case iphoneX
}

public final class Display {
    class var width:CGFloat { return UIScreen.main.bounds.size.width }
    class var height:CGFloat { return UIScreen.main.bounds.size.height }
    class var maxLength:CGFloat { return max(width, height) }
    class var minLength:CGFloat { return min(width, height) }
    class var zoomed:Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina:Bool { return UIScreen.main.scale >= 2.0 }
    class var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    class var carplay:Bool { return UIDevice.current.userInterfaceIdiom == .carPlay }
    class var tv:Bool { return UIDevice.current.userInterfaceIdiom == .tv }
    class var typeIsLike:DisplayType {
        if phone && maxLength < 568 {
            return .iphone4
        }
        else if phone && maxLength == 568 {
            return .iphone5
        }
        else if phone && maxLength == 667 {
            return .iphone6
        }
        else if phone && maxLength == 736 {
            return .iphone6plus
        }
        else if phone && maxLength == 812 {
            return .iphoneX
        }
        return .unknown
    }
}

enum SocialMediaType : Int {
    case facebook = 1
    case twitter = 2
    case google = 3
    
}

struct FontStyle {
    static let Regular      = "-Regular"
    static let Bold         = "-Bold"
    static let Medium       = "-Medium"
    static let Light        = "-Light"
    static let SemiBold     = "-SemiBold"
    static let Italic       = "-Italic"
}

struct WidthStatic {
    static let SurveySubmitButtonWidth:CGFloat = 200
}


struct SettingsMenuList {
    
    // ,\"Comments\":\"\"
    static let SettingsList = "{\"Data\":[{\"ID\":0,\"Name\":\"Push Notification\",\"ImagePath\":\"menu_profile\"},{\"ID\":1,\"Name\":\"Change Password\",\"ImagePath\":\"menu_lock\"},{\"ID\":2,\"Name\":\"Invite Friends\",\"ImagePath\":\"menu_share\"},{\"ID\":3,\"Name\":\"Terms of Use\",\"ImagePath\":\"menu_rate\",},{\"ID\":4,\"Name\":\"Privacy Policy\",\"ImagePath\":\"menu_notification\"},{\"ID\":5,\"Name\":\"Rate App\",\"ImagePath\":\"menu_terms\"}]}"
}

struct Links {
    static let rateusappstorelink = "https://itunes.apple.com/us/app/inoreader-news-app-rss/id892355414?mt=8"
}

struct AppMessage {
    static let LogoutMessage = "Are you sure you want to logout?"
}

struct ButtonTitle {
    
    //Button Title
    static let btnRetry = "Retry"
    static let btnOk = "OK"
    static let btnLogout = "Logout"
    static let btnCancel = "Cancel"
}
struct AlertTitle {
    
    //Error Title
    static let Error                        = "Error"
    static let Warning                      = "Warning"
    static let Success                      = "Success"
    static let RemoveProduct                = "Remove Product"
    static let RemoveProductSubTitle        = "Are you sure you want to remove this product from your cart?"
}

