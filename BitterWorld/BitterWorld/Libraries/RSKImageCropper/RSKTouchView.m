//
// RSKTouchView.m
//

#import "RSKTouchView.h"

@implementation RSKTouchView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
	if ([self pointInside:point withEvent:event]) {
		return self.receiver;
	}
	return nil;
}

@end
