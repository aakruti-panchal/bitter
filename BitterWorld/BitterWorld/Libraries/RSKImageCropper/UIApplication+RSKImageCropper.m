//
// UIApplication+RSKImageCropper.m
//


#import "UIApplication+RSKImageCropper.h"
#import <objc/runtime.h>

static IMP rsk_sharedApplicationOriginalImplementation;

@implementation UIApplication (RSKImageCropper)

+ (void)load
{
    // When you build an extension based on an Xcode template, you get an extension bundle that ends in .appex.
    // https://developer.apple.com/library/ios/documentation/General/Conceptual/ExtensibilityPG/ExtensionCreation.html
    if (![[[NSBundle mainBundle] bundlePath] hasSuffix:@".appex"]) {
        Method sharedApplicationMethod = class_getClassMethod([UIApplication class], @selector(sharedApplication));
        if (sharedApplicationMethod != NULL) {
            IMP sharedApplicationMethodImplementation = method_getImplementation(sharedApplicationMethod);
            Method rsk_sharedApplicationMethod = class_getClassMethod([UIApplication class], @selector(rsk_sharedApplication));
            rsk_sharedApplicationOriginalImplementation = method_setImplementation(rsk_sharedApplicationMethod, sharedApplicationMethodImplementation);
        }
    }
}

+ (UIApplication *)rsk_sharedApplication
{
    return nil;
}

+ (IMP)rsk_sharedApplicationOriginalImplementaion
{
    return rsk_sharedApplicationOriginalImplementation;
}

@end
