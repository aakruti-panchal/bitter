//
// RSKImageCropViewController+Protected.h
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 The methods in the RSKImageCropViewControllerProtectedMethods category
 typically should only be called by subclasses which are implementing new
 image crop view controllers. They may be overridden but must call super.
 */
@interface RSKImageCropViewController (RSKImageCropViewControllerProtectedMethods)

/**
 Asynchronously crops the original image in accordance with the current settings and tells the delegate that the original image will be / has been cropped.
 */
- (void)cropImage;

/**
 Tells the delegate that the crop has been canceled.
 */
- (void)cancelCrop;

/**
 Resets the rotation angle, the position and the zoom scale of the original image to the default values.
 
 @param animated Set this value to YES to animate the reset.
 */
- (void)reset:(BOOL)animated;

/**
 Sets the current rotation angle of the image in radians.
 
 @param rotationAngle The rotation angle of the image in radians.
 */
- (void)setRotationAngle:(CGFloat)rotationAngle;

/**
 Sets the current scale factor for the image.
 
 @param zoomScale The scale factor for the image.
 */
- (void)setZoomScale:(CGFloat)zoomScale;

@end

NS_ASSUME_NONNULL_END
