/*
     File: RSKImageScrollView.h
 
 */

#import <UIKit/UIKit.h>

@interface RSKImageScrollView : UIScrollView

@property (nonatomic, strong) UIImageView *zoomView;
@property (nonatomic, assign) BOOL aspectFill;

- (void)displayImage:(UIImage *)image;

@end
