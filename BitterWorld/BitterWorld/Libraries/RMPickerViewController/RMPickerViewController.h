//
//  RMPickerViewController.h
//  RMPickerViewController
//
//
//

#import "RMActionController.h"


@interface RMPickerViewController : RMActionController <UIPickerView *>

@property (nonatomic, readonly) UIPickerView *pickerView;

@property (nonatomic, retain) NSArray *arrItems;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, retain) id selectedValue;

@property (nonatomic, retain) id customObject;
@property (nonatomic, retain) NSString * displayValueKey;

+ (void)showPickerViewControllerWithTitle:(NSString *)strTitle selectedIndex:(NSInteger)index array:(NSArray<NSString *> *)arrStrings doneBlock:(void(^)(NSInteger selectedIndex, id selectedValue))doneBlock cancelBlock:(void(^)(void))cancelBlock;

+ (void)showPickerViewControllerWithTitle:(NSString *)strTitle selectedIndex:(NSInteger)index array:(NSArray*)arrObjects objectClass:(id)object displayValueKey:(NSString *)key doneBlock:(void(^)(NSInteger selectedIndex, id selectedValue))doneBlock cancelBlock:(void(^)(void))cancelBlock;

@end
