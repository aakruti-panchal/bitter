//
//  WebClient.h
//
//  Created by on 20/05/15.
//
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"

#define kObjcInternetConnectionMessage  @"Please check your internet connection"
#define kObjcTryAgainLaterMessage       @"Please try again later"
#define kObjcErrorDomain                @"Error"
#define kObjcDefaultErrorCode           1234

#define kObjcTokenKey                   @"token"
#define kObjcTokenValue                 @"albycfdeeofgfhui"

#define kObjcSuccessKey                 @"status"
#define kObjcMessageKey                 @"message"
#define kObjcDataKey                    @"data"


#define kObjcWebClient [WebClientObjc sharedInstance]

typedef void (^requestCompletionBlock)(id responseObject, NSError *error);
typedef void (^downloadFileCompletionBlock)(NSURL *filePath, NSData *fileData, NSError *error);

@interface WebClientObjc : NSObject {
    
}

@property (strong, nonatomic) NSURLSessionDataTask *dataTask;

+ (WebClientObjc *)sharedInstance;
+ (AFURLSessionManager *)sessionManager;

+ (NSURLSessionDataTask *)request:(NSMutableURLRequest *)urlRequest uploadProgress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock downloadProgress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock;

+ (NSURLSessionDataTask *)requestWithUrl:(NSString *)strurl requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock;

+ (NSURLSessionDataTask *)requestWithURL:(NSString *)strUrl parameters:(NSDictionary *)parameters requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock;

+ (NSURLSessionDataTask *)multiPartRequestWithURL:(NSString *)strUrl parameters:(NSDictionary *)parameters constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))formData progress:(void (^)(NSProgress *progress))progressBlock requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock;

+ (NSURLSessionDownloadTask *)downloadFile:(NSString *)strUrl downloadProgress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock downloadFileCompletionBlock:(downloadFileCompletionBlock)downloadFileCompletionBlock;

+ (NSURLSessionDownloadTask *)downloadFile:(NSString *)strUrl atPath:(NSString *)strPath downloadProgress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock downloadFileCompletionBlock:(downloadFileCompletionBlock)downloadFileCompletionBlock;

+ (NSURLSessionDownloadTask *)downloadFile:(NSString *)strUrl atPath:(NSString *)strPath canSaveFile:(BOOL)canSave downloadProgress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock downloadFileCompletionBlock:(downloadFileCompletionBlock)downloadFileCompletionBlock;

+ (void)downloadImage:(NSString *)strUrl requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock;

@property (readwrite, nonatomic, strong, setter = af_setActiveImageDownloadReceipt:) AFImageDownloadReceipt *af_activeImageDownloadReceipt;

@end
