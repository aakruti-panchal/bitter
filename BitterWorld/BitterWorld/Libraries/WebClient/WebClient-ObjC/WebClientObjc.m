//
//  WebClientObjc.m
//
//  Created on 20/05/15.
//
//

#import <objc/runtime.h>
#import "WebClientObjc.h"

@implementation WebClientObjc

#pragma mark - Get Response From URL

+ (instancetype)sharedInstance {
    static WebClientObjc *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (AFURLSessionManager *)sessionManager {
    static AFURLSessionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sharedInstance = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    });
    return sharedInstance;
}

+ (NSURLSessionDataTask *)request:(NSMutableURLRequest *)urlRequest uploadProgress:(void (^)(NSProgress *uploadProgress)) uploadProgressBlock downloadProgress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock {
    if(!isInternetConnected()) {
        if(requestCompletionBlock) {
            requestCompletionBlock(nil, [NSError errorWithDomain:kObjcErrorDomain code:kObjcDefaultErrorCode userInfo:@{NSLocalizedDescriptionKey : kObjcInternetConnectionMessage}]);
        }
        return nil;
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLSessionDataTask *dataTask = [[self sessionManager] dataTaskWithRequest:urlRequest uploadProgress:^(NSProgress *uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(uploadProgressBlock) {
                uploadProgressBlock(uploadProgress);
            }
        });
    } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(downloadProgressBlock) {
                downloadProgressBlock(downloadProgress);
            }
        });
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if(requestCompletionBlock) {
            requestCompletionBlock(responseObject,error);
        }
        
    }];
    [dataTask resume];
    return dataTask;
}

#pragma mark -

+ (NSURLSessionDataTask *)requestWithUrl:(NSString *)strurl requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock {
    NSURL *url = [NSURL URLWithString:strurl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    return [self request:request uploadProgress:nil downloadProgress:nil requestCompletionBlock:requestCompletionBlock];
}


+ (NSURLSessionDataTask *)requestWithURL:(NSString *)strUrl parameters:(NSDictionary *)parameters requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock {
    NSMutableDictionary *dictParameters = [parameters mutableCopy];
    if(!dictParameters) {
        dictParameters = [NSMutableDictionary dictionary];
    }
    //[dictParameters setObject:kObjcTokenValue forKey:kObjcTokenKey];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:strUrl parameters:dictParameters error:nil];
    return [self request:request uploadProgress:nil downloadProgress:nil requestCompletionBlock:^(id responseObject, NSError *error) {
        [self handleResponse:responseObject error:error requestCompletionBlock:requestCompletionBlock];
    }];
}

#pragma mark -

+ (NSURLSessionDataTask *)multiPartRequestWithURL:(NSString *)strUrl parameters:(NSDictionary *)parameters constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))formData progress:(void (^)(NSProgress *progress))progressBlock requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock {
    NSMutableDictionary *dictParameters = [parameters mutableCopy];
    if(!dictParameters) {
        dictParameters = [NSMutableDictionary dictionary];
    }
    //[dictParameters setObject:kObjcTokenValue forKey:kObjcTokenKey];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:strUrl parameters:dictParameters constructingBodyWithBlock:formData error:nil];
    [request setTimeoutInterval:3600];

    NSURLSessionDataTask *dataTask = [self request:request uploadProgress:^(NSProgress *uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(progressBlock) {
                progressBlock(uploadProgress);
            }
        });
    } downloadProgress:nil requestCompletionBlock:^(id responseObject, NSError *error) {
        [self handleResponse:responseObject error:error requestCompletionBlock:requestCompletionBlock];
    }];
    [dataTask resume];
    return dataTask;
}

#pragma mark -

+ (void)handleResponse:(id)responseObject error:(NSError *)error requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock {
    if(error || ![responseObject isKindOfClass:[NSDictionary class]]) {
        if(error.code != kObjcDefaultErrorCode) {
            error = [NSError errorWithDomain:kObjcErrorDomain code:kObjcDefaultErrorCode userInfo:@{NSLocalizedDescriptionKey : kObjcTryAgainLaterMessage}];
        }
        if(requestCompletionBlock) {
            requestCompletionBlock(responseObject, error);
        }        
    }
    else {
        BOOL success = [[responseObject objectForKey:kObjcSuccessKey] boolValue];
        if(success) {
            if(requestCompletionBlock) {
                requestCompletionBlock(responseObject, error);
            }
        }
        else {
            if(requestCompletionBlock) {
                requestCompletionBlock(nil, [NSError errorWithDomain:kObjcErrorDomain code:[[responseObject objectForKey:kObjcSuccessKey] integerValue] userInfo:@{NSLocalizedDescriptionKey : [responseObject objectForKey:kObjcMessageKey]}]);
            }
        }
    }
}

#pragma mark - Download File

+ (NSURLSessionDownloadTask *)downloadFile:(NSString *)strUrl downloadProgress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock downloadFileCompletionBlock:(downloadFileCompletionBlock)downloadFileCompletionBlock {
    return [self downloadFile:strUrl atPath:nil canSaveFile:FALSE downloadProgress:downloadProgressBlock downloadFileCompletionBlock:downloadFileCompletionBlock];
}

+ (NSURLSessionDownloadTask *)downloadFile:(NSString *)strUrl atPath:(NSString *)strPath downloadProgress:(void (^)(NSProgress *downloadProgress)) downloadProgressBlock downloadFileCompletionBlock:(downloadFileCompletionBlock)downloadFileCompletionBlock {
    return [self downloadFile:strUrl atPath:strPath canSaveFile:TRUE downloadProgress:downloadProgressBlock downloadFileCompletionBlock:downloadFileCompletionBlock];
}

+ (NSURLSessionDownloadTask *)downloadFile:(NSString *)strUrl atPath:(NSString *)strPath canSaveFile:(BOOL)canSave downloadProgress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock downloadFileCompletionBlock:(downloadFileCompletionBlock)downloadFileCompletionBlock {
    
    
    NSURL *URL = [NSURL URLWithString:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDownloadTask *downloadTask = [[self sessionManager] downloadTaskWithRequest:request progress:^(NSProgress *downloadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(downloadProgressBlock) {
                downloadProgressBlock(downloadProgress);
            }
        });
        
    } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSURL *fileUrl;
        if(strPath) {
            fileUrl = [NSURL fileURLWithPath:strPath];
        }
        else {
            fileUrl = [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        }
        return fileUrl;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if(!error) {
            NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath.path];
            if(downloadFileCompletionBlock) {
                downloadFileCompletionBlock(canSave ? filePath: nil, data, nil);
            }
        }
        else {
            if(downloadFileCompletionBlock) {
                downloadFileCompletionBlock(nil, nil, error);
            }
        }
        if(!canSave) {
            if([[NSFileManager defaultManager] fileExistsAtPath:filePath.path]) {
                [[NSFileManager defaultManager] removeItemAtURL:filePath error:nil];
            }
        }
    }];
    [downloadTask resume];
    return downloadTask;
}

#pragma mark - Download Image

+ (void)downloadImage:(NSString *)strUrl requestCompletionBlock:(requestCompletionBlock)requestCompletionBlock {
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    [urlRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    AFImageDownloader *downloader = [AFImageDownloader defaultInstance];
    id <AFImageRequestCache> imageCache = downloader.imageCache;
    
    UIImage *cachedImage = [imageCache imageforRequest:urlRequest withAdditionalIdentifier:nil];
    if (cachedImage) {
        if (requestCompletionBlock) {
            requestCompletionBlock(cachedImage,nil);
        }
        [kObjcWebClient clearActiveDownloadInformation];
    } else {
        NSUUID *downloadID = [NSUUID UUID];
        AFImageDownloadReceipt *receipt;
        receipt = [downloader
                   downloadImageForURLRequest:urlRequest
                   withReceiptID:downloadID
                   success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
                       
                       if ([kObjcWebClient.af_activeImageDownloadReceipt.receiptID isEqual:downloadID]) {
                           if (requestCompletionBlock) {
                               requestCompletionBlock(responseObject, nil);
                           }
                           [kObjcWebClient clearActiveDownloadInformation];
                       }
                       
                   }
                   failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                       if ([kObjcWebClient.af_activeImageDownloadReceipt.receiptID isEqual:downloadID]) {
                           if (requestCompletionBlock) {
                               requestCompletionBlock(nil, error);
                           }
                           [kObjcWebClient clearActiveDownloadInformation];
                       }
                   }];
        kObjcWebClient.af_activeImageDownloadReceipt = receipt;
    }
}

- (void)clearActiveDownloadInformation {
    self.af_activeImageDownloadReceipt = nil;
}

- (AFImageDownloadReceipt *)af_activeImageDownloadReceipt {
    return (AFImageDownloadReceipt *)objc_getAssociatedObject(self, @selector(af_activeImageDownloadReceipt));
}

- (void)af_setActiveImageDownloadReceipt:(AFImageDownloadReceipt *)imageDownloadReceipt {
    objc_setAssociatedObject(self, @selector(af_activeImageDownloadReceipt), imageDownloadReceipt, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
