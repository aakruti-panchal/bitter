//
//  FullImageView.swift
//  SignUp
//
//  Created by Mac33 on 30/12/16.
//  Copyright © 2016 JadavMehul. All rights reserved.//

import UIKit
import Alamofire
//import AlamofireImage

class FullImageView: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate{
    
    //Variables
    fileprivate var pageViewController  : UIPageViewController?
    var imageIndex                      : Int! = 0
    var selectedIndexPath               : IndexPath? = nil
    var isSelected                      : Bool = true
    var imageArray                      = [String]()
    var isSwipGesture:Bool              = true     // If you want to use left/right swipe, set isSwipGesture true
    
    //Outlets
    @IBOutlet weak var scrollView                   : UIScrollView!
    @IBOutlet weak var imageView                    : UIImageView!
    @IBOutlet weak var btnLeftArrow                 : UIButton!
    @IBOutlet weak var btnRightArrow                : UIButton!
    @IBOutlet weak var txtImageNumber               : UILabel!
    @IBOutlet weak var collectionView               : UICollectionView!
    @IBOutlet weak var viewCollection               : UIView!
    @IBOutlet var SwipRightGesture                  : UISwipeGestureRecognizer!
    @IBOutlet var SwipLeftGesture                   : UISwipeGestureRecognizer!
    
    //LayoutConstraint Outlets
    @IBOutlet weak var topViewConstraint            : NSLayoutConstraint!
    @IBOutlet weak var constraintCollectionBottom   : NSLayoutConstraint!
    
    @IBOutlet weak var IBbtnClose                   : UIButton!
    var borderColor                                 : UIColor!
    
    let zoomSc: Float = 3.0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setView()
        self.txtImageNumber.isHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setView() {
        
        self.borderColor = K.Color.Red //AppColor.AppTheme_Primary
        
        self.btnRightArrow.tintColor = K.Color.AppTheme_Primary
        self.btnLeftArrow.tintColor = K.Color.AppTheme_Primary
        
        setImage(index: imageIndex)
//        if(isSwipGesture){
//            btnLeftArrow.isHidden       = true
//            btnRightArrow.isHidden      = true
//        }else{
//            SwipLeftGesture.isEnabled   = false
//            SwipRightGesture.isEnabled  = false
//        }
        collectionView.register(UINib(nibName: "FullImageViewCell", bundle: nil), forCellWithReuseIdentifier: "FullImageViewCell")
        self.txtImageNumber.font = Common().getFont(fontSize: 17.0, fontStyle: FontStyle.Regular)
        self.txtImageNumber.text = getLableTitle()
        scrollView.minimumZoomScale     = 1.0
        scrollView.maximumZoomScale     = 3.0
        
        btnLeftArrow.isHidden       = true
        btnRightArrow.isHidden      = true
        /*
        if self.imageIndex == 0 {
            if imageArray.count > 1 {
                btnLeftArrow.isHidden       = true
                btnRightArrow.isHidden      = false
            } else {
                btnLeftArrow.isHidden       = true
                btnRightArrow.isHidden      = true
            }
        } else {
            if self.imageIndex == self.imageArray.count-1{
                btnLeftArrow.isHidden       = false
                btnRightArrow.isHidden      = true
            } else {
                btnLeftArrow.isHidden       = false
                btnRightArrow.isHidden      = false
            }
        }
        */
        
        self.txtImageNumber.textColor = K.Color.Red
        self.IBbtnClose.tintColor = K.Color.AppTheme_Primary
        
    }
    
}

//MARK: IBAction Action Method AND All Gesture Selections
//- See more at: http://www.theappguruz.com/blog/gesture-recognizer-using-swift#sthash.d4CSmhrf.dpuf
extension FullImageView {
    
    @IBAction func rightsideanimation(gesture: UIGestureRecognizer){
        if 0 < imageIndex {
            scrollView.zoomScale = 0
            let animation: CATransition =  self.CATransitionFunc(isRight: true)
            imageIndex = imageIndex - 1;
            self.txtImageNumber.text    = getLableTitle()
            if NSURL(string: (imageArray[imageIndex])) != nil {
                self.setImage(index: imageIndex)
            }
            self.imageView.layer.add(animation, forKey: kCATransition)
            if(self.selectedIndexPath != nil){
                self.selectedIndexPath?.row = imageIndex
                self.collectionView.scrollToItem(at: self.selectedIndexPath!, at: .right, animated: true)
                //self.collectionView.reloadItems(at: [self.selectedIndexPath! as IndexPath])
                self.collectionView.reloadData()
            }
        }
    }
    @IBAction func leftsideanimation(gesture: UIGestureRecognizer) {
        if (imageArray.count > (imageIndex+1)){
            scrollView.zoomScale = 0
            let animation: CATransition =  self.CATransitionFunc(isRight: false)
            imageIndex                  = imageIndex + 1;
            self.txtImageNumber.text    = getLableTitle()
            self.imageView.layer.add(animation, forKey: kCATransition)
            if NSURL(string: (imageArray[imageIndex])) != nil {
                self.setImage(index: imageIndex)
            }
            if(self.selectedIndexPath != nil){
                self.selectedIndexPath?.row = imageIndex
                self.collectionView.scrollToItem(at: self.selectedIndexPath!, at: .left, animated: true)
                //self.collectionView.reloadItems(at: [self.selectedIndexPath! as IndexPath])
                self.collectionView.reloadData()
            }
        }
    }
    @IBAction func imageTapped(sender: AnyObject)
    {
        if(scrollView.zoomScale >= 3){
            scrollView.setZoomScale(0, animated: true)
        }else{
            let tappedPoint     = sender.location(in: self.imageView)
            let scrollViewSize  = scrollView.bounds.size
            let w: CGFloat      = scrollViewSize.width / scrollView.maximumZoomScale
            let h: CGFloat      = scrollViewSize.height / scrollView.maximumZoomScale
            let x: CGFloat      = tappedPoint.x - (w / 2.0)
            let y: CGFloat      = tappedPoint.y - (h / 2.0)
            let rectTozoom      = CGRect(x: x, y: y, width: w, height: h)
            self.scrollView.zoom(to: rectTozoom, animated: true)
        }
    }
    @IBAction func imageOneTapped(sender: AnyObject)
    {
        if(constraintCollectionBottom.constant==5){
            UIView .animate(withDuration: 0.5, delay: 0.0, options:UIViewAnimationOptions.curveEaseInOut, animations: {
                self.constraintCollectionBottom.constant = -75
                self.view.layoutIfNeeded()
                }, completion: {
                    (value: Bool) in
            })
        }else{
            UIView .animate(withDuration: 0.5, delay: 0.0, options:UIViewAnimationOptions.curveEaseInOut, animations: {
                self.constraintCollectionBottom.constant = 5
                self.view.layoutIfNeeded()
                }, completion: {
                    (value: Bool) in
            })
        }
    }
    @IBAction func btnLeftRight(_ sender: AnyObject) {
        
        self.btnRightArrow.isHidden = false
        self.btnLeftArrow.isHidden = false
        if(sender.tag==1){
            if(imageIndex == 0){
                self.btnLeftArrow.isHidden = true
            }
            if(imageIndex > 0){
                imageIndex = imageIndex-1
                if(imageIndex == imageArray.count){
                    imageIndex = imageArray.count-1
                }
                if(imageIndex == 0){
                    self.btnLeftArrow.isHidden = true
                }
            }
        }else{
            if(imageIndex+1 == imageArray.count){
                self.btnRightArrow.isHidden = true;
            }
            if(imageIndex+1 < imageArray.count){
                imageIndex = imageIndex+1
                if(imageIndex+1 == imageArray.count){
                    self.btnRightArrow.isHidden = true;
                    imageIndex = imageArray.count-1
                }
            }
        }
        self.txtImageNumber.text = getLableTitle()
        setImage(index: imageIndex)
        if(self.selectedIndexPath != nil){
            self.selectedIndexPath?.row = imageIndex
            self.collectionView.scrollToItem(at: self.selectedIndexPath!, at: .left, animated: true)
            self.collectionView.reloadData()
        }
        scrollView.zoomScale = 0
    }
    @IBAction func btnClose(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: CollectionView Methods
extension FullImageView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return imageArray.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        var cell:FullImageViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "FullImageViewCell", for: indexPath) as? FullImageViewCell
        if (cell == nil) {
            let nib: NSArray = Bundle.main.loadNibNamed("FullImageViewCell", owner: self, options: nil)! as NSArray
            cell = (nib.object(at: 0) as? FullImageViewCell)!
        }
        self.selectedIndexPath  = indexPath
        self.selectedIndexPath?.row = imageIndex
        
        if (indexPath.row == self.selectedIndexPath?.row) {
            let bcolor : UIColor = borderColor //UIColor( red: 72/255.0, green: 197/255.0, blue:147/255.0, alpha: 1.0 )
            cell?.layer.borderColor     = bcolor.cgColor
            cell?.layer.borderWidth     = 2
            cell?.layer.cornerRadius    = 3
            cell?.backgroundColor=UIColor.white
        }else{
            let bcolor : UIColor = UIColor( red: 0.2, green: 0.2, blue:0.2, alpha: 0.3 )
            cell?.layer.borderColor     = bcolor.cgColor
            cell?.layer.borderWidth     = 0.5
            cell?.layer.cornerRadius    = 3
            cell?.backgroundColor=UIColor.white
        }
        //Set Images
        //let url:URL = URL(string: imageArray[indexPath.row])!
        //cell?.imgCell.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_Test"))
        let imgURL = imageArray[indexPath.row]
        cell?.imgCell.setImageWithURL(imgURL, placeHolderImage: UIImage(named: "ic_Test"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        setImage(index: indexPath.row)
//        if(!isSwipGesture){
//            hiddeShowButtob(indexPath: indexPath)
//        }
        imageIndex = indexPath.row
        scrollView.zoomScale = 0
        //self.selectedIndexPath  = indexPath
        self.isSelected         = true
        self.txtImageNumber.text    = String(indexPath.row+1)+"/"+String(imageArray.count)

        if(selectedIndexPath != nil){
            selectedIndexPath?.row = imageIndex
            self.setCell(indexPath: selectedIndexPath!, bcolor: UIColor( red: 0.2, green: 0.2, blue:0.2, alpha: 0.3 ))
//            self.selectedIndexPath = nil
//            self.imageIndex = nil
        }
        
//        self.setCell(indexPath: indexPath, bcolor: borderColor)
        self.collectionView.reloadData()
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath){
        
        self.setCell(indexPath: indexPath, bcolor: UIColor( red: 0.2, green: 0.2, blue:0.2, alpha: 0.3 ))
    }
    
    func setCell(indexPath : IndexPath, bcolor : UIColor){
        
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = bcolor.cgColor
        cell?.layer.borderWidth = 2
        cell?.layer.cornerRadius = 3
        cell?.backgroundColor=UIColor.white
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionat section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(0, 100, 0, 0)
//    }
}

//MARK: ScrollView Delegate Methods
extension FullImageView {
    
    @objc(viewForZoomingInScrollView:) func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        if scrollView.zoomScale > 1 {
            
            if let image = imageView.image {
                
                let ratioW = imageView.frame.width / image.size.width
                let ratioH = imageView.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW:ratioH
                
                let newWidth = image.size.width*ratio
                let newHeight = image.size.height*ratio
                
                let left = 0.5 * (newWidth * scrollView.zoomScale > imageView.frame.width ? (newWidth - imageView.frame.width) : (scrollView.frame.width - scrollView.contentSize.width))
                let top = 0.5 * (newHeight * scrollView.zoomScale > imageView.frame.height ? (newHeight - imageView.frame.height) : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsetsMake(top, left, top, left)
            }
        } else {
            scrollView.contentInset = UIEdgeInsets.zero
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.zoomScale > CGFloat(zoomSc) {
            self.scrollView.zoomScale = CGFloat(zoomSc)
        }
        
        /*
        if(scrollView.contentOffset.x < -100){
            self.rigthSide()
        }else if((scrollView.contentOffset.x + UIScreen.main.bounds.width) > scrollView.contentSize.width+100){
            self.leftSide()
  */
    }
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return self.imageView
//    }
    
}

//MARK: Other Methods
extension FullImageView{
    
    func getLableTitle()->String{
        return String(imageIndex+1)+"/"+String(imageArray.count)
    }
    
    func setImage(index:Int?=0){
        //let url:URL = URL(string: imageArray[index!])!
        //imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_Test"))
        let imgURL = imageArray[index!]
        imageView.setImageWithURL(imgURL, placeHolderImage: UIImage(named: "ic_Test"), activityIndicatorViewStyle: .gray, completionBlock: nil)
        
    }
    func CATransitionFunc(isRight:Bool)->CATransition{
        
        let animation: CATransition = CATransition()
        animation.delegate          = self as? CAAnimationDelegate
        animation.type              = kCATransitionPush
        isRight ? (animation.subtype = kCATransitionFromLeft) : (animation.subtype = kCATransitionFromRight)
        animation.duration          = 0.7
        animation.timingFunction    = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return animation
    }
    func hiddeShowButtob(indexPath:IndexPath){
        
        self.btnRightArrow.isHidden = false
        self.btnLeftArrow.isHidden = false
        if(indexPath.row == 0){
            self.btnLeftArrow.isHidden = true
        }
        if(indexPath.row+1 == imageArray.count){
            self.btnRightArrow.isHidden = true
        }
    }
    func rigthSide(){
    
        if 0 < imageIndex {
            scrollView.zoomScale = 0
            let animation: CATransition =  self.CATransitionFunc(isRight: true)
            imageIndex = imageIndex - 1;
            self.txtImageNumber.text    = getLableTitle()
            if NSURL(string: (imageArray[imageIndex])) != nil {
                self.setImage(index: imageIndex)
            }
            self.imageView.layer.add(animation, forKey: kCATransition)
            if(self.selectedIndexPath != nil){
                self.selectedIndexPath?.row = imageIndex
                self.collectionView.scrollToItem(at: self.selectedIndexPath!, at: .right, animated: true)
                //self.collectionView.reloadItems(at: [self.selectedIndexPath! as IndexPath])
                self.collectionView.reloadData()
            }
        }
    }
    func leftSide(){
    
        if (imageArray.count > (imageIndex+1)){
            scrollView.zoomScale = 0
            let animation: CATransition =  self.CATransitionFunc(isRight: false)
            imageIndex                  = imageIndex + 1;
            self.txtImageNumber.text    = getLableTitle()
            self.imageView.layer.add(animation, forKey: kCATransition)
            if NSURL(string: (imageArray[imageIndex])) != nil {
                self.setImage(index: imageIndex)
            }
            if(self.selectedIndexPath != nil){
                self.selectedIndexPath?.row = imageIndex
                self.collectionView.scrollToItem(at: self.selectedIndexPath!, at: .left, animated: true)
                //self.collectionView.reloadItems(at: [self.selectedIndexPath! as IndexPath])
                self.collectionView.reloadData()
            }
        }
    }
}

//MARK: you can also use bellow Gesture
//
// func setGesture(){
// let swipeRight = UISwipeGestureRecognizer(target: self, action: Selector(("respondToSwipeGesture:")))
// swipeRight.direction = UISwipeGestureRecognizerDirection.right
// self.view.addGestureRecognizer(swipeRight)
// 
// let swipeLeft = UISwipeGestureRecognizer(target: self, action: Selector(("respondToSwipeGesture:")))
// swipeLeft.direction = UISwipeGestureRecognizerDirection.left
// self.view.addGestureRecognizer(swipeLeft)
// }
// func respondToSwipeGesture(gesture: UIGestureRecognizer) {
// 
// if let swipeGesture = gesture as? UISwipeGestureRecognizer {
// switch swipeGesture.direction {
// case UISwipeGestureRecognizerDirection.right:
// print("Swiped right")
// case UISwipeGestureRecognizerDirection.down:
// print("Swiped down")
// case UISwipeGestureRecognizerDirection.left:
// print("Swiped left")
// case UISwipeGestureRecognizerDirection.up:
// print("Swiped up")
// default:
// break
// }
// }
// }
//
