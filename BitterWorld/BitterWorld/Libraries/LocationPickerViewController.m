//
//  PropertyMapViewController.m
//
//  Createdon 31/12/15.
//  Copyright © 2015 rane. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

#import "LocationPickerViewController.h"

#import <BitterWorld-Swift.h>

BOOL SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(NSString *version) {
    return ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] != NSOrderedAscending);
}
typedef void(^addressCompletion)(NSString *);


@interface LocationPickerViewController () <UIGestureRecognizerDelegate, GMSAutocompleteTableDataSourceDelegate, GMSMapViewDelegate> {
   
}

@end

@implementation LocationPickerViewController
{
    UITableViewController *_resultsController;
    GMSAutocompleteTableDataSource *_tableDataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [_mapViewLocations setMyLocationEnabled:TRUE];
    [_mapViewLocations setDelegate:self];
    
    if(AppUtility.shared.currentLocation.coordinate.latitude != 0.0 &&
       AppUtility.shared.currentLocation.coordinate.longitude != 0.0) {
        GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:AppUtility.shared.currentLocation.coordinate zoom:17.0];
        [_mapViewLocations setCamera:cameraPosition];
        
        CLLocation* currentLocation = [[CLLocation alloc] initWithLatitude:AppUtility.shared.currentLocation.coordinate.latitude longitude:AppUtility.shared.currentLocation.coordinate.longitude];

        
        [self getAddressFromLocation:currentLocation complationBlock:^(NSString * address) {
            if(address) {
               // [searchBarText setText:address];
                //[searchBarText becomeFirstResponder];
                //[searchBarText resignFirstResponder];
            }
        }];
        
    }
    
    if (_defaultSelectedLocation) {
        [self addPinAtLocation:_defaultSelectedLocation.coordinate];
        [searchBarText setHidden:YES];
        [btnCurrentLocation setHidden:YES];
        [btnAddLocationTitle setHidden:YES];
        [AddLocationView setHidden:YES];
        
    }
    else {
        [btnAddLocationTitle setHidden:NO];
        [searchBarText setHidden:NO];
        [btnCurrentLocation setHidden:NO];
        [AddLocationView setHidden:NO];
        [self addPinAtLocation:AppUtility.shared.currentLocation.coordinate];
    }
    
    [viewDisplayLocation.layer setCornerRadius:5];
    [searchBarText setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor] size:searchBarText.size]];
    
    [btnAddLocationTitle setTitle:@"Set Location" forState:UIControlStateNormal];
    
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = _tableDataSource;
    _resultsController.tableView.dataSource = _tableDataSource;
    
    // show cancel button.
    [searchBarText setShowsCancelButton:NO];
    
    [AddLocationView setCornerRadius:5];
    [AddLocationView setClipsToBounds:YES];
}

#pragma mark - Navigation Action

- (IBAction) btnBackAction:(id)sender {
    [self.view endEditing:YES];
    if (_isPresented) {
        [self dismissViewControllerAnimated:true completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark -

- (IBAction)btnCurrentLocationAction:(id)sender {
    [self addPinAtLocation:AppUtility.shared.currentLocation.coordinate];
}

- (IBAction)btnSelectLocationAction:(id)sender {
    if ([_delegate respondsToSelector:@selector(didSelectedCoordinate:withFormatedAddress:withPlaceId:)]) {
        [_delegate didSelectedCoordinate:marker.position withFormatedAddress:marker.title withPlaceId:strSelectedPlaceId];
        if (_isPresented) {
            [self dismissViewControllerAnimated:true completion:nil];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        //[self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark: - MapView

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [SVProgressHUD show];
    [btnCurrentLocation setSelected:NO];
    [self addPinAtLocation:coordinate];
}

#pragma mark - Select Location

- (void)addPinAtLocation:(CLLocationCoordinate2D) location {
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f",location.latitude, location.longitude];
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:strUrl] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error == nil) {
            id responseData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (responseData) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([[responseData valueForKey:@"status"] isEqualToString:@"OK"]) {
                        NSArray *addressArray = [responseData valueForKey:@"results"];
                        if (addressArray.count > 0) {
                            strSelectedPlaceId = [[addressArray firstObject] valueForKey:@"place_id"];
                            NSString * formatedAddress = [[addressArray firstObject] valueForKey:@"formatted_address"];
                            if(marker == nil) {
                                marker = [GMSMarker markerWithPosition:location];
                            }
                            else {
                                marker.position = location;
                            }
                            marker.title = [self getValidString:formatedAddress];
                            marker.icon = [UIImage imageNamed:@"pin_gray_icon"];
                            marker.map = _mapViewLocations;
                            
                            GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:location zoom:self.mapViewLocations.camera.zoom];
                            [_mapViewLocations animateToCameraPosition:cameraPosition];
                            lblSelectedAddress.text = formatedAddress;
                            //searchBarText.text = formatedAddress;
                            
                        }
                    }
                });
            }
        }
    }] resume];
}



- (void) getLocationPlaceid:(CLLocationCoordinate2D) location complationBlock:(placeidCompletion)complationBlock{
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f",location.latitude, location.longitude];
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:strUrl] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error == nil) {
            id responseData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (responseData) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if ([[responseData valueForKey:@"status"] isEqualToString:@"OK"]) {
                        NSArray *addressArray = [responseData valueForKey:@"results"];
                        if (addressArray.count > 0) {
                            strSelectedPlaceId = [[addressArray firstObject] valueForKey:@"place_id"];
                            NSString * formatedAddress = [[addressArray firstObject] valueForKey:@"formatted_address"];
                            
                            complationBlock(strSelectedPlaceId,formatedAddress);
                        }
                    }
                });
            }
        }
    }] resume];
    
    
}


- (void) addPinAtLocation:(CLLocationCoordinate2D) location withAddress:(NSString *) address {
    if(address) {
        if(marker == nil) {
            marker = [GMSMarker markerWithPosition:location];
        }
        else {
            marker.position = location;
        }
        marker.title = [self getValidString:address];
        marker.icon = [UIImage imageNamed:@"pin_gray_icon"];
        marker.map = _mapViewLocations;

        GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:location zoom:self.mapViewLocations.camera.zoom];
        [_mapViewLocations animateToCameraPosition:cameraPosition];
    
        lblSelectedAddress.text = address;
        searchBarText.text = @"";
        [SVProgressHUD dismiss];
    }
}

#pragma mark - GMSAutocompleteTableDataSourceDelegate

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource didAutocompleteWithPlace:(GMSPlace *)place {
    [searchBarText resignFirstResponder];
    [SVProgressHUD show];
    [btnCurrentLocation setSelected:NO];
    strSelectedPlaceId = place.placeID;
    [[GMSPlacesClient sharedClient] lookUpPlaceID:strSelectedPlaceId callback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            //NSLog(@"Place Details error %@", [error localizedDescription]);
            [SVProgressHUD dismiss];
            return;
        }
        
        if (place != nil) {
            [searchBarText resignFirstResponder];
            [self addPinAtLocation:place.coordinate withAddress:place.formattedAddress];
        } else {
            //NSLog(@"No place details for %@", placeID);
            [SVProgressHUD dismiss];
        }
    }];
}

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [searchBarText resignFirstResponder];
    searchBarText.text = @"";
}

- (void)didUpdateAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

- (void)didRequestAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

#pragma mark - UITextFieldDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    if ([searchBarText isEqual:searchBar]) {
        UITextField *searchField = nil;
        for (UIView *subview in searchBar.subviews) {
            if ([subview isKindOfClass:[UITextField class]]) {
                searchField = (UITextField *)subview;
                break;
            }
            for (UIView *innerView in subview.subviews) {
                if ([innerView isKindOfClass:[UITextField class]]) {
                    searchField = (UITextField *)innerView;
                    break;
                }
            }
        }
        
        if (searchField) {
            [searchField setTextColor:[UIColor blackColor]];
            searchField.enablesReturnKeyAutomatically = NO;
        }
        
        [self addChildViewController:_resultsController];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            CGFloat yPosition = searchBarText.y+searchBarText.height;
            CGRect rect = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height-yPosition);
            _resultsController.view.frame = rect;
            _resultsController.view.alpha = 0.0f;
            [self.view addSubview:_resultsController.view];
            [UIView animateWithDuration:0.5
                             animations:^{
                                 _resultsController.view.alpha = 1.0f;
                             } ];
            [_resultsController didMoveToParentViewController:self];
        });
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if ([searchBarText isEqual:searchBar]) {
        [_resultsController willMoveToParentViewController:nil];
        [UIView animateWithDuration:0.5
                         animations:^{
                             _resultsController.view.alpha = 0.0f;
                         }
                         completion:^(BOOL finished) {
                             [_resultsController.view removeFromSuperview];
                             [_resultsController removeFromParentViewController];
                         }];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - Private Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_tableDataSource sourceTextHasChanged:searchBar.text];
}

#pragma mark -

- (NSString *) getValidString:(NSString *)string {
    if ([string isKindOfClass:[NSString class]]) {
        if ([(NSString *)string length] > 0) {
            return (NSString *)string;
        }
        return @"";
    }
    return @"";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [mapView setCenterCoordinate:mapView.userLocation.coordinate zoomLevel:10.0 animated:YES];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKOverlayPathRenderer *overlayPathView;
    if ([overlay isKindOfClass:[MKPolygon class]])
    {
        overlayPathView = [[MKPolygonRenderer alloc] initWithPolygon:(MKPolygon*)overlay];
        overlayPathView.fillColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
        overlayPathView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        overlayPathView.lineWidth = 3;
        
        return overlayPathView;
    }
    
    else if ([overlay isKindOfClass:[MKPolyline class]])
    {
        overlayPathView = [[MKPolylineRenderer alloc] initWithPolyline:(MKPolyline*)overlay];
        overlayPathView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        overlayPathView.lineWidth = 3;
        
        return overlayPathView;
    }
    
    return nil;
}

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
         }
     }];
}

@end
