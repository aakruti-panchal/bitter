//
//  InstagramAuthViewController.swift
//
//  Created by Harry on 27/10/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import WebKit
import Alamofire


typealias AuthCompletion = (_ accessToken : String?, _ userInfo: Dictionary<String, Any>?, _ error: Error?) -> Void

class InstagramAuthViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet var viewTopBar: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnClose: UIButton!
    var webViewInstagram: WKWebView?
    
    static let baseURL = "https://api.instagram.com/"
    var userInfoServiceName = baseURL + "v1/users/self"
    var authServiceName = baseURL + "oauth/authorize/"
    
    var clientID: String! = "3d5429a75f6f44c0a337cf16af9e4dda"
    var redirectURI: String! = "http://mapall.org"
    var authCompletion: AuthCompletion?
    var authScope: String! = "basic+public_content"

    class func showInViewController(_ viewController: UIViewController, withCompletion completion: AuthCompletion?) {
        let instagramAuthViewController = InstagramAuthViewController(nibName: "InstagramAuthViewController", bundle: nil)
        instagramAuthViewController.authCompletion = completion
        viewController.present(instagramAuthViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarStyle = .default
        viewTopBar.height = K.topBarHeight
        let webConfiguration = WKWebViewConfiguration()
//        webConfiguration.websiteDataStore = .`default()
        webViewInstagram = WKWebView(frame: CGRect(x: 0, y: viewTopBar.height, width: self.view.width, height: self.view.height - viewTopBar.height), configuration: webConfiguration)
        webViewInstagram?.navigationDelegate = self
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            // print(cookie.name+"="+cookie.value)
            cookieJar.deleteCookie(cookie)
        }
        cookieJar.cookieAcceptPolicy = .always
        self.view.addSubview(webViewInstagram!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadAuthorizationURL()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Other Methods
    
    private func loadAuthorizationURL() {
        let authorizationURL = URL(string: authServiceName)
        var components = URLComponents(url: authorizationURL!, resolvingAgainstBaseURL: false)!
        components.queryItems = [
            URLQueryItem(name: "client_id", value: self.clientID),
            URLQueryItem(name: "redirect_uri", value: self.redirectURI),
            URLQueryItem(name: "response_type", value: "token"),
            URLQueryItem(name: "scope", value: self.authScope ?? "")
        ]
        let request = URLRequest(url: components.url!)
        webViewInstagram?.load(request)
    }
    
    func endAuthentication(accessToken: String?) {
        SVProgressHUD.show()
        let strUrl = userInfoServiceName + "?access_token=\(accessToken!)"
        Alamofire.request(strUrl, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    SVProgressHUD.dismiss()
                    self.authCompletion?(accessToken, value as? Dictionary<String, Any>, nil)
                    self.dismiss(animated: true, completion: nil)
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    self.authCompletion?(accessToken, nil, error)
                    self.dismiss(animated: true, completion: nil)
                }
        }
    }
    
    //MARK: - Button
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - WebView
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let urlString = navigationAction.request.url!.absoluteString
        if let range = urlString.range(of: "#access_token=") {
            let location = range.upperBound
            let code = urlString[location...]
            endAuthentication(accessToken: String(code))
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        let error = NSError(domain: kErrorDomain, code: kDefaultErrorCode, userInfo: [NSLocalizedDescriptionKey: kTryAgainLaterMessage])
        self.authCompletion?(nil, nil, error)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
