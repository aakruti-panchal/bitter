//
//  PropertyMapViewController.h
//
//  Created on 31/12/15.
//  Copyright © 2015. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@protocol LocationPickerDelegate <NSObject>
typedef void(^placeidCompletion)(NSString *,NSString *);
typedef void(^addressCompletion)(NSString *);
@optional
- (void) didSelectedCoordinate:(CLLocationCoordinate2D)coordinate withFormatedAddress:(NSString *)address withPlaceId:(NSString *)googlePlaceId;

@end

@interface LocationPickerViewController : UIViewController <CLLocationManagerDelegate>
{
    NSDictionary *selectedLocation;
    CLLocationManager *locationManager;
    
    IBOutlet UISearchBar *searchBarText;
    
    IBOutlet UIView *viewDisplayLocation;
    IBOutlet UIButton *btnCurrentLocation;
    IBOutlet UILabel *lblSelectedAddress;
    
    IBOutlet UIView *AddLocationView;
    IBOutlet UIButton *btnAddLocationTitle;
    
    GMSMarker *marker;
    NSString *strSelectedPlaceId;
}

@property (nonatomic, assign) BOOL isPresented;

@property (nonatomic, strong) NSString *customTitle;
@property (nonatomic, strong) CLLocation *defaultSelectedLocation;
@property (nonatomic, strong) id <LocationPickerDelegate> delegate;
@property (nonatomic, strong) IBOutlet GMSMapView *mapViewLocations;

- (IBAction)btnCurrentLocationAction:(id)sender;
- (IBAction)btnSelectLocationAction:(id)sender;

- (void) addPinAtLocation:(CLLocationCoordinate2D) location;

-(void) getLocationPlaceid:(CLLocationCoordinate2D) location complationBlock:(placeidCompletion)complationBlock;
-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock;
@end
