//
//  CircleProgressView.h
//  CircularProgressControl
//
//  Created by Carlos Eduardo Arantes Ferreira on 22/11/14.
//  Copyright (c) 2014 Mobistart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleShapeLayer.h"

@interface CircleProgressView : UIControl

@property (nonatomic) NSTimeInterval elapsedTime;

@property (nonatomic) NSTimeInterval timeLimit;

@property (nonatomic, retain) NSString *status;

@property (assign, nonatomic) double percent;
@property (nonatomic,strong) UIColor *backgroundStrokeColor;
@property (nonatomic, strong) CircleShapeLayer *progressLayer;
@property (strong, nonatomic) UILabel *progressLabel;
-(void)getRemainingTime:(NSTimeInterval)elapsedTime callBack:(void (^)(NSString *count, NSString *unit))completionBlock;

@end
