//
//  CustomTextField.swift
//
//  Created by Harry.p on 25/10/17.
//
//

import UIKit

@IBDesignable

class CustomTextField: UITextField {
    
    var spacing : CGFloat = 7

    @IBInspectable var showBottomLine: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable var lineColor: UIColor? {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable var iconMargin: CGFloat = 0.0 
    @IBInspectable var leftIcon: UIImage?  {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable var rightIcon: UIImage? {
        didSet {
            setNeedsDisplay()
        }
    }
//    @IBInspectable var showBottomSubLine: Bool = false  {
//        didSet {
//            setNeedsDisplay()
//        }
//    }
//    @IBInspectable var subLineWidth: CGFloat = 0.0 {
//        didSet {
//            setNeedsDisplay()
//        }
//    }
//    @IBInspectable var subLineColor: UIColor? {
//        didSet {
//            setNeedsDisplay()
//        }
//    }
    @IBInspectable var enableMaterialPlaceHolder: Bool  = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        didSet {
            if (placeholder ?? "").count  > 0 {
                let str = NSAttributedString(string: placeholder!, attributes: [.foregroundColor: placeHolderColor ?? UIColor.lightGray])
                tintColor = textColor
                attributedPlaceholder = str
            }
        }
    }
    
    override var text: String? {
        didSet {
            textFieldDidChange(self)
        }
    }
    
    override var placeholder: String? {
        didSet {
            if (placeholder ?? "").count  > 0 {
                let str = NSAttributedString(string: placeholder!, attributes: [.foregroundColor: placeHolderColor ?? UIColor.lightGray])
                tintColor = textColor
                attributedPlaceholder = str
            }
        }
    }
    
    var imgViewLeftIcon: UIImageView?
    var imgViewRightIcon: UIImageView?
    var viewBottomLine: UIView?
//    var viewBottomSubLine: UIView?
    var placeHolderLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializedOnce()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializedOnce()
    }
    
    func initializedOnce() {
        clipsToBounds = false
        addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        placeHolderColor = UIColor.white
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        borderStyle = .none
        let defaultLineColor = K.Color.gray
//        let defaultSubLineColor = UIColor.red
        let subLineHeight: CGFloat = 2
        let imageSize = CGSize(width: 20, height: 20)
        if leftIcon != nil {
            if imgViewLeftIcon == nil {
                let xPosission: CGFloat = 0
                imgViewLeftIcon = UIImageView(frame: CGRect(x: xPosission, y: 0, w: imageSize.width, h: imageSize.height))
                imgViewLeftIcon?.centerY = (frame.size.height / 2) + iconMargin
                imgViewLeftIcon?.contentMode = .scaleAspectFit
                imgViewLeftIcon?.clipsToBounds = true
            }
            imgViewLeftIcon?.image = leftIcon
            addSubview(imgViewLeftIcon!)
        }
        else {
            imgViewLeftIcon?.removeFromSuperview()
        }
        if rightIcon != nil {
            if imgViewRightIcon == nil {
                let xPosission: CGFloat = frame.size.width - imageSize.width
                imgViewRightIcon = UIImageView(frame: CGRect(x: xPosission, y: 0, w: imageSize.width, h: imageSize.height))
                imgViewRightIcon?.centerY = (frame.size.height / 2) + iconMargin
                imgViewRightIcon?.contentMode = .scaleAspectFit
                imgViewRightIcon?.clipsToBounds = true
            }
            imgViewRightIcon?.image = rightIcon
            addSubview(imgViewRightIcon!)
        }
        else {
            imgViewRightIcon?.removeFromSuperview()
        }
        if showBottomLine {
            if viewBottomLine == nil {
                viewBottomLine = UIView(frame: CGRect(x: 0, y: frame.size.height - subLineHeight, width: frame.size.width, height: 0.5))
            }
            viewBottomLine?.backgroundColor = lineColor ?? defaultLineColor
            addSubview(viewBottomLine!)
        }
        else {
            viewBottomLine?.removeFromSuperview()
        }
        
//        if showBottomSubLine {
//            if viewBottomSubLine == nil {
//                viewBottomSubLine = UIView(frame: CGRect(x: 0, y: frame.size.height - subLineHeight, width: subLineWidth == 0 ? 30 : subLineWidth, height: subLineHeight))
//            }
//            viewBottomSubLine?.backgroundColor = subLineColor ?? defaultSubLineColor
//            addSubview(viewBottomSubLine!)
//        }
//        else {
//            viewBottomSubLine?.removeFromSuperview()
//        }
        if enableMaterialPlaceHolder {
            if placeHolderLabel == nil {
                placeHolderLabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
            }
            placeHolderLabel?.alpha = 0
            placeHolderLabel?.attributedText = attributedPlaceholder
            placeHolderLabel?.sizeToFit()
            if (text ?? "")!.count > 0 {
                textFieldDidChange(self)
            }
            self.addSubview(placeHolderLabel!)
        }
        else {
            placeHolderLabel?.removeFromSuperview()
        }
    }
    
    
    func rect(forBounds bounds: CGRect) -> CGRect {
        var newBounds: CGRect = bounds
        if leftIcon != nil && rightIcon != nil {
            if imgViewLeftIcon != nil && imgViewRightIcon != nil {
                newBounds.size.width = newBounds.size.width - imgViewLeftIcon!.width + imgViewRightIcon!.width + (spacing * 2)
                newBounds.origin.x = imgViewLeftIcon!.width + spacing
                placeHolderLabel?.x = newBounds.origin.x
            }
        }
        else if leftIcon != nil && rightIcon == nil {
            if imgViewLeftIcon != nil {
                newBounds.size.width = newBounds.size.width - imgViewLeftIcon!.width + spacing
                newBounds.origin.x = imgViewLeftIcon!.width + spacing
                placeHolderLabel?.x = newBounds.origin.x
            }
        }
        else if leftIcon == nil && rightIcon != nil {
            if imgViewRightIcon != nil {
                newBounds.size.width = newBounds.size.width - imgViewRightIcon!.width + spacing
                newBounds.origin.x = 0
                placeHolderLabel?.x = newBounds.origin.x
            }
        }
        viewBottomLine?.x = newBounds.origin.x
        viewBottomLine?.width = self.width - newBounds.origin.x
        return newBounds
    }
    
    @IBAction func textFieldDidChange(_ sender: Any) {
        if enableMaterialPlaceHolder {
            if text == nil || (text ?? "").count > 0 {
                placeHolderLabel?.alpha = 1
                attributedPlaceholder = nil
            }
            let duration: Double = 0.5
            let delay: Double = 0.0
            let damping: CGFloat = 0.6
            let velocity: CGFloat = 1
            
            UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: damping, initialSpringVelocity: velocity, options: .curveEaseInOut, animations: {() -> Void in
                if self.text == nil || (self.text ?? "").count == 0 {
                    self.placeHolderLabel?.center = CGPoint(x: self.center.x, y: self.frame.size.height / 2)
                }
                else {
                    var yDisplacement: CGFloat = self.placeHolderLabel!.height
                    if yDisplacement < self.height {
                        let margin: CGFloat = self.height - yDisplacement
                        yDisplacement -= margin / 3
                    }
                    self.placeHolderLabel?.y = -yDisplacement
                }
            }, completion: {(_ finished: Bool) -> Void in
            })
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: rect(forBounds: bounds))
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return super.placeholderRect(forBounds: rect(forBounds: bounds))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return super.editingRect(forBounds: rect(forBounds: bounds))
    }
}
