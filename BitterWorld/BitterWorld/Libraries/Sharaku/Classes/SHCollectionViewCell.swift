//
//  SHCollectionViewCell.swift
//  Pods
//
//  Created by 母利 睦人 on 2017/01/04.
//
//

import UIKit

class SHCollectionViewCell: UICollectionViewCell {
    @IBOutlet var filterNameLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var containerView: UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
        self.imageView.layer.cornerRadius = self.imageView.layer.frame.width/2
        self.containerView.layer.cornerRadius = self.imageView.layer.frame.width/2
        
       
    }
}
