
//
//  AppUtility.swift
//

import UIKit
import Foundation
import CoreLocation
import AVFoundation
import MobileCoreServices
import ObjectMapper
import Floaty
protocol MenuProtocol: class {
    func showMenuOptions(identifier : String)
}
class AppUtility: NSObject,CLLocationManagerDelegate,RSKImageCropViewControllerDelegate,FloatyDelegate {
    
    /*
     NOTE: This class contains all the common methods
     */
    //@objc var tabBarController:   TabBarController?
    @objc var navigationController     : UINavigationController?
    
    var strCheckOutEventIds = ""
    var isSideMenuOptionViewOpenFromHomeScreen : Bool = false
    var arrUserRole : NSMutableArray = NSMutableArray()
    
    var isShowClosebutton : Bool = false
    var isMenuPadding : Bool = false
    
    var RSKImageCropedCompletion : ((UIImage?) -> Void)?
    var locationManager : CLLocationManager = CLLocationManager()
    @objc var currentLocation : CLLocation = CLLocation(latitude: 0, longitude: 0)
    
    var isFiltersOptionsValueChanged : Bool = false
    //Delegate
    weak var delegate: MenuProtocol?

    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .popup)
//        let width = ModalSize.sideMargin(value: 5)
//        let height = ModalSize.custom(size: 468)
//        let center = ModalCenterPosition.bottomCenter
        let customType = PresentationType.fullScreen//custom(width: width, height: height, center: center)
        
        let customPresenter = Presentr(presentationType: customType)
        customPresenter.transitionType = .coverVertical
        customPresenter.dismissTransitionType = .coverVertical
        customPresenter.roundCorners = false
        customPresenter.backgroundColor = .black
        customPresenter.backgroundOpacity = 0.0
        customPresenter.dismissOnSwipe = false
        return customPresenter
        
    }()
    
    //MARK: -
    @objc static let shared = AppUtility()
    
    override init() {
        super.init()
        intializeOnce()
    }
    
    func intializeOnce() -> Void {
        
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.distanceFilter = 500 //Meter
        locationManager.startUpdatingLocation()
        
        self.SETUP_SVPROGRESS()
        
        if #available(iOS 9.0, *) {
            let searchBarTextAttributes: [String : AnyObject] = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white, NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        } else {
            // Fallback on earlier versions
        }
        
        let settings = UIUserNotificationSettings(types: [.alert , .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
       // let navFont = UIFont(name: kRegularFontName, size: 18.0)
        //UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : K.Color.green, NSAttributedStringKey.font: navFont!]
        UINavigationBar.appearance().barTintColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        
        //let titleView = UIImageView(image: UIImage(named: "ic_logo_topbar.png"))
        //        self.navigationItem.titleView = titleView

        
        

        if FileManager.default.fileExists(atPath: K.imagesFolderPath) == false {
            do {
                try FileManager.default.createDirectory(atPath: K.imagesFolderPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
        }
    }
    
    func addNavigationShadow() {
        
        AppUtility.shared.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        AppUtility.shared.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        AppUtility.shared.navigationController?.navigationBar.layer.shadowRadius = 4.0
        AppUtility.shared.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        AppUtility.shared.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    
    func resetFilters() {
        isFiltersOptionsValueChanged = false
      
    }
    
    //MARK: - Instance Functions
    func SETUP_SVPROGRESS() {
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.gradient)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setForegroundColor(K.Color.darkGray)
    }
    
    //MARK: - Class Functions
    class func GET_CONTROLLER(_ storyboardName: String = "Main", controllerName: String) -> UIViewController {
        let storyBoard = STORY_BOARD(storyboardName)
        return storyBoard.instantiateViewController(withIdentifier: controllerName)
    }
    class func STORY_BOARD(_ storyboardMame: String = "Main") -> UIStoryboard {
        let storyboard = UIStoryboard(name: storyboardMame, bundle: nil)
        return storyboard
    }
    
    //MARK: - CLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            currentLocation = locations[0]
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    class func RGBACOLOR(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: (red) / 255.0, green: (green) / 255.0, blue: (blue) / 255.0, alpha: alpha)
    }
    
    class func RGBCOLOR(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return RGBACOLOR(red: red, green: green, blue: blue, alpha: 1)
    }
    
    //MARK: - Image Selection With Cropper
    
    func showVideoSelectionOption(viewController: UIViewController, completion: @escaping (_ mediaUrl: URL?, _ image: UIImage?) -> Void) {
        UIAlertController.show(in: viewController, withTitle: nil, message: "Select Video", preferredStyle: .actionSheet, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: ["Video from camera","Video from library"], popoverPresentationControllerBlock: nil, tap: { controller   , action  , buttonIndex in
            if controller.cancelButtonIndex != buttonIndex {
                let sourceT : UIImagePickerControllerSourceType = buttonIndex == 2 ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                    UIImagePickerController.image(with: sourceT, isVideo: true, didFinish: { (imagePicker, info) in
                        let mediaUrl: URL? = (info?[UIImagePickerControllerMediaURL] as? URL) ?? (info?[UIImagePickerControllerReferenceURL] as? URL)
                        let attachmentVideoUrl = URL(fileURLWithPath: NSString.temporaryPath().appendingPathComponent(String(format: "%.f.mp4", Date().timeIntervalSince1970)))
                        
                        if mediaUrl != nil{
                            SVProgressHUD.show()
                            AppUtility.shared.convertVideoToMp4(mediaUrl!, withFileUrl: attachmentVideoUrl, completion: { (success) in
                                completion(attachmentVideoUrl, nil)
                                SVProgressHUD.dismiss()
                            })
                        }
                    }, completion: nil)
                })
            }
        })
    }
    
    func showMediaSelectionOptionWithoutCamera(viewController: UIViewController, completion: @escaping (_ mediaUrl: URL, _ image: UIImage?) -> Void) {
        UIAlertController.show(in: viewController, withTitle: nil, message: "Select Media", preferredStyle: .actionSheet, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: ["Photo Gallery", "Video Gallery"], popoverPresentationControllerBlock: nil, tap: { controller   , action  , buttonIndex in
            if controller.cancelButtonIndex != buttonIndex {
                var isVideo = false
                if buttonIndex == 3 {
                    isVideo = true
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                    UIImagePickerController.image(with: .photoLibrary, isVideo: isVideo, didFinish: { (imagePicker, info) in
                        let mediaUrl: URL? = (info?[UIImagePickerControllerMediaURL] as? URL) ?? (info?[UIImagePickerControllerReferenceURL] as? URL)
                        let image : UIImage? = info?[UIImagePickerControllerOriginalImage] as? UIImage
                        if mediaUrl != nil {
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                                completion(mediaUrl!, image)
                            })
                        }
                    }, completion: nil)
                })
            }
        })
    }
    
    func showImageSelectionOption(viewController: UIViewController, isCropEnable : Bool = false, completion: @escaping(_ image: UIImage?) -> Void) {
        UIAlertController.show(in: viewController, withTitle: nil, message: "Select Picture", preferredStyle: .actionSheet, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: ["Take Photo", "Choose From Library"], popoverPresentationControllerBlock: nil, tap: { controller   , action  , buttonIndex in
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                if buttonIndex == 2 || buttonIndex == 3 {
                    let sourceT : UIImagePickerControllerSourceType = buttonIndex == 2 ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
                    UIImagePickerController.image(with: sourceT, didFinish: {imagePicker, info in
                        let imgProfilePhoto : UIImage! = info?[UIImagePickerControllerOriginalImage] as? UIImage
                        if imgProfilePhoto != nil && isCropEnable == true {
                            self.openImageCropper(image: imgProfilePhoto, viewController: viewController, completion: { (croppedImage) in
                                completion(croppedImage)
                            })
                        }else if imgProfilePhoto != nil {
                            completion(imgProfilePhoto)
                        }
                    }, completion: nil)
                }
            })
        })
    }
    
    func openImageCropper(image : UIImage, viewController : UIViewController, completion: ((UIImage?) -> ())?) -> Void {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
            self.RSKImageCropedCompletion = completion
            let imageCropVC = RSKImageCropViewController(image: image)
            imageCropVC.delegate = self;
            viewController.present(imageCropVC, animated: true, completion: nil)
        })
        
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        SVProgressHUD.dismiss()
        controller.dismiss(animated: true, completion: nil)
        //RSKImageCropedCompletion?(nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        SVProgressHUD.dismiss()
        controller.dismiss(animated: true, completion: nil)
        RSKImageCropedCompletion?(croppedImage)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, willCropImage originalImage: UIImage) {
        SVProgressHUD.show()
    }

    //MARK: - Manage Notification Redirection

    class func handleNotification(userInfo: NSDictionary) {
        print(userInfo)
       
    }

//    class func thumbFromVideoURL(videoURL: URL) -> UIImage? {
//        let asset = AVURLAsset(url: videoURL, options: nil)
//        let imgGenerator = AVAssetImageGenerator(asset: asset)
//        imgGenerator.appliesPreferredTrackTransform = true
//        do {
//            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
//            let thumb = UIImage(cgImage: cgImage)
//            return thumb
//        }
//        catch _ {
//            ISMessages.show("Error while creating thumb from video")
//            return nil
//        }
//    }
    
    
    func thumbFromVieoURL(videoURL: URL) -> UIImage? {
        let asset = AVURLAsset(url: videoURL, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        do {
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumb = UIImage(cgImage: cgImage)
            return thumb
        }
        catch _ {
            ISMessages.show(K.Message.cantGenerateThumb)
            return nil
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths.first
        return documentsDirectory!
    }
    
    func convertVideoToMp4(_ mediaUrl: URL, withMediaType mediaType: String? = "", withFileUrl fileUrl: URL, completion: @escaping (Bool) -> ()) {
        if FileManager.default.fileExists(atPath: fileUrl.path) {
            try? FileManager.default.removeItem(atPath: fileUrl.path)
        }
        let asset: AVAsset! = AVURLAsset(url: mediaUrl, options: nil)
        if mediaType == kUTTypeMPEG4 as String {
            try? FileManager.default.copyItem(atPath:mediaUrl.path, toPath:mediaUrl.path)
            completion(true)
        }
        
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith: asset)
        if compatiblePresets.contains(AVAssetExportPresetLowQuality) {
            var bgTask: UIBackgroundTaskIdentifier?
            bgTask = UIApplication.shared.beginBackgroundTask(withName: "bgTask", expirationHandler: {() -> Void in
                UIApplication.shared.endBackgroundTask(bgTask!)
                bgTask = UIBackgroundTaskInvalid
            })
            var exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
            exportSession?.shouldOptimizeForNetworkUse = true
            exportSession?.outputURL = fileUrl
            exportSession?.outputFileType = AVFileType.mp4
            exportSession?.exportAsynchronously(completionHandler: {
                DispatchQueue.main.async {
                    switch exportSession!.status {
                    case .failed:
                        break
                    case .cancelled:
                        completion(false)
                        exportSession = nil
                        break
                    case .completed:
                        completion(true)
                        break
                    case .exporting:
                        break
                    default:
                        break
                    }
                    UIApplication.shared.endBackgroundTask(bgTask!)
                    bgTask = UIBackgroundTaskInvalid
                }
            })
        }
    }
    
    
    func addFloatingButton(to view: UIView, floaty: Floaty) {
        
        let loginMsg = "please login?"
        
        floaty.buttonImage = UIImage(named: "ic_menu")
        floaty.buttonColor = .white
        floaty.hasShadow = false
        floaty.addItem("Feeds", icon: UIImage(named: "menu_feeds")){ item in
            self.delegate?.showMenuOptions(identifier: "HomeViewController")
        }
        floaty.addItem("Chat", icon: UIImage(named: "menu_chat")){ item in
            if User.loggedInUser() != nil {
                self.delegate?.showMenuOptions(identifier: "ChatViewController")
            } else {
                ISMessages.show(loginMsg, type: .warning)
            }
            
        }
        floaty.addItem("My Friends", icon: UIImage(named: "menu_friends")){ item in
            if User.loggedInUser() != nil {
                self.delegate?.showMenuOptions(identifier: "MyFriendViewController")
            } else {
                ISMessages.show(loginMsg, type: .warning)
            }
            
        }
        
        floaty.addItem("Settings", icon: UIImage(named: "menu_settings")) { item in
            if User.loggedInUser() != nil {
                self.delegate?.showMenuOptions(identifier: "SettingsViewController")
            } else {
                ISMessages.show(loginMsg, type: .warning)
            }
            
        }
        floaty.addItem("My Profile", icon: UIImage(named: "menu_profile"))
        { item in
            if User.loggedInUser() != nil {
                self.delegate?.showMenuOptions(identifier: "ProfileViewController")
            } else {
                ISMessages.show(loginMsg, type: .warning)
            }
        }
        
        floaty.addItem("Notifications",icon: UIImage(named: "menu_notifications"))
        { item in
            if User.loggedInUser() != nil {
                self.delegate?.showMenuOptions(identifier: "NotificationListViewController")
            } else {
                ISMessages.show(loginMsg, type: .warning)
            }
            
            
        }
        
        floaty.paddingX = 20 //self.view.frame.width/2 - floaty.frame.width/2
        floaty.paddingY = 20
        floaty.fabDelegate = self
        floaty.overlayColor = UIColor.black.withAlphaComponent(0.8)
        floaty.openAnimationType = .slideUp
        
        floaty.tag = 100

        view.addSubview(floaty)
       // UIApplication.shared.keyWindow?.addSubview(floaty)

        
    }
    
    func removeFloatingButton(to view: UIView, floaty : Floaty){
        

    }
    
    func showCMSContent(_ cmsId: String, title: String, controller: UIViewController) -> Void {
        let cmsController = AppUtility.GET_CONTROLLER(controllerName: "MyFriendViewController") as! MyFriendViewController
       
        controller.navigationController?.pushViewController(cmsController, animated: true)
    }
    
    
    //MARK: - Upate Device Token
    class func updateDeviceToken() -> Void {
//        if (User.loggedInUser() != nil && StandardUserDefaults.value(forKey: K.Key.DeviceToken) != nil) {
//            let reqParam = ["user_id" : User.loggedInUser()!.ID , "device_type" : "ios", "device_token" : StandardUserDefaults.value(forKey: K.Key.DeviceToken) as? String ?? ""] as [String : Any]
            /*_ = WebClient.requestWithUrl(url: K.URL.UPDATE_DEVICE_TOKEN, parameters: reqParam, completion: { (response, error) in
                if error == nil {
                    NSLog("Device token did update = %@", reqParam)
                    print("Device token did update \(reqParam)")
                }else {
                    print("Error while save device token")
                }
            })*/
//        }
    }
    
    
    //MARK: - WebService
    
    class func getCommonData(completion : (() -> ())? = nil) -> Void {
        /*if (User.loggedInUser() != nil) {
            let reqParam = ["user_id" : User.loggedInUser()!.ID] as [String : Any]
            _ = WebClient.requestWithUrl(url: K.URL.GET_COMMON_DATA, parameters: reqParam, completion: { (response, error) in
                if error == nil {
                    let dictData = ((response as! NSDictionary).value(forKey: "data")) as! NSDictionary
                    AppUtility.shared.notificationCount = dictData["notification_count"] as! Int
                    AppUtility.shared.myCheckCount = dictData["my_check_in"] as! Int
                    AppUtility.shared.myFollowerCount = dictData["follower_count"] as! Int
                    
                    completion?()
                }else {
                    
                }
            })
        }*/
    }
    
    //MARK: - Webservice
    class func uploadMedia(media: Any, dirName: String, model: String, completion: @escaping (Dictionary<String,Any>?,String?, Error?) -> ()) {
        WebClient.multiPartRequestWithUrl(url: K.URL.UPLOAD_MEDIA, parameters: ["dir_name" : dirName, "model":model], multiPartFormDataBlock: { (formData) in
            if media is UIImage {
                formData?.append(UIImageJPEGRepresentation(media as! UIImage , 0.5)!, withName: "media_file", fileName: "file.jpg", mimeType: "image/jpg")
            }
            else {
                formData?.append(media as! URL, withName: "media_file")
            }
            
        }) { (responseObject, error) in
            if error == nil {
                
                let dict = responseObject as? [String: Any]
                let fileNames = dict?["filename"]
                print(fileNames ?? "")
                completion(responseObject as? Dictionary<String, Any>, fileNames as? String, error)
                
            }
            else {
                completion(nil, nil, error)
            }
        }
    }
    
func convertMinutesToFormattedTime(minute : Int) -> String {
    
    var hours = minute / 60
    let minutes = (minute % 60)
    if hours > 24 {
        hours %= 24
    }
    
    var fullTime = ""
    if hours > 12 {
        hours %= 12
        fullTime = String(format: "%i:%02i", hours, minutes)
        fullTime += " pm"
    }else {
        fullTime = String(format: "%i:%02i", hours, minutes)
        fullTime += " am"
    }
    
    
    return fullTime
}

func convertSecondsToFormattedTime(seconds : Int) -> String {
    
    let hours = seconds / 3600
    let minutes = ((seconds % 3600) / 60)
    let second = ((seconds % 3600) % 60)
    let fullTime = String(format: "%02i:%02i:%02i", hours, minutes, second)
    
    return fullTime
}


func convertTimeToMiliSeconds(time : String) -> Double {
    
    //"00:00:57.489"
    let arr =  time.split(".").first?.split(":")
    if arr?.count == 3 {
        let hours   = arr?[0].toInt()
        let min     = arr?[1].toInt()
        let seconds = arr?[2].toInt()
        
        let totalMilliSeconds = (hours! * 3600000) + (min! * 60000) + (seconds! * 1000)
        
        let miliseconds = time.split(".").last?.toDouble() ?? 0
        return Double(totalMilliSeconds) + miliseconds
    }
    
    return 0
}
func thumbImageURL(fullUrl : String) -> String {
    var thumbUrl = fullUrl
    if fullUrl.contains("thumb/") == false {
        let lastPathComponent = fullUrl.lastPathComponent;
        thumbUrl = fullUrl.replacing(lastPathComponent, with: "thumb/"+lastPathComponent)
    }
    
    return thumbUrl
}
func fullImageURL(imageUrl : String) -> String {
    
    if imageUrl.contains("thumb/") == true {
        
        return imageUrl.replacing("thumb/", with: "")
    }
    
    return imageUrl
}
    

    
    
   
}

func fixedSpaceBarButton(width : CGFloat = 10) -> UIBarButtonItem {
    let fixedSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
    fixedSpace.width = width;
    return fixedSpace
}
